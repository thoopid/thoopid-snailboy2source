package
{
	import flash.utils.ByteArray;
	
	import starling.utils.AssetManager;

	public class Assets
	{
		// UI and Screen data
//		[Embed(source = "../assets/screens/screensData.json",mimeType="application/octet-stream")]
		private static var ScreensDATA:Class;
		
//		[Embed(source = "../assets/screens/uiElementsData.json",mimeType="application/octet-stream")]
		private static var UiElementsDATA:Class;
		
		public static var ScreenObj:Object;
		public static var UiObj:Object;
		
		private static var _assets:AssetManager;
		
		public function Assets()
		{
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public static function get assets():AssetManager
		{
			return _assets;
		}

		public static function init(scale:Number):void
		{
			_assets = new AssetManager(scale);
			
//			var bytes:ByteArray = new ScreensDATA() as ByteArray;
//			var str:String = bytes.readUTFBytes(bytes.length);
//			
//			ScreenObj = JSON.parse(str);
//			
//			bytes = new UiElementsDATA() as ByteArray;
//			str = bytes.readUTFBytes(bytes.length);
//			UiObj = JSON.parse(str);
		}
		
		public static function getSCREEN_DATA(id:String):Object
		{
			var jsonObj:Object = ScreenObj;
			if(!jsonObj.screens) return {};
			
			for (var i:int = 0; i < jsonObj.screens.length; i++) 
			{
				if(jsonObj.screens[i].id == id)
				{
					return jsonObj.screens[i];
				}
			}
			
			return {};
		}
		
		public static function getUI_DATA(id:String):Object
		{
			var jsonObj:Object = UiObj;
			if(!jsonObj.ui) return {};
			
			for (var i:int = 0; i < jsonObj.ui.length; i++) 
			{
				if(jsonObj.ui[i].id == id)
				{
					return jsonObj.ui[i];
				}
			}
			
			return {};
		}
	}
}