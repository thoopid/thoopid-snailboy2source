package
{
	public class Config
	{
		public static var GAME_WIDTH:Number = 480;
		public static var GAME_HEIGHT:Number = 320;
		
		public static var WORLD_WIDTH:Number = 10000;
		public static var WORLD_HEIGHT:Number = 10000;
		
		public static var SCALE_FACTOR:Number = 1;
		public static var SCALE_RATIO:Number = 1;
		public static var SCALE_NATIVE:Number = 1;
		
		public static const DEBUG:Boolean = false;
		public static const DEBUG_PHYSICS:Boolean = false;
		public static const DEBUG_CONSOLE:Boolean = true;
		public static const DEBUG_CHARACTER:Boolean = false;
		public static const DEBUG_ENABLE_LAYERS:Boolean = false;
		
		public static var IOS_APP_ID:String = "";
		public static var FB_APP_ID:String = "";
		public static var FLOX_AUTH_MODE:int = 1;					// 1 for game center 2 for Facebook
		
		// Flox deets
		public static var FLOX_GAME_ID:String = "yfL13yj1QQq2vhnn";					// ID
		public static var FLOX_GAME_KEY:String = "KkbBa4VkN8YpgXPK";				// Key
//		public static var GAME_BG_COLOR:uint = 0x000c1c;
//		public static var GAME_PARALAX_COLOR:uint = 0x0f2b4f;
//		public static var GAME_BG_COLOR:uint = 0x010618;
//		public static var GAME_PARALAX_COLOR:uint = 0x010618;
		public static var GAME_BG_COLOR:uint = 0x001529;
		public static var GAME_PARALAX_COLOR:uint = 0x001529;
		public static var GAME_ZOOM_KINGDOM:Number = 0.65;
		public static var GAME_ZOOM_AREA_HIGHLIGHT:Number = 1.3;
		public static var GAME_ZOOM_LEVEL:Number = 1;
		
		
		public function Config()
		{
			 
		}
		
		public static function get GAME_VIEW_H_CENTER():Number
		{
			return GAME_WIDTH / 2;
		}
		public static function get GAME_VIEW_V_CENTER():Number
		{
			return GAME_HEIGHT / 2;
		}
	}
}