/**
* ...
* @author shaun smith
* @version 0.1
*/

package {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.utils.getTimer;
	import flash.utils.Timer;

	public class FPS extends Sprite {
		private var _bmpData:BitmapData;
		private var _bmp:Bitmap;
		private var _started:Boolean;
		private var _then:int;
		private var _ms:int;
		private var _mem:TextField;
		private var _inf:TextField;
		private var _timer:Timer;
		
		public function FPS(autoStart:Boolean = true, autoShow:Boolean = false)
		{
			_inf = new TextField();
			_inf.autoSize = TextFieldAutoSize.LEFT;
			_inf.selectable = false;
			_inf.textColor = 0xFFFFFF;
			_inf.text = Capabilities.version;
			_inf.y = 68;
			
			_mem = new TextField();
			_mem.autoSize = TextFieldAutoSize.LEFT;
			_mem.selectable = false;
			_mem.textColor = 0xFFFFFF;
			_mem.y = 82;
			
			_bmpData = new BitmapData(100, 100, false, 0x333333);
			_bmp = new Bitmap(_bmpData);
			
			addChild(_bmp);
			addChild(_mem);
			addChild(_inf);
			mouseEnabled = false;
			mouseChildren = false;
			
			_timer = new Timer(2000);
			_timer.addEventListener(TimerEvent.TIMER, onTimer);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStageListener);
			if (autoStart) start();
			if (!autoShow) visible = false;
		}
		
		//_____________________________________________________________________ API
		
		public function start():void
		{
			if (_started) return;
			_started = true;
			_then = flash.utils.getTimer();
			addEventListener(Event.ENTER_FRAME, enterFrameListener);
			_timer.start();
		}
		
		public function stop():void
		{
			if (!_started) return;
			_started = false;
			removeEventListener(Event.ENTER_FRAME, enterFrameListener);
			_timer.stop();
		}
		
		//_____________________________________________________________________ CALCULATE FPS
		
		private function enterFrameListener(e:Event):void
		{
			_ms = flash.utils.getTimer() - _then;
			_then = flash.utils.getTimer();
			if (_ms > 99) _ms = 99;
			_bmpData.lock();
			_bmpData.scroll( -1, 0);
			while (_ms--) _bmpData.setPixel(98, _ms, 0xCCCCCC);
			_bmpData.unlock();
		}
		
		//_____________________________________________________________________ PRINT MEM USAGE
		
		private function onTimer(e:TimerEvent):void
		{
			_mem.text = String(Math.round(System.totalMemory/1024/128)/8) + ' MB';
		}
		
		//_____________________________________________________________________ USE "ctrl-alt-f" TO TOGGLE
		
		private function addedToStageListener(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownListener);
		}
		
		private function keyDownListener(e:KeyboardEvent):void
		{
			// trace('e.keyCode=' + e.keyCode);
			if (e.keyCode == 70 && e.ctrlKey && e.altKey) visible = !visible;
		}
		
	}
	
}
