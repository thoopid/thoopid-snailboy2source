package
{
	import flash.desktop.NativeApplication;
	import flash.display.Stage;
	import flash.display.StageAspectRatio;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;

	public class MobileConfig
	{
		public static const OS_DESKTOP:String = "pc";
		public static const OS_IOS:String = "ios";
		public static const OS_ANDROID:String = "android";
		
		public static var VIEW_H_CENTER:Number;
		public static var VIEW_V_CENTER:Number;
		public static var VIEW_WIDTH:Number;
		public static var VIEW_HEIGHT:Number;
		public static var VIEW_ASPECT:String;
		public static var VIEW_RECT:Rectangle;
		public static var IS_MOBILE:Boolean = false;
		public static var IS_IOS:Boolean = false;
		public static var IS_ANDROID:Boolean = false;
		public static var IS_IPHONE4:Boolean = false;
		public static var APPLICATION_SCALE:Number = 1;
		public static var PERFORMANCE_ENABLED:Boolean = true;
		public static var VERSION_NUMBER:String = "0.0.0";
		
		
		public static var CURRENT_OS:String = MobileConfig.operatingSystem;
		
		// Content height for application
		public static var CONTENT_HEIGHT:Number;
		
		public static function init(stage:Stage):void
		{
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			VERSION_NUMBER = appXml.ns::versionNumber[0];
			
			if(IS_MOBILE)
			{
				VIEW_WIDTH = stage.fullScreenWidth;
				VIEW_HEIGHT = stage.fullScreenHeight;
			} else
			{
				VIEW_WIDTH = stage.stageWidth;
				VIEW_HEIGHT = stage.stageHeight;
			}
			VIEW_H_CENTER = Math.floor(VIEW_WIDTH * 0.5);
			VIEW_V_CENTER = Math.floor(VIEW_HEIGHT * 0.5);
			VIEW_RECT = new Rectangle(0,0,VIEW_WIDTH,VIEW_HEIGHT);
			
			VIEW_ASPECT = stage.stageWidth >= stage.stageHeight ? StageAspectRatio.LANDSCAPE : StageAspectRatio.PORTRAIT;
		}
		
		public static function get operatingSystem():String
		{
			var platform:String = Capabilities.version.slice(0, 3);
			
			switch(platform)
			{
				case "WIN" :
				case "MAC" :
				case "LNX" :
					IS_MOBILE = false;
					return OS_DESKTOP;
					break;
				case "AND" :
					IS_MOBILE = true;
					IS_ANDROID = true;
					return OS_ANDROID;
					break;
				case "IOS" :
					IS_MOBILE = true;
					IS_IOS = true;
					return OS_IOS;
					break;
			}
			return OS_DESKTOP;
//			var os:String = Capabilities.os.toLowerCase();
//			
//			IS_IOS = os.indexOf("iphone")!=-1;
////			IS_IPHONE4 = os.indexOf("iphone3,1")!=-1;
//			IS_ANDROID = os.indexOf("linux")!=-1;
//			IS_MOBILE = (os.indexOf("iphone")!=-1 || os.indexOf("mobile")!=-1 || os.indexOf("linux") != -1);
//			
//			if(IS_IOS)
//			{
//				return OS_IOS;
//			} else if (IS_ANDROID)
//			{
//				return OS_ANDROID;
//			}
//			
//			return OS_DESKTOP;
		}
	}
}