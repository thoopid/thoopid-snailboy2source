package
{
	import com.thoopid.games.sb2.constants.game.PlayerElements;
	import com.thoopid.games.sb2.view.AppContext;
	import com.thoopid.games.sb2.view.game.level.base.ALevel;
	
	import flash.display.Bitmap;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import citrus.core.starling.StarlingCitrusEngine;
	import citrus.core.starling.ViewportMode;
	import citrus.utils.Mobile;
	
	import starling.core.Starling;
	
	import uk.soulwire.utils.display.DisplayUtils;
	
//	[SWF(width="320", height="480", backgroundColor="#4fa1ff", frameRate="60")]
//	[SWF(width="640", height="1136", backgroundColor="#4fa1ff", frameRate="60")]
//	[SWF(width="800", height="480", backgroundColor="#4fa1ff", frameRate="60")]
	[SWF(backgroundColor="#4fa1ff", frameRate="60")]
	public class SnailboyHydroCore extends StarlingCitrusEngine
	{
		// Private
		[Embed(source="../assets/images/splash.png")]
		private static var BackgroundHD:Class;
		
		private static var _instance:SnailboyHydroCore;
		private static var _background:Bitmap;
		
		private var _inited:Boolean = false;
		private var _context:AppContext;
		
//		private static var _tf:TextField;
		
		public function SnailboyHydroCore()
		{
			super();
			
			_instance = this;
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
//			this.addChild(_tf = new TextField);
//			_tf.width = 200;
//			_tf.height = 150;
//			_tf.multiline = true;
			
			MobileConfig.init(this.stage);
			
			addChild(_background = new BackgroundHD());
			_background.smoothing = true;
			DisplayUtils.fitIntoRect(_background, MobileConfig.VIEW_RECT,true,"C");
			BackgroundHD = null;
		}
		
//		public static function DBTrace(message:String):void
//		{
//			_tf.appendText(message+"\n");
//		}
		
		public static function get background():Bitmap
		{
			return _background;
		}

		public static function set background(value:Bitmap):void
		{
			_background = value;
		}

		public static function getInstance():SnailboyHydroCore
		{
			return _instance;
		}
		
		public function setState(lvl:ALevel):void
		{
			state = lvl;
		}
		
		override public function handleStarlingReady():void
		{
			stage.quality = StageQuality.LOW;
			
			this.starling.stage.color = Config.GAME_BG_COLOR;
			this.starling.stage.touchable = true;
			this.starling.root.touchable = true;
//			this.starling.simulateMultitouch  = Capabilities.isDebugger;
			this.starling.enableErrorChecking = Capabilities.isDebugger;
			
			// Setup asset manger and Context
			Assets.init(Config.SCALE_FACTOR);
			
			trace("Starling Version: "+Starling.VERSION);
			
			// Application context
			_context = new AppContext(_starling.stage,true);
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function handleAddedToStage(e:Event):void
		{
			super.handleAddedToStage(e);
			stage.displayState = StageDisplayState.FULL_SCREEN;
			stage.quality = StageQuality.HIGH;
			
			setup();
		}
		
		protected function setup():void
		{
			// Init Constants
			PlayerElements.init();
			
			Config.SCALE_FACTOR = MobileConfig.VIEW_WIDTH <= 480 ? 1 : 2; // midway between 320 and 640
			
			var vRatio:Number = MobileConfig.VIEW_WIDTH / MobileConfig.VIEW_HEIGHT;
			Config.SCALE_RATIO = vRatio;
			Config.SCALE_NATIVE = MobileConfig.VIEW_WIDTH / 960;
			Config.GAME_WIDTH = Config.GAME_HEIGHT * vRatio;
			
			// Setup console if needed
			this.console.enabled = Config.DEBUG_CONSOLE;
			
			Starling.handleLostContext = Mobile.isAndroid();
			Starling.multitouchEnabled = false;
			_viewportMode = ViewportMode.LETTERBOX;
			_baseWidth = Config.GAME_WIDTH;
			_baseHeight = Config.GAME_HEIGHT;
			
			setUpStarling(Config.DEBUG, 0, new Rectangle(0,0,Config.GAME_WIDTH, Config.GAME_HEIGHT));
		}
	}
}