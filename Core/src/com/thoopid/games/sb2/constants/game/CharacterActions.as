package com.thoopid.games.sb2.constants.game
{
	public class CharacterActions
	{
		public static const JUMP:String = "jump";
		public static const SWIM_START:String = "swimStart";
		public static const SWIM_STOP:String = "swimStop";
	}
}