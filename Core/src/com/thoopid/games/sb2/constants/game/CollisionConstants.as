package com.thoopid.games.sb2.constants.game
{
	import nape.callbacks.CbType;
	import nape.dynamics.InteractionFilter;
	import nape.dynamics.InteractionGroup;

	public class CollisionConstants
	{
		// Interactions
		public static var heroCollitionType:CbType = new CbType();
		public static var stickyWallCollitionTypeLeft:CbType = new CbType();
		public static var stickyWallCollitionTypeRight:CbType = new CbType();
		public static var normalPlatformCollitionType:CbType = new CbType();
		public static var collectableCollitionType:CbType = new CbType();
		public static var secretAreaCollitionType:CbType = new CbType();
		public static var crateCollitionType:CbType = new CbType();
		public static var tutorialCollitionType:CbType = new CbType();
		public static var checkpointCollitionType:CbType = new CbType();
		public static var shootPlatformCollitionType:CbType = new CbType();
		
		
		// Collision groups
		public static const COLLISION_HERO_GROUP:int = 1;
		public static const COLLISION_PLATFORM_GROUP:int = 2;
		public static const COLLISION_SENSOR_GROUP:int = 3;
		public static const COLLISION_VALCANO_GROUP:int = 4;
		public static const COLLISION_VALCANO_END_GROUP:int = 5;
		public static const COLLISION_PORTAL_GROUP:int = 6;
		public static const COLLISION_BUBBLE_GROUP:int = 7;
		public static const COLLISION_WALL_GROUP:int = 8;
		public static const COLLISION_OBSTACLE_GROUP:int = 9;
		public static const COLLISION_HINGE_GROUP:int = 10;
		
		public static const COLLISION_HERO_MASK_COLISION:int = COLLISION_PLATFORM_GROUP | COLLISION_OBSTACLE_GROUP | ~(COLLISION_HINGE_GROUP);
		public static const COLLISION_HERO_MASK_SENSOR:int = COLLISION_SENSOR_GROUP | COLLISION_PORTAL_GROUP | COLLISION_BUBBLE_GROUP;
		
		public static const COLLISION_PLATFORM_MASK:int = COLLISION_HERO_GROUP | COLLISION_VALCANO_GROUP;
		public static const COLLISION_SENSOR_MASK:int = COLLISION_HERO_GROUP;
		
		public static const COLLISION_VALCANO_MASK_COLISION:int = ~(COLLISION_VALCANO_GROUP);
		public static const COLLISION_VALCANO_MASK_SENSOR:int = COLLISION_HERO_GROUP | COLLISION_HINGE_GROUP;
		
		public static const COLLISION_PORTAL_MASK_COLISION:int = 0;
		public static const COLLISION_PORTAL_MASK_SENSOR:int = COLLISION_HERO_GROUP;
		
		public static const COLLISION_BUBBLE_MASK_COLISION:int = ~(COLLISION_BUBBLE_GROUP);
		public static const COLLISION_BUBBLE_MASK_SENSOR:int = COLLISION_HERO_GROUP;
		
		public static const COLLISION_OBSTACLE_MASK_COLISION:int = COLLISION_HERO_GROUP | COLLISION_PLATFORM_GROUP;
		public static const COLLISION_OBSTACLE_MASK_SENSOR:int = COLLISION_SENSOR_GROUP;
		
		public static const COLLISION_HINGE_MASK_COLISION:int = 0;
		public static const COLLISION_HINGE_MASK_SENSOR:int = COLLISION_VALCANO_GROUP;
		
		public static const COLLISION_WALL_MASK:int = COLLISION_BUBBLE_GROUP | COLLISION_VALCANO_GROUP;
		
		public static const COLLISION_HERO_FILTER:InteractionFilter = new InteractionFilter(COLLISION_HERO_GROUP, COLLISION_HERO_MASK_COLISION,COLLISION_HERO_GROUP, COLLISION_HERO_MASK_SENSOR);
		public static const COLLISION_PLATFORM_FILTER:InteractionFilter = new InteractionFilter(COLLISION_PLATFORM_GROUP, COLLISION_PLATFORM_MASK);
		public static const COLLISION_SENSOR_FILTER:InteractionFilter = new InteractionFilter(0,0,COLLISION_SENSOR_GROUP, COLLISION_SENSOR_MASK);
		public static const COLLISION_VALCANO_FILTER:InteractionFilter = new InteractionFilter(COLLISION_VALCANO_GROUP,COLLISION_VALCANO_MASK_COLISION,COLLISION_VALCANO_GROUP,COLLISION_VALCANO_MASK_SENSOR);
		public static const COLLISION_BUBBLE_FILTER:InteractionFilter = new InteractionFilter(COLLISION_BUBBLE_GROUP,COLLISION_BUBBLE_MASK_COLISION,COLLISION_BUBBLE_GROUP,COLLISION_BUBBLE_MASK_SENSOR);
		public static const COLLISION_PORTAL_FILTER:InteractionFilter = new InteractionFilter(COLLISION_PORTAL_GROUP, COLLISION_PORTAL_MASK_COLISION,COLLISION_PORTAL_GROUP, COLLISION_PORTAL_MASK_SENSOR);
		public static const COLLISION_WALL_FILTER:InteractionFilter = new InteractionFilter(COLLISION_WALL_GROUP, COLLISION_WALL_MASK,COLLISION_WALL_GROUP, COLLISION_WALL_MASK);
		public static const COLLISION_OBSTACLE_FILTER:InteractionFilter = new InteractionFilter(COLLISION_OBSTACLE_GROUP, COLLISION_OBSTACLE_MASK_COLISION,COLLISION_OBSTACLE_GROUP, COLLISION_OBSTACLE_MASK_SENSOR);
		public static const COLLISION_HINGE_FILTER:InteractionFilter = new InteractionFilter(0, 0,COLLISION_HINGE_GROUP, COLLISION_HINGE_MASK_SENSOR);
		
		public static const COLLISION_HERO_FILTER_NONE:InteractionFilter = new InteractionFilter(0,0,0,0);
		
		public static const FAKE_INTERACTION_GROUP:InteractionGroup = new InteractionGroup(true);
	}
}