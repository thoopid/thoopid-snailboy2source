package com.thoopid.games.sb2.constants.game
{
	public class GameCollectableTypes
	{
		public static const TYPE_STORY:String = "storyCollectable";
		public static const TYPE_TRESURE:String = "treasureCollectable";
		public static const TYPE_CONSUME:String = "consumeCollectable";
		
		// Story Items
		public static const STORY_ITEM_1_1:String = "iq.orbShard_01";
		public static const STORY_ITEM_1_2:String = "iq.orbShard_01";
		public static const STORY_ITEM_1_3:String = "iq.orbShard_01";
		public static const STORY_ITEM_1_4:String = "iq.orbShard_01";
		public static const STORY_ITEM_1_5:String = "iq.orbShard_01";
		
		// Tresure Elements
		public static const TREASURE_VASE:String = "iq.vase";
		public static const TREASURE_IDOL:String = "iq.idol";
		public static const TREASURE_BOTTLE:String = "iq.bottle";
		public static const TREASURE_GEM_1:String = "ig.crystal.01";
		public static const TREASURE_GEM_2:String = "ig.crystal.02";
		public static const TREASURE_GEM_3:String = "ig.crystal.03";
		public static const TREASURE_GEM_4:String = "ig.crystal.04";
		public static const TREASURE_GEM_5:String = "ig.crystal.05";
		
		// Consumable Elements
//		public static const CONSUMEABLE_VIAL_SMALL:String = "smallVial";
//		public static const CONSUMEABLE_VIAL_MEDIUM:String = "mediumVial";
//		public static const CONSUMEABLE_VIAL_LARGE:String = "largeVial";
//		public static const CONSUMEABLE_SLIME:String = "slimeyCollectable";
//		
//		// Ora's
//		public static const ORAS_GAUGE:String = "oraGaugeFreeze";
		
		public static const TYPE_VIAL_SMALL:String = "ic.potion.sml";
		public static const TYPE_VIAL_MED:String = "ic.potion.med";
		public static const TYPE_VIAL_LARGE:String = "ic.potion.lrg";
		public static const TYPE_SLIME:String = "slimeyCollectable";
		
		public static const TYPE_ORA_GAUGE_REPLENISH:String = "ia.superGauge";
		
		public static const COST_VIAL_SML:Number = 500;
		public static const COST_VIAL_MED:Number = 1000;
		public static const COST_VIAL_LARGE:Number = 2000;
		public static const COST_ORA_GAUGE:Number = 5000;
	}
}