package com.thoopid.games.sb2.constants.game
{
	import flash.geom.Point;

	public class GameConstants
	{
		// Flox entities
		public static var FLOX_CONSTANTS_ENTITY_ID:String = "FloxConstantsSnailboy";
		
		// Character Values
		
			public static const HERO_POSITION_NAME:String = "HeroPosisionObject";
			
			public static var HERO_BODY_WIDTH:Number = 15;
			public static var HERO_BODY_HEIGHT:Number = 50;
			public static var HERO_NAME:String = "SnailboyHero";
			
			// Pull angles
			public static const HERO_PULL_ANGLE_MAX:Number = -15;
			public static const HERO_PULL_ANGLE_MIN:Number = -165;
			
			// Jump angles
			public static const HERO_JUMP_ANGLE_NORMAL_START:Number = 0;
			public static const HERO_JUMP_ANGLE_NORMAL_MIN:Number = -90;
			public static const HERO_JUMP_ANGLE_NORMAL_MAX:Number = -180;
			public static const HERO_JUMP_ANGLE_UP_MAX:Number = -100;
			public static const HERO_JUMP_ANGLE_UP_MIN:Number = -80;
			public static const HERO_JUMP_ANGLE_BACK_SUMI_MAX_RIGHT:Number = -61;
			public static const HERO_JUMP_ANGLE_BACK_SUMI_MIN_RIGHT:Number = -79;
			public static const HERO_JUMP_ANGLE_BACK_SUMI_MAX_LEFT:Number = -101;
			public static const HERO_JUMP_ANGLE_BACK_SUMI_MIN_LEFT:Number = -119;
			public static const HERO_JUMP_ANGLE_FWD_SUMI_MAX_RIGHT:Number = -45;
			public static const HERO_JUMP_ANGLE_FWD_SUMI_MIN_RIGHT:Number = -60;
			public static const HERO_JUMP_ANGLE_FWD_SUMI_MAX_LEFT:Number = -120;
			public static const HERO_JUMP_ANGLE_FWD_SUMI_MIN_LEFT:Number = -135;
			
		// Game/Level values
		
			public static var GAME_GRAVITY:Number = 200;
			public static var GAME_DRAG:Number = 0.6;
			// Pullback max power
			public static var HERO_PULLBACK_MIN:int = 15;
			public static var HERO_PULLBACK_THRESHOLD:int = 80;
			public static var HERO_PULLBACK_MAX:int = 100;
			public static var HERO_JUMP_POWER_NORMAL:Number = 2.8;
			public static var HERO_JUMP_SUPER:Number = 0.8;
			public static var HERO_SUPER_JUMP_COST:Number = 100;
			
			public static var HERO_SWIM_SPEED:Number = 1;
			public static var HERO_SWIM_IMP_X:Number = 40;
			public static var HERO_SWIM_IMP_Y:Number = 30;
			public static var HERO_SWIM_IMP_COST:Number = 2.6;
			public static var HERO_SWIM_PADDLE_COST:Number = 2.6;
			
			public static var HERO_MASS_GRAV_NONE:Number = 0;
			public static var HERO_MASS_GRAV_WALL:Number = 0.06;
			public static var HERO_MASS_GRAV_SWIM:Number = 0.05;
			public static var HERO_MASS_GRAV_WALK:Number = 0.5;
			public static var HERO_MASS_GRAV:Number = 0.25;
		
		
			// Gauge and consumable Values
			public static var GAUGE_VIAL_SMALL:Number = 100;
			public static var GAUGE_VIAL_MEDIUM:Number = 200;
			public static var GAUGE_VIAL_LARGE:Number = 300;
			public static var GAUGE_SLIME_NORMAL:Number = 8;
			// Coin Values
			public static var COIN_VALUE_SLIMEY:Number = 1;
			public static var COIN_VALUE_STORY_ITEM:Number = 50;
			public static var COIN_VALUE_TREASURE:Number = 10;
		
		// Camera props
			public static const CAM_TARGET_EASE_FAST:Point = new Point(0.04, 0.04);
			public static const CAM_TARGET_EASE_SLOW:Point = new Point(0.02, 0.02);
			
		// Sounds
			public static const SOUND_MENU:String = "menu_cue";
			public static const SOUND_BG:String = "bg_sound";
			public static const SOUND_AMIENCE:String = "underwater_ambience";
			public static const SOUND_JUMP:String = "jump_bound";
			public static const SOUND_FALL:String = "jump_fall";
			public static const SOUND_LAND:String = "jump_land_fwd";
			public static const SOUND_SWIM:String = "";
			public static const SOUND_SHELL:String = "shell";
			public static const SOUND_PULL:String = "stretch";
			public static const SOUND_STICK:String = "stick";
			public static const SOUND_SLIMEY:String = "slime";
			public static const SOUND_PORTAL:String = "portal";
			public static const SOUND_EXIT:String = "shell";
			public static const SOUND_BUTTON_UI:String = "button_ui";
			
		
		public static function init():void
		{
			
		}
	}
}