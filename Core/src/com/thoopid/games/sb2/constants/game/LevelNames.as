package com.thoopid.games.sb2.constants.game
{
	public class LevelNames
	{
		public static const KINGDOM_1:String = "1_0_0";
		public static const KINGDOM_2:String = "2_0_0";
		public static const KINGDOM_3:String = "3_0_0";
		
		public static const KINGDOM_1_NAME:String = "KORAL KINGDOM";
		public static const KINGDOM_2_NAME:String = "CRYSTAL CAVES";
		public static const KINGDOM_3_NAME:String = "MOUNT MAGMA";
		
		public static const KINGDOM_1_AREA_1:String = "1_1_0";
			public static const KINGDOM_1_LEVEL_1:String = "1_1_1";
			public static const KINGDOM_1_LEVEL_2:String = "1_1_2";
			public static const KINGDOM_1_LEVEL_3:String = "1_1_3";
			public static const KINGDOM_1_LEVEL_4:String = "1_1_4";
			public static const KINGDOM_1_LEVEL_5:String = "1_1_5";
		
		public static const KINGDOM_1_AREA_2:String = "1_2_0";
			public static const KINGDOM_1_LEVEL_6:String = "1_2_6";
			public static const KINGDOM_1_LEVEL_7:String = "1_2_7";
			public static const KINGDOM_1_LEVEL_8:String = "1_2_8";
			public static const KINGDOM_1_LEVEL_9:String = "1_2_9";
			public static const KINGDOM_1_LEVEL_10:String = "1_2_10";
		
		public static const KINGDOM_1_AREA_3:String = "1_3_0";
			public static const KINGDOM_1_LEVEL_11:String = "1_3_11";
			public static const KINGDOM_1_LEVEL_12:String = "1_3_12";
			public static const KINGDOM_1_LEVEL_13:String = "1_3_13";
			public static const KINGDOM_1_LEVEL_14:String = "1_3_14";
			public static const KINGDOM_1_LEVEL_15:String = "1_3_15";
		
		public static const KINGDOM_1_AREA_4:String = "1_4_0";
			public static const KINGDOM_1_LEVEL_16:String = "1_4_16";
			public static const KINGDOM_1_LEVEL_17:String = "1_4_17";
			public static const KINGDOM_1_LEVEL_18:String = "1_4_18";
			public static const KINGDOM_1_LEVEL_19:String = "1_4_19";
			public static const KINGDOM_1_LEVEL_20:String = "1_4_20";
		
		public static const KINGDOM_1_AREA_5:String = "1_5_0";
			public static const KINGDOM_1_LEVEL_21:String = "1_5_21";
			public static const KINGDOM_1_LEVEL_22:String = "1_5_22";
			public static const KINGDOM_1_LEVEL_23:String = "1_5_23";
			public static const KINGDOM_1_LEVEL_24:String = "1_5_24";
			public static const KINGDOM_1_LEVEL_25:String = "1_5_25";
			
		// UI LEVELS
		public static const WELCOME_LEVEL:String = "welcomeLevel";
		public static const INTRO_LEVEL:String = "introLevel";
		public static const ACHIEVEMENTS_LEVEL:String = "achievementsLevel";
		public static const KINGDOM_SELECT_LEVEL:String = "kingdomSelectLevel";
		public static const LEVEL_SELECT_LEVEL:String = "LevelSelectScreen";
		public static const MENU_LEVEL:String = "menuLevel";
		public static const OPTIONS_LEVEL:String = "optionsLevel";
		public static const TREASURE_LEVEL:String = "treasureLevel";
		public static const GAUGE_LEVEL:String = "gaugeLevel";
							
	}
}