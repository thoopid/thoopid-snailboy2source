package com.thoopid.games.sb2.constants.game
{
	public class ObjectNames
	{
		public static const LEVEL_END:String = "levelEnd";
		public static const LEVEL_STORY_ITEM:String = "levelStoryItemCollectable";
		public static const LEVEL_TRESURE_CHEST:String = "tresureChest";
		public static const LEVEL_CRATE:String = "crate";
		public static const LEVEL_CLAM:String = "tresureClam";
	}
}