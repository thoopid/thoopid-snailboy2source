package com.thoopid.games.sb2.constants.game
{
	import flash.utils.Dictionary;

	public class PlayerElements
	{
		public static const GAUGE_GAUGE:String = "gauge";
		public static const GAUGE_JUMP:String = "jump";
		public static const GAUGE_SWIM:String = "swim";
		
		public static const BADGE_COINS:String = "coinsBadge";
		public static const BADGE_DEATHS:String = "deathsBadge";
		public static const BADGE_JUMP:String = "jumpBadge";
		public static const BADGE_SECRET_AREA:String = "secretAreaBadge";
		public static const BADGE_SLIME:String = "slimeBadge";
		public static const BADGE_STORY_ITEM:String = "storyItemBadge";
		
		public static const BADGE_MESSAGES:Dictionary = new Dictionary();
		
		public static function init():void
		{
			BADGE_MESSAGES[BADGE_COINS] = "COINS EARNED";
			BADGE_MESSAGES[BADGE_DEATHS] = "NO DEATHS";
			BADGE_MESSAGES[BADGE_JUMP] = "GOLD JUMP";
			BADGE_MESSAGES[BADGE_SECRET_AREA] = "AREA FOUND";
			BADGE_MESSAGES[BADGE_SLIME] = "ALL SLIMIES";
			BADGE_MESSAGES[BADGE_STORY_ITEM] = "ITEM FOUND";
		}
		
		public static function getBadgeMsg(id:String):String
		{
			return BADGE_MESSAGES[id] || "";
		}
		
	}
}