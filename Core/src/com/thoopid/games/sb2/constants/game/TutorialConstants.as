package com.thoopid.games.sb2.constants.game
{
	public class TutorialConstants
	{
		public static const TUT_STEP_NONE:String = "tut_0000";
		public static const TUT_STEP_JUMP:String = "tut_01";
		public static const TUT_STEP_SUPER_JUMP:String = "tut_02";
		public static const TUT_STEP_GAME_SLOTS:String = "tut_03";
		public static const TUT_STEP_GAME_SLOTS_COMPLETE:String = "tut_03_complete";
		public static const TUT_STEP_STICK:String = "tut_04";
		public static const TUT_STEP_STORY_ELEMENT:String = "tut_05";
		public static const TUT_STEP_SECRET_AREA:String = "tut_06";
		public static const TUT_STEP_SWIM:String = "tut_07";
		public static const TUT_STEP_SWIM_PAUSE:String = "tut_08";
		public static const TUT_STEP_EXIT:String = "tut_09";
		public static const TUT_STEP_INVENTORY_OPEN:String = "tut_10";
		
		public static const TUT_STEP_DRAG:String = "tut_11";
		public static const TUT_STEP_INVENTORY_BUY:String = "tut_12";
		public static const TUT_STEP_INVENTORY_CLOSE:String = "tut_12_2";
		
		public static const TUT_STEP_LEVEL_SELECT:String = "tut_13";
	}
}