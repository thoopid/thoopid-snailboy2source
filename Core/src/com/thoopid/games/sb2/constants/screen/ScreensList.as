package com.thoopid.games.sb2.constants.screen
{
	public class ScreensList
	{
		public static const WELCOME_SCREEN:String = "WelcomeScreen";
		public static const LEVEL_RESULT:String = "LevelResult";
		public static const HUD:String = "Hud";
		public static const GAUGE_UPGRADE:String = "GaugeUpgrade";
		public static const CONFIRM_SCREEN:String = "ConfirmScreen";
		public static const ITEM_INFO_SCREEN:String = "InfoScreen";
		public static const MESSAGE_SCREEN:String = "MessageScreen";
		public static const INVENTORY_SCREEN:String = "InventoryScreen";
		public static const ACHIEVEMENTS_SCREEN:String = "AchievementsScreen";
		public static const KINGDOM_SELECT_SCREEN:String = "KingdomSelectScreen";
		public static const LEVEL_SELECT_SCREEN:String = "LevelSelectScreen";
		public static const SHOP_SCREEN:String = "ShopScreen";
		public static const OPTIONS_SCREEN:String = "OptionsScreen";
		public static const STORY_SCREEN_MAIN:String = "StoryScreenMain";
		public static const STORY_SCREEN_SECONDARY:String = "StoryScreenSecondary";
		public static const MENU_SCREEN:String = "MenuScreen";
		public static const TREASURE_SCREEN:String = "TreasureScreen";
	}
}