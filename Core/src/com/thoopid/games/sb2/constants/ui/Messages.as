package com.thoopid.games.sb2.constants.ui
{
	public class Messages
	{
		public static const MSG_SHORT_COLLECT_STORY:String = "Yeah, item found!";
		
		public static const MSG_SHORT_COLLECT_TRESURE:String = "Ohh, finally found some tresure!";
		
		public static const MSG_SHORT_COLLECT_VIAL:String = "Gauge refill. Yeah!";
		
		public static const MSG_SHORT_KINGDOM_CLEARED:String = "Well done, you have collected all the shells!";
		
		public static const MSG_NOT_ENOUGH_COIN_TITLE:String = "OOPS!";
		public static const MSG_NOT_ENOUGH_COIN_SUBTITLE:String = "U NEED COIN";
		public static const MSG_NOT_ENOUGH_COIN_COPY:String = "You cannot purchase this item, do you want to buy some more coin from the shop?.";
	}
}