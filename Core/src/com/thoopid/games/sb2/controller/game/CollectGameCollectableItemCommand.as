package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.constants.ui.Messages;
	import com.thoopid.games.sb2.controller.signals.game.ShowCoinCollectedSignal;
	import com.thoopid.games.sb2.controller.signals.game.ShowSlimeCollectedSignal;
	import com.thoopid.games.sb2.controller.signals.game.UseGameConsumableSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ShortMessageDisplaySignal;
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.game.objects.CollectableItem;
	
	import citrus.core.CitrusEngine;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class CollectGameCollectableItemCommand extends SignalCommand
	{
		[Inject]
		public var itemCollected:CollectableItem;
		
		// Signals
		[Inject]
		public var useConsumeable:UseGameConsumableSignal;
		[Inject]
		public var shortMessageSignal:ShortMessageDisplaySignal;
		[Inject]
		public var showCoinCollectedSignal:ShowCoinCollectedSignal;
		[Inject]
		public var showSlimeCollectedSignal:ShowSlimeCollectedSignal;
		
		// Models
		[Inject]
		public var gameModel:GameModel;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		
		public function CollectGameCollectableItemCommand()
		{
			super();
		}
		
		override public function execute():void
		{
//			trace(" You Have Collected a : "+itemCollected.collectableType + " - " + itemCollected.typeId);
			
			switch(itemCollected.collectableType)
			{
				// These Items effect your gauge and game directly
				case GameCollectableTypes.TYPE_CONSUME:
					
					switch(itemCollected.typeId)
					{
						case GameCollectableTypes.TYPE_VIAL_SMALL:
						case GameCollectableTypes.TYPE_VIAL_MED:
						case GameCollectableTypes.TYPE_VIAL_LARGE:
							useConsumeable.dispatch(itemCollected.typeId);
							
							// Messageging and reward
							shortMessageSignal.dispatch(Messages.MSG_SHORT_COLLECT_VIAL);
							
							// Sound
							CitrusEngine.getInstance().sound.playSound(GameConstants.SOUND_SLIMEY);
							break;
						
						case GameCollectableTypes.TYPE_SLIME:
							useConsumeable.dispatch(itemCollected.typeId);
							
							// Also report it to the gameModel
							gameModel.collectSlimy();
							gameModel.addCoins(GameConstants.COIN_VALUE_SLIMEY);
													
							showSlimeCollectedSignal.dispatch(itemCollected.viewClip);
							
							// Sound
							CitrusEngine.getInstance().sound.playSound(GameConstants.SOUND_SLIMEY);
							break;
					}
					
					break;
				
				case GameCollectableTypes.TYPE_STORY:
					
					gameModel.currentLevel.storyItemCollected = true;
					playerService.player.hasRevealedAreaItemCollected = false;
					gameModel.addCoins(GameConstants.COIN_VALUE_STORY_ITEM);
					showCoinCollectedSignal.dispatch(itemCollected.viewClip);
					
					// Messageging and reward
					shortMessageSignal.dispatch(Messages.MSG_SHORT_COLLECT_STORY);
					
					// Sound
					CitrusEngine.getInstance().sound.playSound(GameConstants.SOUND_SHELL);
					break;
				
				case GameCollectableTypes.TYPE_TRESURE:
					
					// Todo add to the Treasure model
					gameModel.addCoins(GameConstants.COIN_VALUE_TREASURE);
					showCoinCollectedSignal.dispatch(itemCollected.viewClip);
					
					// Sound
					CitrusEngine.getInstance().sound.playSound(GameConstants.SOUND_SLIMEY);
					
							// Messageging and reward
							shortMessageSignal.dispatch(Messages.MSG_SHORT_COLLECT_TRESURE);
					switch(itemCollected.typeId)
					{
						case GameCollectableTypes.TREASURE_BOTTLE:
						case GameCollectableTypes.TREASURE_IDOL:
						case GameCollectableTypes.TREASURE_VASE:
							break;
						case GameCollectableTypes.TREASURE_BOTTLE:
							
							break;
					}
					
					break;
			}
		}
	}
}