package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.controller.signals.game.SaveLevelScoreSignal;
	import com.thoopid.games.sb2.controller.signals.system.LevelResultToggleSignal;
	
	import citrus.core.CitrusEngine;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class LevelCompleteCommand extends SignalCommand
	{
		[Inject]
		public var levelResultToggleSignal:LevelResultToggleSignal;
		[Inject]
		public var saveLevelScoreSignal:SaveLevelScoreSignal;
		
		public function LevelCompleteCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			// Save the level and promt the level result screen
			saveLevelScoreSignal.dispatch();
			
			// Show the level Result Screen and confirm its the end of the level
			levelResultToggleSignal.dispatch(true,true);
			
			// Sound
			CitrusEngine.getInstance().sound.playSound(GameConstants.SOUND_EXIT);
		}
	}
}