package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.model.game.LevelVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class NewGameCommand extends SignalCommand
	{
		[Inject]
		public var levelModel:LevelModel;
		
		[Inject]
		public var gameModel:GameModel;
		[Inject]
		public var gaugeModel:GaugeModel;
		
		[Inject]
		public var playerService:PlayerService;
		
		[Inject]
		public var levelId:String;
		
		public function NewGameCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			// Level VO
			var newLevelVO:LevelVO = levelModel.getLevel(levelId);
			if(newLevelVO)
			{
				// Make Sure the Story Item is only presented once
				var currentUserLevelVO:LevelVO = playerService.getPlayerLevel(levelId);
//				if(currentUserLevelVO)
//				{
//					newLevelVO.storyItemCollected = currentUserLevelVO.storyItemCollected;
//					newLevelVO.deathBadgeCollected = currentUserLevelVO.deathBadgeCollected;
//					newLevelVO.secretAreaBadgeCollected = currentUserLevelVO.secretAreaBadgeCollected;
//					newLevelVO.jumpBadgeCollected = currentUserLevelVO.jumpBadgeCollected;
//					newLevelVO.slimeBadgeCollected = currentUserLevelVO.slimeBadgeCollected;
//					newLevelVO.locked = currentUserLevelVO.locked;
//				}
				
				gameModel.newGame(currentUserLevelVO);
				
			} else
			{
				throw new Error("Cannot Find a Level: ");
			}
		}
	}
}