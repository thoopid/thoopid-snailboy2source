package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.controller.signals.system.LevelResultToggleSignal;
	import com.thoopid.games.sb2.model.game.GameModel;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class PauseLevelCommand extends SignalCommand
	{
		[Inject]
		public var gameModel:GameModel;
		
		[Inject]
		public var levelResultScreenSignal:LevelResultToggleSignal;
		
		public function PauseLevelCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			gameModel.pauseGame();
			
			levelResultScreenSignal.dispatch(true, false);
		}
	}
}