package com.thoopid.games.sb2.controller.game
{
	import com.gamua.flox.Player;
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.controller.signals.game.ToggleOraSignal;
	import com.thoopid.games.sb2.controller.signals.game.UnlockLevelSignal;
	import com.thoopid.games.sb2.controller.signals.ui.RemoveSlotItemSignal;
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.model.game.LevelVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class SaveLevelScoreCommand extends SignalCommand
	{
		// Models
		[Inject]
		public var gameModel:GameModel;
		[Inject]
		public var levelModel:LevelModel;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var toggleOraSignal:ToggleOraSignal;
		[Inject]
		public var removeSlotItemSignal:RemoveSlotItemSignal;
		[Inject]
		public var unlockLevelSignal:UnlockLevelSignal;
		
		public function SaveLevelScoreCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			var currentUserLevelVO:LevelVO = playerService.getPlayerLevel(gameModel.currentLevel.levelId);
			if(currentUserLevelVO)
			{
				// Check on Properties that need to be updated
					// If more slimies collected this time update
					// if story item collected update
				// Check badges
				
				
//				if(currentUserLevelVO.slimiesCollected > gameModel.currentLevel.slimiesCollected) gameModel.currentLevel.slimiesCollected = currentUserLevelVO.slimiesCollected;
//				if(currentUserLevelVO.deaths < gameModel.currentLevel.slimiesCollected) gameModel.currentLevel.deaths = currentUserLevelVO.deaths;
			}
			
			// Update badges
			if(gameModel.currentLevel.deaths <= 0)
			{
				gameModel.currentLevel.deathBadgeCollected = true;
			}
			
			if(gameModel.currentLevel.slimiesCollected >= gameModel.currentLevel.maxSlimies)
			{
				gameModel.currentLevel.slimeBadgeCollected = true;
			}
			
			// Check if Hero collected the story item
			// Highligh that the area shard was collected - Use this property when returning to the Leve selection to highlight the collected shard, set back to false once Shard collected shown
			
			
			// TODO - remove this line
			//------------------------------------------
//			gameModel.currentLevel.storyItemCollected = true;
//			playerService.player.hasRevealedAreaItemCollected = false;
			//------------------------------------------
			
			if(gameModel.currentLevel.storyItemCollected && !playerService.player.hasRevealedAreaItemCollected) 
				gameModel.revealAreaItemCollected = true;
			
			// Check if user has used an Gauge regen Ora and remove
			if(playerService.inventoryGetItem(GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH)) 
			{
				toggleOraSignal.dispatch(GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH, false);
				removeSlotItemSignal.dispatch(GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH, false);
			}
			
			// Implement the save
			playerService.saveLevel(gameModel.currentLevel);
			playerService.grantCoins(gameModel.currentLevel.coinsCount);
			
			// Unlock next Level
			unlockLevelSignal.dispatch(playerService.getNextLevelIdToUnlock());
			
			// Only save once
			Player.current.saveQueued();
		}
	}
}