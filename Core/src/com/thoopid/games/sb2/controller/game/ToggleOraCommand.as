package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class ToggleOraCommand extends SignalCommand
	{
		[Inject]
		public var id:String;
		[Inject]
		public var toggle:Boolean;
		
		[Inject]
		public var gameModel:GameModel;
		[Inject]
		public var gaugeModel:GaugeModel;
		
		public function ToggleOraCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			switch(id)
			{
				case GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH:
					gaugeModel.toggleGaugeRegen((toggle) ? 6 : 0);
					break;
			}
		}
	}
}