package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.controller.signals.game.ToggleSoftPauseGame;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorial_ShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GrantInventoryItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ItemInfoPopupRequestSignal;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.model.game.StoryDataModel;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.game.level.util.TouchInputController;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class TriggerTutorialCommand extends SignalCommand
	{
		
		[Inject]
		public var state:String;
		
		[Inject]
		public var tutModel:TutorialModel;
		[Inject]
		public var storyDataModel:StoryDataModel;
		
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var grantInventoryItemSignal:GrantInventoryItemSignal;
		[Inject]
		public var triggerTutorial_ShowSignal:TriggerTutorial_ShowSignal;
		[Inject]
		public var itemInfoRequestSignal:ItemInfoPopupRequestSignal;
		[Inject]
		public var softPauseSignal:ToggleSoftPauseGame;
		
		
		public function TriggerTutorialCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			tutModel.state = state;
			
			if(state == TutorialConstants.TUT_STEP_NONE) 
			{
				triggerTutorial_ShowSignal.dispatch(state);
				return;
			}
			
			if(playerService.hasCompletedTutorial(state)) return;
			
			triggerTutorial_ShowSignal.dispatch(state);
			
			switch(state)
			{
				case TutorialConstants.TUT_STEP_JUMP:
					break;
				case TutorialConstants.TUT_STEP_SUPER_JUMP:
					playerService.player.superEnabled = true;
					playerService.player.gaugeUpgradeEnabled = true;
					TouchInputController.getInstance().canSuperJump = true;
					break;
				case TutorialConstants.TUT_STEP_SWIM:
					playerService.player.swimEnabled = true;
					TouchInputController.getInstance().canSwim = true;
					break;
				case TutorialConstants.TUT_STEP_SWIM_PAUSE:
					softPauseSignal.dispatch(true);
					break;
				case TutorialConstants.TUT_STEP_GAME_SLOTS:
					grantInventoryItemSignal.dispatch(new InventoryItemVO(GameCollectableTypes.TYPE_VIAL_SMALL,0,1));
					break;
				case TutorialConstants.TUT_STEP_GAME_SLOTS_COMPLETE:
					break;
				case TutorialConstants.TUT_STEP_STORY_ELEMENT:
//					itemInfoRequestSignal.dispatch(storyDataModel.getItemById(GameCollectableTypes.STORY_ITEM_1_1));
					break;
				case TutorialConstants.TUT_STEP_INVENTORY_BUY:

					break;
			}
			
			playerService.completeTutorial(state);
		}
	}
}