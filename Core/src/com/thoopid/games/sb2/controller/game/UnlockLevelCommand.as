package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.model.game.LevelVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class UnlockLevelCommand extends SignalCommand
	{
		// Payload
		[Inject]
		public var levelId:String;
		
		// Models
		[Inject]
		public var levelModel:LevelModel;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		public function UnlockLevelCommand()
		{
			super();
		}
		
		override public function execute():void
		{
//			var level:LevelVO = levelModel.getLevel(levelId);
//			level.locked = false;
			
			playerService.unlockLevel(levelId);
		}
	}
}