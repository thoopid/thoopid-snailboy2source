package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.controller.signals.game.ToggleOraSignal;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class UseGameConsumableCommand extends SignalCommand
	{
		[Inject]
		public var id:String;
		
		// Models
		[Inject]
		public var gaugeModel:GaugeModel;
		
		// Signals
		[Inject]
		public var toggleOraSignal:ToggleOraSignal;
		
		public function UseGameConsumableCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			switch(id)
			{
				case GameCollectableTypes.TYPE_VIAL_SMALL:
					gaugeModel.replenish(GameConstants.GAUGE_VIAL_SMALL);
					break;
				case GameCollectableTypes.TYPE_VIAL_MED:
					gaugeModel.replenish(GameConstants.GAUGE_VIAL_MEDIUM);
					break;
				case GameCollectableTypes.TYPE_VIAL_LARGE:
					gaugeModel.replenish(GameConstants.GAUGE_VIAL_LARGE);
					break;
				case GameCollectableTypes.TYPE_SLIME:
					gaugeModel.replenish(GameConstants.GAUGE_SLIME_NORMAL);
					break;
				
				// ORA's
				case GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH:
					toggleOraSignal.dispatch(id, true);
					break;
			}
		}
	}
}