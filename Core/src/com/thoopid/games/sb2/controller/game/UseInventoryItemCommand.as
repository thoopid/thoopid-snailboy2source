package com.thoopid.games.sb2.controller.game
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.game.UseGameConsumableSignal;
	import com.thoopid.games.sb2.controller.signals.ui.RemoveSlotItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.SlotsUpdateSignal;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class UseInventoryItemCommand extends SignalCommand
	{
		[Inject]
		public var id:String;
		
		// Signals
		[Inject]
		public var useConsumeable:UseGameConsumableSignal;
		[Inject]
		public var slotsUpdateSignal:SlotsUpdateSignal;
		[Inject]
		public var removeSlotItemSignal:RemoveSlotItemSignal;
		[Inject]
		public var triggerTutSignal:TriggerTutorialSignal;
		
		[Inject]
		public var playerService:PlayerService;
		
		public function UseInventoryItemCommand()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function execute():void
		{
			var mustRemoveFromSlot:Boolean = true;
			switch(id)
			{
				case GameCollectableTypes.TYPE_VIAL_SMALL:
				case GameCollectableTypes.TYPE_VIAL_MED:
				case GameCollectableTypes.TYPE_VIAL_LARGE:
					
					// Also report this tutorial finished
					triggerTutSignal.dispatch(TutorialConstants.TUT_STEP_GAME_SLOTS_COMPLETE);
					
					useConsumeable.dispatch(id);
					break;
				case GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH:
					mustRemoveFromSlot = false;
					useConsumeable.dispatch(GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH);
					break;
			}
			
			if(mustRemoveFromSlot)
			{
				removeSlotItemSignal.dispatch(id, false);
//				playerService.slotsRemove(id);
//				slotsUpdateSignal.dispatch();
			}
		}
	}
}