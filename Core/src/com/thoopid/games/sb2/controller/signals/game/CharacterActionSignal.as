package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class CharacterActionSignal extends Signal
	{
		public function CharacterActionSignal(...parameters)
		{
			super(String);
		}
	}
}