package com.thoopid.games.sb2.controller.signals.game
{
	import com.thoopid.games.sb2.view.game.objects.CollectableItem;
	
	import org.osflash.signals.Signal;
	
	public class CollectGameCollectableItemSignal extends Signal
	{
		public function CollectGameCollectableItemSignal(...parameters)
		{
			super(CollectableItem);
		}
	}
}