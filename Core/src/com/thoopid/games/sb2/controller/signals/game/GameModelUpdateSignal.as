package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class GameModelUpdateSignal extends Signal
	{
		public function GameModelUpdateSignal(...parameters)
		{
			super(parameters);
		}
	}
}