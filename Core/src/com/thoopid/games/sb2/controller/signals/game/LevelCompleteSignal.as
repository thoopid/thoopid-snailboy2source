package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class LevelCompleteSignal extends Signal
	{
		public function LevelCompleteSignal(...parameters)
		{
			super(parameters);
		}
	}
}