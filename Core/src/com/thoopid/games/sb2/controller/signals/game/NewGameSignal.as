package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class NewGameSignal extends Signal
	{
		public function NewGameSignal(...parameters)
		{
			super(String);
		}
	}
}