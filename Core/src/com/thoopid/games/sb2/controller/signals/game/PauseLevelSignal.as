package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class PauseLevelSignal extends Signal
	{
		public function PauseLevelSignal(...parameters)
		{
			super();
		}
	}
}