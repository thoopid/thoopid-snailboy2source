package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class ResumeLevelSignal extends Signal
	{
		public function ResumeLevelSignal(...parameters)
		{
			super(parameters);
		}
	}
}