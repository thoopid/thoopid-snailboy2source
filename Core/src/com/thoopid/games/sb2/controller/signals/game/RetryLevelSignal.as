package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class RetryLevelSignal extends Signal
	{
		public function RetryLevelSignal(...parameters)
		{
			super(parameters);
		}
	}
}