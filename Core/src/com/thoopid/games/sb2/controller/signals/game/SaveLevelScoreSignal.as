package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class SaveLevelScoreSignal extends Signal
	{
		public function SaveLevelScoreSignal(...parameters)
		{
			super(parameters);
		}
	}
}