package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	import starling.display.DisplayObject;
	
	public class ShowCoinCollectedSignal extends Signal
	{
		public function ShowCoinCollectedSignal(...parameters)
		{
			super(DisplayObject);
		}
	}
}