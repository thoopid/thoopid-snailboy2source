package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	import starling.display.DisplayObject;
	
	public class ShowSlimeCollectedSignal extends Signal
	{
		public function ShowSlimeCollectedSignal(...parameters)
		{
			super(DisplayObject);
		}
	}
}