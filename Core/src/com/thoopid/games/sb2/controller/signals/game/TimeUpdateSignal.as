package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class TimeUpdateSignal extends Signal
	{
		public function TimeUpdateSignal(...parameters)
		{
			super(Number);
		}
	}
}