package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class ToggleOraSignal extends Signal
	{
		public function ToggleOraSignal(...parameters)
		{
			super(String, Boolean);
		}
	}
}