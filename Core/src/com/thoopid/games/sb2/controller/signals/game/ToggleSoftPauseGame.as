package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class ToggleSoftPauseGame extends Signal
	{
		public function ToggleSoftPauseGame(...parameters)
		{
			super(Boolean);
		}
	}
}