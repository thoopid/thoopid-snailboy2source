package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class TriggerTutorialSignal extends Signal
	{
		public function TriggerTutorialSignal(...parameters)
		{
			super(String);
		}
	}
}