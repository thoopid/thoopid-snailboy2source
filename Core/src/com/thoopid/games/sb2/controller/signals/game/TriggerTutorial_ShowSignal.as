package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class TriggerTutorial_ShowSignal extends Signal
	{
		public function TriggerTutorial_ShowSignal(...parameters)
		{
			super(String);
		}
	}
}