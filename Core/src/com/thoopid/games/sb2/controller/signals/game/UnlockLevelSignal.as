package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class UnlockLevelSignal extends Signal
	{
		public function UnlockLevelSignal(...parameters)
		{
			super(String);
		}
	}
}