package com.thoopid.games.sb2.controller.signals.game
{
	import com.thoopid.games.sb2.model.game.ConsumableItemVO;
	
	import org.osflash.signals.Signal;
	
	public class UseGameConsumableSignal extends Signal
	{
		public function UseGameConsumableSignal(...parameters)
		{
			super(String);
		}
	}
}