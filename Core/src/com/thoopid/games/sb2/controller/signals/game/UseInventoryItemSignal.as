package com.thoopid.games.sb2.controller.signals.game
{
	import org.osflash.signals.Signal;
	
	public class UseInventoryItemSignal extends Signal
	{
		public function UseInventoryItemSignal(...parameters)
		{
			super(String);
		}
	}
}