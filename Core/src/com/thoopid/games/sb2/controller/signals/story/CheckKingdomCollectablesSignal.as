package com.thoopid.games.sb2.controller.signals.story
{
	import org.osflash.signals.Signal;
	
	public class CheckKingdomCollectablesSignal extends Signal
	{
		public function CheckKingdomCollectablesSignal(...parameters)
		{
			super(String);
		}
	}
}