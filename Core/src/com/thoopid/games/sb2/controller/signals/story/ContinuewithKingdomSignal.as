package com.thoopid.games.sb2.controller.signals.story
{
	import org.osflash.signals.Signal;
	
	public class ContinuewithKingdomSignal extends Signal
	{
		public function ContinuewithKingdomSignal(...parameters)
		{
			super(String);
		}
	}
}