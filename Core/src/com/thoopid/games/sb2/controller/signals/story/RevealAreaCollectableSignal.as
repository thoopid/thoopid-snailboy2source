package com.thoopid.games.sb2.controller.signals.story
{
	import org.osflash.signals.Signal;
	
	public class RevealAreaCollectableSignal extends Signal
	{
		public function RevealAreaCollectableSignal(...parameters)
		{
			super(String);
		}
	}
}