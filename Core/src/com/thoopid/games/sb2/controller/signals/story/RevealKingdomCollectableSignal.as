package com.thoopid.games.sb2.controller.signals.story
{
	import org.osflash.signals.Signal;
	
	public class RevealKingdomCollectableSignal extends Signal
	{
		public function RevealKingdomCollectableSignal(...parameters)
		{
			super(String);
		}
	}
}