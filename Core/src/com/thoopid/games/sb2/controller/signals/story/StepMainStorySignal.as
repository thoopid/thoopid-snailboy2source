package com.thoopid.games.sb2.controller.signals.story
{
	import org.osflash.signals.Signal;
	
	public class StepMainStorySignal extends Signal
	{
		public function StepMainStorySignal(...parameters)
		{
			super(parameters);
		}
	}
}