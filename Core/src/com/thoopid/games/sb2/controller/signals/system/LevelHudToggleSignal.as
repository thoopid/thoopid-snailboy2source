package com.thoopid.games.sb2.controller.signals.system
{
	import org.osflash.signals.Signal;
	
	public class LevelHudToggleSignal extends Signal
	{
		public function LevelHudToggleSignal(...parameters)
		{
			super(Boolean);
		}
	}
}