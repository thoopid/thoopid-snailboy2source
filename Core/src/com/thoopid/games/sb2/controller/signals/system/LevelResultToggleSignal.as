package com.thoopid.games.sb2.controller.signals.system
{
	import org.osflash.signals.Signal;
	
	public class LevelResultToggleSignal extends Signal
	{
		public function LevelResultToggleSignal(...parameters)
		{
			// Params - Toggle Screen
			// Params - is level complete or false if just paused, because this same ui gets used when its just paused
			super(Boolean, Boolean);
		}
	}
}