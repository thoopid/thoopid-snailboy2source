package com.thoopid.games.sb2.controller.signals.system
{
	import org.osflash.signals.Signal;
	
	public class LoadLevelSignal extends Signal
	{
		public function LoadLevelSignal(...parameters)
		{
			super(String);
		}
	}
}