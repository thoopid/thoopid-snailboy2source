package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class ConfirmCallbackSignal extends Signal
	{
		public function ConfirmCallbackSignal(...parameters)
		{
			super(Boolean);
		}
	}
}