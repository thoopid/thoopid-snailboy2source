package com.thoopid.games.sb2.controller.signals.ui
{
	import com.thoopid.games.sb2.model.ui.ConfirmPopupVO;
	
	import org.osflash.signals.Signal;
	
	public class ConfirmPopupRequestSignal extends Signal
	{
		public function ConfirmPopupRequestSignal(...parameters)
		{
			super(ConfirmPopupVO);
		}
	}
}