package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class EquiptSlotItemSignal extends Signal
	{
		public function EquiptSlotItemSignal(...parameters)
		{
			super(String);
		}
	}
}