package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class GameHudShowSignal extends Signal
	{
		public function GameHudShowSignal(...parameters)
		{
			super(Boolean);
		}
	}
}