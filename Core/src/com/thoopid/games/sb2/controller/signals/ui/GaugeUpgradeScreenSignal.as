package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class GaugeUpgradeScreenSignal extends Signal
	{
		public function GaugeUpgradeScreenSignal(...parameters)
		{
			super(Boolean);
		}
	}
}