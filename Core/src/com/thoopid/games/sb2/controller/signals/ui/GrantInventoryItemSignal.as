package com.thoopid.games.sb2.controller.signals.ui
{
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	
	import org.osflash.signals.Signal;
	
	public class GrantInventoryItemSignal extends Signal
	{
		public function GrantInventoryItemSignal(...parameters)
		{
			super(InventoryItemVO);
		}
	}
}