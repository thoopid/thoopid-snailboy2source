package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class InventoryShowSignal extends Signal
	{
		public function InventoryShowSignal(...parameters)
		{
			super(Boolean);
		}
	}
}