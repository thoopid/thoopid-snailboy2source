package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class InventoryUpdatedSignal extends Signal
	{
		public function InventoryUpdatedSignal(...parameters)
		{
			super(parameters);
		}
	}
}