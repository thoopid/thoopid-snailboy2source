package com.thoopid.games.sb2.controller.signals.ui
{
	import com.thoopid.games.sb2.model.game.ItemInfoVO;
	
	import org.osflash.signals.Signal;
	
	public class ItemInfoPopupRequestSignal extends Signal
	{
		public function ItemInfoPopupRequestSignal(...parameters)
		{
			super(ItemInfoVO);
		}
	}
}