package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class KingdomAreaUpdateSignal extends Signal
	{
		public function KingdomAreaUpdateSignal(...parameters)
		{
			super(parameters);
		}
	}
}