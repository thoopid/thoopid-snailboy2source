package com.thoopid.games.sb2.controller.signals.ui
{
	import com.thoopid.games.sb2.model.ui.MessagePopupVO;
	
	import org.osflash.signals.Signal;
	
	public class MessagePopupRequestSignal extends Signal
	{
		public function MessagePopupRequestSignal(...parameters)
		{
			super(MessagePopupVO);
		}
	}
}