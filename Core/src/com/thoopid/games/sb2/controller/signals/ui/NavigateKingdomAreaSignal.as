package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class NavigateKingdomAreaSignal extends Signal
	{
		public function NavigateKingdomAreaSignal(...parameters)
		{
			super(Number);
		}
	}
}