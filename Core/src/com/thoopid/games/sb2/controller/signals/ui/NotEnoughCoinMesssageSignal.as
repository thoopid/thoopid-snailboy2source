package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class NotEnoughCoinMesssageSignal extends Signal
	{
		public function NotEnoughCoinMesssageSignal(...parameters)
		{
			super(parameters);
		}
	}
}