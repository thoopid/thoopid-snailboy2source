package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class OptionsShowSignal extends Signal
	{
		public function OptionsShowSignal(...parameters)
		{
			super(Boolean);
		}
	}
}
