package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class PlayerStatsShowSignal extends Signal
	{
		public function PlayerStatsShowSignal(...parameters)
		{
			super(Boolean);
		}
	}
}