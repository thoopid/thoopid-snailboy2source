package com.thoopid.games.sb2.controller.signals.ui
{
	import com.thoopid.games.sb2.model.game.ShopItemVO;
	
	import org.osflash.signals.Signal;
	
	public class PurchaseCoinSignal extends Signal
	{
		public function PurchaseCoinSignal(...parameters)
		{
			super(String);
		}
	}
}