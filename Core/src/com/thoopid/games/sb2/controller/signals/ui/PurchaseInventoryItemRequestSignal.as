package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class PurchaseInventoryItemRequestSignal extends Signal
	{
		public function PurchaseInventoryItemRequestSignal(...parameters)
		{
			super(String);
		}
	}
}