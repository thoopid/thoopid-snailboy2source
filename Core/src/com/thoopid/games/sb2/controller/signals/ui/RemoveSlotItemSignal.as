package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class RemoveSlotItemSignal extends Signal
	{
		public function RemoveSlotItemSignal(...parameters)
		{
			super(String, Boolean);
		}
	}
}