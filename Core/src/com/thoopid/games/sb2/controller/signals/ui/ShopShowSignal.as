package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class ShopShowSignal extends Signal
	{
		public function ShopShowSignal(...parameters)
		{
			super(Boolean);
		}
	}
}