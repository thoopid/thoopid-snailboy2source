package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class ShortMessageDisplaySignal extends Signal
	{
		public function ShortMessageDisplaySignal(...parameters)
		{
			super(String);
		}
	}
}