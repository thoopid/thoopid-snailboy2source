package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class SlotsUpdateSignal extends Signal
	{
		public function SlotsUpdateSignal(...parameters)
		{
			super(parameters);
		}
	}
}