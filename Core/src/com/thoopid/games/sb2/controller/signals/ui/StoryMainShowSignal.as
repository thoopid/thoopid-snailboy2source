package com.thoopid.games.sb2.controller.signals.ui
{
	import com.thoopid.games.sb2.model.game.StoryLineItemVO;
	
	import org.osflash.signals.Signal;
	
	public class StoryMainShowSignal extends Signal
	{
		public function StoryMainShowSignal(...parameters)
		{
			super(Boolean, StoryLineItemVO);
		}
	}
}