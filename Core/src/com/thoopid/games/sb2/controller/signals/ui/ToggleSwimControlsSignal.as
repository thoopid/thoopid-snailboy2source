package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class ToggleSwimControlsSignal extends Signal
	{
		public function ToggleSwimControlsSignal(...parameters)
		{
			super(Boolean);
		}
	}
}