package com.thoopid.games.sb2.controller.signals.ui
{
	import org.osflash.signals.Signal;
	
	public class UpgradeGaugeRequestSignal extends Signal
	{
		public function UpgradeGaugeRequestSignal(...parameters)
		{
			super(String);
		}
	}
}