package com.thoopid.games.sb2.controller.story
{
	import com.thoopid.games.sb2.constants.ui.Messages;
	import com.thoopid.games.sb2.controller.signals.story.ContinuewithKingdomSignal;
	import com.thoopid.games.sb2.controller.signals.story.RevealAreaCollectableSignal;
	import com.thoopid.games.sb2.controller.signals.story.RevealKingdomCollectableSignal;
	import com.thoopid.games.sb2.controller.signals.story.StepMainStorySignal;
	import com.thoopid.games.sb2.controller.signals.ui.ShortMessageDisplaySignal;
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.model.game.StoryDataModel;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	import starling.core.Starling;
	
	public class CheckKingdomCollectablesCommand extends SignalCommand
	{
		// Signals
		[Inject]
		public var revealAreaCollectableSignal:RevealAreaCollectableSignal;
		[Inject]
		public var revealKingdomCollectableSignal:RevealKingdomCollectableSignal;
		[Inject]
		public var continuewithKingdomSignal:ContinuewithKingdomSignal;
		[Inject]
		public var shortMessageSignal:ShortMessageDisplaySignal;
		[Inject]
		public var stepMainStorySignal:StepMainStorySignal;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		// Models
		[Inject]
		public var gameModel:GameModel;
		[Inject]
		public var storyModel:StoryDataModel;
		
		[Inject]
		public var kingdomId:String;
		
		public function CheckKingdomCollectablesCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			check();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function check():void
		{
			if(gameModel.revealAreaItemCollected)
			{
				// Check to see if the 5 items in the area has been collected then enable KingdomCollectable Reveal
				if(playerService.getAreaItemsCollectedCount(playerService.player.activeArea) == 5)
				{
					gameModel.revealKingdomItemCollected = true;
				}
				
				// Skip to next Story
				playerService.player.mustSkipToNextStoryItem = true;
				
				playerService.player.hasRevealedAreaItemCollected = true;
				gameModel.revealAreaItemCollected = false;
				
				revealAreaCollectableSignal.dispatch(playerService.player.activeArea);
				
				Starling.juggler.delayCall(check, 3);
				return;
			}
			
			if(gameModel.revealKingdomItemCollected)
			{
				playerService.player.hasRevealedKingdomCollected = true;
				gameModel.revealKingdomItemCollected = false;
					
				revealKingdomCollectableSignal.dispatch(kingdomId);
				
				shortMessageSignal.dispatch(Messages.MSG_SHORT_KINGDOM_CLEARED);
				
				Starling.juggler.delayCall(check, 5);
				return;
			}
			
			// Now Reveal the story
			if(playerService.player.mustSkipToNextStoryItem)
			{
				playerService.player.mustSkipToNextStoryItem = false;
				stepMainStorySignal.dispatch();
			}
			
			continuewithKingdomSignal.dispatch("");
		}
	}
}