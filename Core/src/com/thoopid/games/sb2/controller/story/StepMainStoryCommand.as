package com.thoopid.games.sb2.controller.story
{
	import com.thoopid.games.sb2.controller.signals.ui.StoryMainShowSignal;
	import com.thoopid.games.sb2.model.game.StoryDataModel;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class StepMainStoryCommand extends SignalCommand
	{
		[Inject]
		public var storyModel:StoryDataModel;
		
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var storyMainShowSignal:StoryMainShowSignal;
		
		public function StepMainStoryCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			storyMainShowSignal.dispatch(true, storyModel.getStoryLineItem(playerService.player.mainStoryIndex));
			playerService.player.mainStoryIndex++;
		}
	}
}