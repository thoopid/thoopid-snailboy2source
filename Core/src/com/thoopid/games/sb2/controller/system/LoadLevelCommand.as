package com.thoopid.games.sb2.controller.system
{
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.model.sound.SoundModel;
	import com.thoopid.games.sb2.view.game.level.base.ALevel;
	
	import flash.geom.Rectangle;
	
	import citrus.core.CitrusEngine;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Quad;
	
	public class LoadLevelCommand extends SignalCommand
	{
		[Inject]
		public var levelModel:LevelModel;
		[Inject]
		public var soundModel:SoundModel;
		
		[Inject]
		public var levelId:String;

		private var _fade:Quad;
		
		public function LoadLevelCommand()
		{
			super();
		}
		
		override public function execute():void
		{
//			_fade = new Scale9Image(new Scale9Textures(Assets.assets.getTexture("empty"), new Rectangle(1,1,2,2)));
			_fade = new Quad(Config.GAME_WIDTH,Config.GAME_HEIGHT,Config.GAME_BG_COLOR);
//			_fade.color = Config.GAME_BG_COLOR;
//			_fade.width = Config.GAME_WIDTH;
//			_fade.height = Config.GAME_HEIGHT;
			_fade.alpha = 0;
			
			Starling.current.stage.addChild(_fade);
			Starling.juggler.tween(_fade, 0.6, {alpha:1, transition:Transitions.EASE_OUT, onComplete:nextLevel});
			
//			switch(levelId)
//			{
//				case LevelNames.KINGDOM_1:
//					
//					break;
//			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function nextLevel():void
		{
			levelModel.levelManager.onLevelChanged.add(onLevelDone);
			levelModel.levelManager.gotoLevel(levelModel.getLevelIndex(levelId));
		}
		
		private function onLevelDone(lvl:ALevel):void
		{
			levelModel.levelManager.onLevelChanged.remove(onLevelDone);
			Starling.juggler.tween(_fade, 0.5, {alpha:0, delay:0.2, transition:Transitions.EASE_OUT, onComplete:cleanup});
			
			// Play sound for a level
			soundModel.playLevelSound(levelId);
		}
		
		private function cleanup():void
		{
			_fade.removeFromParent();
			_fade = null;
		}
	}
}