 package com.thoopid.games.sb2.controller.system
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.controller.signals.game.UnlockLevelSignal;
	import com.thoopid.games.sb2.controller.signals.system.LoadLevelSignal;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.model.game.StoryDataModel;
	import com.thoopid.games.sb2.model.sound.SoundModel;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.game.elements.AreaElement_Chest;
	import com.thoopid.games.sb2.view.game.elements.AreaElement_Pipes;
	import com.thoopid.games.sb2.view.game.elements.AreaElement_Scroll;
	import com.thoopid.games.sb2.view.game.elements.AreaElement_Shards;
	import com.thoopid.games.sb2.view.game.elements.AreaElement_Tomb;
	import com.thoopid.games.sb2.view.game.elements.KingdomElement_1;
	import com.thoopid.games.sb2.view.game.objects.AirVent;
	import com.thoopid.games.sb2.view.game.objects.AirVentTrigger;
	import com.thoopid.games.sb2.view.game.objects.BlowFish;
	import com.thoopid.games.sb2.view.game.objects.BubbleTrigger;
	import com.thoopid.games.sb2.view.game.objects.CaveLevelSelector;
	import com.thoopid.games.sb2.view.game.objects.CheckpointSensor;
	import com.thoopid.games.sb2.view.game.objects.CheckpointVisual;
	import com.thoopid.games.sb2.view.game.objects.Clam;
	import com.thoopid.games.sb2.view.game.objects.CollectConsumable;
	import com.thoopid.games.sb2.view.game.objects.CollectGemItem;
	import com.thoopid.games.sb2.view.game.objects.CollectLostTreasure;
	import com.thoopid.games.sb2.view.game.objects.CollectStoryItem;
	import com.thoopid.games.sb2.view.game.objects.Crack;
	import com.thoopid.games.sb2.view.game.objects.Crate;
	import com.thoopid.games.sb2.view.game.objects.Crystal;
	import com.thoopid.games.sb2.view.game.objects.CrystalTrigger;
	import com.thoopid.games.sb2.view.game.objects.Hinge;
	import com.thoopid.games.sb2.view.game.objects.HingeTrigger;
	import com.thoopid.games.sb2.view.game.objects.KillZone;
	import com.thoopid.games.sb2.view.game.objects.LevelEnd;
	import com.thoopid.games.sb2.view.game.objects.Mine;
	import com.thoopid.games.sb2.view.game.objects.MinionGround;
	import com.thoopid.games.sb2.view.game.objects.NormalLandingPlatform;
	import com.thoopid.games.sb2.view.game.objects.SecrectPortal;
	import com.thoopid.games.sb2.view.game.objects.ShootShroom;
	import com.thoopid.games.sb2.view.game.objects.SnailboyHeroMarker;
	import com.thoopid.games.sb2.view.game.objects.SpringShroom;
	import com.thoopid.games.sb2.view.game.objects.StickablePlatformLeft;
	import com.thoopid.games.sb2.view.game.objects.StickablePlatformRight;
	import com.thoopid.games.sb2.view.game.objects.TresureChest;
	import com.thoopid.games.sb2.view.game.objects.TutorialObject;
	import com.thoopid.games.sb2.view.game.objects.VolcanoVent;
	import com.thoopid.games.sb2.view.game.objects.Wall;
	import com.thoopid.games.sb2.view.ui.comps.CenteredImage;
	import com.thoopid.games.sb2.view.ui.comps.CoinValue;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.ImagePool;
	import com.thoopid.games.sb2.view.ui.comps.MovieclipPool;
	import com.thoopid.games.sb2.view.ui.comps.Overlay;
	import com.thoopid.games.sb2.view.ui.comps.PlainBg;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.comps.PoolMovieclip;
	import com.thoopid.games.sb2.view.ui.comps.UIImage;
	import com.thoopid.games.sb2.view.ui.comps.UiMaskElement;
	import com.thoopid.games.sb2.view.ui.comps.UiSprite;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	import com.thoopid.games.sb2.view.ui.comps.button.IconCenteredButton;
	import com.thoopid.games.sb2.view.ui.comps.label.LevelResultHeader;
	import com.thoopid.games.sb2.view.ui.popup.ConfirmPopup;
	import com.thoopid.games.sb2.view.ui.popup.ItemInfoPopup;
	import com.thoopid.games.sb2.view.ui.popup.MessagePopup;
	import com.thoopid.games.sb2.view.ui.popup.ShortMessagePopup;
	import com.thoopid.games.sb2.view.ui.screens.GaugeUpgradeScreen;
	import com.thoopid.games.sb2.view.ui.screens.InventoryScreen;
	import com.thoopid.games.sb2.view.ui.screens.LevelResultScreen;
	import com.thoopid.games.sb2.view.ui.screens.OptionsScreen;
	import com.thoopid.games.sb2.view.ui.screens.ShopScreen;
	import com.thoopid.games.sb2.view.ui.screens.StoryScreenMain;
	import com.thoopid.games.sb2.view.ui.screens.hud.HudScreen;
	import com.thoopid.games.sb2.view.ui.screens.hud.HudSlime;
	import com.thoopid.games.sb2.view.ui.screens.levelresult.BadgeIcon;
	import com.thoopid.games.sb2.view.ui.screens.levelresult.CaveBadgeIcon;
	import com.thoopid.games.sb2.view.ui.screens.levelresult.CaveItemIcon;
	import com.thoopid.games.sb2.view.ui.screens.menu.MenuScreenButton;
	import com.thoopid.games.sb2.view.ui.screens.options.OptionScreenButton;
	import com.thoopid.games.sb2.view.ui.screens.story.SpeachBubbleGeneric;
	import com.thoopid.games.sb2.view.ui.theme.ApplicationTheme;
	import com.thoopid.games.sb2.view.ui.theme.ConfirmScreenHeaderLabel;
	
	import flash.filesystem.File;
	import flash.system.Capabilities;
	import flash.system.System;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.formatString;
	
	public class PrepApplicationCommand extends SignalCommand
	{
		// Signals
		[Inject]
		public var loadLevelSignal:LoadLevelSignal;
		[Inject]
		public var unlockLevelSignal:UnlockLevelSignal;
		
		// Service
		[Inject]
		public var playerService:PlayerService;
		
		// Models
		[Inject]
		public var levelModel:LevelModel;
		[Inject]
		public var gaugeModel:GaugeModel;
		[Inject]
		public var storyDataModel:StoryDataModel;
		[Inject]
		public var tutorialModel:TutorialModel;
		[Inject]
		public var soundModel:SoundModel;
		
		private var _theme:ApplicationTheme;
		
		public function PrepApplicationCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			var assets:AssetManager = Assets.assets;
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(File.applicationDirectory.resolvePath(formatString("textures/{0}/{1}x",MobileConfig.CURRENT_OS,assets.scaleFactor)));
			assets.enqueue(File.applicationDirectory.resolvePath("sounds/"));
			assets.enqueue(File.applicationDirectory.resolvePath("effects/"));
			assets.enqueue(File.applicationDirectory.resolvePath("ui/"));
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					Starling.juggler.delayCall(function():void
					{
						// Done
						init();
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
				}
			});
		}
		
		private function init():void
		{
			// Assets to be used by LayoutBuilder
			var assetsList:Array = [NormalLandingPlatform, 
				StickablePlatformLeft, 
				StickablePlatformRight, 
				AirVent, 
				Wall, 
				KillZone, 
				Mine, 
				SnailboyHeroMarker, 
				SecrectPortal, 
				Crate, 
				Crack, 
				Hinge, 
				HingeTrigger, 
				SpringShroom,
				ShootShroom,
				TresureChest,
				Clam,
				CaveLevelSelector,
				BubbleTrigger,
				CrystalTrigger,
				Crystal,
				VolcanoVent,
				AirVentTrigger,
				BlowFish,
				MinionGround,
				SnailboyHeroMarker,
				CaveLevelSelector,
				CollectConsumable,
				CollectStoryItem,
				CollectLostTreasure,
				CollectGemItem,
				LevelEnd,
				TutorialObject,
				CheckpointSensor,
				CheckpointVisual,
				
				// Level Specific Elements
				KingdomElement_1,
				AreaElement_Shards,
				AreaElement_Pipes,
				AreaElement_Chest,
				AreaElement_Tomb,
				AreaElement_Scroll
			];
			
			var uiElementsList:Array = [
				CenteredButton,
				CenteredImage,
				UIImage,
				MenuScreenButton,
				OptionScreenButton,
				Overlay,
				UiMaskElement,
				PlainBg,
				
				// Theme Elements
				LevelResultHeader,
				ConfirmScreenHeaderLabel,
				CoinValue,
				BadgeIcon,
				CaveBadgeIcon,
				CaveItemIcon,
				UiSprite,
				OptionScreenButton,
				IconCenteredButton,
				HudSlime,
				SpeachBubbleGeneric
			];
			
			// Level model
			levelModel.init();
			
			// Player Service
			playerService.init(levelModel.levels);
				// Unlock first level
				unlockLevelSignal.dispatch(LevelNames.KINGDOM_1_LEVEL_1);
			
			
			// Data models
			storyDataModel.init();
			
			// Start Tutorial
			tutorialModel.start();
			
			// Sound Model
			soundModel.init();
			
			// Assets init
			// Creating binary from previously embed screen and Ui assets JSON data
			//88888888888888888888888888888888888888888888888888888888888888888888888888888
			var obj:Object = Assets.assets.getObject("screensData");
			Assets.ScreenObj = obj;
			
			Assets.UiObj = Assets.assets.getObject("uiElementsData");
			//88888888888888888888888888888888888888888888888888888888888888888888888888888
			
			
			// Check to see if the default set of inventory items have been added, this is were we can add inventory items for the next releases
			var defaultInventoryList:Vector.<InventoryItemVO> = new Vector.<InventoryItemVO>;
			defaultInventoryList.push(new InventoryItemVO(GameCollectableTypes.TYPE_VIAL_SMALL,GameCollectableTypes.COST_VIAL_SML));
			defaultInventoryList.push(new InventoryItemVO(GameCollectableTypes.TYPE_VIAL_MED,GameCollectableTypes.COST_VIAL_MED));
			defaultInventoryList.push(new InventoryItemVO(GameCollectableTypes.TYPE_VIAL_LARGE,GameCollectableTypes.COST_VIAL_LARGE));
			defaultInventoryList.push(new InventoryItemVO(GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH,GameCollectableTypes.COST_ORA_GAUGE));
			
			for (var i:int = 0; i < defaultInventoryList.length; i++) 
			{
				var existingItem:Object = playerService.inventoryGetItem(defaultInventoryList[i].id);
				if(existingItem == null)
				{
					playerService.inventoryAdd(defaultInventoryList[i]);
				}
			}
			
			// Init Gauge Model
			gaugeModel.init(playerService.player.gaugeLevel,playerService.player.jumpLevel,playerService.player.swimLevel);
			
			// Setup Factories
			Factory.imagePool = new ImagePool(PoolImage,1000,Assets.assets.getTexture("empty"));
			Factory.movieclipPool = new MovieclipPool(PoolMovieclip,50,Vector.<Texture>([Assets.assets.getTexture("empty")]));
			
			
			// Theme
			var base:Sprite = Starling.current.root as Sprite;
			_theme = new ApplicationTheme(Starling.current.stage);
			
			// UI and Screen elements
			base.addChild(new HudScreen());
			base.addChild(new LevelResultScreen());
			base.addChild(new InventoryScreen());
			base.addChild(new OptionsScreen());
			base.addChild(new GaugeUpgradeScreen());
			base.addChild(new ShortMessagePopup());
			base.addChild(new ShopScreen());
			base.addChild(new ItemInfoPopup());
			base.addChild(new ConfirmPopup());
			base.addChild(new MessagePopup());
			base.addChild(new StoryScreenMain());
			
			
			// Remove background image
			if(SnailboyHydroCore.background)
			{
				SnailboyHydroCore.background.bitmapData.dispose();
				SnailboyHydroCore.background.parent.removeChild(SnailboyHydroCore.background);
				SnailboyHydroCore.background = null;
			}
			
			// Load StartingLevel
			loadLevelSignal.dispatch(LevelNames.WELCOME_LEVEL);
//			Starling.juggler.delayCall(loadLevelSignal.dispatch, 1, LevelNames.KINGDOM_1_LEVEL_2);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		
	}
}