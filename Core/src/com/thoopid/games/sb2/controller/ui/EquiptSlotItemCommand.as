package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryUpdatedSignal;
	import com.thoopid.games.sb2.controller.signals.ui.SlotsUpdateSignal;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class EquiptSlotItemCommand extends SignalCommand
	{
		[Inject]
		public var id:String;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var slotsUpdateSignal:SlotsUpdateSignal;
		[Inject]
		public var inventoryUpdatedSignal:InventoryUpdatedSignal;
		[Inject]
		public var triggerTutSignal:TriggerTutorialSignal;
		
		// Model
		[Inject]
		public var tutorialModel:TutorialModel;
		
		public function EquiptSlotItemCommand()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function execute():void
		{
			var slots:Vector.<InventoryItemVO> = playerService.slotsGetAll();
			
			var added:Boolean = false;
			// try and add to a current slot already containing this type
			for (var i:int = 0; i < slots.length; i++) 
			{
				if(slots[i].id == id)
				{
					add();
					added = true;
				}
			}
			
			if(!added) 
			{
				if(playerService.slotsAvailable())
				{
					add();
				}
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function add():void
		{
			playerService.slotsAdd(new InventoryItemVO(id,0,1));
			playerService.inventoryRemove(id);
			
			
			slotsUpdateSignal.dispatch();
			inventoryUpdatedSignal.dispatch();
		}
	}
}