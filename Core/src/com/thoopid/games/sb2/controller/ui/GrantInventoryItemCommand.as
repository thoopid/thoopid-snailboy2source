package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.ui.EquiptSlotItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryUpdatedSignal;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class GrantInventoryItemCommand extends SignalCommand
	{
		[Inject]
		public var item:InventoryItemVO;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var equptSignal:EquiptSlotItemSignal;
		[Inject]
		public var inventoryUpdatedSignal:InventoryUpdatedSignal;
		[Inject]
		public var triggerTutorialSignal:TriggerTutorialSignal;
		
		public function GrantInventoryItemCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			playerService.inventoryAdd(item);
			equptSignal.dispatch(item.id);
			inventoryUpdatedSignal.dispatch();
		}
	}
}