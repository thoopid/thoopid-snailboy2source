package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.controller.signals.ui.KingdomAreaUpdateSignal;
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class NavigateKingdomAreaCommand extends SignalCommand
	{
		[Inject]
		public var direction:Number = 1;
		
		[Inject]
		public var levelModel:LevelModel;
		[Inject]
		public var playerService:PlayerService;
		
		[Inject]
		public var kingdomAreaUpdateSignal:KingdomAreaUpdateSignal;
		
		public function NavigateKingdomAreaCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			var activeAreaList:Array = playerService.player.activeArea.split("_");
			var newIndex:Number = Number(activeAreaList[1]) + direction;
			if(newIndex > 5) newIndex = 1;
			if(newIndex < 1) newIndex = 5;
			activeAreaList[1] = ""+newIndex;
			var newAreaId:String =activeAreaList.join("_");
			
			playerService.player.activeArea = newAreaId;
			
			kingdomAreaUpdateSignal.dispatch();
			
//			var currentLevel:LevelVO = levelModel.getLevelByAreaId(playerService.player.activeArea);
//			var levelIndex:int = levelModel.getLevelIndex(currentLevel.levelId);
//			levelIndex += direction;
//			var nextLevel:LevelVO = levelModel.getLevelByIndex(levelIndex);
//			
//			if(nextLevel)
//			{
//				playerService.player.activeArea = nextLevel.areaId;
//				
//				kingdomAreaUpdateSignal.dispatch();
//			}
		}
	}
}