package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.constants.ui.Messages;
	import com.thoopid.games.sb2.controller.signals.ui.MessagePopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ShopShowSignal;
	import com.thoopid.games.sb2.model.ui.MessagePopupVO;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class NotEnoughCoinCommand extends SignalCommand
	{
		[Inject]
		public var messegePopupRequestSignal:MessagePopupRequestSignal;
		[Inject]
		public var shopShowSignal:ShopShowSignal;
		
		public function NotEnoughCoinCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			var vo:MessagePopupVO = new MessagePopupVO(Messages.MSG_NOT_ENOUGH_COIN_TITLE
														,Messages.MSG_NOT_ENOUGH_COIN_SUBTITLE
														,Messages.MSG_NOT_ENOUGH_COIN_COPY,
														goToShop
			);
			messegePopupRequestSignal.dispatch(vo);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function goToShop(data:Object=null):void
		{
			shopShowSignal.dispatch(true);
		}
	}
}