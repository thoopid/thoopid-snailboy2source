package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmCallbackSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmPopupRequestSignal;
	import com.thoopid.games.sb2.model.game.ShopItemVO;
	import com.thoopid.games.sb2.model.game.ShopModel;
	import com.thoopid.games.sb2.model.ui.ConfirmPopupVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class PurchaseCoinsCommand extends SignalCommand
	{
		[Inject]
		public var id:String;
		
		[Inject]
		public var shopModel:ShopModel;
		
		[Inject]
		public var confirmCallbackSignal:ConfirmCallbackSignal;
		[Inject]
		public var confirmPopupSignal:ConfirmPopupRequestSignal;
		
		[Inject]
		public var playerService:PlayerService;
		
		private var _shopItemVo:ShopItemVO;
		
		public function PurchaseCoinsCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			_shopItemVo = shopModel.getItembyId(id);
			if(_shopItemVo)
			{
				confirmCallbackSignal.addOnce(onConfirm);
				var confirmRequest:ConfirmPopupVO = new ConfirmPopupVO("PURCHASE","COINS","This purchase will give you "+_shopItemVo.quantity+" coins.", _shopItemVo.quantity, confirmCallbackSignal);
				confirmPopupSignal.dispatch(confirmRequest);
			}
		}
		
		private function onConfirm(success:Boolean):void
		{
			if(success)
			{
				playerService.grantCoins(_shopItemVo.quantity);
			}
			
			confirmCallbackSignal = null;
			confirmPopupSignal = null;
		}
	}
}