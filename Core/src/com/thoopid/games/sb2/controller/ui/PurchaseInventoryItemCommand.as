package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmCallbackSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmPopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.EquiptSlotItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GrantInventoryItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.MessagePopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.NotEnoughCoinMesssageSignal;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	import com.thoopid.games.sb2.model.ui.ConfirmPopupVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class PurchaseInventoryItemCommand extends SignalCommand
	{
		[Inject]
		public var id:String;
		
		// Signals
		[Inject]
		public var confirmCallbackSignal:ConfirmCallbackSignal;
		[Inject]
		public var confirmPopupSignal:ConfirmPopupRequestSignal;
		[Inject]
		public var equptSignal:EquiptSlotItemSignal;
		[Inject]
		public var messegePopupRequestSignal:MessagePopupRequestSignal;
		[Inject]
		public var grantInventoryItemSignal:GrantInventoryItemSignal;
		[Inject]
		public var notEnoughCoinSignal:NotEnoughCoinMesssageSignal;
		[Inject]
		public var triggerTutSignal:TriggerTutorialSignal;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		// Model
		[Inject]
		public var tutorialModel:TutorialModel;
		
		private var _iItem:InventoryItemVO;

		private var _confirmRequest:ConfirmPopupVO;
		
		public function PurchaseInventoryItemCommand()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function execute():void
		{
			// Check if user can afford
			
			// Check if user has inventory stock - then just equipt
			
			var currentItem:InventoryItemVO = new InventoryItemVO().parse(playerService.inventoryGetItem(id));
			if(currentItem.quantity > 0)
			{
				equptSignal.dispatch(id);
				return;
			}
			
			// No stock available so Purchase:---> 
			confirmCallbackSignal.addOnce(onConfirm);
			_confirmRequest = new ConfirmPopupVO("PURCHASE","","This vial gives your gauge refil", 0, confirmCallbackSignal);
			
			_iItem = new InventoryItemVO(id);
			
			switch(id)
			{
				case GameCollectableTypes.TYPE_VIAL_SMALL:
					
					_iItem.price = GameCollectableTypes.COST_VIAL_SML;
					
					_confirmRequest.sub = "SMALL VIAL";
					_confirmRequest.cost = GameCollectableTypes.COST_VIAL_SML;
					confirmPopupSignal.dispatch(_confirmRequest);
					break;
				
				case GameCollectableTypes.TYPE_VIAL_MED:
					
					_iItem.price = GameCollectableTypes.COST_VIAL_MED;
					
					_confirmRequest.sub = "MEDIUM VIAL";
					_confirmRequest.cost = GameCollectableTypes.COST_VIAL_MED;
					confirmPopupSignal.dispatch(_confirmRequest);
					break;
				
				case GameCollectableTypes.TYPE_VIAL_LARGE:
					_iItem.price = GameCollectableTypes.COST_VIAL_LARGE;
					
					_confirmRequest.sub = "LARGE VIAL";
					_confirmRequest.cost = GameCollectableTypes.COST_VIAL_LARGE;
					confirmPopupSignal.dispatch(_confirmRequest);
					break;
				
				case GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH:
					_iItem.price = GameCollectableTypes.COST_ORA_GAUGE;
					
					_confirmRequest.message = "This item freezes your gauge depletion and allows you to super jump and swim forever. Lasts for 1 entire level.";
					_confirmRequest.sub = "FREEZE GAUGE";
					_confirmRequest.cost = GameCollectableTypes.COST_ORA_GAUGE;
					confirmPopupSignal.dispatch(_confirmRequest);
					break;
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onConfirm(success:Boolean):void
		{
			if(success)
			{
				var cost:Number = _confirmRequest.cost;
				if(cost > playerService.player.coins)
				{
					notEnoughCoinSignal.dispatch();
					
				} else
				{
					grantInventoryItemSignal.dispatch(_iItem);
					playerService.deductCoins(_confirmRequest.cost);
					
					// Also initiate tutorials steps
					if(tutorialModel.enabled)
					{
						triggerTutSignal.dispatch(TutorialConstants.TUT_STEP_INVENTORY_CLOSE);
					}
				}
			}
			
			confirmCallbackSignal = null;
			confirmPopupSignal = null;
		}
	}
}