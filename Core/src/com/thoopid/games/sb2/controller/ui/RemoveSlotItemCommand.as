package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.controller.signals.ui.InventoryUpdatedSignal;
	import com.thoopid.games.sb2.controller.signals.ui.SlotsUpdateSignal;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class RemoveSlotItemCommand extends SignalCommand
	{
		[Inject]
		public var type:String;
		[Inject]
		public var mustAddBackToInventory:Boolean;
		
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var slotsUpdateSignal:SlotsUpdateSignal;
		[Inject]
		public var inventoryUpdatedSignal:InventoryUpdatedSignal;
		
		public function RemoveSlotItemCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			playerService.slotsRemove(type);
			if(mustAddBackToInventory) playerService.inventoryAdd(new InventoryItemVO(type,0,0));
			
			slotsUpdateSignal.dispatch();
			if(mustAddBackToInventory) inventoryUpdatedSignal.dispatch();
		}
	}
}