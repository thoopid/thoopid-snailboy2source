package com.thoopid.games.sb2.controller.ui
{
	import com.thoopid.games.sb2.constants.game.PlayerElements;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmCallbackSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmPopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.MessagePopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.NotEnoughCoinMesssageSignal;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.ui.ConfirmPopupVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class UpgradeGaugeRequestCommand extends SignalCommand
	{
		[Inject]
		public var type:String;
		
		// Signals
		[Inject]
		public var confirmCallbackSignal:ConfirmCallbackSignal;
		[Inject]
		public var confirmPopupSignal:ConfirmPopupRequestSignal;
		[Inject]
		public var messegePopupRequestSignal:MessagePopupRequestSignal;
		[Inject]
		public var notEnoughCoinSignal:NotEnoughCoinMesssageSignal;
		
		// Models
		[Inject]
		public var gaugeModel:GaugeModel;
		// Services
		[Inject]
		public var playerService:PlayerService;

		private var _confirmRequest:ConfirmPopupVO;
		
		public function UpgradeGaugeRequestCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			var cost:Number = gaugeModel.getNextUpgradeCost(type);
			confirmCallbackSignal.addOnce(onConfirm);
			_confirmRequest = new ConfirmPopupVO("UPGRADE","","Upgrade the following gauge item message her. This is the description of what the upgrade will give you as a user.", cost, confirmCallbackSignal);
			
			switch(type)
			{
				case PlayerElements.GAUGE_GAUGE:
					_confirmRequest.sub = "SIZE";
					confirmPopupSignal.dispatch(_confirmRequest);
					break;
				case PlayerElements.GAUGE_SWIM:
					_confirmRequest.sub = "SWIM";
					confirmPopupSignal.dispatch(_confirmRequest);
					break;
				case PlayerElements.GAUGE_JUMP:
					_confirmRequest.sub = "JUMP";
					confirmPopupSignal.dispatch(_confirmRequest);
					break;
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onConfirm(success:Boolean):void
		{
			if(success)
			{
				if(_confirmRequest.cost > playerService.player.coins)
				{
					notEnoughCoinSignal.dispatch();
					
				} else
				{
					if(type == PlayerElements.GAUGE_GAUGE)
					{
						var gaugeLevel:int = gaugeModel.upgradeGauge();
						playerService.player.gaugeLevel = gaugeLevel;
						
						// Deduct money
						playerService.deductCoins(gaugeModel.currentGaugeVo.cost);
						
					} else if(type == PlayerElements.GAUGE_SWIM)
					{
						var swimLevel:int = gaugeModel.upgradeSwim();
						playerService.player.swimLevel = swimLevel;
						
						// Deduct money
						playerService.deductCoins(gaugeModel.currentSwimVo.cost);
					} else if(type == PlayerElements.GAUGE_JUMP)
					{
						var jumpLevel:int = gaugeModel.upgradeJump();
						playerService.player.jumpLevel = jumpLevel;
						
						// Deduct money
						playerService.deductCoins(gaugeModel.currentJumpVo.cost);
					}
				}
				
			}
		}
	}
}