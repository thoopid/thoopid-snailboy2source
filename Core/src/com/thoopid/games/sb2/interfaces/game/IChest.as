package com.thoopid.games.sb2.interfaces.game
{
	import org.osflash.signals.Signal;

	public interface IChest
	{
		function get onOpenSignal():Signal
		function get x():Number
		function get y():Number
		function open():void
	}
}