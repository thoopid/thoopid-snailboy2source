package com.thoopid.games.sb2.interfaces.game
{
	import nape.phys.Body;

	public interface IEnableable
	{
		function get x():Number
		function get y():Number
		function get isRendered():Boolean
		function set isRendered(v:Boolean):void
		function get body():Body
		function reset():void
	}
}