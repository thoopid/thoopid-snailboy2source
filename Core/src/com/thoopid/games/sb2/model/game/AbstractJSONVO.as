package com.thoopid.games.sb2.model.game
{
	import com.gamua.flox.utils.describeType;
	

	public class AbstractJSONVO extends Object
	{
		public function AbstractJSONVO()
		{
			
		}
		
		public function parse(o:Object):*
		{
//			for (var i:Object in o)
//			{
////				try
////				{
//					if(this[i] is AbstractJSONVO)
//					{
//						(this[i] as AbstractJSONVO).parse(o[i]);
//					} else
//					{
//						this[i] = o[i];
//					}
////				} catch(e:Error) {
////					trace("Error parsing VO item: "+o[i] + " : " + i);
////				}
//			}
//			return this;
			
			var typeDescription:XML = describeType(this);
			for each (var variable:XML in typeDescription.variable)
			{
				var propertyName:String = variable.@name.toString();
				var access:String = variable.@access.toString(); 
				var nonSerializedMetaData:XMLList = variable.metadata.(@name == "NonSerialized");
				
				if (access != "writeonly" && nonSerializedMetaData.length() == 0)
				{
					if(o[propertyName] != null) this[propertyName] = o[propertyName];
				}
			}
			
			return this;
		}
	}
}