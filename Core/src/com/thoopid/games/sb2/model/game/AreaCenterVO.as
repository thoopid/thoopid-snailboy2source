package com.thoopid.games.sb2.model.game
{
	public class AreaCenterVO extends Object
	{
		public var areaId:String;
		public var x:Number;
		public var y:Number;
		public function AreaCenterVO(id:String,x:Number,y:Number)
		{
			super();
			this.areaId = id;
			this.x = x;
			this.y = y;
		}
	}
}