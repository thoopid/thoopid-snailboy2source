package com.thoopid.games.sb2.model.game
{
	public class ConsumableItemVO extends AbstractJSONVO
	{
		public var typeId:String;
		
		public function ConsumableItemVO(typeId:String)
		{
			this.typeId = typeId;
		}
	}
}