package com.thoopid.games.sb2.model.game
{
	import com.thoopid.games.sb2.controller.signals.game.GameModelUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.game.TimeUpdateSignal;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.robotlegs.mvcs.Actor;
	
	public class GameModel extends Actor
	{
		[Inject]
		public var timeUpdateSignal:TimeUpdateSignal;
		
		[Inject]
		public var gameModelUpdateSignal:GameModelUpdateSignal;

		// Vars
		public var currentLevel:LevelVO;
		
		public var revealAreaItemCollected:Boolean = false;
		public var revealKingdomItemCollected:Boolean = false;
		
		// Timer stuff
		private var _gameTimer:Timer;
		private var _takenTime:Number;
		private var _gamePaused:Boolean = true;
		
		public function GameModel()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get takenTime():Number
		{
			return _takenTime;
		}
		
		public function get gamePaused():Boolean
		{
			return _gamePaused;
		}
		
		public function get stars():Number
		{
			var stars:Number = 0;
			return stars;
		}

		public function newGame(levelVO:LevelVO):void
		{
			if(!_gameTimer)
			{
				_gameTimer = new Timer(100);
				_gameTimer.addEventListener(TimerEvent.TIMER, onGameTimer, false, 0, true);
			}
			
			currentLevel = levelVO;
		}
		
		public function startGame():void
		{
			resetScores();
			
			_gameTimer.stop();
			_gameTimer.reset();
			_gameTimer.start();
			
			_gamePaused = false;
		}
		
		public function pauseGame():void
		{
			_gamePaused = true;
			_gameTimer.stop();
		}
		
		public function resumeGame():void
		{
			_gamePaused = false;
			_gameTimer.start();
		}
		
		public function stopGame():void
		{
			_gamePaused = false;
			_gameTimer.stop();
			resetScores();
		}
		
		public function clearGame():void
		{
			currentLevel = null;
		}
		
		public function collectSlimy():void
		{
			currentLevel.slimiesCollected++;
			gameModelUpdateSignal.dispatch();
		}
		
		public function addCoins(c:Number):void
		{
			currentLevel.coinsCount += c;
			gameModelUpdateSignal.dispatch();
		}
		
		public function addScore(s:Number):void
		{
			currentLevel.score += s;
			gameModelUpdateSignal.dispatch();
		}
		
		public function collectStoryItem():void
		{
			currentLevel.storyItemCollected = true;
		}
		
		public function die():void
		{
			currentLevel.deaths++;
		}
		
		public function finishLevel():void
		{
			pauseGame();
			// Flox Loging
//			AnalitycsService.getInstance().trackEvent(LoggingConstants.CAT_LEVEL,LoggingConstants.ACTION_COMPLETE,currentLevel.levelId.toString());
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function resetScores():void
		{
			_takenTime = 0;
			
			currentLevel.score = 0;
			currentLevel.slimiesCollected = 0;
			currentLevel.deaths = 0;
			currentLevel.coinsCount = 0;
			
			gameModelUpdateSignal.dispatch();
		}
		
		private function onGameTimer(event:TimerEvent):void
		{
			_takenTime++;
			timeUpdateSignal.dispatch(_takenTime);
		}
		
		private function onSystemPlayingHandler(playing:Boolean):void
		{
			
		}
	}
}