package com.thoopid.games.sb2.model.game
{
	public class GaugeLevelVO extends AbstractJSONVO
	{
		public var level:int = 0;
		public var capacity:Number = 200;
		public var volume:Number = 200;
		public var superSimPercentage:Number = 0;
		
		public var isFrozen:Boolean = false;
		
		public var cost:Number = 100;
		
		public function GaugeLevelVO()
		{
			
		}
	}
}