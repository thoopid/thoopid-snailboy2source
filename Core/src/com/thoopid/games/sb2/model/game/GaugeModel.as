package com.thoopid.games.sb2.model.game
{
	import com.thoopid.games.sb2.constants.game.PlayerElements;
	import com.thoopid.games.sb2.util.MathTrigUtil;
	
	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;

	public class GaugeModel extends Actor
	{
		public var updateModelSignal:Signal = new Signal(GaugeVO, Boolean);
		
		public var gauge:GaugeVO;
		
		public var gaugeRegenVal:Number = 0;
		
		// Current level of each section
		private var _gaugeLevel:int = 1;
		private var _swimLevel:int = 1;
		private var _jumpLevel:int = 1;
		
		// each individual upgradeable section levels
		private var _gaugeLevelsList:Vector.<GaugeLevelVO>;
		private var _swimLevelsList:Vector.<SwimLevelVO>;
		private var _jumpLevelsList:Vector.<JumpLevelVO>;
		
		public function GaugeModel()
		{
			// Setup gauge levels
			_gaugeLevelsList = new Vector.<GaugeLevelVO>();
//			_gaugeLevelsList.push(new GaugeLevelVO().parse({capacity:150, volume:150, level:0}));
			_gaugeLevelsList.push(new GaugeLevelVO().parse({capacity:200, volume:200, level:1, cost:100}));
			_gaugeLevelsList.push(new GaugeLevelVO().parse({capacity:300, volume:300, level:2, cost:500}));
			_gaugeLevelsList.push(new GaugeLevelVO().parse({capacity:400, volume:400, level:3, cost:1000}));
			_gaugeLevelsList.push(new GaugeLevelVO().parse({capacity:500, volume:500, level:4, cost:2000}));
			_gaugeLevelsList.push(new GaugeLevelVO().parse({capacity:600, volume:600, level:5, cost:5000}));
			
			// Jump
			_jumpLevelsList = new Vector.<JumpLevelVO>();
//			_jumpLevelsList.push(new JumpLevelVO().parse({powerJump:2.5, powerSuperJump:0, costSuperJump:75, level:0}));
			_jumpLevelsList.push(new JumpLevelVO().parse({powerJump:2.6, powerSuperJump:0, costSuperJump:100, level:1, cost:100}));
			_jumpLevelsList.push(new JumpLevelVO().parse({powerJump:2.7, powerSuperJump:0.116, costSuperJump:125, level:2, cost:500}));
			_jumpLevelsList.push(new JumpLevelVO().parse({powerJump:2.8, powerSuperJump:0.223, costSuperJump:150, level:3, cost:1000}));
			_jumpLevelsList.push(new JumpLevelVO().parse({powerJump:2.9, powerSuperJump:0.323, costSuperJump:175, level:4, cost:2000}));
			_jumpLevelsList.push(new JumpLevelVO().parse({powerJump:3.0, powerSuperJump:0.416, costSuperJump:200, level:5, cost:5000}));
			
			_swimLevelsList = new Vector.<SwimLevelVO>();
//			_swimLevelsList.push(new SwimLevelVO().parse({speedSwim:0.8, cost:0.0001, level:0}));
			_swimLevelsList.push(new SwimLevelVO().parse({speedSwim:1.5, costSwim:1, level:1, cost:100}));
			_swimLevelsList.push(new SwimLevelVO().parse({speedSwim:1.75, costSwim:1.3, level:2, cost:500}));
			_swimLevelsList.push(new SwimLevelVO().parse({speedSwim:2.0, costSwim:1.7, level:3, cost:1000}));
			_swimLevelsList.push(new SwimLevelVO().parse({speedSwim:2.25, costSwim:2, level:4, cost:2000}));
			_swimLevelsList.push(new SwimLevelVO().parse({speedSwim:2.5, costSwim:2.1, level:5, cost:5000}));
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		private var _updateStepMax:int = 30;
		private var _updateStep:int = 0;
		public function update():void
		{
			_updateStep++;
			if(_updateStep >= _updateStepMax)
			{
				_updateStep = 0;
				if(gaugeRegenVal > 0) replenish(gaugeRegenVal);
			}
		}
		
		public function init(gaugeLevel:int=0,jumpLevel:int=0,swimLevel:int=0):void
		{
			_gaugeLevel = gaugeLevel;
			_jumpLevel = jumpLevel;
			_swimLevel = swimLevel;
			
			gauge = new GaugeVO();
			gauge.gauge = _gaugeLevelsList[_gaugeLevel-1];
			gauge.jump = _jumpLevelsList[_jumpLevel-1];
			gauge.swim = _swimLevelsList[_swimLevel-1];
		}
		
		public function upgradeGauge(isDownGrade:Boolean=false):int
		{
			_gaugeLevel = MathTrigUtil.limit(_gaugeLevel+((isDownGrade) ? -1 : 1), 0, _gaugeLevelsList.length);
			gauge.gauge = _gaugeLevelsList[_gaugeLevel-1];
			reportUpdate();
			return _gaugeLevel;
		}
		public function upgradeJump(isDownGrade:Boolean=false):int
		{
			_jumpLevel = MathTrigUtil.limit(_jumpLevel+((isDownGrade) ? -1 : 1), 0, _jumpLevelsList.length);
			gauge.jump = _jumpLevelsList[_jumpLevel-1];
			reportUpdate();
			return _jumpLevel;
		}
		public function upgradeSwim(isDownGrade:Boolean=false):int
		{
			_swimLevel = MathTrigUtil.limit(_swimLevel+((isDownGrade) ? -1 : 1), 0, _swimLevelsList.length);
			gauge.swim = _swimLevelsList[_swimLevel-1];
			reportUpdate();
			return _swimLevel;
		}
		
		public function get currentGaugeVo():GaugeLevelVO
		{
			return _gaugeLevelsList[_gaugeLevel-1];
		}
		public function get currentJumpVo():JumpLevelVO
		{
			return _jumpLevelsList[_jumpLevel-1];
		}
		public function get currentSwimVo():SwimLevelVO
		{
			return _swimLevelsList[_swimLevel-1];
		}
		
		public function getNextUpgradeCost(type:String):Number
		{
			var cost:Number = 10000000;
			var index:int = 0;
			switch(type)
			{
				case PlayerElements.GAUGE_GAUGE:
					index = MathTrigUtil.limit(_gaugeLevel+1, 0, _gaugeLevelsList.length);
					cost = _gaugeLevelsList[index-1].cost;
					break;
				case PlayerElements.GAUGE_JUMP:
					index = MathTrigUtil.limit(_jumpLevel+1, 0, _jumpLevelsList.length);
					cost = _jumpLevelsList[index-1].cost;
					
					break;
				case PlayerElements.GAUGE_SWIM:
					index = MathTrigUtil.limit(_swimLevel+1, 0, _swimLevelsList.length);
					cost = _swimLevelsList[index-1].cost;
					
					break;
			}
			
			return cost;
		}
		
		public function replenish(val:Number):void
		{
			if(gauge.gauge.isFrozen) return;
				
			gauge.gauge.volume += val;
			if(gauge.gauge.volume >= gauge.gauge.capacity)
			{
				gauge.gauge.volume = gauge.gauge.capacity;
			}
			reportUpdate(true);
		}
		
		public function toggleGaugeFreeze(toggle:Boolean):void
		{
			gauge.gauge.isFrozen = toggle;
		}
		
		public function toggleGaugeRegen(val:Number):void
		{
			gaugeRegenVal = val;
		}
		
		public function resetGauge():void
		{
			gauge.gauge.volume = gauge.gauge.capacity;
			reportUpdate();
		}
		
		//*********************
		//   IN GAME FUNCTIONS
		//*********************
		
		public function depGauge(val:Number):void
		{
			if(gauge.gauge.isFrozen) return;
			
			gauge.gauge.volume-= val;
			if(gauge.gauge.volume <= 0)
			{
				gauge.gauge.volume = 0;
			}
			reportUpdate();
		}
		
		public function simulateSuper(percentage:Number):void
		{
			gauge.gauge.superSimPercentage = percentage;
			reportUpdate();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function reportUpdate(replenish:Boolean = false):void
		{
			updateModelSignal.dispatch(gauge, replenish);
		}
	}
}