package com.thoopid.games.sb2.model.game
{
	public class InventoryItemVO extends AbstractJSONVO
	{
		public var id:String;
		public var price:Number;
		public var quantity:Number;
		
		public function InventoryItemVO(id:String="", price:Number=0, quantity:Number=0)
		{
			super();
			
			this.id = id;
			this.price = price;
			this.quantity = quantity;
		}
	}
}