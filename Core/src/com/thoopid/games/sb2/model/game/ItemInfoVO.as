package com.thoopid.games.sb2.model.game
{
	public class ItemInfoVO extends AbstractJSONVO
	{
		public var id:String;
		public var title:String;
		public var name:String;
		public var info:String;
		
		public function ItemInfoVO()
		{
			super();
		}
	}
}