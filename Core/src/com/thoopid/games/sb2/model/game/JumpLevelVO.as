package com.thoopid.games.sb2.model.game
{
	public class JumpLevelVO extends AbstractJSONVO
	{
		public var level:int = 0;	
		public var powerJump:Number = 2.8;	
		public var powerSuperJump:Number = 2.8;	
		public var costSuperJump:Number = 100;
		
		public var cost:Number = 100;
		
		public function JumpLevelVO()
		{
		}
	}
}