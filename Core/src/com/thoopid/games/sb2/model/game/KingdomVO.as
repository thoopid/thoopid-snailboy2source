package com.thoopid.games.sb2.model.game
{
	public class KingdomVO extends AbstractJSONVO
	{
		public var kingdomId:String;
		public var name:String;
		public var locked:Boolean = true;
		
		public function KingdomVO()
		{
			super();
		}
	}
}