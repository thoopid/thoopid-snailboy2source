package com.thoopid.games.sb2.model.game
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.level.AchievementsLevel;
	import com.thoopid.games.sb2.view.game.level.IntroLevel;
	import com.thoopid.games.sb2.view.game.level.K_1_0_0;
	import com.thoopid.games.sb2.view.game.level.K_1_1_1;
	import com.thoopid.games.sb2.view.game.level.K_1_1_10;
	import com.thoopid.games.sb2.view.game.level.K_1_1_11;
	import com.thoopid.games.sb2.view.game.level.K_1_1_12;
	import com.thoopid.games.sb2.view.game.level.K_1_1_13;
	import com.thoopid.games.sb2.view.game.level.K_1_1_14;
	import com.thoopid.games.sb2.view.game.level.K_1_1_15;
	import com.thoopid.games.sb2.view.game.level.K_1_1_16;
	import com.thoopid.games.sb2.view.game.level.K_1_1_17;
	import com.thoopid.games.sb2.view.game.level.K_1_1_18;
	import com.thoopid.games.sb2.view.game.level.K_1_1_19;
	import com.thoopid.games.sb2.view.game.level.K_1_1_2;
	import com.thoopid.games.sb2.view.game.level.K_1_1_20;
	import com.thoopid.games.sb2.view.game.level.K_1_1_21;
	import com.thoopid.games.sb2.view.game.level.K_1_1_22;
	import com.thoopid.games.sb2.view.game.level.K_1_1_23;
	import com.thoopid.games.sb2.view.game.level.K_1_1_24;
	import com.thoopid.games.sb2.view.game.level.K_1_1_25;
	import com.thoopid.games.sb2.view.game.level.K_1_1_3;
	import com.thoopid.games.sb2.view.game.level.K_1_1_4;
	import com.thoopid.games.sb2.view.game.level.K_1_1_5;
	import com.thoopid.games.sb2.view.game.level.K_1_1_6;
	import com.thoopid.games.sb2.view.game.level.K_1_1_7;
	import com.thoopid.games.sb2.view.game.level.K_1_1_8;
	import com.thoopid.games.sb2.view.game.level.K_1_1_9;
	import com.thoopid.games.sb2.view.game.level.KingdomSelectionLevel;
	import com.thoopid.games.sb2.view.game.level.MenuLevel;
	import com.thoopid.games.sb2.view.game.level.TreasureLevel;
	import com.thoopid.games.sb2.view.game.level.WelcomeLevel;
	import com.thoopid.games.sb2.view.game.level.base.ALevel;
	
	import flash.display.MovieClip;
	import flash.system.ApplicationDomain;
	import flash.utils.Timer;
	
	import citrus.utils.LevelManager;
	
	import org.robotlegs.mvcs.Actor;
	
	public class LevelModel extends Actor
	{
//		[Inject]
//		public var bonusLevelRespawnTimerSignal:BonusLevelRespawnTimerSignal;
		
//		[Inject]
//		public var bonusLevelUnlockedSignal:BonusLevelUnlockedSignal;
		
//		[Inject]
//		public var playerService:PlayerService;
		
		// Game level details
		public var levels:Vector.<LevelVO>;
		
		// Kingdom Setups
		public var kingdoms:Vector.<KingdomVO>;
		
		// Level selector props
		private var _levelManager:LevelManager;
		private var _bonusLevelRespawnTimer:Timer;
		private var _bonusLevelRespawnTime:Number;
		
		public function LevelModel()
		{
			super();
			
		}
		
		//*********************
		//   PUBLIC
		//*********************

		public function get levelManager():LevelManager
		{
			return _levelManager;
		}

		public function init():void
		{
			_levelManager = new LevelManager(ALevel);
			_levelManager.applicationDomain = ApplicationDomain.currentDomain; // to be able to load your SWF level on iOS
			_levelManager.checkPolicyFile = false;
			_levelManager.onLevelChanged.add(onLevelChanged);
			
			levels = new Vector.<LevelVO>();
			
			// Kingdom 1 - Area 1
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_1, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_1, K_1_1_1, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_2, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_1, K_1_1_2, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_3, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_1, K_1_1_3, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_4, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_1, K_1_1_4, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_5, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_1, K_1_1_5, MovieClip));
			// Kingdom 1 - Area 2
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_6, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_2, K_1_1_6, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_7, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_2, K_1_1_7, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_8, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_2, K_1_1_8, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_9, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_2, K_1_1_9, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_10, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_2, K_1_1_10, MovieClip));
			// Kingdom 1 - Area 3
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_11, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_3, K_1_1_11, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_12, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_3, K_1_1_12, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_13, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_3, K_1_1_13, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_14, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_3, K_1_1_14, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_15, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_3, K_1_1_15, MovieClip));
			// Kingdom 1 - Area 4
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_16, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_16, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_17, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_17, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_18, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_18, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_19, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_19, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_20, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_20, MovieClip));
			// Kingdom 1 - Area 5
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_21, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_21, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_22, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_22, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_23, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_23, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_24, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_24, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_1_LEVEL_25, LevelNames.KINGDOM_1, LevelNames.KINGDOM_1_AREA_4, K_1_1_25, MovieClip));
			
			// Level User Interfaces
			levels.push(new LevelVO(LevelNames.KINGDOM_1, LevelNames.KINGDOM_1, "", K_1_0_0, MovieClip));
			
			// UI LEVELS
			levels.push(new LevelVO(LevelNames.WELCOME_LEVEL, "", "", WelcomeLevel, MovieClip));
			levels.push(new LevelVO(LevelNames.INTRO_LEVEL, "", "", IntroLevel, MovieClip));
			levels.push(new LevelVO(LevelNames.ACHIEVEMENTS_LEVEL, "", "", AchievementsLevel, MovieClip));
			levels.push(new LevelVO(LevelNames.KINGDOM_SELECT_LEVEL, "", "", KingdomSelectionLevel, MovieClip));
			levels.push(new LevelVO(LevelNames.MENU_LEVEL, "", "", MenuLevel, MovieClip));
			levels.push(new LevelVO(LevelNames.TREASURE_LEVEL, "", "", TreasureLevel, MovieClip));
			
			// Setup Kingdoms
			kingdoms = new Vector.<KingdomVO>();
			
			kingdoms.push(new KingdomVO().parse({kingdomId:LevelNames.KINGDOM_1, name:LevelNames.KINGDOM_1_NAME}));
			kingdoms.push(new KingdomVO().parse({kingdomId:LevelNames.KINGDOM_2, name:LevelNames.KINGDOM_2_NAME}));
			kingdoms.push(new KingdomVO().parse({kingdomId:LevelNames.KINGDOM_3, name:LevelNames.KINGDOM_3_NAME}));
			
			_levelManager.levels = levelsForLevelManager;
		}
		
		public function getLevel(levelId:String):LevelVO
		{
			var i:int = levels.length;
			while(i--)
			{
				if(levels[i].levelId == levelId)
				{
					return levels[i];
				}
			}
			return null;
		}
		
		public function getLevelByIndex(index:int):LevelVO
		{
			if(levels[index]) 
				return levels[index];
			else
				return null;
		}
		
		public function getLevelByAreaId(areaId:String):LevelVO
		{
			var i:int = levels.length;
			while(i--)
			{
				if(levels[i].areaId == areaId)
				{
					return levels[i];
				}
			}
			return null;
		}
		
		public function getLevelIndex(id:String):int
		{
			var i:int = levels.length;
			while(i--)
			{
				if(levels[i].levelId == id)
				{
					return i+1;
				}
			}
			return 0;
		}
		
		public function get levelsForLevelManager():Array
		{
			// Push Game Levels and UI Levels
			var a:Array = [];
			for (var i:int = 0; i < levels.length; i++) 
			{
				a.push([levels[i].levelClass, levels[i].levelPath]);
			}
			return a;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onLevelChanged(lvl:ALevel):void
		{
			SnailboyHydroCore.getInstance().setState(lvl);
		}
	}
}