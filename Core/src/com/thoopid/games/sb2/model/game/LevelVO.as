package com.thoopid.games.sb2.model.game
{
	public class LevelVO extends AbstractJSONVO
	{
		// Score
		public var levelId:String;
		
		public var levelIndex:int = 0;
		
		public var kingdomId:String;
		
		public var areaId:String;
		
		public var locked:Boolean = true;
		
		// Level Achievements/Badges
			public var storyItemCollected:Boolean = false;
			
			public var deathBadgeCollected:Boolean = false;
			
			public var slimeBadgeCollected:Boolean = false;
			
			public var secretAreaBadgeCollected:Boolean = false;
			
			public var jumpBadgeCollected:Boolean = false;
		
		// Must reset on New/retry level
		//-------------------------------------------------
		public var slimiesCollected:Number = 0;
		public var deaths:Number = 0;
		public var score:Number = 0;
		public var maxSlimies:Number = 0;
		public var coinsCount:Number = 0;
		//-------------------------------------------------
		
		// NOT FOR SAVING TO THE USE CLASS
		//--------------------------------------------------------
		private var _levelClass:Class;
		
		private var _levelPath:Object;
		//--------------------------------------------------------
		
		public function LevelVO(levelId:String="", kingdomId:String="", areaId:String="", levelClass:Class=null, levelPath:Object=null)
		{
			this.levelId = levelId;
			this.kingdomId = kingdomId;
			this.areaId = areaId;
			this._levelClass = levelClass;
			this._levelPath = levelPath;
		}
		
		public function get isUiLevel():Boolean
		{
			return this.kingdomId == "";
		}

		[NonSerialized]
		public function get levelPath():Object
		{
			return _levelPath;
		}

		[NonSerialized]
		public function get levelClass():Class
		{
			return _levelClass;
		}

	}
}