package com.thoopid.games.sb2.model.game
{
	public class ShopItemVO extends AbstractJSONVO
	{
		public var id:String = "";
		public var image:String = "";
		public var name:String = "";
		public var cost:String = "";
		public var quantity:Number = 0;
		
		public function ShopItemVO()
		{
			super();
		}
	}
}