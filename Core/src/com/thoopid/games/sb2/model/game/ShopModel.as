package com.thoopid.games.sb2.model.game
{
	import org.robotlegs.mvcs.Actor;
	
	public class ShopModel extends Actor
	{
		public var shopItems:Vector.<ShopItemVO> = new Vector.<ShopItemVO>();
		public function ShopModel()
		{
			super();
			
			shopItems.push(new ShopItemVO().parse({id:"1", image:"ui_shop_tier1", name:"SPARE CHANGE", cost:"0.99", quantity:1000}));
			shopItems.push(new ShopItemVO().parse({id:"2", image:"ui_shop_tier2", name:"COINS FOR LUCK", cost:"1.99", quantity:3000}));
			shopItems.push(new ShopItemVO().parse({id:"3", image:"ui_shop_tier3", name:"SWAG OF THE SEA", cost:"3.99", quantity:5000}));
			shopItems.push(new ShopItemVO().parse({id:"4", image:"ui_shop_tier4", name:"HIGH ROLLER", cost:"5.99", quantity:7000}));
			shopItems.push(new ShopItemVO().parse({id:"5", image:"ui_shop_tier5", name:"HIDDEN FORTUNE", cost:"29.99", quantity:14000}));
		}
		
		public function getItembyId(id:String):ShopItemVO
		{
			var i:int = shopItems.length;
			while(i--)
			{
				if(shopItems[i].id == id) return shopItems[i];
			}
			return null;
		}
	}
}