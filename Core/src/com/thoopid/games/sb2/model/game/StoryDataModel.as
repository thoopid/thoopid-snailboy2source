package com.thoopid.games.sb2.model.game
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	
	import org.robotlegs.mvcs.Actor;
	
	public class StoryDataModel extends Actor
	{
		public var items:Vector.<ItemInfoVO> = new Vector.<ItemInfoVO>();
		public var storyLine:Vector.<StoryLineItemVO> = new Vector.<StoryLineItemVO>();
		
		public function StoryDataModel()
		{
			super();
		}
		
		public function init():void
		{
			items.push(new ItemInfoVO().parse({id:"iq.orbShard_01", title:"ORB", info:"Collect all these pieces to complete the ORB!", name:"SHARD"}));
			items.push(new ItemInfoVO().parse({id:"iq.orbShard_02", title:"ORB", info:"Collect all these pieces to complete the ORB!", name:"SHARD"}));
			items.push(new ItemInfoVO().parse({id:"iq.orbShard_03", title:"ORB", info:"Collect all these pieces to complete the ORB!", name:"SHARD"}));
			items.push(new ItemInfoVO().parse({id:"iq.orbShard_04", title:"ORB", info:"Collect all these pieces to complete the ORB!", name:"SHARD"}));
			items.push(new ItemInfoVO().parse({id:"iq.orbShard_05", title:"ORB", info:"Collect all these pieces to complete the ORB!", name:"SHARD"}));
			
			items.push(new ItemInfoVO().parse({id:GameCollectableTypes.TYPE_VIAL_SMALL, title:"SMALL", info:"These little postions fill your gauge!", name:"SLIME VIAL"}));
			items.push(new ItemInfoVO().parse({id:GameCollectableTypes.TYPE_VIAL_MED, title:"MEDIUM", info:"These little postions fill your gauge!", name:"SLIME VIAL"}));
			items.push(new ItemInfoVO().parse({id:GameCollectableTypes.TYPE_VIAL_LARGE, title:"LARGE", info:"These little postions fill your gauge!", name:"SLIME VIAL"}));
			items.push(new ItemInfoVO().parse({id:GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH, title:"OURA", info:"Automatic regfill of your gauge!", name:"SUPER GAUGE"}));
			
			// Story Line
			storyLine.push(new StoryLineItemVO().parse({id:"1", imageId:"ui_char_ally_large", message:"Mission 1: This shell has been shattered into 5 pieces throughout the Area. Find all 5 pieces to restore the it. Follow the slime trail."}));
			storyLine.push(new StoryLineItemVO().parse({id:"2", imageId:"ui_char_ally_large", message:"You found a piece of the shell only 4 more to go."}));
			storyLine.push(new StoryLineItemVO().parse({id:"3", imageId:"ui_char_ally_large", message:"You found a piece of the shell only 3 more to go"}));
			storyLine.push(new StoryLineItemVO().parse({id:"4", imageId:"ui_char_ally_large", message:"You found a piece of the shell only 2 more to go"}));
			storyLine.push(new StoryLineItemVO().parse({id:"5", imageId:"ui_char_ally_large", message:"You found a piece of the shell only 1 more to go"}));
			storyLine.push(new StoryLineItemVO().parse({id:"6", imageId:"ui_char_ally_large", message:"One shell down! 4 more to go!"}));
		}
		
		public function getItemById(id:String):ItemInfoVO
		{
			var i:int = items.length;
			while(i--)
			{
				if(items[i].id == id) return items[i];
			}
			return null;
		}
		
		public function getStoryLineItem(index:int):StoryLineItemVO
		{
			if(index >= storyLine.length) return storyLine[0];
			else return storyLine[index]
		}
	}
}