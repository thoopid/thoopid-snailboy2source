package com.thoopid.games.sb2.model.game
{
	public class StoryLineItemVO extends AbstractJSONVO
	{
		public var id:String;
		public var imageId:String;
		public var message:String;
		
		public function StoryLineItemVO()
		{
			super();
		}
	}
}