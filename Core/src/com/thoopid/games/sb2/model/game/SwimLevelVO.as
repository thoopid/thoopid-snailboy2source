package com.thoopid.games.sb2.model.game
{
	public class SwimLevelVO extends AbstractJSONVO
	{
		public var level:int = 0;
		public var speedSwim:Number = 1;
		public var costSwim:Number = 2.6;		// 160 per second
		
		public var cost:Number = 100;
		
		public function SwimLevelVO()
		{
		}
	}
}