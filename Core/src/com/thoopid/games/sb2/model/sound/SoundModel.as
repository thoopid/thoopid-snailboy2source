package com.thoopid.games.sb2.model.sound
{
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.constants.game.LevelNames;
	
	import flash.utils.Dictionary;
	
	import citrus.core.CitrusEngine;
	import citrus.sounds.SoundManager;
	
	import org.robotlegs.mvcs.Actor;
	
	public class SoundModel extends Actor
	{
		public var currentSound:String = "";
		
		private var _soundUiHash:Dictionary = new Dictionary();
		
		public function SoundModel()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init():void
		{
			// SOUNDS
			//********************************************************
			var soundManager:SoundManager = CitrusEngine.getInstance().sound;
			addSound(GameConstants.SOUND_MENU,-1);
			addSound(GameConstants.SOUND_AMIENCE,-1);
			addSound(GameConstants.SOUND_JUMP);
			addSound(GameConstants.SOUND_STICK);
			addSound(GameConstants.SOUND_SLIMEY);
			addSound(GameConstants.SOUND_LAND);
			addSound(GameConstants.SOUND_PULL);
			addSound(GameConstants.SOUND_FALL);
			addSound(GameConstants.SOUND_EXIT);
			addSound(GameConstants.SOUND_SHELL);
			addSound(GameConstants.SOUND_BUTTON_UI);
			//********************************************************
			
			// Setup sounds hashes for ui levels
			_soundUiHash[LevelNames.MENU_LEVEL] = GameConstants.SOUND_MENU;
			_soundUiHash[LevelNames.KINGDOM_SELECT_LEVEL] = GameConstants.SOUND_MENU;
			_soundUiHash[LevelNames.ACHIEVEMENTS_LEVEL] = GameConstants.SOUND_MENU;
			_soundUiHash[LevelNames.WELCOME_LEVEL] = GameConstants.SOUND_MENU;
			_soundUiHash[LevelNames.KINGDOM_1] = GameConstants.SOUND_MENU;
		}
		
		public function getSoundForLevel(id:String):String
		{
			if(_soundUiHash[id])
			{
				return _soundUiHash[id];
			}
			
			return GameConstants.SOUND_AMIENCE;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function addSound(id:String, loops:int=0):void
		{
			CitrusEngine.getInstance().sound.addSound(id, {sound:Assets.assets.getSound(id), loops:loops});
		}
		
		public function playLevelSound(levelId:String):void
		{
			var newSound:String = getSoundForLevel(levelId);
			if(currentSound == newSound) return;
			
			if(currentSound)
			{
				CitrusEngine.getInstance().sound.stopSound(currentSound);
			}
			
			CitrusEngine.getInstance().sound.playSound(newSound);
			currentSound = newSound;
			
		}
	}
}