package com.thoopid.games.sb2.model.tut
{
	import org.robotlegs.mvcs.Actor;
	
	public class TutorialModel extends Actor
	{
		public var enabled:Boolean = false;
		
		public var state:String;
		
		public function TutorialModel()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function start():void
		{
			enabled = true;
		}
		
		public function stop():void
		{
			enabled = false;
		}
	}
}