package com.thoopid.games.sb2.model.ui
{
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmCallbackSignal;
	import com.thoopid.games.sb2.model.game.AbstractJSONVO;
	
	public class ConfirmPopupVO extends AbstractJSONVO
	{
		public var title:String;
		public var sub:String;
		public var message:String;
		public var cost:Number;
		public var callbackSignal:ConfirmCallbackSignal;
		
		public function ConfirmPopupVO(title:String, sub:String, message:String, cost:Number, callbackSignal:ConfirmCallbackSignal)
		{
			super();
			
			this.title = title;
			this.sub = sub;
			this.message = message;
			this.cost = cost;
			this.callbackSignal = callbackSignal;
		}
	}
}