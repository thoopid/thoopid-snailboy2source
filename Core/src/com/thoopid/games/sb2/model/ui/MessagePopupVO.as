package com.thoopid.games.sb2.model.ui
{
	public class MessagePopupVO extends Object
	{
		public var title:String = "";
		public var subtitle:String = "";
		public var messege:String = "";
		public var callback:Function = null;
		public var callbackData:Object = null;
		
		public function MessagePopupVO(title:String, subtitle:String, messege:String, callback:Function=null, callbackData:Object = null)
		{
			this.title = title;
			this.subtitle = subtitle;
			this.messege = messege;
			this.callback = callback;
			this.callbackData = callbackData;
		}
	}
}