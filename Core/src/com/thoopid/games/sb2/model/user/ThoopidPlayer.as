package com.thoopid.games.sb2.model.user
{
	import com.gamua.flox.Player;
	import com.thoopid.games.sb2.constants.game.LevelNames;

	public class ThoopidPlayer extends Player
	{
		private var _levels:Array;
		
		private var _kingdoms:Array;
		
		private var _inventoryItems:Array;
		
		private var _collectedItems:Array;
		
		private var _slotItems:Array;
		
		private var _slotAvail:int = 2;
		
		private var _coins:Number = 0;
		
		private var _activeArea:String = "1_1_0";
		
		private var _hasRevealedAreaItemCollected:Boolean = false;
		
		private var _hasRevealedKingdomCollected:Boolean = false;
		
		private var _mustSkipToNextStoryItem:Boolean = true;
		
		private var _mainStoryIndex:int = 0;
		
		private var _isNewPlayer:Boolean = false;
		
		private var _superEnabled:Boolean = false;
		
		private var _swimEnabled:Boolean = false;
		
		private var _gaugeUpgradeEnabled:Boolean = false;
		
		private var _inventoryEnabled:Boolean = false;
		
		private var _tutorialList:Array = [];
		
		
		
		// Gauge Propertes
		private var _gaugeLevel:int = 1;
		private var _swimLevel:int = 1;
		private var _jumpLevel:int = 1;
		
		public function ThoopidPlayer()
		{
			
		}
		
		//*********************
		//   GETTERS AND SETTERS
		//*********************

		public function get mainStoryIndex():int
		{
			return _mainStoryIndex;
		}

		public function set mainStoryIndex(value:int):void
		{
			_mainStoryIndex = value;
		}

		public function get mustSkipToNextStoryItem():Boolean
		{
			return _mustSkipToNextStoryItem;
		}

		public function set mustSkipToNextStoryItem(value:Boolean):void
		{
			_mustSkipToNextStoryItem = value;
		}

		public function get tutorialList():Array
		{
			return _tutorialList;
		}

		public function set tutorialList(value:Array):void
		{
			_tutorialList = value;
		}

		public function get inventoryEnabled():Boolean
		{
			return _inventoryEnabled;
		}

		public function set inventoryEnabled(value:Boolean):void
		{
			_inventoryEnabled = value;
		}

		public function get gaugeUpgradeEnabled():Boolean
		{
			return _gaugeUpgradeEnabled;
		}

		public function set gaugeUpgradeEnabled(value:Boolean):void
		{
			_gaugeUpgradeEnabled = value;
		}

		public function get collectedItems():Array
		{
			return _collectedItems;
		}

		public function set collectedItems(value:Array):void
		{
			_collectedItems = value;
		}

		public function get swimEnabled():Boolean
		{
			return _swimEnabled;
		}

		public function set swimEnabled(value:Boolean):void
		{
			_swimEnabled = value;
		}

		public function get superEnabled():Boolean
		{
			return _superEnabled;
		}

		public function set superEnabled(value:Boolean):void
		{
			_superEnabled = value;
		}

		public function get slotAvail():int
		{
			return _slotAvail;
		}

		public function set slotAvail(value:int):void
		{
			_slotAvail = value;
		}

		public function get slotItems():Array
		{
			return _slotItems;
		}

		public function set slotItems(value:Array):void
		{
			_slotItems = value;
		}

		public function get inventoryItems():Array
		{
			return _inventoryItems;
		}

		public function set inventoryItems(value:Array):void
		{
			_inventoryItems = value;
		}

		public function get kingdoms():Array
		{
			return _kingdoms;
		}

		public function set kingdoms(value:Array):void
		{
			_kingdoms = value;
		}

		public function get jumpLevel():int
		{
			return _jumpLevel;
		}

		public function set jumpLevel(value:int):void
		{
			_jumpLevel = value;
		}

		public function get swimLevel():int
		{
			return _swimLevel;
		}

		public function set swimLevel(value:int):void
		{
			_swimLevel = value;
		}

		public function get gaugeLevel():int
		{
			return _gaugeLevel;
		}

		public function set gaugeLevel(value:int):void
		{
			_gaugeLevel = value;
		}

		public function get hasRevealedKingdomCollected():Boolean
		{
			return _hasRevealedKingdomCollected;
		}

		public function set hasRevealedKingdomCollected(value:Boolean):void
		{
			_hasRevealedKingdomCollected = value;
		}

		public function get hasRevealedAreaItemCollected():Boolean
		{
			return _hasRevealedAreaItemCollected;
		}

		public function set hasRevealedAreaItemCollected(value:Boolean):void
		{
			_hasRevealedAreaItemCollected = value;
		}

		public function get activeArea():String
		{
			return _activeArea;
		}

		public function set activeArea(value:String):void
		{
			_activeArea = value;
		}

		public function get isNewPlayer():Boolean
		{
			return _isNewPlayer;
		}

		public function set isNewPlayer(value:Boolean):void
		{
			_isNewPlayer = value;
		}
		
		public function get coins():Number
		{
			return _coins;
		}

		public function set coins(value:Number):void
		{
			_coins = value;
		}

		public function get levels():Array
		{
			return _levels;
		}

		public function set levels(value:Array):void
		{
			_levels = value;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init():void
		{
//			_gaugeLevel = 1;
//			_jumpLevel = 1;
//			_swimLevel = 1;
			
//			_slotAvail = 3;
			
//			_swimEnabled = true;
			
			
			if(!_levels)
			{
				_isNewPlayer = true;
				levels = [];
				coins = 5000;
				_activeArea = LevelNames.KINGDOM_1_AREA_1;
			}
			
			if(!_kingdoms) kingdoms = [LevelNames.KINGDOM_1];
			if(!_inventoryItems) inventoryItems = [];
			if(!_collectedItems) collectedItems = [];
			if(!_slotItems) slotItems = [];
		}
	}
}