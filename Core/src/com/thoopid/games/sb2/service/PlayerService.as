package com.thoopid.games.sb2.service
{
	import com.gamua.flox.Flox;
	import com.gamua.flox.Player;
	import com.gamua.flox.utils.describeType;
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.model.game.LevelVO;
	import com.thoopid.games.sb2.model.user.ThoopidPlayer;
	
	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;
	

	public class PlayerService extends Actor
	{
		public var onCoinsUpdated:Signal = new Signal();
		
		private var _player:ThoopidPlayer;
		
		public function PlayerService()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get player():ThoopidPlayer
		{
			return Player.current as ThoopidPlayer;
		}

		public function init(levels:Vector.<LevelVO>):void
		{
			Flox.playerClass = ThoopidPlayer;
			Flox.init(Config.FLOX_GAME_ID, Config.FLOX_GAME_KEY, MobileConfig.VERSION_NUMBER);
			
			player.init();
			
			// Update Player levels with levels from the game
			for (var i:int = 0; i < levels.length; i++) 
			{
				preAddLevelToPlayerlevels(levels[i]);
			}
			
			Player.current.saveQueued();
		}
		
		/////////////////////////////////////////////////////////////////////////////////////
		// PUSHING information to Player
		/////////////////////////////////////////////////////////////////////////////////////
		public function saveLevel(level:LevelVO):void
		{
			var i:int = player.levels.length;
			while(i--)
			{
				if(player.levels[i].levelId == level.levelId)
				{
					parseObject(level, player.levels[i]);
					return;
				}
			}
			
			// Not found so add
			player.levels.push(level);
		}
		
		public function unlockLevel(levelId:String):void
		{
			var i:int = player.levels.length;
			while(i--)
			{
				if(player.levels[i].levelId == levelId)
				{
					player.levels[i].locked = false;
					return;
				}
			}
		}
		
		public function grantCoins(coinsCount:Number):void
		{
			player.coins += coinsCount;
			
			onCoinsUpdated.dispatch();
		}
		
		public function deductCoins(coinsCount:int):void
		{
			player.coins -= coinsCount;
			
			onCoinsUpdated.dispatch();
			
		}
		public function unlockKingdom(kingdomid:String):void
		{
			player.kingdoms.push(kingdomid);
		}
		
		//**************************
		// TUTORIALS
		//**************************
			public function completeTutorial(id:String):void
			{
				player.tutorialList.push(id);
			}
			
			public function hasCompletedTutorial(id:String):Boolean
			{
				return player.tutorialList.indexOf(id) != -1;
			}
		//**************************
		
		
		/////////////////////////////////////////////////////////////////////////////////////
		// PULLING information from Player
		/////////////////////////////////////////////////////////////////////////////////////
		public function getAreaItemsCollectedCount(areaId:String):Number
		{
			var count:Number = 0;
			for (var i:int = 0; i < player.levels.length; i++) 
			{
				if(player.levels[i].areaId == areaId)
				{
					if(player.levels[i].storyItemCollected == true) count++;
				}
			}
			
			return count;
		}
		
		public function getKingdomCollectedCount(kingdomId:String):Number
		{
			var count:Number = 0;
			for (var i:int = 0; i < player.levels.length; i++) 
			{
				if(player.levels[i].kingdomId == kingdomId)
				{
					if(player.levels[i].storyItemCollected == true) count++;
				}
			}
			
			return Math.floor(count / 5);
		}
		
		public function getPlayerLevel(levelId:String):LevelVO
		{
			var i:int = player.levels.length;
			while(i--)
			{
				if(player.levels[i].levelId == levelId)
				{
					var levelVO:LevelVO = new LevelVO();
					levelVO.parse(player.levels[i]);
					return levelVO;
				}
			}
			
			return null;
		}
		
		public function getKingdomsUnlocked():Array
		{
			return player.kingdoms;
		}
		
		public function getNextLevelIdToUnlock():String
		{
			var id:String = LevelNames.KINGDOM_1_LEVEL_1;
			for (var i:int = 0; i < player.levels.length; i++) 
			{
				if(player.levels[i].locked)
				{
					return player.levels[i].levelId;
				}
			}
			
			return id;
		}
		
		// ******************************************************************
		// INVENTORY METHODS
		// ******************************************************************
		
			public function inventoryAdd(item:InventoryItemVO):void
			{
				var i:Object = inventoryGetItem(item.id);
				if(i)
				{
					i.quantity++;
				} else
				{
					player.inventoryItems.push(item);
				}
			}
			
			public function inventoryRemove(id:String):void
			{
				var i:int = player.inventoryItems.length;
				while(i--)
				{
					if(player.inventoryItems[i].id == id)
					{
						if(player.inventoryItems[i].quantity > 0) player.inventoryItems[i].quantity--;
					}
				}
			}
			
			public function inventoryGetItem(id:String):Object
			{
				var i:int = player.inventoryItems.length;
				while(i--)
				{
					if(player.inventoryItems[i].id == id)
					{
						return player.inventoryItems[i];
					}
				}
				return null;
			}
			
			public function inventoryGetAll():Vector.<InventoryItemVO>
			{
				var items:Vector.<InventoryItemVO> = new Vector.<InventoryItemVO>();
				for (var i:int = 0; i < player.inventoryItems.length; i++) 
				{
					items.push(new InventoryItemVO().parse(player.inventoryItems[i]));
				}
				
				return items;
			}
		// ******************************************************************
			
			// ******************************************************************
			// SLOTS METHODS
			// ******************************************************************
			
			public function slotsAdd(item:InventoryItemVO):void
			{
				var i:Object = slotsGetItem(item.id);
				if(i)
				{
					i.quantity++;
				} else
				{
					player.slotItems.push(item);
				}
			}
			
			public function slotsRemove(id:String):void
			{
				var i:int = player.slotItems.length;
				while(i--)
				{
					if(player.slotItems[i].id == id)
					{
						if(player.slotItems[i].quantity > 0)
						{
							player.slotItems[i].quantity--;
						}
						
						if(player.slotItems[i].quantity <= 0)
						{
							player.slotItems.splice(i,1);
						}
					}
				}
			}
			
			public function slotsGetItem(id:String):Object
			{
				var i:int = player.slotItems.length;
				while(i--)
				{
					if(player.slotItems[i].id == id)
					{
						return player.slotItems[i];
					}
				}
				return null;
			}
			
			public function slotsGetAll():Vector.<InventoryItemVO>
			{
				var items:Vector.<InventoryItemVO> = new Vector.<InventoryItemVO>();
				for (var i:int = 0; i < player.slotItems.length; i++) 
				{
					items.push(new InventoryItemVO().parse(player.slotItems[i]));
				}
				
				return items;
			}
			
			public function slotsAvailable():Boolean
			{
				return player.slotAvail > player.slotItems.length;
			}
			// ******************************************************************
		
		public function parseObject(data:Object, target:Object):void
		{
			var typeDescription:XML = describeType(data);
			for each (var variable:XML in typeDescription.variable)
			{
				var propertyName:String = variable.@name.toString();
				var access:String = variable.@access.toString(); 
				var nonSerializedMetaData:XMLList = variable.metadata.(@name == "NonSerialized");
				
				if (access != "writeonly" && nonSerializedMetaData.length() == 0)
				{
//					target[propertyName] = data[propertyName];
					
					if(data[propertyName] != null) target[propertyName] = data[propertyName];
				}
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function preAddLevelToPlayerlevels(level:LevelVO):void
		{
			if(level.isUiLevel) return;
			
			var i:int = player.levels.length;
			while(i--)
			{
				if(player.levels[i].levelId == level.levelId)
				{
					return;
				}
			}
			
			// Not found so add
			player.levels.push(level);
		}
	}
}