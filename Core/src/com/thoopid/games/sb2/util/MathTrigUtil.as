package com.thoopid.games.sb2.util
{
	public class MathTrigUtil
	{
		public static function degreesToRadians(degrees:Number):Number {
			return degrees * Math.PI / 180;
		}
		
		public static function radiansToDegrees(radians:Number):Number{
			return radians * 180 / Math.PI;
		}
		public static function limit(value:Number, lowLimit:Number, highLimit:Number):Number {
			return Math.max(lowLimit, Math.min(highLimit, value)); 
		}
	}
}