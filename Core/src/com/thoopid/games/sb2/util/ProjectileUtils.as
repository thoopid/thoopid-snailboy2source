package com.thoopid.games.sb2.util
{

    /**
     * Projetile trajectory utils.
     * @author maciek grzybowski, 10.11.13 14:29
     *
     */
    public class ProjectileUtils {

        /**
         * Checks if projectile can hit (x, y) coordinate with initial velocity length under given gravity.
         * @param x
         * @param y
         * @param velocity initial velocity
         * @param gravity gravity value; should be greater than 0
         * @return
         */
        public static function canHitCoordinate(x:Number, y:Number, velocity:Number, gravity:Number):Boolean {
            return calculateDelta(x, y, velocity, gravity) >= 0;
        }

        /**
         * Calculates angle to hit given (x, y) coordinate with given velocity and gravity.
         * @param x
         * @param y
         * @param velocity initial velocity
         * @param gravity gravity value; should be greater than 0
         * @return angle in radians
         */
        public static function calculateAngle1ToHitCoordinate(x:Number, y:Number, velocity:Number, gravity:Number):Number {
            if (x == 0) return y > 0 ? -Math.PI * 0.5 : Math.PI * 0.5;
            var delta:Number = calculateDelta(x, y, velocity, gravity);
            var sqrtDelta:Number = Math.sqrt(delta);
            return Math.atan((velocity * velocity - sqrtDelta)/(gravity * x));;
        }

        /**
         * Calculates angle to hit given (x, y) coordinate with given velocity and gravity.
         * @param x
         * @param y
         * @param velocity initial velocity
         * @param gravity gravity value; should be greater than 0
         * @return angle in radians
         */
        public static function calculateAngle2ToHitCoordinate(x:Number, y:Number, velocity:Number, gravity:Number):Number {
            if (x == 0) return -Math.PI * 0.5;
            var delta:Number = calculateDelta(x, y, velocity, gravity);
            var sqrtDelta:Number = Math.sqrt(delta);
            return Math.atan((velocity * velocity + sqrtDelta)/(gravity * x));
        }

        public static function calculateDelta(x:Number, y:Number, velocity:Number, gravity:Number):Number {
            return velocity * velocity * velocity * velocity - gravity * (gravity * x * x + 2 * y * velocity * velocity)
        }
		
		public static function calculateVelocity1ToHitCoordinate(x:Number, y:Number, angle:Number, gravity:Number):Number {
			var ta:Number = Math.abs(Math.atan(angle));
			//v = (sqrt(g) * sqrt(x) * sqrt((tan(o)*tan(o))+1)) / sqrt(2 * tan(o) - (2 * g * y) / x); // velocity
			return (Math.sqrt(gravity) * Math.sqrt(x) * Math.sqrt(ta*ta+1)) / Math.sqrt(2 * ta - (2 * gravity * y) / x);
		}
    }
}
