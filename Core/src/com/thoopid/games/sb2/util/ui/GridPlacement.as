package com.thoopid.games.sb2.util.ui
{
	import starling.display.DisplayObject;

	public class GridPlacement
	{
		/**
		 * Work in progress
		 */
		public static function PlaceOnGrid(items:Array, colums:Number, h_padd:Number=0, v_padd:Number=0):void
		{
			var boxNum:int = items.length;
			var box:DisplayObject;
			for (var i:int = 0; i<boxNum; i++)
			{
				box = items[i];
				box.x = h_padd * (i % colums);
				box.y = v_padd * int(i / colums);
			}
		}
	}
}