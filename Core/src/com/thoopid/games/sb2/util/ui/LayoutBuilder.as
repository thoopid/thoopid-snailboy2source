package com.thoopid.games.sb2.util.ui
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	
	import flash.utils.getDefinitionByName;
	
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	import starling.utils.deg2rad;
	
	public class LayoutBuilder
	{
		public function LayoutBuilder() {
		}
		
		public static function buildUiFromData(root:Sprite, data:Object, assetManager:AssetManager):void {
			var objectClass:Class;
			var element:DisplayObject;
			var item:Object;
			var holder:Sprite;
			var xPos:Number;
			var yPos:Number;
			var texture:String;
			var params:Object;
			for (var i:int = 0; i < data.items.length; i++) {
				item = data.items[i];
				if(item.params.parentClip != "") {
					holder = root.getChildByName(item.params.parentClip) as Sprite;
					xPos = item.x;
					yPos = item.y;
				} else if(item.params.fixed) {
					xPos = item.x;
					yPos = item.y;
				} else {
					xPos = Config.GAME_WIDTH * item.xPer;
					yPos = Config.GAME_HEIGHT * item.yPer;
				}
				
				if(!holder) holder = root;
				params = {width:item.width, height:item.height, rotation:item.r, name:item.name};
				for (var pp:Object in item.params) {
					params[pp] = item.params[pp];
				}
				
				objectClass = getDefinitionByName(params.className) as Class;
				if(objectClass) {
					element = new objectClass(params);
					holder.addChild(element);
					element.x = xPos;
					element.y = yPos;
					
					element.rotation = deg2rad(params.rotation);
					element.name = item.name;
				}
				
				holder = null;
			}
			
			// Cleanup
			item = null;
			element = null;
			objectClass = null;
			data = null;
		}
		
		public static function buildUiElementFromDATA(root:Sprite, data:Object, assetManager:AssetManager):void
		{
			var objectClass:Class;
			var element:DisplayObject;
			var holder:Sprite;
			var params:Object = {};
			for (var i:int = 0; i < data.items.length; i++) 
			{
				params = data.items[i];
				holder = root;
				if(params.className && params.className != "")
				{
					objectClass = getDefinitionByName(params.className) as Class;
					if(objectClass)
					{
						element = new objectClass(params);
					}
				} else
				{
					element = Factory.imagePool.getImage(assetManager.getTexture(params.i),true);
				}
				
				if(element)
				{
					if(params.fixed == null || params.fixed == true)
					{
						element.x = params.x;
						element.y = params.y;
					} else
					{
						element.x = Config.GAME_WIDTH * params.xPer;
						element.y = Config.GAME_HEIGHT * params.yPer;
					}
					element.name = params.name;
					element.rotation = deg2rad(params.rotation);
					holder.addChild(element);
					element.width = params.width;
					element.height = params.height;
				}
				holder = null;
			}
			
			// Cleanup
			element = null;
			objectClass = null;
			data = null;
		}
		
		public static function findChild( dispobj:DisplayObjectContainer, childname:String ):DisplayObject
		{
			if (dispobj == null)
			{
				return null;
			}
			
			for (var j:int = 0; j < dispobj.numChildren; ++j)
			{ 
				var obj:DisplayObject = dispobj.getChildAt( j ) as DisplayObject;
				if (obj.name == childname)
				{
					return obj;
				}
				if (obj is DisplayObjectContainer)
				{
					var doc:DisplayObjectContainer = obj as DisplayObjectContainer;
					if (doc.numChildren > 0)
					{
						var ret:DisplayObject = findChild( doc, childname );
						if (ret != null)
						{
							return ret;
						}
					}
				} 
			}
			return null;
		}
	}
}