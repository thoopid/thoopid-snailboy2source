package com.thoopid.games.sb2.util.ui
{
	import flash.utils.Dictionary;
	
	import coza.runtime.utils.number.RandomRange;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;

	public class Warrble
	{
		private static var _instance:Warrble;
		
		private var _dict:Dictionary;
		
		public function Warrble()
		{
			
		}
		
		public static function getInstance():Warrble
		{
			if(!_instance)
			{
				_instance = new Warrble();
				_instance._dict = new Dictionary();
			}
			return _instance;
		}
		
		public function add(item:DisplayObject):void
		{
			if(_dict[item]) return;
			
			item.y = 3;
			_dict[item] = Starling.juggler.tween(item, RandomRange.randRange(0.7,1), {y:-3/*RandomRange.randRange(-3,3)*/, repeatCount:999, reverse:true, transition:Transitions.LINEAR});
		}
		
		public function remove(item:DisplayObject):void
		{
			clean(item);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onRemove(e:Event=null):void
		{
			clean(e.target as DisplayObject);
		}
		
		private function clean(item:DisplayObject):void
		{
			if(_dict[item]) delete _dict[item];
			Starling.juggler.removeTweens(item);
		}
	}
}