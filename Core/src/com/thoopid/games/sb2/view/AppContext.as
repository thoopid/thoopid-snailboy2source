package com.thoopid.games.sb2.view
{
	import com.thoopid.games.sb2.controller.game.CollectGameCollectableItemCommand;
	import com.thoopid.games.sb2.controller.game.LevelCompleteCommand;
	import com.thoopid.games.sb2.controller.game.NewGameCommand;
	import com.thoopid.games.sb2.controller.game.PauseLevelCommand;
	import com.thoopid.games.sb2.controller.game.SaveLevelScoreCommand;
	import com.thoopid.games.sb2.controller.game.ToggleOraCommand;
	import com.thoopid.games.sb2.controller.game.TriggerTutorialCommand;
	import com.thoopid.games.sb2.controller.game.UnlockLevelCommand;
	import com.thoopid.games.sb2.controller.game.UseGameConsumableCommand;
	import com.thoopid.games.sb2.controller.game.UseInventoryItemCommand;
	import com.thoopid.games.sb2.controller.signals.game.CharacterActionSignal;
	import com.thoopid.games.sb2.controller.signals.game.CollectGameCollectableItemSignal;
	import com.thoopid.games.sb2.controller.signals.game.GameModelUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.game.LevelCompleteSignal;
	import com.thoopid.games.sb2.controller.signals.game.NewGameSignal;
	import com.thoopid.games.sb2.controller.signals.game.PauseLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.ResumeLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.RetryLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.SaveLevelScoreSignal;
	import com.thoopid.games.sb2.controller.signals.game.ShowCoinCollectedSignal;
	import com.thoopid.games.sb2.controller.signals.game.ShowSlimeCollectedSignal;
	import com.thoopid.games.sb2.controller.signals.game.TimeUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.game.ToggleOraSignal;
	import com.thoopid.games.sb2.controller.signals.game.ToggleSoftPauseGame;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorial_ShowSignal;
	import com.thoopid.games.sb2.controller.signals.game.UnlockLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.UseGameConsumableSignal;
	import com.thoopid.games.sb2.controller.signals.game.UseInventoryItemSignal;
	import com.thoopid.games.sb2.controller.signals.story.CheckKingdomCollectablesSignal;
	import com.thoopid.games.sb2.controller.signals.story.ContinuewithKingdomSignal;
	import com.thoopid.games.sb2.controller.signals.story.RevealAreaCollectableSignal;
	import com.thoopid.games.sb2.controller.signals.story.RevealKingdomCollectableSignal;
	import com.thoopid.games.sb2.controller.signals.story.StepMainStorySignal;
	import com.thoopid.games.sb2.controller.signals.system.LevelResultToggleSignal;
	import com.thoopid.games.sb2.controller.signals.system.LoadLevelSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmCallbackSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmPopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.EquiptSlotItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GameHudShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GaugeUpgradeScreenSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GrantInventoryItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryUpdatedSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ItemInfoPopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.KingdomAreaUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.ui.MessagePopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.NavigateKingdomAreaSignal;
	import com.thoopid.games.sb2.controller.signals.ui.NotEnoughCoinMesssageSignal;
	import com.thoopid.games.sb2.controller.signals.ui.OptionsShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.PurchaseCoinSignal;
	import com.thoopid.games.sb2.controller.signals.ui.PurchaseInventoryItemRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.RemoveSlotItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ShopShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ShortMessageDisplaySignal;
	import com.thoopid.games.sb2.controller.signals.ui.SlotsUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.ui.StoryMainShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ToggleSwimControlsSignal;
	import com.thoopid.games.sb2.controller.signals.ui.UpgradeGaugeRequestSignal;
	import com.thoopid.games.sb2.controller.story.CheckKingdomCollectablesCommand;
	import com.thoopid.games.sb2.controller.story.StepMainStoryCommand;
	import com.thoopid.games.sb2.controller.system.LoadLevelCommand;
	import com.thoopid.games.sb2.controller.system.PrepApplicationCommand;
	import com.thoopid.games.sb2.controller.ui.EquiptSlotItemCommand;
	import com.thoopid.games.sb2.controller.ui.GrantInventoryItemCommand;
	import com.thoopid.games.sb2.controller.ui.NavigateKingdomAreaCommand;
	import com.thoopid.games.sb2.controller.ui.NotEnoughCoinCommand;
	import com.thoopid.games.sb2.controller.ui.PurchaseCoinsCommand;
	import com.thoopid.games.sb2.controller.ui.PurchaseInventoryItemCommand;
	import com.thoopid.games.sb2.controller.ui.RemoveSlotItemCommand;
	import com.thoopid.games.sb2.controller.ui.UpgradeGaugeRequestCommand;
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.model.game.ShopModel;
	import com.thoopid.games.sb2.model.game.StoryDataModel;
	import com.thoopid.games.sb2.model.sound.SoundModel;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.game.elements.mediators.AreaElementBaseMediator;
	import com.thoopid.games.sb2.view.game.elements.mediators.KingdomElementBaseMediator;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementBaseView;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Chest;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Pipes;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Scroll;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Shards;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Tomb;
	import com.thoopid.games.sb2.view.game.elements.ui.CaveSelectorView;
	import com.thoopid.games.sb2.view.game.elements.ui.CaveSelectorViewMediator;
	import com.thoopid.games.sb2.view.game.elements.ui.KingdomElementBaseView;
	import com.thoopid.games.sb2.view.game.elements.ui.KingdomElementView_1;
	import com.thoopid.games.sb2.view.game.level.AchievementsLevel;
	import com.thoopid.games.sb2.view.game.level.AchievementsLevelMediator;
	import com.thoopid.games.sb2.view.game.level.GameLevelMediator;
	import com.thoopid.games.sb2.view.game.level.IntroLevel;
	import com.thoopid.games.sb2.view.game.level.IntroLevelMediator;
	import com.thoopid.games.sb2.view.game.level.K_1_0_0;
	import com.thoopid.games.sb2.view.game.level.K_1_1_1;
	import com.thoopid.games.sb2.view.game.level.K_1_1_10;
	import com.thoopid.games.sb2.view.game.level.K_1_1_11;
	import com.thoopid.games.sb2.view.game.level.K_1_1_12;
	import com.thoopid.games.sb2.view.game.level.K_1_1_13;
	import com.thoopid.games.sb2.view.game.level.K_1_1_14;
	import com.thoopid.games.sb2.view.game.level.K_1_1_15;
	import com.thoopid.games.sb2.view.game.level.K_1_1_16;
	import com.thoopid.games.sb2.view.game.level.K_1_1_17;
	import com.thoopid.games.sb2.view.game.level.K_1_1_18;
	import com.thoopid.games.sb2.view.game.level.K_1_1_19;
	import com.thoopid.games.sb2.view.game.level.K_1_1_2;
	import com.thoopid.games.sb2.view.game.level.K_1_1_20;
	import com.thoopid.games.sb2.view.game.level.K_1_1_21;
	import com.thoopid.games.sb2.view.game.level.K_1_1_22;
	import com.thoopid.games.sb2.view.game.level.K_1_1_23;
	import com.thoopid.games.sb2.view.game.level.K_1_1_24;
	import com.thoopid.games.sb2.view.game.level.K_1_1_25;
	import com.thoopid.games.sb2.view.game.level.K_1_1_3;
	import com.thoopid.games.sb2.view.game.level.K_1_1_4;
	import com.thoopid.games.sb2.view.game.level.K_1_1_5;
	import com.thoopid.games.sb2.view.game.level.K_1_1_6;
	import com.thoopid.games.sb2.view.game.level.K_1_1_7;
	import com.thoopid.games.sb2.view.game.level.K_1_1_8;
	import com.thoopid.games.sb2.view.game.level.K_1_1_9;
	import com.thoopid.games.sb2.view.game.level.KingdomSelectionLevel;
	import com.thoopid.games.sb2.view.game.level.KingdomSelectionLevelMediator;
	import com.thoopid.games.sb2.view.game.level.MenuLevel;
	import com.thoopid.games.sb2.view.game.level.MenuLevelMediator;
	import com.thoopid.games.sb2.view.game.level.TreasureLevel;
	import com.thoopid.games.sb2.view.game.level.TreasureLevelMediator;
	import com.thoopid.games.sb2.view.game.level.WelcomeLevel;
	import com.thoopid.games.sb2.view.game.level.WelcomeLevelMediator;
	import com.thoopid.games.sb2.view.game.level.base.KingdomLevelMediator;
	import com.thoopid.games.sb2.view.game.level.base.LevelBase;
	import com.thoopid.games.sb2.view.game.objects.view.Character;
	import com.thoopid.games.sb2.view.game.objects.view.CharacterMediator;
	import com.thoopid.games.sb2.view.game.tut.DragTutorial;
	import com.thoopid.games.sb2.view.game.tut.ExitTutorial;
	import com.thoopid.games.sb2.view.game.tut.GaugeRefilTutorial;
	import com.thoopid.games.sb2.view.game.tut.InventoryBuyTutorial;
	import com.thoopid.games.sb2.view.game.tut.InventoryCloseTutorial;
	import com.thoopid.games.sb2.view.game.tut.InventoryOpenTutorial;
	import com.thoopid.games.sb2.view.game.tut.PullTutorial;
	import com.thoopid.games.sb2.view.game.tut.SecretAreaTutorial;
	import com.thoopid.games.sb2.view.game.tut.SelectLevelTutorial;
	import com.thoopid.games.sb2.view.game.tut.StickTutorial;
	import com.thoopid.games.sb2.view.game.tut.StoryItemTutorial;
	import com.thoopid.games.sb2.view.game.tut.SuperJumpTutorial;
	import com.thoopid.games.sb2.view.game.tut.SwimTutorial;
	import com.thoopid.games.sb2.view.game.tut.TutElementBaseMediator;
	import com.thoopid.games.sb2.view.game.tut.TutElementBaseView;
	import com.thoopid.games.sb2.view.ui.comps.CoinValue;
	import com.thoopid.games.sb2.view.ui.comps.CoinValueMediator;
	import com.thoopid.games.sb2.view.ui.popup.ConfirmPopup;
	import com.thoopid.games.sb2.view.ui.popup.ConfirmPopupMediator;
	import com.thoopid.games.sb2.view.ui.popup.ItemInfoPopup;
	import com.thoopid.games.sb2.view.ui.popup.ItemInfoPopupMediator;
	import com.thoopid.games.sb2.view.ui.popup.MessagePopup;
	import com.thoopid.games.sb2.view.ui.popup.MessagePoupMediator;
	import com.thoopid.games.sb2.view.ui.popup.ShortMessagePopup;
	import com.thoopid.games.sb2.view.ui.popup.ShortMessagePopupMediator;
	import com.thoopid.games.sb2.view.ui.screens.AbstractSection;
	import com.thoopid.games.sb2.view.ui.screens.GaugeUpgradeScreen;
	import com.thoopid.games.sb2.view.ui.screens.GaugeUpgradeScreenMediator;
	import com.thoopid.games.sb2.view.ui.screens.InventoryScreen;
	import com.thoopid.games.sb2.view.ui.screens.InventoryScreenMediator;
	import com.thoopid.games.sb2.view.ui.screens.LevelResultScreen;
	import com.thoopid.games.sb2.view.ui.screens.LevelResultScreenMediator;
	import com.thoopid.games.sb2.view.ui.screens.OptionsScreen;
	import com.thoopid.games.sb2.view.ui.screens.OptionsScreenMediator;
	import com.thoopid.games.sb2.view.ui.screens.ShopScreen;
	import com.thoopid.games.sb2.view.ui.screens.ShopScreenMediator;
	import com.thoopid.games.sb2.view.ui.screens.StoryScreenMain;
	import com.thoopid.games.sb2.view.ui.screens.StoryScreenMainMediator;
	import com.thoopid.games.sb2.view.ui.screens.hud.GaugeView;
	import com.thoopid.games.sb2.view.ui.screens.hud.GaugeViewMediator;
	import com.thoopid.games.sb2.view.ui.screens.hud.HudScreen;
	import com.thoopid.games.sb2.view.ui.screens.hud.HudScreenMediator;
	import com.thoopid.games.sb2.view.ui.screens.hud.HudSlots;
	import com.thoopid.games.sb2.view.ui.screens.hud.HudSlotsMediator;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.StarlingSignalContext;
	
	import starling.display.DisplayObjectContainer;
	
	public class AppContext extends StarlingSignalContext
	{
		public function AppContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);
		}
		
		override public function startup():void
		{
			// Views
			mediatorMap.mapView(K_1_0_0, KingdomLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_1, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_2, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_3, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_4, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_5, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_6, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_7, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_8, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_9, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_10, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_11, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_12, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_13, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_14, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_15, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_16, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_17, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_18, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_19, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_20, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_21, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_22, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_23, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_24, GameLevelMediator, LevelBase);
			mediatorMap.mapView(K_1_1_25, GameLevelMediator, LevelBase);
			
			mediatorMap.mapView(WelcomeLevel, WelcomeLevelMediator, LevelBase);
			mediatorMap.mapView(IntroLevel, IntroLevelMediator, LevelBase);
			mediatorMap.mapView(AchievementsLevel, AchievementsLevelMediator, LevelBase);
			mediatorMap.mapView(KingdomSelectionLevel, KingdomSelectionLevelMediator, LevelBase);
			mediatorMap.mapView(MenuLevel, MenuLevelMediator, LevelBase);
			mediatorMap.mapView(TreasureLevel, TreasureLevelMediator, LevelBase);
			
			// Screens
			mediatorMap.mapView(LevelResultScreen, LevelResultScreenMediator, AbstractSection);
			mediatorMap.mapView(GaugeUpgradeScreen, GaugeUpgradeScreenMediator, AbstractSection);
			mediatorMap.mapView(InventoryScreen, InventoryScreenMediator, AbstractSection);
			mediatorMap.mapView(OptionsScreen, OptionsScreenMediator, AbstractSection);
			mediatorMap.mapView(ShopScreen, ShopScreenMediator, AbstractSection);
			mediatorMap.mapView(HudScreen, HudScreenMediator, AbstractSection);
			mediatorMap.mapView(StoryScreenMain, StoryScreenMainMediator, AbstractSection);
			
			// Components
			mediatorMap.mapView(GaugeView, GaugeViewMediator);
			mediatorMap.mapView(ShortMessagePopup, ShortMessagePopupMediator);
			mediatorMap.mapView(ConfirmPopup, ConfirmPopupMediator);
			mediatorMap.mapView(ItemInfoPopup, ItemInfoPopupMediator);
			mediatorMap.mapView(MessagePopup, MessagePoupMediator);
			mediatorMap.mapView(Character, CharacterMediator);
			mediatorMap.mapView(CoinValue, CoinValueMediator);
			mediatorMap.mapView(CaveSelectorView, CaveSelectorViewMediator);
			mediatorMap.mapView(HudSlots, HudSlotsMediator);
			
				// Area Elements
				mediatorMap.mapView(AreaElementView_Shards, AreaElementBaseMediator, AreaElementBaseView);
				mediatorMap.mapView(AreaElementView_Chest, AreaElementBaseMediator, AreaElementBaseView);
				mediatorMap.mapView(AreaElementView_Scroll, AreaElementBaseMediator, AreaElementBaseView);
				mediatorMap.mapView(AreaElementView_Pipes, AreaElementBaseMediator, AreaElementBaseView);
				mediatorMap.mapView(AreaElementView_Tomb, AreaElementBaseMediator, AreaElementBaseView);
				// Kingdom Elements
				mediatorMap.mapView(KingdomElementView_1, KingdomElementBaseMediator, KingdomElementBaseView);
				
				// Tutorials
				mediatorMap.mapView(PullTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(SuperJumpTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(GaugeRefilTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(StickTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(StoryItemTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(SecretAreaTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(SwimTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(ExitTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(DragTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(InventoryOpenTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(InventoryBuyTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(InventoryCloseTutorial, TutElementBaseMediator, TutElementBaseView);
				mediatorMap.mapView(SelectLevelTutorial, TutElementBaseMediator, TutElementBaseView);
			
			// Services
			injector.mapSingleton(PlayerService);
			
			// Models
			injector.mapSingleton(LevelModel);
			injector.mapSingleton(GaugeModel);
			injector.mapSingleton(GameModel);
			injector.mapSingleton(TutorialModel);
			injector.mapSingleton(StoryDataModel);
			injector.mapSingleton(ShopModel);
			injector.mapSingleton(SoundModel);
			
			// Signals
			
				// Game Signals
				injector.mapSingleton(ResumeLevelSignal);
				injector.mapSingleton(RetryLevelSignal);
				injector.mapSingleton(TimeUpdateSignal);
				injector.mapSingleton(GameModelUpdateSignal);
				injector.mapSingleton(CharacterActionSignal);
				injector.mapSingleton(TriggerTutorial_ShowSignal);
				injector.mapSingleton(ShowCoinCollectedSignal);
				injector.mapSingleton(ShowSlimeCollectedSignal);
				injector.mapSingleton(ToggleSoftPauseGame);
				
				// Story
				injector.mapSingleton(RevealAreaCollectableSignal);
				injector.mapSingleton(RevealKingdomCollectableSignal);
				injector.mapSingleton(ContinuewithKingdomSignal);
				
				// System Signals
				injector.mapSingleton(LevelResultToggleSignal);
				
				// UI
				injector.mapSingleton(ShortMessageDisplaySignal);
				injector.mapSingleton(GaugeUpgradeScreenSignal);
				injector.mapSingleton(ConfirmPopupRequestSignal);
				injector.mapSingleton(ConfirmCallbackSignal);
				injector.mapSingleton(InventoryShowSignal);
				injector.mapSingleton(OptionsShowSignal);
				injector.mapSingleton(GameHudShowSignal);
				injector.mapSingleton(InventoryUpdatedSignal);
				injector.mapSingleton(SlotsUpdateSignal);
				injector.mapSingleton(KingdomAreaUpdateSignal);
				injector.mapSingleton(MessagePopupRequestSignal);
				injector.mapSingleton(ItemInfoPopupRequestSignal);
				injector.mapSingleton(ShopShowSignal);
				injector.mapSingleton(ToggleSwimControlsSignal);
				injector.mapSingleton(StoryMainShowSignal);
			
			// Commands
			
				// System Commands
				signalCommandMap.mapSignalClass(LoadLevelSignal, LoadLevelCommand);
			
				// Game Commands
				signalCommandMap.mapSignalClass(LevelCompleteSignal, LevelCompleteCommand);
				signalCommandMap.mapSignalClass(CollectGameCollectableItemSignal, CollectGameCollectableItemCommand);
				signalCommandMap.mapSignalClass(UseGameConsumableSignal, UseGameConsumableCommand);
				signalCommandMap.mapSignalClass(NewGameSignal, NewGameCommand);
				signalCommandMap.mapSignalClass(SaveLevelScoreSignal, SaveLevelScoreCommand);
				signalCommandMap.mapSignalClass(UnlockLevelSignal, UnlockLevelCommand);
				signalCommandMap.mapSignalClass(PauseLevelSignal, PauseLevelCommand);
				signalCommandMap.mapSignalClass(UseInventoryItemSignal, UseInventoryItemCommand);
				signalCommandMap.mapSignalClass(ToggleOraSignal, ToggleOraCommand);
				signalCommandMap.mapSignalClass(TriggerTutorialSignal, TriggerTutorialCommand);
				
				// Story
				signalCommandMap.mapSignalClass(CheckKingdomCollectablesSignal, CheckKingdomCollectablesCommand);
				signalCommandMap.mapSignalClass(StepMainStorySignal, StepMainStoryCommand);
				
				// UI
				signalCommandMap.mapSignalClass(UpgradeGaugeRequestSignal, UpgradeGaugeRequestCommand);
				signalCommandMap.mapSignalClass(PurchaseInventoryItemRequestSignal, PurchaseInventoryItemCommand);
				signalCommandMap.mapSignalClass(PurchaseCoinSignal, PurchaseCoinsCommand);
				signalCommandMap.mapSignalClass(EquiptSlotItemSignal, EquiptSlotItemCommand);
				signalCommandMap.mapSignalClass(RemoveSlotItemSignal, RemoveSlotItemCommand);
				signalCommandMap.mapSignalClass(NavigateKingdomAreaSignal, NavigateKingdomAreaCommand);
				signalCommandMap.mapSignalClass(GrantInventoryItemSignal, GrantInventoryItemCommand);
				signalCommandMap.mapSignalClass(NotEnoughCoinMesssageSignal, NotEnoughCoinCommand);
			
			// Startup
			commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, PrepApplicationCommand);
			
			// Man Contect Starling view
//			mediatorMap.mapView(AppView, AppViewMediator);
			super.startup();
		}
	}
}