package com.thoopid.games.sb2.view.effect
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	public class AreaRevealEffect extends Sprite
	{
		private var _smoke:PoolImage;
		private var _ring2:PoolImage;
		private var _ring:PoolImage;
		public function AreaRevealEffect()
		{
			super();
			
			this.addChild(_ring = Factory.imagePool.getImage(Assets.assets.getTexture("iq.ringFX"),true));
			this.addChild(_ring2 = Factory.imagePool.getImage(Assets.assets.getTexture("iq.ringFX"),true));
			
			_ring.color = 0xFFFFFF;
			_ring2.color = 0xFFFFFF;
			
			_ring.rotation = deg2rad(-90);
			_ring2.rotation = deg2rad(-90);
			_ring.scaleX = _ring.scaleY = 0.1;
			_ring2.scaleX = _ring2.scaleY = 0.1;
			
			Starling.juggler.tween(_ring, 1.4, {alpha:0, scaleX:10, scaleY:28, transition:Transitions.EASE_OUT});
			Starling.juggler.tween(_ring2, 1.2, {alpha:0, scaleX:14, scaleY:30, transition:Transitions.EASE_OUT, delay:0.2});
		}
		
		override public function dispose():void
		{
			super.dispose();
			Starling.juggler.removeTweens(_ring);
			Starling.juggler.removeTweens(_ring2);
			
			_ring = null;
			_ring2 = null;
		}
	}
}