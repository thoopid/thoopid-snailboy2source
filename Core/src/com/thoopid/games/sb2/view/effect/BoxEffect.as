package com.thoopid.games.sb2.view.effect
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	public class BoxEffect extends Sprite
	{
		private var _particle:PoolImage;
		private var _glow:PoolImage;
		private var _particle2:PoolImage;
		
		public function BoxEffect()
		{
			super();
			
			this.addChild(_glow = Factory.imagePool.getImage(Assets.assets.getTexture("ib.glowFX"),true));
			this.addChild(_particle = Factory.imagePool.getImage(Assets.assets.getTexture("ib.particleFX"),true));
			this.addChild(_particle2 = Factory.imagePool.getImage(Assets.assets.getTexture("ib.particleFX"),true));
			
			_glow.scaleX = _glow.scaleY = 0.6;
			_particle.scaleX = _particle.scaleY = 0.6;
			_particle2.scaleX = _particle2.scaleY = 0.5;
			_particle.alpha = 0;
			_particle2.alpha = 0;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function set enabled(v:Boolean):void
		{
			if(v)
			{
				Starling.juggler.tween(_glow, 1.4, {scaleX:1, scaleY:1, repeatCount:999, reverse:true, transition:Transitions.EASE_OUT});
				Starling.juggler.tween(_glow, 15, {rotation:deg2rad(360), repeatCount:999, transition:Transitions.LINEAR});
				Starling.juggler.tween(_particle, 2.4, {scaleX:0.8, scaleY:0.8, alpha:0.5, rotation:deg2rad(10), repeatCount:999, reverse:true, transition:Transitions.EASE_OUT});
				Starling.juggler.tween(_particle2, 3, {scaleX:0.9, scaleY:0.9, alpha:0.5, rotation:deg2rad(-20), repeatCount:999, reverse:true, transition:Transitions.EASE_OUT});
			} else
			{
				Starling.juggler.removeTweens(_glow);
				Starling.juggler.removeTweens(_particle);
				Starling.juggler.removeTweens(_particle2);
			}
		}
		
		override public function dispose():void
		{
			Starling.juggler.removeTweens(_glow);
			Starling.juggler.removeTweens(_particle);
			Starling.juggler.removeTweens(_particle2);
			super.dispose();
		}
	}
}