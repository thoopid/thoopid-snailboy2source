package com.thoopid.games.sb2.view.effect
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.DelayedCall;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class LevelEndCaveEffect extends Sprite
	{
		private var _smokeLayers:Array;
		private var _dc:DelayedCall;
		private var _layer1:PoolImage;
		private var _layer2:PoolImage;
		public function LevelEndCaveEffect()
		{
			super();
			
//			_smokeLayers = [];

//			var layer:Image;
//			for (var i:int = 0; i < 10; i++) 
//			{
//				this.addChild(layer = Factory.imagePool.getImage(Assets.assets.getTexture("g.levelExitFX"),true));
//				layer.color = 0xd2f5f5;
//				layer.blendMode = BlendMode.SCREEN;
//				_smokeLayers.push(layer);
//				
//				this.addChild(layer = Factory.imagePool.getImage(Assets.assets.getTexture("iq.dustFX"),true));
//				layer.color = 0xd2f5f5;
//				_smokeLayers.push(layer);
//			}
			
			this.addChild(_layer1 = Factory.imagePool.getImage(Assets.assets.getTexture("g.levelExit.rays"),true));
			this.addChild(_layer2 = Factory.imagePool.getImage(Assets.assets.getTexture("g.levelExit.rays"),true));
			
			_layer1.scaleX = _layer1.scaleY = 1.2;
			_layer2.scaleX = _layer2.scaleY = 0.85;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function set x(value:Number):void
		{
			super.x = 6;
		}
		
		override public function set y(value:Number):void
		{
			super.y = 6;
		}
		
		override public function dispose():void
		{
			super.dispose();
			enabled = false;
			
			var i:int = this.numChildren;
			while(i--)
			{
				this.removeChildAt(i);
			}
//			for (var i:int = 0; i < _smokeLayers.length; i++) 
//			{
//				Starling.juggler.removeTweens(_smokeLayers[i]);
//			}
//			
//			if(_dc) Starling.juggler.remove(_dc);
//			
//			_smokeLayers = null;
			
		}
		
		public function set enabled(v:Boolean):void
		{
			if(v)
			{
				this.removeEventListener(Event.ENTER_FRAME, loop);
				this.addEventListener(Event.ENTER_FRAME, loop);
			} else
			{
				this.removeEventListener(Event.ENTER_FRAME, loop);
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function start():void
		{
//			for (var i:int = 0; i < _smokeLayers.length; i++) 
//			{
//				_dc = Starling.juggler.delayCall(resetLayer, 0.4 * i, _smokeLayers[i]);
//			}
			
			
		}
		
		private function loop(e:Event):void
		{
			_layer1.rotation += 0.01;
			_layer2.rotation -= 0.013;
		}
		
//		private function resetLayer(layer:Image):void
//		{
//			Starling.juggler.removeTweens(layer);
//			
//			layer.alpha = 0;
//			var s:Number = RandomRange.randRange(0.8,1);
//			layer.scaleX = s;
//			layer.scaleY = s;
//			layer.rotation = deg2rad(RandomRange.randRange(0,90));
//			Starling.juggler.tween(layer, 0.3, {alpha:0.25, transition:Transitions.LINEAR});
//			Starling.juggler.tween(layer, 7.6, {scaleX:0, rotation:deg2rad(layer.rotation + RandomRange.randRange(120, 260)), scaleY:0, transition:Transitions.EASE_OUT, onComplete:resetLayer, onCompleteArgs:[layer]});
//			
//		}
	}
}