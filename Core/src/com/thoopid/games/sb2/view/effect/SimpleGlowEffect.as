package com.thoopid.games.sb2.view.effect
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class SimpleGlowEffect extends Sprite
	{
		private var _glow:PoolImage;
		public function SimpleGlowEffect()
		{
			super();
			
			this.addChild(_glow = Factory.imagePool.getImage(Assets.assets.getTexture("ic.glowFX"),true));
			_glow.alpha = 0.2;
			Starling.juggler.tween(_glow, 1.4, {alpha:1, repeatCount:999, reverse:true, transition:Transitions.EASE_OUT});
		}
		
		override public function dispose():void
		{
			super.dispose();
			Starling.juggler.removeTweens(_glow);
		}
	}
}