package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import citrus.objects.CitrusSprite;
	
	import nape.phys.Body;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementBaseView;
	
	public class AreaElement extends CitrusSprite implements IEnableable
	{
		public var areaId:String = "";
		
		protected var _areaElement:AreaElementBaseView;
		protected var _isRendered:Boolean = true;
		
		public function AreaElement(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isRendered():Boolean
		{
			return _isRendered;
		}
		
		public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			
			_isRendered = v;
			_areaElement.visible = _isRendered;
		}
		
		public function reset():void
		{
			
		}
		
		public function get body():Body
		{
			return null;
		}
	}
}