package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Chest;

	public class AreaElement_Chest extends AreaElement
	{
		public function AreaElement_Chest(name:String, params:Object=null)
		{
			super(name, params);
			this.areaId = LevelNames.KINGDOM_1_AREA_3;
			this.view = _areaElement = new AreaElementView_Chest(name);
		}
	}
}