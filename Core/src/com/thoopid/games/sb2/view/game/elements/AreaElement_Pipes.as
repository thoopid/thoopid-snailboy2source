package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Pipes;

	public class AreaElement_Pipes extends AreaElement
	{
		public function AreaElement_Pipes(name:String, params:Object=null)
		{
			super(name, params);
			this.areaId = LevelNames.KINGDOM_1_AREA_2;
			this.view = _areaElement = new AreaElementView_Pipes(name);
		}
	}
}