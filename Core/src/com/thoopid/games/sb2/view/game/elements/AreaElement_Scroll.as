package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Scroll;

	public class AreaElement_Scroll extends AreaElement
	{
		public function AreaElement_Scroll(name:String, params:Object=null)
		{
			super(name, params);
			this.areaId = LevelNames.KINGDOM_1_AREA_5;
			this.view = _areaElement = new AreaElementView_Scroll(name);
		}
	}
}