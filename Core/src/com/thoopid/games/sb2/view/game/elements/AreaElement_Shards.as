package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Shards;
	
	public class AreaElement_Shards extends AreaElement
	{
		public function AreaElement_Shards(name:String, params:Object=null)
		{
			super(name, params);
			this.areaId = LevelNames.KINGDOM_1_AREA_1;
			this.view = _areaElement = new AreaElementView_Shards(name);
		}
	}
}