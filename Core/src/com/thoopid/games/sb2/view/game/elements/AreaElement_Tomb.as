package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementView_Tomb;

	public class AreaElement_Tomb extends AreaElement
	{
		public function AreaElement_Tomb(name:String, params:Object=null)
		{
			super(name, params);
			this.areaId = LevelNames.KINGDOM_1_AREA_4;
			this.view = _areaElement = new AreaElementView_Tomb(name);
		}
	}
}