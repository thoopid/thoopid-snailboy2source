package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import citrus.objects.CitrusSprite;
	
	import nape.phys.Body;
	import com.thoopid.games.sb2.view.game.elements.ui.KingdomElementBaseView;
	
	public class KingdomElement extends CitrusSprite implements IEnableable
	{
		public var kingdomId:String = "";
		
		protected var _isRendered:Boolean = true;
		protected var _base:KingdomElementBaseView;
		
		public function KingdomElement(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isRendered():Boolean
		{
			return _isRendered;
		}
		
		public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			
			_isRendered = v;
			_base.visible = _isRendered;
		}
		
		public function reset():void
		{
			
		}
		
		public function get body():Body
		{
			return null;
		}
	}
}