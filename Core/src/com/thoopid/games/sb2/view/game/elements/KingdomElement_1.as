package com.thoopid.games.sb2.view.game.elements
{
	import com.thoopid.games.sb2.view.game.elements.ui.KingdomElementView_1;
	
	public class KingdomElement_1 extends KingdomElement
	{
		public function KingdomElement_1(name:String, params:Object=null)
		{
			super(name, params);
			
			this.view = _base = new KingdomElementView_1(name);
		}
	}
}