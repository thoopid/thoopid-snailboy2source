package com.thoopid.games.sb2.view.game.elements.mediators
{
	import com.thoopid.games.sb2.controller.signals.story.RevealAreaCollectableSignal;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.game.elements.ui.AreaElementBaseView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class AreaElementBaseMediator extends StarlingMediator
	{
		[Inject]
		public var view:AreaElementBaseView;
		
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var revealAreaCollectableSignal:RevealAreaCollectableSignal;
		
		public function AreaElementBaseMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			revealAreaCollectableSignal.addOnce(onRevealHandler);
			
			view.reflectCollected(playerService.getAreaItemsCollectedCount(view.areaId));
		}
		
		override public function onRemove():void
		{
			revealAreaCollectableSignal.remove(onRevealHandler);
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onRevealHandler(id:String):void
		{
			if(view.areaId == id)
			{
				view.revealCollected();
			}
		}
	}
}