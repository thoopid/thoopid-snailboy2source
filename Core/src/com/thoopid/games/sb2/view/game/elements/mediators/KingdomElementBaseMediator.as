package com.thoopid.games.sb2.view.game.elements.mediators
{
	import com.thoopid.games.sb2.controller.signals.story.RevealKingdomCollectableSignal;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.game.elements.ui.KingdomElementBaseView;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class KingdomElementBaseMediator extends StarlingMediator
	{
		[Inject]
		public var view:KingdomElementBaseView;
		
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var revealKingdomCollectableSignal:RevealKingdomCollectableSignal;
		
		public function KingdomElementBaseMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			revealKingdomCollectableSignal.addOnce(onRevealHandler);
			
			view.reflectCollected(playerService.getKingdomCollectedCount(view.kingdomId));
		}
		
		override public function onRemove():void
		{
			revealKingdomCollectableSignal.remove(onRevealHandler);
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onRevealHandler(kingdomId:String):void
		{
			view.revealCollected();
		}
	}
}