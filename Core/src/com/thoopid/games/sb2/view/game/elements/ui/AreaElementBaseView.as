package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.view.effect.AreaRevealEffect;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class AreaElementBaseView extends Sprite
	{
		public var areaId:String = "";
		
		public function AreaElementBaseView(areaId:String)
		{
			this.areaId = areaId;
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function reflectCollected(count:Number):void
		{
			
		}
		
		public function revealCollected():void
		{
			this.addChild(new AreaRevealEffect());
		}
		
		private function onParticleDone(e:Event):void
		{
			
		}
	}
}