package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;
	
	public class AreaElementView_Chest extends AreaElementBaseView
	{
		private var _locksOpen:Array = [];
		private var _locksShut:Array = [];
		private var _orb:Image;
		private var _chestShut:Image;
		private var _chestOpen:Image;
		
		public function AreaElementView_Chest(id:String)
		{
			super(LevelNames.KINGDOM_1_AREA_3);
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(id), Assets.assets);
			
			_locksOpen.push(LayoutBuilder.findChild(this,"chestLockOpen_1"));
			_locksOpen.push(LayoutBuilder.findChild(this,"chestLockOpen_2"));
			_locksOpen.push(LayoutBuilder.findChild(this,"chestLockOpen_3"));
			_locksOpen.push(LayoutBuilder.findChild(this,"chestLockOpen_4"));
			_locksOpen.push(LayoutBuilder.findChild(this,"chestLockOpen_5"));
			
			_locksShut.push(LayoutBuilder.findChild(this,"chestLockShut_1"));
			_locksShut.push(LayoutBuilder.findChild(this,"chestLockShut_2"));
			_locksShut.push(LayoutBuilder.findChild(this,"chestLockShut_3"));
			_locksShut.push(LayoutBuilder.findChild(this,"chestLockShut_4"));
			_locksShut.push(LayoutBuilder.findChild(this,"chestLockShut_5"));
			
			_orb = LayoutBuilder.findChild(this,"shell") as Image;
			
			_chestShut = LayoutBuilder.findChild(this,"chestShut") as Image;
			
			_chestOpen = LayoutBuilder.findChild(this,"chestOpen") as Image;
			
			_chestOpen.visible = false;
			_orb.visible = false;
			
			//			
			var i:int = _locksOpen.length;
			while(i--)
			{
				_locksOpen[i].visible = false;
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function reflectCollected(count:Number):void
		{
			Starling.juggler.delayCall(showShards, 2, count);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function showShards(count:Number):void
		{
			for (var i:int = 0; i < _locksOpen.length; i++) 
			{
				if(i < count) 
				{
					_locksOpen[i].visible = true;
					_locksOpen[i].alpha = 0;
					Starling.juggler.tween(_locksOpen[i], 0.6, {alpha:1, scaleX:1, scaleY:1, transition:Transitions.EASE_OUT_BACK, delay:0.2 * i});
					
					Starling.juggler.tween(_locksShut[i], 0.6, {alpha:0, y:30, transition:Transitions.EASE_OUT_BACK, delay:0.2 * i});
				}
			}
			
			_chestOpen.visible = count == 5;
			_chestShut.visible = count < 5;
		}
	}
}