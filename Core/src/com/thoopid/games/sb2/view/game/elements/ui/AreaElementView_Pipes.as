package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;

	public class AreaElementView_Pipes extends AreaElementBaseView
	{
		private var _valves:Vector.<Image> = new Vector.<Image>();
		private var _lights:Vector.<Image> = new Vector.<Image>();
		private var _shell:DisplayObject;
		
		public function AreaElementView_Pipes(id:String)
		{
			super(LevelNames.KINGDOM_1_AREA_2);
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(id), Assets.assets);
			
			_valves.push(LayoutBuilder.findChild(this,"valve_1"));
			_valves.push(LayoutBuilder.findChild(this,"valve_2"));
			_valves.push(LayoutBuilder.findChild(this,"valve_3"));
			_valves.push(LayoutBuilder.findChild(this,"valve_4"));
			_valves.push(LayoutBuilder.findChild(this,"valve_5"));
			
			_lights.push(LayoutBuilder.findChild(this,"light_1"));
			_lights.push(LayoutBuilder.findChild(this,"light_2"));
			_lights.push(LayoutBuilder.findChild(this,"light_3"));
			_lights.push(LayoutBuilder.findChild(this,"light_4"));
			_lights.push(LayoutBuilder.findChild(this,"light_5"));
			
			_shell = LayoutBuilder.findChild(this,"shell");
			
			var i:int = _valves.length;
			while(i--)
			{
				_valves[i].visible = false;
			}
			
			i = _lights.length;
			while(i--)
			{
				_lights[i].visible = false;
			}
			
			_shell.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function reflectCollected(count:Number):void
		{
			Starling.juggler.delayCall(showShards, 2, count);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function showShards(count:Number):void
		{
			for (var i:int = 0; i < _valves.length; i++) 
			{
				if(i < count) 
				{
					_valves[i].visible = true;
					_valves[i].alpha = 0;
					var posY:Number = _valves[i].y;
					_valves[i].y = _valves[i].y - 15;
					Starling.juggler.tween(_valves[i], 0.6, {alpha:1, y:posY, transition:Transitions.EASE_OUT, delay:0.2 * i});
					
					_lights[i].visible = true;
					_lights[i].alpha = 0;
					Starling.juggler.tween(_lights[i], 0.8, {alpha:1, transition:Transitions.EASE_OUT_BACK, delay:0.2 * i});
				}
			}
			
			if(count >= 5)
			{
				_shell.visible = true;
				_shell.alpha = 0;
				var toY:Number = _shell.y;
				_shell.y = _shell.y + 50;
				Starling.juggler.tween(_shell, 1, {y:toY, alpha:1, transition:Transitions.EASE_OUT_BACK});
			}
		}
	}
}