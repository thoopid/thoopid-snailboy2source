package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;

	public class AreaElementView_Scroll extends AreaElementBaseView
	{
		public function AreaElementView_Scroll(id:String)
		{
			super(LevelNames.KINGDOM_1_AREA_3);
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(id), Assets.assets);
		}
	}
}