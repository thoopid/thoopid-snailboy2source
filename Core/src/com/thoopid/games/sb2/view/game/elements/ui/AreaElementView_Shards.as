package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;

	public class AreaElementView_Shards extends AreaElementBaseView
	{
		private var _baseImage:PoolImage;
		
		private var _shards:Vector.<Image> = new Vector.<Image>();
		private var _symbols:Vector.<Image> = new Vector.<Image>();
		
		
		public function AreaElementView_Shards(id:String)
		{
			super(LevelNames.KINGDOM_1_AREA_1);
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(id), Assets.assets);
			
			_shards.push(LayoutBuilder.findChild(this,"shard_1"));
			_shards.push(LayoutBuilder.findChild(this,"shard_2"));
			_shards.push(LayoutBuilder.findChild(this,"shard_3"));
			_shards.push(LayoutBuilder.findChild(this,"shard_4"));
			_shards.push(LayoutBuilder.findChild(this,"shard_5"));
			
			_symbols.push(LayoutBuilder.findChild(this,"fx_1"));
			_symbols.push(LayoutBuilder.findChild(this,"fx_2"));
			_symbols.push(LayoutBuilder.findChild(this,"fx_3"));
			_symbols.push(LayoutBuilder.findChild(this,"fx_4"));
			_symbols.push(LayoutBuilder.findChild(this,"fx_5"));

			var i:int = _shards.length;
			while(i--)
			{
				_shards[i].visible = false;
			}
			
			i = _symbols.length
			while(i--)
			{
				_symbols[i].visible = false;
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function reflectCollected(count:Number):void
		{
			Starling.juggler.delayCall(showShards, 1, count);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function showShards(count:Number):void
		{
			for (var i:int = 0; i < _shards.length; i++) 
			{
				if(i < count) 
				{
					_shards[i].visible = true;
					_shards[i].alpha = 0;
					_shards[i].scaleX = 0;
					_shards[i].scaleY = 0;
					Starling.juggler.tween(_shards[i], 0.6, {alpha:1, scaleX:1, scaleY:1, transition:Transitions.EASE_OUT_BACK, delay:0.2 * i});
					
					_symbols[i].visible = true;
					_symbols[i].alpha = 0;
					Starling.juggler.tween(_symbols[i], 0.8, {alpha:1, transition:Transitions.EASE_OUT_BACK, delay:0.2 * i});
				}
			}
		}
	}
}