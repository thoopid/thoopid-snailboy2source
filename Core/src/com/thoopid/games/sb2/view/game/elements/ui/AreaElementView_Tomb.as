package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;

	public class AreaElementView_Tomb extends AreaElementBaseView
	{
		private var _fx:Vector.<Image> = new Vector.<Image>();
		private var _leftDoor:Image;
		private var _rightDoor:Image;
		private var _shell:DisplayObject;
		
		public function AreaElementView_Tomb(id:String)
		{
			super(LevelNames.KINGDOM_1_AREA_4);
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(id), Assets.assets);
			
			_fx.push(LayoutBuilder.findChild(this,"rockFX_1") as Image);
			_fx.push(LayoutBuilder.findChild(this,"rockFX_2") as Image);
			_fx.push(LayoutBuilder.findChild(this,"rockFX_3") as Image);
			_fx.push(LayoutBuilder.findChild(this,"rockFX_4") as Image);
			_fx.push(LayoutBuilder.findChild(this,"rockFX_5") as Image);
			
			_leftDoor = LayoutBuilder.findChild(this,"door_left") as Image;
			_rightDoor = LayoutBuilder.findChild(this,"door_right") as Image;
			
			_shell = LayoutBuilder.findChild(this,"shell");
			
			var i:int = _fx.length;
			while(i--)
			{
				_fx[i].visible = false;
			}
			
			_shell.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function reflectCollected(count:Number):void
		{
			Starling.juggler.delayCall(showShards, 1, count);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function showShards(count:Number):void
		{
			for (var i:int = 0; i < _fx.length; i++) 
			{
				if(i < count) 
				{
					_fx[i].visible = true;
					_fx[i].alpha = 0;
					Starling.juggler.tween(_fx[i], 1, {alpha:1, transition:Transitions.EASE_OUT, delay:0.2 * i});
				}
			}
			
			if(count >= 5)
			{
				_shell.visible = true;
				_shell.alpha = 0;
				Starling.juggler.tween(_shell, 1.5, {alpha:1, transition:Transitions.EASE_OUT_BACK});
				
				Starling.juggler.tween(_leftDoor, 1, {x:_leftDoor.x - 35, transition:Transitions.EASE_OUT});
				Starling.juggler.tween(_rightDoor, 1, {x:_rightDoor.x + 35, transition:Transitions.EASE_OUT});
			}
		}
	}
}