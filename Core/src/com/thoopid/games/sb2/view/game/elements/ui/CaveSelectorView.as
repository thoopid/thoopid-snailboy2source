package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.model.game.LevelVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	import com.thoopid.games.sb2.view.ui.screens.levelresult.BadgeIcon;
	import com.thoopid.games.sb2.view.ui.screens.levelresult.CaveItemIcon;
	
	import flash.geom.Point;
	
	import feathers.controls.Label;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class CaveSelectorView extends Sprite
	{
		private var _active:Boolean = false;
		
		private var _id:String;
		private var _empty:Quad;
		
		private var _deathBadge:BadgeIcon;
		private var _slimeBadge:BadgeIcon;
		private var _secretBadge:BadgeIcon;
		private var _mcItem:CaveItemIcon;
		private var _jumpBadge:BadgeIcon;
		private var _btnPlay:CenteredButton;
		private var _label:Label;
		private var _bubble:Image;
		
		private var _deathStartP:Point;
		private var _slimeStartP:Point;
		private var _secretStartP:Point;
		private var _itemStartP:Point;
		private var _jumpStartP:Point;
		private var _btnLock:DisplayObject;
		
		public function CaveSelectorView(id:String)
		{
			super();
			
			_id = id;
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA("CaveSelectItem"), Assets.assets);
			
			_label = (LayoutBuilder.findChild(this, "tfLevelName") as Label);
			_label.text = "0"+_id.split("_")[_id.split("_").length-1];
			
			_bubble = (LayoutBuilder.findChild(this,"mcBubble") as Image);
			
			_deathBadge = (LayoutBuilder.findChild(this,"deathsBadge") as BadgeIcon);
			_slimeBadge = (LayoutBuilder.findChild(this,"slimeBadge") as BadgeIcon);
			_secretBadge = (LayoutBuilder.findChild(this,"secretAreaBadge") as BadgeIcon);
			_jumpBadge = (LayoutBuilder.findChild(this,"jumpBadge") as BadgeIcon);
			_mcItem = (LayoutBuilder.findChild(this,"mcItem") as CaveItemIcon);
			_btnPlay = (LayoutBuilder.findChild(this,"btnPlay") as CenteredButton);
			_btnLock = (LayoutBuilder.findChild(this,"btnLocked") as CenteredButton);
			
			_deathStartP = new Point(_deathBadge.x, _deathBadge.y);
			_slimeStartP = new Point(_slimeBadge.x, _slimeBadge.y);
			_secretStartP = new Point(_secretBadge.x, _secretBadge.y);
			_jumpStartP = new Point(_jumpBadge.x, _jumpBadge.y);
			_itemStartP = new Point(_mcItem.x, _mcItem.y);
			
			// Reset to center
			_btnPlay.alpha = 0;
			_btnPlay.scaleX = 0;
			_btnPlay.scaleY = 0;
			
			_deathBadge.alpha = 0;
			_deathBadge.x = 0;
			_deathBadge.y = 0;
			
			_slimeBadge.alpha = 0;
			_slimeBadge.x = 0;
			_slimeBadge.y = 0;
			
			_secretBadge.alpha = 0;
			_secretBadge.x = 0;
			_secretBadge.y = 0;
			
			_jumpBadge.alpha = 0;
			_jumpBadge.x = 0;
			_jumpBadge.y = 0;
			
			_mcItem.alpha = 0;
			_mcItem.x = 0;
			_mcItem.y = 0;
			
			
//			this.addChild(_empty = Factory.imagePool.getImage(Assets.assets.getTexture("empty"),true));
			this.addChild(_empty = new Quad(50,50));
			_empty.alpha = 0;
			_empty.width = 50;
			_empty.height = 50;
		}

		public function get active():Boolean
		{
			return _active;
		}

		public function get id():String
		{
			return _id;
		}
		
		public function reflectModel(levelVO:LevelVO):void
		{
			this.touchable = false;
			if(levelVO == null) levelVO = new LevelVO();
			
			_deathBadge.enable = levelVO.deathBadgeCollected;
			_slimeBadge.enable = levelVO.slimeBadgeCollected;
			_secretBadge.enable = levelVO.secretAreaBadgeCollected;
			
			_mcItem.enable = levelVO.storyItemCollected;
			
			_btnLock.visible = levelVO.locked;
			_bubble.visible = !levelVO.locked;
			_label.visible = !levelVO.locked;
			_btnPlay.visible = !levelVO.locked;
			
			this.touchable = !levelVO.locked;
		}
		
		public function toggle(t:Boolean):void
		{
			_active = t;
			var time:Number = 0.6;
			
			_bubble.visible = !t;
			_label.visible = !t;
//			_btnPlay.visible = t;
			
			Starling.juggler.tween(_btnPlay, time, {scaleX:(t) ? 1 : 0, scaleY:(t) ? 1 : 0, alpha:(t) ? 1 : 0, transition:Transitions.EASE_OUT_BACK});
			
			Starling.juggler.tween(_deathBadge, time, {x:(t) ? _deathStartP.x : 0, y:(t) ? _deathStartP.y : 0, alpha:(t) ? 1 : 0, transition:Transitions.EASE_OUT_BACK});
			Starling.juggler.tween(_slimeBadge, time, {delay:0.05, x:(t) ? _slimeStartP.x : 0, y:(t) ? _slimeStartP.y : 0, alpha:(t) ? 1 : 0, transition:Transitions.EASE_OUT_BACK});
			Starling.juggler.tween(_secretBadge, time, {delay:0.1, x:(t) ? _secretStartP.x : 0, y:(t) ? _secretStartP.y : 0, alpha:(t) ? 1 : 0, transition:Transitions.EASE_OUT_BACK});
			Starling.juggler.tween(_jumpBadge, time, {delay:0.15, x:(t) ? _jumpStartP.x : 0, y:(t) ? _jumpStartP.y : 0, alpha:(t) ? 1 : 0, transition:Transitions.EASE_OUT_BACK});
			Starling.juggler.tween(_mcItem, time, {delay:0.2, x:(t) ? _itemStartP.x : 0, y:(t) ? _itemStartP.y : 0, alpha:(t) ? 1 : 0, transition:Transitions.EASE_OUT_BACK});
				
				
		}
	}
}