package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class CaveSelectorViewMediator extends StarlingMediator
	{
		[Inject]
		public var view:CaveSelectorView;
		
		[Inject]
		public var playerService:PlayerService;
		
		public function CaveSelectorViewMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			view.reflectModel(playerService.getPlayerLevel(view.id));
		}
	}
}