package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.view.effect.AreaRevealEffect;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.particles.PDParticleSystem;
	
	public class KingdomElementBaseView extends Sprite
	{
		public var kingdomId:String;
		public function KingdomElementBaseView(id:String)
		{
			super();
			kingdomId = id;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function reflectCollected(count:Number):void
		{
			
		}
		
		override public function dispose():void
		{
			
			super.dispose();
		}
		
		public function revealCollected():void
		{
			Starling.juggler.delayCall(addEffect,0.5);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onParticleDone(e:Event):void
		{
			var s:PDParticleSystem = e.target as PDParticleSystem;
			if(s)
			{
				Starling.juggler.remove(s);
				s.removeFromParent();
			}
		}
		
		private function addEffect():void
		{
			var effect:AreaRevealEffect = new AreaRevealEffect();
			effect.y = 184;
			this.addChild(effect);
			
		}
	}
}