package com.thoopid.games.sb2.view.game.elements.ui
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;

	public class KingdomElementView_1 extends KingdomElementBaseView
	{
		private var _baseImage:PoolImage;
		private var _shells:Vector.<Image> = new Vector.<Image>();
		
		public function KingdomElementView_1(id:String)
		{
			super(LevelNames.KINGDOM_1);
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(id), Assets.assets);
			
			_shells.push(LayoutBuilder.findChild(this,"shell_01"));
			_shells.push(LayoutBuilder.findChild(this,"shell_02"));
			_shells.push(LayoutBuilder.findChild(this,"shell_03"));
			_shells.push(LayoutBuilder.findChild(this,"shell_04"));
			_shells.push(LayoutBuilder.findChild(this,"shell_05"));
			
			var i:int = _shells.length;
			while(i--)
			{
				_shells[i].visible = false;
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function reflectCollected(count:Number):void
		{
			Starling.juggler.delayCall(showCrystals, 3, count);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function showCrystals(count:Number):void
		{
			for (var i:int = 0; i < _shells.length; i++) 
			{
				if(i < count) 
				{
					_shells[i].visible = true;
					_shells[i].alpha = 0;
//					_shells[i].scaleX = 0;
//					_shells[i].scaleY = 0;
					Starling.juggler.tween(_shells[i], 0.6, {alpha:1, scaleX:1, scaleY:1, transition:Transitions.EASE_OUT_BACK, delay:0.2 * i});
				}
			}
		}
	}
}