package com.thoopid.games.sb2.view.game.factory
{
	import com.thoopid.games.sb2.view.game.objects.ParalaxLayer;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.extensions.particles.PDParticleSystem;
	
	
	public class FireballEffectsFactory
	{
		private static var _instance:FireballEffectsFactory;
		private var _items:Vector.<PDParticleSystem>;
		private var _layer:ParalaxLayer;
		
		public function FireballEffectsFactory()
		{
			_items = new Vector.<PDParticleSystem>();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public static function getInstance():FireballEffectsFactory
		{
			if(!_instance)
			{
				_instance = new FireballEffectsFactory();
			}
			return _instance;
		}
		
		public function cache(v:Number=10, layer:ParalaxLayer=null):void
		{
			if(_items.length > 0) return;
			
			_layer = layer;
			var i:int = v;
			while(i--)
			{
				_items.push(buildParticles());
			}
		}
		
		public function add(s:PDParticleSystem):void
		{
			if(_items.indexOf(s) != -1) 
			{
				throw new Error("Pd System already exists");
				return;
			}
			_items.push(s);
		}
		
		public function getItem():PDParticleSystem
		{
			if(_items.length < 1)
			{
				// TODO - optimize for release
				throw new Error("OOPS Recheck this object pool: ");
				cache(1, _layer);
			}
			var s:PDParticleSystem = _items.shift();
			_layer.holder.addChild(s);
			return s;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function buildParticles():PDParticleSystem
		{
			var effect:PDParticleSystem = new PDParticleSystem(Assets.assets.getXml("LavaEffect"), Assets.assets.getTexture("lavaEffectTexture"));
//			var effect:PDParticleSystem = new PDParticleSystem(Assets.assets.getXml("LavaEffect"), Assets.assets.getTexture("pp.crystalA.fx.01"));
			effect.addEventListener(Event.COMPLETE, onParticleDone);
			return effect;
		}
		
		private function onParticleDone(e:Event):void
		{
			var s:PDParticleSystem = e.target as PDParticleSystem;
			if(s)
			{
				Starling.juggler.remove(s);
				s.removeFromParent();
				add(s);
			}
		}
		
		public function destroy():void
		{
			var i:int = _items.length;
			while(i--)
			{
				_items[i].removeFromParent(true);
			}
			_items = new Vector.<PDParticleSystem>();
			
			_layer = null;
		}
	}
}