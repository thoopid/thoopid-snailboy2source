package com.thoopid.games.sb2.view.game.factory
{
	import citrus.objects.NapePhysicsObject;
	
	import nape.space.Space;
	

	public class ObjectFactory
	{
		private var _items:Vector.<NapePhysicsObject>;
		private var _name:String;
		private var _space:Space;
		private var _type:Class;
		
		public function ObjectFactory(name:String)
		{
			_name = name;
			_items = new Vector.<NapePhysicsObject>();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get space():Space
		{
			return _space;
		}

		public function set space(value:Space):void
		{
			_space = value;
		}

		public function cache(v:Number, type:Class, params:Object):Vector.<NapePhysicsObject>
		{
			var i:int = v;
			_type = type;
			while(i--)
			{
				add(new type(_name+"_PoolObject_"+i,params));
			}
			return _items;
		}
		
		public function getItem():Object
		{
			if(_items.length < 1)
			{
				throw new Error("Out Of Slime!!!");
				return null;
			}
			var s:Object = _items.shift();
			if(s.body) s.body.space = _space;
			return s;
		}
		
		public function add(b:NapePhysicsObject):void
		{
			if(_items.indexOf(b) != -1) return;
			if(b.body) b.body.space = null;
			_items.push(b);
		}
		
		public function destroy():void
		{
			_items = new Vector.<NapePhysicsObject>();
		}
	}
}