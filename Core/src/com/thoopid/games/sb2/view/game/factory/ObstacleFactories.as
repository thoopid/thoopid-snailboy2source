package com.thoopid.games.sb2.view.game.factory
{
	import com.thoopid.games.sb2.view.game.objects.Bubble;
	import com.thoopid.games.sb2.view.game.objects.Mine;
	import com.thoopid.games.sb2.view.game.objects.SlimeCollectible;
	import com.thoopid.games.sb2.view.game.objects.VolcanoFireball;
	
	import nape.space.Space;

	public class ObstacleFactories
	{
		private static var _instance:ObstacleFactories;
		private var _volcanoMisslePool:ObjectFactory;
		private var _minesPool:ObjectFactory;
		private var _slimePool:ObjectFactory;
		private var _bubblePool:ObjectFactory;
		private var _space:Space;
		private var _inited:Boolean;
		
		public function ObstacleFactories()
		{
			
		}
		
		public function get space():Space
		{
			return _space;
		}

		public function set space(value:Space):void
		{
			_space = value;
			_volcanoMisslePool.space = value;
			_minesPool.space = value;
			slimePool.space = value;
			_bubblePool.space = value;
		}

		public function get volcanoMisslePool():ObjectFactory
		{
			return _volcanoMisslePool;
		}
		
		public function get minesPool():ObjectFactory
		{
			return _minesPool;
		}
		
		public function get slimePool():ObjectFactory
		{
			return _slimePool;
		}
		
		public function get bubblePool():ObjectFactory
		{
			return _bubblePool;
		}

		public static function getInstance():ObstacleFactories
		{
			if(!_instance)
			{
				_instance = new ObstacleFactories();
			}
			return _instance;
		}
		
		public function destroy():void
		{
			volcanoMisslePool.destroy();
			minesPool.destroy();
			slimePool.destroy();
			bubblePool.destroy();
			
			_volcanoMisslePool = null;
			_minesPool = null;
			_slimePool = null;
			_bubblePool = null;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(s:Space):void
		{
			
//			if(!_inited)
//			{
//				_inited = true;
				
				_volcanoMisslePool = new ObjectFactory("VolcanoMissiles");
				_volcanoMisslePool.cache(20,VolcanoFireball,{width:20, height:20, explodeDuration:2000, fuseDuration:1000});
				
				_minesPool = new ObjectFactory("Mines");
				_minesPool.cache(15,Mine,{width:40, height:40});
				
				_slimePool = new ObjectFactory("Slimeys");
				_slimePool.cache(1000,SlimeCollectible,{width:20, height:20});
				
				_bubblePool = new ObjectFactory("Bubbles");
				_bubblePool.cache(5,Bubble,{width:40, height:40});
//			}
			this.space = s;
			
		}
		
		//*********************
		//   PRIVATE
		//*********************
	}
}