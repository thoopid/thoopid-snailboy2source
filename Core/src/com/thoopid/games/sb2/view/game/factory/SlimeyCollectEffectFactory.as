package com.thoopid.games.sb2.view.game.factory
{
	import starling.core.Starling;
	import starling.events.Event;
	import starling.extensions.particles.PDParticleSystem;
	

	public class SlimeyCollectEffectFactory
	{
		private static var _instance:SlimeyCollectEffectFactory;
		private var _items:Vector.<PDParticleSystem>;
		public function SlimeyCollectEffectFactory()
		{
			_items = new Vector.<PDParticleSystem>();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public static function getInstance():SlimeyCollectEffectFactory
		{
			if(!_instance)
			{
				_instance = new SlimeyCollectEffectFactory();
			}
			return _instance;
		}
		
		public function cache(v:Number=10):void
		{
			if(_items.length > 0) return;
			
			var i:int = v;
			while(i--)
			{
				_items.push(buildParticles());
			}
		}
		
		public function add(s:PDParticleSystem):void
		{
			if(_items.indexOf(s) != -1) 
			{
				throw new Error("Pd System already exists");
				return;
			}
			_items.push(s);
		}
		
		public function getItem():PDParticleSystem
		{
			if(_items.length < 1)
			{
				// TODO - optimize for release
				throw new Error("OOPS Recheck this object pool: ");
				cache(1);
			}
			var s:PDParticleSystem = _items.shift();
			return s;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function buildParticles():PDParticleSystem
		{
			var coinCollectEffect:PDParticleSystem = new PDParticleSystem(Assets.assets.getXml("SlimeCollectParticleEffect"), Assets.assets.getTexture("SlimeEffectTexture"));
			coinCollectEffect.addEventListener(Event.COMPLETE, onParticleDone);
			return coinCollectEffect;
		}
		
		private function onParticleDone(e:Event):void
		{
			var s:PDParticleSystem = e.target as PDParticleSystem;
			if(s)
			{
//				trace("Reusing Slimey Particle system")
				Starling.juggler.remove(s);
				s.removeFromParent();
				add(s);
			}
		}
		
		public function destroy():void
		{
			var i:int = _items.length;
			while(i--)
			{
				_items[i].removeFromParent(true);
			}
			_items = new Vector.<PDParticleSystem>();
		}
	}
}