package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.level.base.UserinterfaceLevelBase;
	
	import flash.display.MovieClip;
	
	public class AchievementsLevel extends UserinterfaceLevelBase
	{
		
		public function AchievementsLevel(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, LevelNames.ACHIEVEMENTS_LEVEL);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function initialize():void
		{
			super.initialize();
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.ACHIEVEMENTS_SCREEN), Assets.assets);
		}
	}
}