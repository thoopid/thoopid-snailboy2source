package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	
	import starling.events.Event;
	import com.thoopid.games.sb2.view.game.level.base.UserInterfaceLevelBaseMediator;
	
	public class AchievementsLevelMediator extends UserInterfaceLevelBaseMediator
	{
		public function AchievementsLevelMediator()
		{
			super();
		}
		
		public function get achievementsLevel():AchievementsLevel
		{
			return view as AchievementsLevel;
		}
		
		override protected function onLevelReady():void
		{
			super.onLevelReady();
		}
		
		override public function onRemove():void
		{
			super.onRemove();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnClose":
					loadLevelSignal.dispatch(LevelNames.MENU_LEVEL);
					break;
			}
		}
	}
}