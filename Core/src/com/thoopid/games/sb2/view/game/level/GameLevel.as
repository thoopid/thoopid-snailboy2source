package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.constants.game.ObjectNames;
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.interfaces.game.IChest;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.game.GaugeVO;
	import com.thoopid.games.sb2.model.game.LevelVO;
	import com.thoopid.games.sb2.util.MathTrigUtil;
	import com.thoopid.games.sb2.view.game.level.base.LevelBase;
	import com.thoopid.games.sb2.view.game.level.util.TouchInputController;
	import com.thoopid.games.sb2.view.game.objects.CheckpointSensor;
	import com.thoopid.games.sb2.view.game.objects.CheckpointVisual;
	import com.thoopid.games.sb2.view.game.objects.CollectGemItem;
	import com.thoopid.games.sb2.view.game.objects.CollectStoryItem;
	import com.thoopid.games.sb2.view.game.objects.CollectableItem;
	import com.thoopid.games.sb2.view.game.objects.LevelEnd;
	import com.thoopid.games.sb2.view.game.objects.SecrectPortal;
	import com.thoopid.games.sb2.view.game.objects.SnailboyHero;
	import com.thoopid.games.sb2.view.game.objects.SnailboyHeroMarker;
	import com.thoopid.games.sb2.view.game.objects.TutorialSensor;
	
	import flash.geom.Point;
	
	import citrus.core.CitrusEngine;
	import citrus.objects.NapePhysicsObject;
	import citrus.physics.nape.NapeUtils;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.geom.Vec2;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	
	public class GameLevel extends LevelBase
	{
		// Interaction signals from a levels
		public var onCollectableItemSignal:Signal = new Signal(CollectableItem);
		public var onGaugeDepleteSignal:Signal = new Signal(Number);
		public var onGaugeSuperSimSignal:Signal = new Signal(Number);
		public var onHeroDieSignal:Signal = new Signal();
		public var onHeroSwimSignal:Signal = new Signal(Boolean);
		public var onTresureOpenSignal:Signal = new Signal();
		public var onSecretAreaSignal:Signal = new Signal();
		public var onTutorialSignal:Signal = new Signal(String);
		
		private var _touchInput:TouchInputController;
		private var _gameplayActive:Boolean = false;
		
		
		private var _gaugeModel:GaugeModel;
		
		// IEnableable Items
		private var _levelVO:LevelVO;
		private var _heroStart:Vec2;
		
		// Collectable Elements
		private var _gems:Vector.<CollectGemItem> = new Vector.<CollectGemItem>();

		public function GameLevel(mc:Object,name:String="0")
		{
			super(mc,name);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get touchInput():TouchInputController
		{
			return _touchInput;
		}

		public function init(gaugeModel:GaugeModel, levelVO:LevelVO):void
		{
			_gaugeModel = gaugeModel;
			_levelVO = levelVO;
		}
		
		public function reflectGaugeModel(gauge:GaugeVO):void
		{
			if(_touchInput) _touchInput.gauge = gauge;
		}
		
		override public function update(timeDelta:Number):void
		{
			if(_hero) _performanceTarget = _hero.body.position;
			
			_gaugeModel.update();
			
			super.update(timeDelta);
			
			if(_touchInput) _touchInput.update();
			
			// TODO move this to the Hero to update the model
			if(_hero && _hero.paddleing) onGaugeDepleteSignal.dispatch(_gaugeModel.gauge.swim.costSwim) //_gaugeModel.depGauge(0.5);
		}
		
		override public function restartLevel():void
		{
			super.restartLevel();
			
			// Reset Hero
			_hero.x = _heroStart.x;
			_hero.y = _heroStart.y;
		}
		
		override public function destroy():void
		{
			_physics.space.listeners.clear();
			
			super.destroy();
			
			onCollectableItemSignal.removeAll();
			onCollectableItemSignal = null;
			onGaugeDepleteSignal.removeAll();
			onGaugeDepleteSignal = null;
			onGaugeSuperSimSignal.removeAll();
			onGaugeSuperSimSignal = null;
			onHeroDieSignal.removeAll();
			onHeroDieSignal = null;
			onHeroSwimSignal.removeAll();
			onHeroSwimSignal = null;
			onTresureOpenSignal.removeAll();
			onTresureOpenSignal = null;
			onSecretAreaSignal.removeAll();
			onSecretAreaSignal = null;
			onTutorialSignal.removeAll();
			onTutorialSignal = null;
			
			_areaCenters = null;
			
			_touchInput.dispose();
			
		}
		
		override protected function buildLevel():void
		{
			super.buildLevel();
			
			// Hide Collectable Story item if already collected
			if(_levelVO.storyItemCollected)
			{
				var collectableStoryItem:CollectStoryItem = this.getObjectByName(ObjectNames.LEVEL_STORY_ITEM) as CollectStoryItem;
				if(collectableStoryItem) this.remove(collectableStoryItem);
			}
			
			//****************************************************************
			// Setup Level Item Callbacks
			//****************************************************************
			// Level End
			var levelEnd:LevelEnd = this.getObjectByName(ObjectNames.LEVEL_END) as LevelEnd;
			if(levelEnd)
			{
				levelEnd.onBeginContact.add(onLevelEndCallback);
			}
			
			var heroToCollectableListener:InteractionListener = new InteractionListener(CbEvent.BEGIN,InteractionType.ANY,CollisionConstants.heroCollitionType,CollisionConstants.collectableCollitionType,heroToCollectable)
			var heroToSecretAreaListener:InteractionListener = new InteractionListener(CbEvent.BEGIN,InteractionType.SENSOR,CollisionConstants.heroCollitionType,CollisionConstants.secretAreaCollitionType,heroToSecretArea)
			var heroToCrateListener:InteractionListener = new InteractionListener(CbEvent.BEGIN,InteractionType.SENSOR,CollisionConstants.heroCollitionType,CollisionConstants.crateCollitionType,heroToCrate)
			var heroToTutListener:InteractionListener = new InteractionListener(CbEvent.BEGIN,InteractionType.SENSOR,CollisionConstants.heroCollitionType,CollisionConstants.tutorialCollitionType,heroToTut)
			var heroToTutExitListener:InteractionListener = new InteractionListener(CbEvent.END,InteractionType.SENSOR,CollisionConstants.heroCollitionType,CollisionConstants.tutorialCollitionType,heroToTutExit)
			var heroToCheckpointListener:InteractionListener = new InteractionListener(CbEvent.BEGIN,InteractionType.SENSOR,CollisionConstants.heroCollitionType,CollisionConstants.checkpointCollitionType,heroToCheckpoint)
			_physics.space.listeners.add(heroToCollectableListener);
			_physics.space.listeners.add(heroToSecretAreaListener);
			_physics.space.listeners.add(heroToTutListener);
			_physics.space.listeners.add(heroToTutExitListener);
			_physics.space.listeners.add(heroToCrateListener);
			_physics.space.listeners.add(heroToCheckpointListener);
			//****************************************************************
			
			// Setup all the collectable gems
			_gems = Vector.<CollectGemItem>(getObjectsByType(CollectGemItem));
			
			// Setup Chest / Tresure Elements
			var chests:Vector.<IChest> = Vector.<IChest>(getObjectsByType(IChest));
			for each (var c:IChest in chests) 
			{
				c.onOpenSignal.add(onChestOpen);
			}
			
			_levelVO.maxSlimies = _totalSlimies;
		}
		
		override public function activateLevel():void
		{
			_hero.reset();
			_gameplayActive = true;
			if(_touchInput) _touchInput.enabled = true;
			
			super.activateLevel();
		}
		
		override public function pause():void
		{
			super.pause();
			
			CitrusEngine.getInstance().playing = false;
			_gameplayActive = false;
			
			if(_touchInput) _touchInput.enabled = false;
		}
		
		override public function softPause(toggle:Boolean):void
		{
			super.softPause(toggle);
			_physics.timeStep = (toggle) ? 0 : 0.05;
			
//			_hero.removeForces();
		}
		
		override public function resume():void
		{
			super.resume()
			CitrusEngine.getInstance().playing = true;
			_gameplayActive = true;
			
			if(_touchInput) _touchInput.enabled = true;
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function setupHero():void
		{
			// Hero
			var heroMarker:SnailboyHeroMarker = getObjectByName("heroPos") as SnailboyHeroMarker;
			if(heroMarker)
			{
				_heroStart = Vec2.get(heroMarker.x, heroMarker.y);
				_hero = new SnailboyHero({x:_heroStart.x,y:_heroStart.y, direction:heroMarker.defaultForward});
				_hero.onHeroDie.add(onHeroDie);
				this.remove(heroMarker);
				this.add(_hero);
			} else
			{
				return;
			}
			
			// Touch input
			_touchInput = new TouchInputController(_hero,_hero.heroView, Starling.current.stage, _physics, _gaugeModel.gauge, _camera);
			_touchInput.enabled = true;
			_touchInput.onLaunchSignal.add(onTouchLaunchHandler);
			_touchInput.onSuperJumpUpdateSignal.add(onSuperJumpUpdateHandler);
			_touchInput.onSwimStartSignal.add(onSwimHandler);
		}
		
		override protected function setupCamera():void
		{
			super.setupCamera();
			_camera.setUp(_hero, _mobileBounds,_camOffsetOriginal, new Point(0.07, 0.07));
		}
		
		override protected function resetAllItems():void
		{
			super.resetAllItems();
			
			var item:Vector.<CollectGemItem> = Vector.<CollectGemItem>(getObjectsByType(CollectGemItem));
			for each (var i:CollectGemItem in item) 
			{
				i.reset();
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		// ***********************************************
		// Sensor Trigger Handlers
		// ***********************************************
		
		private function heroToCollectable(ic:InteractionCallback):void
		{
			var collider:CollectableItem = NapeUtils.CollisionGetOther(_hero, ic) as CollectableItem;
			if(collider)
			{
				if(collider.collect())
				{
					onCollectableItemSignal.dispatch(collider);
				}
			}
		}
		private function heroToSecretArea(ic:InteractionCallback):void
		{
			var collider:SecrectPortal = NapeUtils.CollisionGetOther(_hero, ic) as SecrectPortal;
			if(collider)
			{
				onSecretAreaSignal.dispatch();
				collider.transport(_hero);
			}
		}
		private function heroToTutExit(ic:InteractionCallback):void
		{
			onTutorialSignal.dispatch(TutorialConstants.TUT_STEP_NONE);
		}
		
		private function heroToTut(ic:InteractionCallback):void
		{
			var collider:TutorialSensor = NapeUtils.CollisionGetOther(_hero, ic) as TutorialSensor;
			if(collider)
			{
				onTutorialSignal.dispatch(collider.id);
			}
		}
		private function heroToCheckpoint(ic:InteractionCallback):void
		{
			var collider:CheckpointSensor = NapeUtils.CollisionGetOther(_hero, ic) as CheckpointSensor;
			if(collider)
			{
				var cv:CheckpointVisual = getObjectByName(collider.id) as CheckpointVisual;
				if(cv)
				{
					_currentCheckpoint = collider;
					cv.toggle(true);
				}
			}
		}
		private function heroToCrate(ic:InteractionCallback):void
		{
			var collider:IChest = NapeUtils.CollisionGetOther(_hero, ic) as IChest;
			if(collider)
			{
				collider.open();
			}
		}
		
		private function onLevelEndCallback(ic:InteractionCallback):void
		{
			var exit:NapePhysicsObject = NapeUtils.CollisionGetOther(_hero, ic);
			_hero.levelEnd(exit);
			lvlEnded.dispatch();
			
			_gameplayActive = false;
			if(_touchInput) _touchInput.enabled = false;
		}
		
		// ***********************************************
		
		private function onSuperJumpUpdateHandler(percentage:Number):void
		{
			onGaugeSuperSimSignal.dispatch(percentage);
			_camera.setZoom(MathTrigUtil.limit((1-(percentage * 0.8)),0.55,1));
		}
		
		private function onTouchLaunchHandler(superJumpPercentage:Number):void
		{
			onGaugeDepleteSignal.dispatch(_gaugeModel.gauge.jump.costSuperJump * superJumpPercentage);
		}
		
		private function onSwimHandler(dir:Number):void
		{
			if(_softPaused) softPause(false);
			
			onHeroSwimSignal.dispatch(false);
		}
		
		private function onHeroDie():void
		{
			_gameplayActive = false;
			if(_touchInput) _touchInput.enabled = false;
			Starling.juggler.delayCall(resetToCheckpoint,3);
			
			onHeroDieSignal.dispatch();
		}
		
		private function resetToCheckpoint():void
		{
			if(_currentCheckpoint)
			{
				_hero.x = _currentCheckpoint.x;
				_hero.y = _currentCheckpoint.y - 100;
			}
			
			activateLevel();
		}
		
		private function onChestOpen(pos:Vec2, name:String):void
		{
			for each (var gem:CollectGemItem in _gems) 
			{
				if(gem.linkId == name) gem.reveal(pos);
			}
		}
	}
}