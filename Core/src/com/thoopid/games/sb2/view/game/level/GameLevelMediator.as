package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.controller.signals.game.CollectGameCollectableItemSignal;
	import com.thoopid.games.sb2.controller.signals.game.LevelCompleteSignal;
	import com.thoopid.games.sb2.controller.signals.game.PauseLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.ResumeLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.RetryLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.ToggleOraSignal;
	import com.thoopid.games.sb2.controller.signals.game.ToggleSoftPauseGame;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GameHudShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ToggleSwimControlsSignal;
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.game.GaugeVO;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.view.game.level.base.LevelBaseMediator;
	import com.thoopid.games.sb2.view.game.level.util.TouchInputController;
	import com.thoopid.games.sb2.view.game.objects.CollectableItem;
	
	import citrus.core.CitrusEngine;
	
	public class GameLevelMediator extends LevelBaseMediator
	{
		// Signals
		[Inject]
		public var levelCompleteSignal:LevelCompleteSignal;
		
			// In game actions to dispatch
			[Inject]
			public var collectGameCollectableItem:CollectGameCollectableItemSignal;
			
			// System Signals to respond to
			[Inject]
			public var pauseLevelSignal:PauseLevelSignal;
			[Inject]
			public var retryLevelSignal:RetryLevelSignal;
			[Inject]
			public var resumeLevelSignal:ResumeLevelSignal;
			[Inject]
			public var softPauseSignal:ToggleSoftPauseGame;
			
			// Ui Signals
			[Inject]
			public var gameHudShowSignal:GameHudShowSignal;
			[Inject]
			public var inventoryShowSignal:InventoryShowSignal;
			[Inject]
			public var toggleOraSignal:ToggleOraSignal;
			[Inject]
			public var triggerTutSignal:TriggerTutorialSignal;
			[Inject]
			public var toggleSwimControlsSignal:ToggleSwimControlsSignal;
		
		// model
		[Inject]
		public var gaugeModel:GaugeModel;
		[Inject]
		public var gameModel:GameModel;
		
		public function GameLevelMediator()
		{
			super();
			
			if(Config.DEBUG_CONSOLE)
			{
				CitrusEngine.getInstance().console.addCommand("completeLevel", onLevelEnded);
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get gameView():GameLevel
		{
			return view as GameLevel;
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			// These signals from the views get automatically removed
			//----------------------------------------------------------------
			gameView.lvlEnded.add(onLevelEnded);
			gameView.onCollectableItemSignal.add(onCollectableHandler);
			gameView.onGaugeDepleteSignal.add(onGaugeDepleteHandler);
			gameView.onGaugeSuperSimSignal.add(onGaugeSuperSimHandler);
			gameView.onHeroDieSignal.add(onHeroDieHandler);
			gameView.onSecretAreaSignal.add(onSecretAreaHandler);
			gameView.onTutorialSignal.add(onTutHandler);
			gameView.onHeroSwimSignal.add(onHeroSwimHandler);
			//----------------------------------------------------------------
			
			// Model signals
			gaugeModel.updateModelSignal.add(onGaugeModelUpdated);
			
			// System Signals
			pauseLevelSignal.add(onPauseHandler);
			retryLevelSignal.add(onRetryHandler);
			resumeLevelSignal.add(onResumeHandler);
			inventoryShowSignal.add(onInventoryShow);
			softPauseSignal.add(onSoftPause);
			
			// Setup New Game props
//			newGameSignal.dispatch(view.levelName);
			// Start init
			gameView.init(gaugeModel, gameModel.currentLevel);
		}
		
		override public function onRemove():void
		{
			gaugeModel.updateModelSignal.remove(onGaugeModelUpdated);
			
			pauseLevelSignal.remove(onPauseHandler);
			retryLevelSignal.remove(onRetryHandler);
			resumeLevelSignal.remove(onResumeHandler);
			inventoryShowSignal.remove(onInventoryShow);
			softPauseSignal.remove(onSoftPause);
			
			super.onRemove();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function onLevelReady():void
		{
			startNewGame();
			gameHudShowSignal.dispatch(true);
			super.onLevelReady();
		}
		
		override protected function onLevelRevealed():void
		{
			super.onLevelRevealed();
			
			// Use Ora if Exists, enable this Ora for the level
			var gaugeReplenish:InventoryItemVO = new InventoryItemVO().parse(playerService.inventoryGetItem(GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH));
			if(gaugeReplenish.quantity > 0) 
			{
				toggleOraSignal.dispatch(GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH, true);
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onGaugeModelUpdated(gauge:GaugeVO, isReplenish:Boolean):void
		{
			gameView.reflectGaugeModel(gauge);
		}
		
		// When the level is complete - promt the Level Result Screen
		private function onLevelEnded():void
		{
			levelCompleteSignal.dispatch();
		}
		
		private function onCollectableHandler(item:CollectableItem):void
		{
			collectGameCollectableItem.dispatch(item);
		}
		
		private function onGaugeSuperSimHandler(val:Number):void
		{
			gaugeModel.simulateSuper(val);
		}
		
		private function onGaugeDepleteHandler(val:Number):void
		{
			gaugeModel.depGauge(val);
		}
		
		private function onHeroDieHandler():void
		{
			gameModel.die();
		}
		
		private function onHeroSwimHandler(t:Boolean):void
		{
			toggleSwimControlsSignal.dispatch(t);
		}
		
		private function onSecretAreaHandler():void
		{
			gameModel.currentLevel.secretAreaBadgeCollected = true;
		}
		
		private function onResumeHandler():void
		{
			view.resume();
		}
		
		private function onRetryHandler():void
		{
			startNewGame();
			view.restartLevel();
		}
		
		private function onTutHandler(id:String):void
		{
			triggerTutSignal.dispatch(id);
		}
		
		private function onPauseHandler():void
		{
			view.pause();
		}
		
		private function startNewGame():void
		{
			TouchInputController.getInstance().canSuperJump = playerService.player.superEnabled;
			TouchInputController.getInstance().canSwim = playerService.player.swimEnabled;
			
			gameModel.startGame();
			gaugeModel.resetGauge();
		}
		
		private function onSoftPause(toggle:Boolean):void
		{
			view.softPause(toggle);
		}
		
		private function onInventoryShow(show:Boolean):void
		{
			gameView.touchInput.enabled = !show;
		}
	}
}