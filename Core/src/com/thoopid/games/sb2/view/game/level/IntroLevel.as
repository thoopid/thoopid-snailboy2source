package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.level.base.UserinterfaceLevelBase;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	
	public class IntroLevel extends UserinterfaceLevelBase
	{
		public var introSkipSignal:Signal = new Signal();

		private var _loader:Loader;
		
		public function IntroLevel(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, LevelNames.INTRO_LEVEL);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init():void
		{
			var myUrlRequest:URLRequest = new URLRequest("intro/Intro.swf");
			var myLoaderContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain, null);
			_loader = new Loader();
			Starling.current.nativeStage.addChild(_loader);
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
			_loader.load(myUrlRequest, myLoaderContext);
		}
		
		protected function onIoError(event:Event):void
		{
			introSkipSignal.dispatch();
		}
		
		protected function onComplete(event:Event):void
		{
			_loader.scaleX = Config.SCALE_NATIVE;
			_loader.scaleY = Config.SCALE_NATIVE;
			_loader.x = MobileConfig.VIEW_H_CENTER;
			_loader.y = MobileConfig.VIEW_V_CENTER;
			(_loader.content as MovieClip).gotoAndPlay(2);
			_loader.addEventListener(MouseEvent.CLICK, onNext);
		}
		
		override public function dispose():void
		{
			introSkipSignal.removeAll();
			introSkipSignal = null;
			super.dispose();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function onNext(event:MouseEvent):void
		{
			(_loader.content as MovieClip).stop();
			_loader.parent.removeChild(_loader);
			_loader.unloadAndStop();
			_loader = null;
			
			introSkipSignal.dispatch();
		}
	}
}