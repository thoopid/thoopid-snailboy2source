package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.level.base.UserInterfaceLevelBaseMediator;
	
	public class IntroLevelMediator extends UserInterfaceLevelBaseMediator
	{
		public function IntroLevelMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			(view as IntroLevel).introSkipSignal.addOnce(onSkip);
		}
		
		private function onSkip():void
		{
			// TODO Auto Generated method stub
			loadLevelSignal.dispatch(LevelNames.KINGDOM_SELECT_LEVEL);
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function onLevelRevealed():void
		{
			super.onLevelRevealed();
			
			(view as IntroLevel).init();
		}
	}
}