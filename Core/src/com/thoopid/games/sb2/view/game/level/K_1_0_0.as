package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.level.base.KingdomLevelBase;
	import com.thoopid.games.sb2.view.game.tut.SelectLevelTutorial;
	
	public class K_1_0_0 extends KingdomLevelBase
	{
		public function K_1_0_0(mc:Object, name:String="0")
		{
			super(mc, LevelNames.KINGDOM_1);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override protected function buildLevel():void
		{
			super.buildLevel();
			this.addChild(new SelectLevelTutorial());
		}
	}
}