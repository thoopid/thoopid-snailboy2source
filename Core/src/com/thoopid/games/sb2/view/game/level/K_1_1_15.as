package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	
	public class K_1_1_15 extends GameLevel
	{
		public function K_1_1_15(mc:Object, name:String="0")
		{
			super(mc, LevelNames.KINGDOM_1_LEVEL_15);
		}
	}
}