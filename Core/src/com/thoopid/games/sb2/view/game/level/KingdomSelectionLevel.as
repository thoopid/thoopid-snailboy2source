package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.game.KingdomVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.level.base.UserinterfaceLevelBase;
	import com.thoopid.games.sb2.view.ui.comps.SBScrollContainer;
	import com.thoopid.games.sb2.view.ui.screens.kingdomselect.KingdomSelectItem;
	
	import flash.display.MovieClip;
	
	import org.osflash.signals.Signal;
	
	public class KingdomSelectionLevel extends UserinterfaceLevelBase
	{
		public var itemSelectedSignal:Signal = new Signal(String);
		
		private var _scrollContainer:SBScrollContainer;
				
		public function KingdomSelectionLevel(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, LevelNames.KINGDOM_SELECT_LEVEL);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(kingdoms:Vector.<KingdomVO>, playerUnlockedKingdoms:Array):void
		{
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.KINGDOM_SELECT_SCREEN), Assets.assets);
			
			_scrollContainer = LayoutBuilder.findChild(this, "mcHolder") as SBScrollContainer;
			_scrollContainer.width = Config.GAME_WIDTH - (_scrollContainer.x * 2);
			_scrollContainer.height = Config.GAME_HEIGHT;
			_scrollContainer.verticalScrollPosition = 0;
			_scrollContainer.verticalScrollPolicy = "SCROLL_POLICY_OFF";
			
			var kingdomSelectItem:KingdomSelectItem;
			for (var i:int = 0; i < kingdoms.length; i++) 
			{
				_scrollContainer.addChild(kingdomSelectItem = new KingdomSelectItem({}));
					
				if(playerUnlockedKingdoms.indexOf(kingdoms[i].kingdomId) != -1)
				{
					kingdoms[i].locked = false;
				}
				
				kingdomSelectItem.init(kingdoms[i], itemSelectedSignal);
				
				kingdomSelectItem.x = (120) + (i * 150);
				kingdomSelectItem.y = 105;
			}
		}
		
		override public function initialize():void
		{
			super.initialize();
			
						
		}
	}
}