package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.level.base.UserInterfaceLevelBaseMediator;
	
	import starling.events.Event;
	
	
	public class KingdomSelectionLevelMediator extends UserInterfaceLevelBaseMediator
	{
		public function KingdomSelectionLevelMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override protected function onLevelReady():void
		{
			super.onLevelReady();
			
			(view as KingdomSelectionLevel).itemSelectedSignal.add(onItemSelected);
		}
		
		override protected function onLevelRevealed():void
		{
			super.onLevelRevealed();
			
			(view as KingdomSelectionLevel).init(levelModel.kingdoms, playerService.player.kingdoms);
		}
		
		override protected function onTrigger(e:Event):void
		{
			super.onTrigger(e);
			
			switch(e.target["name"])
			{
				case "btnClose":
					loadLevelSignal.dispatch(LevelNames.MENU_LEVEL);
					break;
			}
		}
		
		private function onItemSelected(id:String):void
		{
			loadLevelSignal.dispatch(id);
		}		

	}
}