package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.level.base.UserinterfaceLevelBase;
	
	import flash.display.MovieClip;
	
	import starling.display.Sprite;
		
	public class MenuLevel extends UserinterfaceLevelBase
	{
						
		private var _parent:Sprite;
		
		public function MenuLevel(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, LevelNames.MENU_LEVEL);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function buildUi():void
		{
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.MENU_SCREEN), Assets.assets);
			
			_parent = LayoutBuilder.findChild(this,"mcParent") as Sprite;
			
			if(_parent.width > Config.GAME_WIDTH)
			{
				_parent.scaleX = _parent.scaleY = 0.8;
			}
			
		}
			
		override public function initialize():void
		{
			super.initialize();
						
		}
	}
}