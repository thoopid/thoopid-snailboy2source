package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.controller.signals.ui.GaugeUpgradeScreenSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.OptionsShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ShopShowSignal;
	import com.thoopid.games.sb2.view.game.level.base.UserInterfaceLevelBaseMediator;
	
	import starling.events.Event;
	
	public class MenuLevelMediator extends UserInterfaceLevelBaseMediator
	{
		
		[Inject]
		public var inventoryShowSignal:InventoryShowSignal;
		
		[Inject]
		public var gaugeUpgradeScreenSignal:GaugeUpgradeScreenSignal;
		[Inject]
		public var optionsShowSignal:OptionsShowSignal;
		[Inject]
		public var shopShowSignal:ShopShowSignal;
		
		private var injector:Object;
		
		
		public function MenuLevelMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get menuLevel():MenuLevel
		{
			return view as MenuLevel;
		}
		
		override protected function onLevelReady():void
		{
			super.onLevelReady();
		}
		
		override protected function onLevelRevealed():void
		{
			super.onLevelRevealed();
			menuLevel.buildUi();
		}
		
		override public function onRemove():void
		{
			super.onRemove();
		}
		
		
		override protected function onTrigger(e:Event):void
		{
//			trace(e.target["name"]);
			switch(e.target["name"])
			{
				case "btnClose":
					loadLevelSignal.dispatch(LevelNames.KINGDOM_SELECT_LEVEL);
					break;
				case "btnPlayer":
					loadLevelSignal.dispatch(LevelNames.ACHIEVEMENTS_LEVEL);
					break;
				case "btnOptions":
					optionsShowSignal.dispatch(true);
					break;
				case "btnTreasure":
					loadLevelSignal.dispatch(LevelNames.TREASURE_LEVEL);
					break;
				case "btnGauge":
					gaugeUpgradeScreenSignal.dispatch(true);
					break;
				case "btnInventory":
					inventoryShowSignal.dispatch(true);
					break;
				case "btnShop":
					shopShowSignal.dispatch(true);
					break;
			}
			//injector.unmap(e);
		}
		
		private function onItemSelected(id:String):void
		{
		
		}		

	}
}