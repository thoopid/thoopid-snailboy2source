package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.level.base.UserinterfaceLevelBase;
	
	import flash.display.MovieClip;
	
	public class TreasureLevel extends UserinterfaceLevelBase
	{
		
		public function TreasureLevel(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, LevelNames.TREASURE_LEVEL);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function initialize():void
		{
			super.initialize();
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.TREASURE_SCREEN), Assets.assets);
		}
	}
}