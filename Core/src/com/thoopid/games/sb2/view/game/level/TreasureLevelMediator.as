package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.level.base.LevelBaseMediator;
	
	import starling.events.Event;
	
	public class TreasureLevelMediator extends LevelBaseMediator
	{
		public function TreasureLevelMediator()
		{
			super();
		}
				
		public function get treasureLevel():TreasureLevel
		{
			return view as TreasureLevel;
		}
		
		override protected function onLevelReady():void
		{
			super.onLevelReady();
			treasureLevel.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		override public function onRemove():void
		{
			treasureLevel.removeEventListener(Event.TRIGGERED, onTrigger);
			super.onRemove();
		}
		
		
		protected function onTrigger(e:Event):void
		{
//			trace(e.target["name"]);
			switch(e.target["name"])
			{
				case "btnClose":
					loadLevelSignal.dispatch(LevelNames.MENU_LEVEL);
					break;
			}
		}
				
	}
}