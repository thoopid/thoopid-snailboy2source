package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.level.base.UserinterfaceLevelBase;
	
	import flash.display.MovieClip;
	
	public class WelcomeLevel extends UserinterfaceLevelBase
	{
		public function WelcomeLevel(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, LevelNames.WELCOME_LEVEL);
		}
		//*********************
		//   PUBLIC
		//*********************
		
		public function init():void
		{
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.WELCOME_SCREEN), Assets.assets);
		}
	}
}