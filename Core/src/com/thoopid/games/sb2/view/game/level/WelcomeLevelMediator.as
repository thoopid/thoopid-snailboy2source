package com.thoopid.games.sb2.view.game.level
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.view.game.level.base.UserInterfaceLevelBaseMediator;
	
	import starling.events.Event;
	
	public class WelcomeLevelMediator extends UserInterfaceLevelBaseMediator
	{
		public function WelcomeLevelMediator()
		{
			super();
		}
		
		override protected function onLevelReady():void
		{
			super.onLevelReady();
		}
		
		override protected function onLevelRevealed():void
		{
			super.onLevelRevealed();
			
			(view as WelcomeLevel).init();
		}
		
		override protected function onTrigger(e:Event):void
		{
			super.onTrigger(e);
			
			switch(e.target["name"])
			{
				case "btnPlay":
					loadLevelSignal.dispatch(LevelNames.INTRO_LEVEL);
					break;
			}
		}
	}
}