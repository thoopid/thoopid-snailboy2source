package com.thoopid.games.sb2.view.game.level.base
{
	import flash.display.MovieClip;
	import flash.system.Capabilities;
	import flash.system.System;
	
	import citrus.core.starling.StarlingState;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.utils.AssetManager;
	
	public class ALevel extends StarlingState
	{
		protected var _levelClip:MovieClip;
		protected var _levelName:String = "1_1";
		protected var _assets:AssetManager;
		protected var _softPaused:Boolean;
		
		private var _lvlEnded:Signal = new Signal();
		private var _lvlReady:Signal = new Signal();
		private var _lvlRevealed:Signal = new Signal();
		
		public function ALevel(levelClip:MovieClip, levelName:String="1_1")
		{
			super();
			
			_assets = Assets.assets;
			
			_levelClip = levelClip;
			_levelName = levelName;
		}
		
		//*********************
		//   PUBLIC
		//*********************

		public function get lvlRevealed():Signal
		{
			return _lvlRevealed;
		}

		public function get lvlReady():Signal
		{
			return _lvlReady;
		}

		public function get lvlEnded():Signal
		{
			return _lvlEnded;
		}

		override public function destroy():void
		{
//			_assets.purge();
//			_assets.dispose();
			_assets.removeObject(_levelName);
			_assets = null;
			_levelClip = null;
			_levelName = null;
			
			_lvlEnded.removeAll();
			_lvlEnded = null;
			_lvlReady.removeAll();
			_lvlReady = null;
			_lvlRevealed.removeAll();
			_lvlRevealed = null;
			
			super.destroy();
		}
		
		public function get levelName():String
		{
			return _levelName;
		}

		public function levelReady():void
		{
			_lvlReady.dispatch();
		}
		public function retryLevel():void {}
		public function completeLevel():void {}
		public function pause():void {}
		public function softPause(toggle:Boolean):void
		{
			_softPaused = toggle;
		}
		public function resume():void {}
		public function restartLevel():void {}
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function loadLevelAssets(... assets):void
		{
			_assets.verbose = Capabilities.isDebugger;
			_assets.enqueue(assets);
			
			_assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					Starling.juggler.delayCall(function():void
					{
						// Done
						buildLevel();
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
				}
			});
		}
		
		protected function buildLevel():void
		{
			
		}
	}
}