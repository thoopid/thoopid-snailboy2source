package com.thoopid.games.sb2.view.game.level.base
{
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.user.ThoopidPlayer;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.elements.AreaElement;
	import com.thoopid.games.sb2.view.game.elements.KingdomElement;
	import com.thoopid.games.sb2.view.game.level.util.KingdomTouchController;
	import com.thoopid.games.sb2.view.game.objects.CaveLevelSelector;
	
	import flash.geom.Point;
	
	import nape.geom.Vec2;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	
	public class KingdomLevelBase extends LevelBase
	{
		public var caveSelected:Signal = new Signal(String);
		private var _dummyTarget:Point;
		private var _player:ThoopidPlayer;
		private var _touchController:KingdomTouchController;
		private var _selectedCave:CaveLevelSelector;
		
		public function KingdomLevelBase(mc:Object, name:String="0")
		{
			super(mc, name);
			
			_performanceDistance = 800;
			
			// Target for Camera - no Hero in these level selections
			_dummyTarget = new Point(100,100);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(player:ThoopidPlayer):void
		{
			_player = player;
		}
		
		override protected function buildLevel():void
		{
			_baseZoom = Config.GAME_ZOOM_KINGDOM;
			
			super.buildLevel();
			
			setActiveArea();
			
			var caves:Vector.<CaveLevelSelector> = Vector.<CaveLevelSelector>(this.getObjectsByType(CaveLevelSelector));
			if(caves && caves.length > 0)
			{
				for each (var c:CaveLevelSelector in caves) 
				{
					c.cavelevelSelected.add(onCaveSelected);
				}
			}	
			
			_touchController = new KingdomTouchController(Starling.current.stage, _camera, _dummyTarget);
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.LEVEL_SELECT_SCREEN), Assets.assets);
		}
		
		public function setActiveArea():void
		{
			// Get Area to focus on
			var i:int = _areaCenters.length;
			while(i--)
			{
				if(_areaCenters[i].areaId == _player.activeArea)
				{
					_dummyTarget.x = _areaCenters[i].x;
					_dummyTarget.y = _areaCenters[i].y;
				}
			}
		}
		
		public function highlightAreaElements(areaId:String):void
		{
			// Get the area Element
			var areaElements:Vector.<AreaElement> = Vector.<AreaElement>(this.getObjectsByType(AreaElement));
			if(areaElements && areaElements.length > 0)
			{
				var i:int = areaElements.length;
				while(i--)
				{
					if(areaElements[i].areaId == areaId)
					{
						_dummyTarget.x = areaElements[i].x;
						_dummyTarget.y = areaElements[i].y;
					}
				}
			}
			_camera.setZoom(Config.GAME_ZOOM_AREA_HIGHLIGHT);
		}
		
		public function highlighKingdomElement():void
		{
			var ke:Vector.<KingdomElement> = Vector.<KingdomElement>(this.getObjectsByType(KingdomElement));
			if(ke && ke.length > 0)
			{
//				_touchController.setFocusPoint(new Point(ke[0].x,ke[0].y));
				
				_camera.setZoom(Config.GAME_ZOOM_KINGDOM);
				
				_dummyTarget.x = ke[0].x;
				_dummyTarget.y = ke[0].y;
			}
		}
		
		public function returnToArea():void
		{
			setActiveArea();
			
			if(_touchController)
			{
				_touchController.setFocusPoint(_dummyTarget);
			}
			
			_camera.setZoom(_baseZoom);
		}
		
		override public function destroy():void
		{
			super.destroy();
			
			_touchController.dispose();
			_touchController = null;
			
			caveSelected.removeAll();
			caveSelected = null;
			
			_player = null;
		}
		
		override public function update(timeDelta:Number):void
		{
			_performanceTarget = Vec2.weak(_dummyTarget.x, _dummyTarget.y);
			super.update(timeDelta);
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function setupCamera():void
		{
			super.setupCamera();
			_camera.setUp(_dummyTarget, _mobileBounds,_camOffsetOriginal, new Point(0.07, 0.07));
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onCaveSelected(cave:CaveLevelSelector):void
		{
			if(_selectedCave && _selectedCave == cave)
			{
				caveSelected.dispatch(cave.levelId);
				return;
			}
			
			if(_selectedCave)
			{
				_selectedCave.toggle(false);
				_selectedCave = null;
			}
			
			_selectedCave = cave;
			_selectedCave.toggle(true);

		}
	}
}