package com.thoopid.games.sb2.view.game.level.base
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.controller.signals.story.CheckKingdomCollectablesSignal;
	import com.thoopid.games.sb2.controller.signals.story.ContinuewithKingdomSignal;
	import com.thoopid.games.sb2.controller.signals.story.RevealAreaCollectableSignal;
	import com.thoopid.games.sb2.controller.signals.story.RevealKingdomCollectableSignal;
	import com.thoopid.games.sb2.controller.signals.ui.KingdomAreaUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.ui.NavigateKingdomAreaSignal;
	
	import starling.animation.IAnimatable;
	import starling.core.Starling;
	import starling.events.Event;
	
	public class KingdomLevelMediator extends UserInterfaceLevelBaseMediator
	{
		// Signals
		[Inject]
		public var continuewithKingdomSignal:ContinuewithKingdomSignal;
		[Inject]
		public var checkKingdomCollectablesSignal:CheckKingdomCollectablesSignal;
		[Inject]
		public var revealKingdomCollectableSignal:RevealKingdomCollectableSignal;
		[Inject]
		public var revealAreaCollectableSignal:RevealAreaCollectableSignal;
		[Inject]
		public var navigateKingdomAreaSignal:NavigateKingdomAreaSignal;
		[Inject]
		public var kingdomAreaUpdateSignal:KingdomAreaUpdateSignal;
		
		private var _kingdomCheckDc:IAnimatable;
		
		public function KingdomLevelMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get kingdomLevelSelectView():KingdomLevelBase
		{
			return view as KingdomLevelBase
		}
		
		override public function onRegister():void
		{
			super.onRegister();
				
			continuewithKingdomSignal.add(onContinueWithKingdom);
			revealKingdomCollectableSignal.add(onRevealKingdomCollectableHandler);
			revealAreaCollectableSignal.add(onRevealAreaCollectableHandler);
			
			kingdomAreaUpdateSignal.add(onAreaUpdateHandler);
			
			kingdomLevelSelectView.init(playerService.player);
			
			_kingdomCheckDc = Starling.juggler.delayCall(checkKingdomCollectablesSignal.dispatch, 2, kingdomLevelSelectView.levelName);
		}
		
		override public function onRemove():void
		{
			if(_kingdomCheckDc) Starling.juggler.remove(_kingdomCheckDc);
			
			continuewithKingdomSignal.remove(onContinueWithKingdom);
			revealKingdomCollectableSignal.remove(onRevealKingdomCollectableHandler);
			revealAreaCollectableSignal.remove(onRevealAreaCollectableHandler);
			
			kingdomAreaUpdateSignal.remove(onAreaUpdateHandler);
			super.onRemove();
		}
		
		//*********************
		//   PROTECTED
		//*********************/
		
		override protected function onTrigger(e:Event):void
		{
			super.onTrigger(e);
			
			switch(e.target["name"])
			{
				case "btnClose":
					loadLevelSignal.dispatch(LevelNames.KINGDOM_SELECT_LEVEL);
					break;
				case "btnNext":
					navigateKingdomAreaSignal.dispatch(1);
					break;
				case "btnPrev":
					navigateKingdomAreaSignal.dispatch(-1);
					break;
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		private function onContinueWithKingdom(id:String):void
		{
			kingdomLevelSelectView.caveSelected.add(onCaveSelected);
			kingdomLevelSelectView.returnToArea();
			// TODO Now reveal Ui
		}
		
		private function onCaveSelected(levelId:String):void
		{
			loadLevelSignal.dispatch(levelId);
		}
		
		private function onRevealKingdomCollectableHandler(kingdomId:String):void
		{
			kingdomLevelSelectView.highlighKingdomElement();
		}
		
		private function onRevealAreaCollectableHandler(areaId:String):void
		{
			kingdomLevelSelectView.highlightAreaElements(areaId);
		}
		
		private function onAreaUpdateHandler():void
		{
			kingdomLevelSelectView.setActiveArea();
		}
	}
}