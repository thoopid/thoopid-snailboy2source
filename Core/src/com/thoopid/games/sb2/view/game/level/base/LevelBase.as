package com.thoopid.games.sb2.view.game.level.base
{
	import com.gamua.flox.utils.formatString;
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.model.game.AreaCenterVO;
	import com.thoopid.games.sb2.view.game.factory.FireballEffectsFactory;
	import com.thoopid.games.sb2.view.game.factory.ObstacleFactories;
	import com.thoopid.games.sb2.view.game.factory.SlimeyCollectEffectFactory;
	import com.thoopid.games.sb2.view.game.level.util.JsonObjectMaker2d;
	import com.thoopid.games.sb2.view.game.objects.CheckpointSensor;
	import com.thoopid.games.sb2.view.game.objects.FadedLayer;
	import com.thoopid.games.sb2.view.game.objects.Mine;
	import com.thoopid.games.sb2.view.game.objects.ParalaxLayer;
	import com.thoopid.games.sb2.view.game.objects.SlimeCollectible;
	import com.thoopid.games.sb2.view.game.objects.SnailboyHero;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	
	import flash.display.MovieClip;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import citrus.core.CitrusEngine;
	import citrus.physics.nape.Nape;
	import citrus.view.starlingview.StarlingCamera;
	
	import feathers.display.Scale9Image;
	
	import nape.geom.Vec2;
	
	import starling.display.Image;
	import starling.filters.ColorMatrixFilter;
	import starling.textures.Texture;
	
	public class LevelBase extends ALevel
	{
		// Camera Vars
		protected var _baseZoom:Number = 1;
		protected var _camera:StarlingCamera;
		protected var _camOffsetOriginal:Point;			// Origianl
		protected var _mobileBounds:Rectangle;
		
		protected var _currentCheckpoint:CheckpointSensor;
//		protected var _checkPoints:Vector.<Point>;
		protected var _areaCenters:Vector.<AreaCenterVO>;
		
		// Level Layers
		protected var _fadedLayer:FadedLayer;
		protected var _paLayer:ParalaxLayer;
		protected var _mainLayer:ParalaxLayer;
		protected var _movingObjectsFront:ParalaxLayer;
		
		protected var _hero:SnailboyHero;
		protected var _performanceTarget:Vec2=Vec2.get();
		protected var _performanceDistance:int = 500;
		
		// Physics
		protected var _physics:Nape;
		
		protected var _totalSlimies:Number = 0;
		
		private var _offsetStep:int=0;
		
		// UI elements
//		protected var _overlay:PoolImage;
//		private var _atmosphereEffect:PDParticleSystem;

		
		public function LevelBase(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, levelName);
			
			if(Config.DEBUG_CONSOLE)
			{
				CitrusEngine.getInstance().console.addCommand("zoom", onZoom);
				CitrusEngine.getInstance().console.addCommand("setPerformanceRange", onPerformanceRange);
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function revealLevel():void
		{
//			Starling.juggler.tween(_overlay, 1, {alpha:0, transition:Transitions.EASE_IN, onComplete:onLevelRevealed});
			onLevelRevealed();
		}
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			if(_fadedLayer)
			{
				_fadedLayer.x = _camera.camPos.x;
				_fadedLayer.y = _camera.camPos.y;
			}
			
//			if(_atmosphereEffect)
//			{
//				_atmosphereEffect.emitterY =  _camera.camPos.y;
//				_atmosphereEffect.emitterX =  _camera.camPos.x;
//			}
			
			_offsetStep++;
			if(_offsetStep >= 20)
			{
				var ie:Vector.<IEnableable> = Vector.<IEnableable>(this.getObjectsByType(IEnableable));
				for each (var ee:IEnableable in ie) 
				{
//					if(Vec2.distance(Vec2.weak(ee.x,ee.y), Vec2.weak(_hero.x,_hero.y)) < 800)
					if(Vec2.distance(Vec2.weak(ee.x,ee.y), Vec2.weak(_performanceTarget.x,_performanceTarget.y)) < _performanceDistance)
					{
						ee.isRendered = true;
						if(ee.body) ee.body.space = _physics.space;
					} else
					{
						ee.isRendered = false;
						if(ee.body) ee.body.space = null; 
					}
				}
				_offsetStep = 0;
			}
		}
		
		override public function initialize():void
		{
			super.initialize();
			
			var appDir:File = File.applicationDirectory;
			loadLevelAssets(
				appDir.resolvePath(formatString("levels/{0}.json",_levelName))
			)
		}
		
		override public function restartLevel():void
		{
			// Slimies
			resetAllItems();
			activateLevel();
		}
		
		public function activateLevel():void
		{
			if(!CitrusEngine.getInstance().playing) CitrusEngine.getInstance().playing = true;
		}
		
		override public function destroy():void
		{
//			Starling.juggler.removeTweens(_overlay);
			
			super.destroy();
			
			// Clear Factories
			ObstacleFactories.getInstance().destroy();
			SlimeyCollectEffectFactory.getInstance().destroy();
			FireballEffectsFactory.getInstance().destroy();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function buildLevel():void
		{
			this.add(_paLayer = new ParalaxLayer("paLayer",{}));
			_paLayer.parallaxX = 0.9;
			this.add(_fadedLayer = new FadedLayer());
			
			Scale9Image
			
			this.add(_mainLayer = new ParalaxLayer("bgLayer",{}));
			_movingObjectsFront = new ParalaxLayer("movingObjectsLayer");
			_movingObjectsFront.init(false,false);
			
			// TEST
			// ***********************************************************
//			_atmosphereEffect = new PDParticleSystem(Assets.assets.getXml("Atmosphere"), Assets.assets.getTexture("SlimeEffectTexture"));
//			_movingObjectsFront.holder.addChild(_atmosphereEffect);
//			Starling.juggler.add(_atmosphereEffect);
//			_atmosphereEffect.emitterX =  Config.GAME_VIEW_H_CENTER;
////			_atmosphereEffect.emitterY =  Config.GAME_VIEW_V_CENTER;
//			_atmosphereEffect.emitterXVariance =  Config.GAME_VIEW_H_CENTER;
//			_atmosphereEffect.emitterYVariance =  Config.GAME_VIEW_V_CENTER;
//			_atmosphereEffect.start();
//			_atmosphereEffect.maxNumParticles = 20;
			// ***********************************************************
			
			setupPhysics();
			
			var items:Object = _assets.getObject(_levelName);
			var image:Image;
			var item:Object;
			var tex:Texture;
			
			_totalSlimies = items.slimiesCount;
			
			// LAYERS ***************************************************
			// PARALAX LAYER
			// ***************************************************
			
			var paItems:Array = items.pa;
			for (var pa:int = 0; pa < paItems.length; pa++) 
			{
				item = paItems[pa];
//				m = item.m;
				if(_assets.getTexture(item.i))
				{
					tex = _assets.getTexture(item.i);
					image = Factory.imagePool.getImage(tex,true);
					image.scaleX = item.sx;
					image.scaleY = item.sy;
					image.rotation = item.r;
					image.x = item.x;
					image.y = item.y;
					
					_paLayer.add(image);
				} else
				{
//					trace("Not Found");
				}
			}
			
			_paLayer.init(false);
			
			// LAYERS ***************************************************
			// Main Layer items
			var mainItems:Array = items.main;
			var f:ColorMatrixFilter = new ColorMatrixFilter();
			f.adjustBrightness(0.4);
			for (var i:int = 0; i < mainItems.length; i++) 
			{
				item = mainItems[i];
				if(_assets.getTexture(item.i))
				{
					tex = _assets.getTexture(item.i);
					image = Factory.imagePool.getImage(tex,true);
					image.scaleX = item.sx;
					image.scaleY = item.sy;
					image.rotation = item.r;
					image.x = item.x;
					image.y = item.y;
					
					_mainLayer.add(image);
				} else
				{
//					trace("Not Found");
				}
			}
			_mainLayer.init(true);
			
			// Physics objects
			JsonObjectMaker2d.FromMovieClip(items.physics);
			_levelClip = null;
			
			this.add(_movingObjectsFront);
			
			// Factories
			ObstacleFactories.getInstance().init(_physics.space);
			SlimeyCollectEffectFactory.getInstance().cache(20);
			FireballEffectsFactory.getInstance().cache(40, _movingObjectsFront);
			
			var moveableObstacles:Array = items.mo;
			var mine:Mine;
			var slimey:SlimeCollectible;
			for (var m:int = 0; m < moveableObstacles.length; m++) 
			{
				// Volcanos
				item = moveableObstacles[m];
				if(item.c == "MineHead")
				{
					mine = ObstacleFactories.getInstance().minesPool.getItem() as Mine;
					if(mine)
					{
						mine.init(item.params);
						this.add(mine);
					}
				} else if(item.c == "SlimeCollectible" || item.c == "SuperSlimeCollectible")
				{
					slimey = ObstacleFactories.getInstance().slimePool.getItem() as SlimeCollectible;
					if(slimey)
					{
						item.params.isSuper = (item.c == "SuperSlimeCollectible");
						slimey.init(item.params);
						this.add(slimey);
					}
				}
			}
			
			//****************************************************************
			// Setup Area Centers
			//****************************************************************
			
			var ac:Array = items.areaCenters;
			_areaCenters = new Vector.<AreaCenterVO>();
			if(ac && ac.length > 0)
			{
				for (var aci:int = 0; aci < ac.length; aci++) 
				{
					_areaCenters.push(new AreaCenterVO(ac[aci].params.areaId, ac[aci].params.x,ac[aci].params.y));
				}
			}
			
			// Camera
			_camOffsetOriginal = new Point(0.5, 0.5);
			_camera = view.camera as StarlingCamera;
			_mobileBounds = new Rectangle(-5000,-5000,Config.WORLD_WIDTH, Config.WORLD_HEIGHT);
			_camera.allowRotation = false;
			_camera.allowZoom = true;
			_camera.zoomEasing = 0.05;
			_camera.setZoom(Config.GAME_ZOOM_LEVEL);
			
			setupHero();
			setupCamera();
			levelReady();
		}
		
		protected function setupHero():void
		{
			
		}
		
		protected function setupCamera():void
		{
			
		}
		
		protected function setupPhysics():void
		{
			// Physics system
			_physics = new Nape("napePhysics");
			_physics.space.worldLinearDrag = GameConstants.GAME_DRAG;
			_physics.visible = Config.DEBUG_PHYSICS;
			_physics.gravity = Vec2.get(0,GameConstants.GAME_GRAVITY);
			add(_physics);
		}
		
		protected function onZoom(z:Number):void
		{
			_camera.setZoom(z);
		}
		protected function onPerformanceRange(z:Number):void
		{
			_performanceDistance = z;
		}
		
		protected function resetAllItems():void
		{
			var item:Vector.<IEnableable> = Vector.<IEnableable>(getObjectsByType(IEnableable));
			for each (var i:IEnableable in item) 
			{
				i.reset();
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onLevelRevealed():void
		{
			lvlRevealed.dispatch();
		}
	}
}