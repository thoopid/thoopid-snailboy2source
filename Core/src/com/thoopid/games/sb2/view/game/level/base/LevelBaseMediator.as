package com.thoopid.games.sb2.view.game.level.base
{
	import com.thoopid.games.sb2.controller.signals.game.NewGameSignal;
	import com.thoopid.games.sb2.controller.signals.system.LoadLevelSignal;
	import com.thoopid.games.sb2.model.game.LevelModel;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class LevelBaseMediator extends StarlingMediator
	{
		[Inject]
		public var view:LevelBase;
		
		[Inject]
		public var levelModel:LevelModel;
		
		// Service
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var loadLevelSignal:LoadLevelSignal;
		[Inject]
		public var newGameSignal:NewGameSignal;
		
		public function LevelBaseMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			newGameSignal.dispatch(view.levelName);
			
			view.lvlReady.add(onLevelReady);
			view.lvlRevealed.add(onLevelRevealed);
		}
		
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function onLevelReady():void
		{
			view.activateLevel();
			view.revealLevel();
		}
		
		protected function onLevelRevealed():void
		{
			
		}
	}
}