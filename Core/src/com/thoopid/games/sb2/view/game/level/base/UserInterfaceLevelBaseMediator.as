package com.thoopid.games.sb2.view.game.level.base
{
	import com.thoopid.games.sb2.controller.signals.ui.GameHudShowSignal;
	
	import starling.events.Event;
	
	public class UserInterfaceLevelBaseMediator extends LevelBaseMediator
	{
		[Inject]
		public var gameHudShowSignal:GameHudShowSignal;
		
		public function UserInterfaceLevelBaseMediator()
		{
			super();
		}
		
		override protected function onLevelReady():void
		{
			super.onLevelReady();
			
			gameHudShowSignal.dispatch(false);
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			view.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		override public function onRemove():void
		{
			view.removeEventListener(Event.TRIGGERED, onTrigger);
			super.onRemove();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function onTrigger(e:Event):void
		{
			
		}
	}
}