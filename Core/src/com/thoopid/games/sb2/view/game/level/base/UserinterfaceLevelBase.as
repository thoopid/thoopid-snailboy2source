package com.thoopid.games.sb2.view.game.level.base
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	import starling.core.Starling;
	import starling.extensions.particles.PDParticleSystem;
	
	public class UserinterfaceLevelBase extends LevelBase
	{
		private var _dummyTarget:Point = new Point(100,100);
		private var _pSys:PDParticleSystem;
		
		public function UserinterfaceLevelBase(levelClip:MovieClip, levelName:String="1_1")
		{
			super(levelClip, levelName);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override protected function buildLevel():void
		{
			_pSys = new PDParticleSystem(Assets.assets.getXml("UiBubbles"), Assets.assets.getTexture("ui_HUD_coin_bubble"));
			this.addChild(_pSys);
			Starling.juggler.add(_pSys);
			_pSys.x = Config.GAME_VIEW_H_CENTER;
			_pSys.emitterXVariance = Config.GAME_VIEW_H_CENTER;
			_pSys.y = Config.GAME_VIEW_V_CENTER;
			_pSys.emitterYVariance = 40;
			_pSys.start();
			
			super.buildLevel();
			
			// Get Area to focus on
			_dummyTarget.x = _areaCenters[0].x;
			_dummyTarget.y = _areaCenters[0].y;
			
		}
		
		override public function destroy():void
		{
			_pSys.stop();
			Starling.juggler.remove(_pSys);
			_pSys.removeFromParent();
			_pSys = null
			
			super.destroy();
		}
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function setupCamera():void
		{
			super.setupCamera();
			_camera.setUp(_dummyTarget, _mobileBounds,_camOffsetOriginal, new Point(0.07, 0.07));
		}
		
	}
}