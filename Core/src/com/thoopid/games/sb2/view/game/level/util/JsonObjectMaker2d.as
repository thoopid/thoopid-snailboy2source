package com.thoopid.games.sb2.view.game.level.util
{
	
	import flash.utils.getDefinitionByName;
	
	import citrus.core.CitrusEngine;
	import citrus.core.CitrusObject;
	
	
	/**
	 * The ObjectMaker is a factory utility class for quickly and easily batch-creating a bunch of CitrusObjects.
	 * Usually the ObjectMaker is used if you laid out your level in a level editor or an XML file.
	 * Pass in your layout object (SWF, XML, or whatever else is supported in the future) to the appropriate method,
	 * and the method will return an array of created CitrusObjects.
	 *
	 * <p>The methods within the ObjectMaker should be called according to what kind of layout file that was created
	 * by your level editor.</p>
	 */
	public class JsonObjectMaker2d {
		
		public function JsonObjectMaker2d() {
		}
		
		/**
		 * You can pass a custom-created MovieClip object into this method to auto-create CitrusObjects.
		 * This method looks at all the children of the MovieClip you passed in, and creates a CitrusObject with the
		 * x, y, width, height, name, and rotation of the of MovieClip.
		 *
		 * <p>You may use the powerful Inspectable metadata tag : in your fla file, add the path to the libraries and
		 * the swcs. Then create your MovieClip, right click on it and convert as a component. Inform the package and class.
		 * You will have access to all its properties.</p>
		 *
		 * <p>You can also add properties directly in your MovieClips, follow this step :</p>
		 *
		 * <p>In order for this to properly create a CitrusObject from a MovieClip, the MovieClip needs to have a variable
		 * called <code>classPath</code> on it, which will provide, in String form, the full
		 * package and class name of the Citrus Object that it is supposed to create (such as "myGame.MyHero"). You can specify
		 * this in frame 1 of the MovieClip asset in Flash.</p>
		 *
		 * <p>You can also initialize your CitrusObject's parameters by creating a "params" variable (of type Object)
		 * on your MovieClip. The "params" object will be passed into the newly created CitrusObject.</p>
		 *
		 * <p>So, within the first frame of each child-MovieClip of the "layout" MovieClip,
		 * you should specify something like the following:</p>
		 *
		 * <p><code>var classPath="citrus.objects.platformer.Hero";</code></p>
		 *
		 * <p><code>var params={view: "Patch.swf", jumpHeight: 14};</code></p>
		 */
		public static function FromMovieClip(items:Array, addToCurrentState:Boolean = true):Array {
			var a:Array = [];
			var n:Number = items.length;
			var child:Object;
			for (var i:uint = 0; i < n; ++i) {
				child = items[i];
				if (child) {
					if (!child.c)
						continue;
					
					var objectClass:Class = getDefinitionByName(child.c) as Class;
					var params:Object = {};
					
					if (child.params)
						params = child.params;
					
					params.x = child.x;
					params.y = child.y;
					params.width = child.w;
					params.height = child.h;
					params.rotation = child.r;
					
					var object:CitrusObject = new objectClass(child.name, params);
					a.push(object);
				}
			}
			
			if (addToCurrentState) {
				var ce:CitrusEngine = CitrusEngine.getInstance();
				for each (object in a) ce.state.add(object);
			}
			
			return a;
		}
		
		private static function Replace(str:String, fnd:String, rpl:String):String {
			return str.split(fnd).join(rpl);
		}
	}
}


