package com.thoopid.games.sb2.view.game.level.util
{
	import flash.geom.Point;
	
	import citrus.view.starlingview.StarlingCamera;
	
	import starling.display.DisplayObject;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class KingdomTouchController
	{
		private var _stage:DisplayObject;
		private var _camera:StarlingCamera;
		
		// Mouse Control
		private var _pressX:Number;
		private var _pressY:Number;
		private var _tempTarget:Point = new Point();
		private var _isPanning:Boolean;
		private var _target:Object;
		
		public function KingdomTouchController(stage:DisplayObject, camera:StarlingCamera, target:Object)
		{
			_stage = stage;
			_camera = camera;
			_target = target;
			_stage.addEventListener(TouchEvent.TOUCH, onTouchHandler);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function setFocusPoint(target:Point):void
		{
			_target = target;
			_tempTarget.x = target.x;
			_tempTarget.y = target.y;
			
			_camera.target = _target;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouchHandler(event:TouchEvent):void
		{
			var stageTouch:Touch = event.getTouch(_stage);
			if (stageTouch)
			{
				switch (stageTouch.phase)
				{
					
					case TouchPhase.BEGAN:
						
						_isPanning = true;
						_pressX = stageTouch.globalX;
						_pressY = stageTouch.globalY;
						_tempTarget.x = _camera.target.x;
						_tempTarget.y = _camera.target.y;
						_camera.target = _tempTarget;
						
						event.stopImmediatePropagation();
						
						break;
					case TouchPhase.ENDED:
						
						_camera.target = _target;
						_isPanning = false;
						event.stopImmediatePropagation();
						break;
					case TouchPhase.MOVED:
						if(_isPanning)
						{
							_tempTarget.x = _target.x + (_pressX - stageTouch.globalX);
							_tempTarget.y = _target.y + (_pressY - stageTouch.globalY);
						}
						break;
				}
			}
		}
		
		public function dispose():void
		{
			_stage.removeEventListener(TouchEvent.TOUCH, onTouchHandler);
			_target = null;
			_tempTarget = null;
			
		}
	}
}