package com.thoopid.games.sb2.view.game.level.util
{
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.model.game.GaugeVO;
	import com.thoopid.games.sb2.view.game.objects.SnailboyHero;
	import com.thoopid.games.sb2.view.game.objects.view.Character;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import citrus.physics.nape.Nape;
	import citrus.view.starlingview.StarlingCamera;
	
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.space.Space;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.ZoomGesture;
	import org.osflash.signals.Signal;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.rad2deg;

	public class TouchInputController
	{
		public var controlJumpEnabled:Boolean = true;
		public var controlSlapEnabled:Boolean = true;
		public var controlStickEnabled:Boolean = true;
		public var onZoomSignal:Signal = new Signal(Number);
		public var onZoomEndedSignal:Signal = new Signal();
		public var onLaunchSignal:Signal = new Signal(Number);
		public var onSuperJumpUpdateSignal:Signal = new Signal(Number);
		public var onSwimStartSignal:Signal = new Signal(Number);
		
		private var _hero:SnailboyHero;
		private var _heroView:Character;
		private var _stage:DisplayObject;
		
		// Mouse Control
		private var _pressX:Number;
		private var _pressY:Number;
		
		// Power and projections
		private var _power:Number=0;
		private var _angle:Number;
		
		private var _enabled:Boolean = false;
		private var _world:Nape;
		
		private var _fakeReleaseItems:Vector.<Image>;
		private var _fakeReleaseLength:int = 40;
		
		private var _characterPressPoint:Point = new Point();
		private var _characterMovePoint:Point = new Point();
		
		private var _velx:Number;
		private var _vely:Number;
		private var _gtl:Point = new Point();
		private var _targetP:Point = new Point();
		private var _time:Number=0;
		private var _lineTex:Texture;
		private var _pullEnabled:Boolean = false;
		private var _isDown:Boolean = false;
		private var _isPanning:Boolean = false;
		private var _canJumpDown:Boolean = false;
		private var _canSwipe:Boolean = false;
		private var _superJump:Boolean = false;
		private var _canSuperJump:Boolean = false;
		private var _canSwim:Boolean = false;
		
		private var _fakeReleasedCleaned:Boolean;

		private var _smoothFakeRelease:Number;
		private var _swipeFailed:Boolean;

		private var _zoomScale:Number;
		
		private var _gauge:GaugeVO;
		private var _superPercentage:Number;
		
		private static var _instance:TouchInputController;
		
		public function TouchInputController(hero:SnailboyHero, heroView:Character, stage:DisplayObject, world:Nape, gauge:GaugeVO, camera:StarlingCamera)
		{
			_instance = this;
			
			_hero = hero;
			_heroView = heroView;
			_stage = stage;
			_world = world;
			_gauge = gauge;
			_camera = camera;
			
			setupFaceSpace();
			
			// Get Line Style from atlas
			var assets:AssetManager = Assets.assets;
			_lineTex = assets.getTexture("LineTexture");
			
			// Fake Release Items
			_fakeReleaseItems = new Vector.<Image>();
			var item:Image;
			for (var i:int = 0; i < _fakeReleaseLength; i++) 
			{
				item = new Image(_lineTex);
				item.visible = false;
				heroView.addChild(item);
				_fakeReleaseItems.push(item);
			}
			
		}
		
		public static function getInstance():TouchInputController
		{
			return _instance;
		}
		
		public function get canSwim():Boolean
		{
			return _canSwim;
		}

		public function set canSwim(value:Boolean):void
		{
			_canSwim = value;
		}

		public function get canSuperJump():Boolean
		{
			return _canSuperJump;
		}

		public function set canSuperJump(value:Boolean):void
		{
			_canSuperJump = value;
		}

		public function get superJump():Boolean
		{
			return _superJump;
		}

		public function get gauge():GaugeVO
		{
			return _gauge;
		}

		public function set gauge(value:GaugeVO):void
		{
			_gauge = value;
		}

		//*********************
		//   PUBLIC
		//*********************
		
		public function dispose():void
		{
			_instance = null;
			
			enabled = false;
			
			var i:int = _fakeReleaseItems.length;
			while(i--)
			{
				_fakeReleaseItems[i].removeFromParent(true);
			}
			
			_fakeReleaseItems = null;
			
			onZoomSignal.removeAll();
			onZoomSignal = null;
			
			onZoomEndedSignal.removeAll();
			onZoomEndedSignal = null;
			
			onLaunchSignal.removeAll();
			onLaunchSignal = null;

			onSwimStartSignal.removeAll();
			onSwimStartSignal = null;
			
			onSuperJumpUpdateSignal.removeAll();
			onSuperJumpUpdateSignal = null;
			
			_heroView = null;
			_hero = null;
		}
		
		public function get enabled():Boolean
		{
			return _enabled;
		}

		public function set enabled(value:Boolean):void
		{
			_enabled = value;
			if(_enabled)
			{
				cleanupListeners();
				
				if(controlJumpEnabled)
				{
//					_heroView.addEventListener(TouchEvent.TOUCH, onTouchHerorHandler);
					_stage.addEventListener(TouchEvent.TOUCH, onTouchHandler);
				}
				
			} else
			{
				cleanupListeners();
				cleanupFakeRelease();
			}
		}
		
		protected function onZoomHandler(event:GestureEvent):void
		{
			const gesture:ZoomGesture = event.target as ZoomGesture;
			onZoomSignal.dispatch(gesture.scaleX);
		}
		
		protected function onZoomEnded(event:GestureEvent):void
		{
			onZoomEndedSignal.dispatch();
		}
		
		public function update():void
		{
			if(_pullEnabled && !_isPanning)
			{
				pull(_power, _angle);
			} else
			{
				cleanupFakeRelease();
				resetToIdle();
			}
			
			if(gauge.gauge.volume <= 0)
			{
				if(_hero.swimming)
				{
//					_hero.swim(false);
					// Stop the hero from using super swim and just use normal swim
//					_hero.swim(false);
					_hero.updateSwimImpulse(0.5);
				}
			}
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		
		//*********************
		//   PRIVATE
		//*********************
		
		// ************************************************************************************
		// Hero Controll Triggers
		// ************************************************************************************
		
		// Pullback Trigger
		private function pull(dist:Number, angle:Number):void
		{
			_hero.pull(_power, angle);
			fakeRelease(_power, angle);
			if(_superJump)
			{
				onSuperJumpUpdateSignal.dispatch(_superPercentage);
			}
		}
		
		// Trigger the hero Jump
		private function launch():void
		{
			_fakeHero.velocity.setxy(0,0);
			_fakeHero.position = _hero.body.position;
			_hero.launch(_power * _gauge.jump.powerJump, _angle, _superJump);
			onSuperJumpUpdateSignal.dispatch(0);
			onLaunchSignal.dispatch(_superPercentage);
			_power = 0;
			_angle = 0;
		}
		
		private function cleanupListeners():void
		{
			_stage.removeEventListener(TouchEvent.TOUCH, onTouchHandler);
		}
		
		private var _pulldeg:Number=0;
		private var _tempTarget:Point = new Point();
		
		private function onTouchHerorHandler(phase:String, p:Point=null):void
		{
			if(phase == TouchPhase.BEGAN)
			{
				if(_hero.canJump)
				{
					_pressX = p.x;
					_pressY = p.y;
					
					_isDown = true;
					_canJumpDown = _hero.canJumpDown;
					_superJump = false;
					
					// Set values for the gauge and jump
					_maxSuperJumpPercentage = _gauge.gauge.volume / _gauge.jump.costSuperJump;
					_maxPulbackDistance = GameConstants.HERO_PULLBACK_THRESHOLD + (_maxSuperJumpPercentage * (GameConstants.HERO_PULLBACK_MAX - GameConstants.HERO_PULLBACK_THRESHOLD));
				} 
			}
			
			if(phase == TouchPhase.ENDED)
			{
				if(!_enabled) return;
				
				if(_isDown && !_isPanning)
				{
					if(_pullEnabled)
					{
						launch();
					} else
					{
						resetToIdle();
					}
				} else
				{
					if(_hero.swimming) _hero.swim(false);
				}
				
				_isDown = false;
				_pullEnabled = false;
			}
			
			var pullDist:Number = 0;
			if(phase == TouchPhase.MOVED)
			{
				if(_isDown && !_isPanning)
				{
					_characterMovePoint.x = p.x;
					_characterMovePoint.y = p.y;
					
					pullDist = Math.sqrt((_pressX-_characterMovePoint.x)*(_pressX-_characterMovePoint.x)+(_pressY-_characterMovePoint.y)*(_pressY-_characterMovePoint.y));
					pullDist *= 0.7;
					_angle = Math.atan2((_pressY - _characterMovePoint.y), (_pressX - _characterMovePoint.x));
					
					var deg:Number = rad2deg(_angle);
					if(pullDist > GameConstants.HERO_PULLBACK_MIN && (deg < GameConstants.HERO_PULL_ANGLE_MAX && deg > GameConstants.HERO_PULL_ANGLE_MIN))
					{
						_superPercentage = 0;
						if(pullDist >= GameConstants.HERO_PULLBACK_MAX)
						{
							pullDist = GameConstants.HERO_PULLBACK_MAX;
						}
						if(pullDist >= _maxPulbackDistance)
						{
							pullDist = _maxPulbackDistance;
						}
						
						if(pullDist > GameConstants.HERO_PULLBACK_THRESHOLD)
						{
							if(!_canSuperJump)
							{
								pullDist = GameConstants.HERO_PULLBACK_THRESHOLD;
								
							} else
							{
								_superJump = true;
								_superPercentage = (pullDist - GameConstants.HERO_PULLBACK_THRESHOLD) / (GameConstants.HERO_PULLBACK_MAX - GameConstants.HERO_PULLBACK_THRESHOLD);
								if(_superPercentage >= _maxSuperJumpPercentage)
								{
									_superPercentage = _maxSuperJumpPercentage;
									pullDist = _maxPulbackDistance;
								}
							}
						} else
						{
							_superJump = false;
						}
						_power = pullDist * (1 + (_gauge.jump.powerSuperJump * _superPercentage));
						_pullEnabled = true;
					} else
					{
						_pullEnabled = false;
					}
				}
			}
		}
		
		private var _touchRect:Rectangle = new Rectangle(10,10,70,70);
		private var _globalPoint:Point = new Point();
		
		private function onTouchHandler(event:TouchEvent):void
		{
			var stageTouch:Touch = event.getTouch(_stage);
			if (stageTouch)
			{
				switch (stageTouch.phase)
				{
					
					case TouchPhase.BEGAN:
						
						var p:Point = stageTouch.getLocation(_heroView)
						_globalPoint.x = stageTouch.globalX;
						_globalPoint.y = stageTouch.globalY;
						if(_touchRect.contains(p.x,p.y))
						{
							onTouchHerorHandler(stageTouch.phase, _globalPoint);
						} else
						{
							
							if(_hero.canSwim && _canSwim)
							{
								trySwim(stageTouch.globalX);
							} else
							{
								_isPanning = true;
								_pressX = stageTouch.globalX;
								_pressY = stageTouch.globalY;
								_tempTarget.x = _camera.target.x;
								_tempTarget.y = _camera.target.y;
								_camera.target = _tempTarget;
							}
						}
						
						break;
					case TouchPhase.ENDED:
						
						onTouchHerorHandler(stageTouch.phase);
						
						_isPanning = false;
						_camera.target = _hero;
						
						if(_hero.swimming) _hero.swim(false);
						event.stopImmediatePropagation();
						break;
					case TouchPhase.MOVED:
						
						if(_isPanning)
						{
							_tempTarget.x = _hero.x + (_pressX - stageTouch.globalX);
							_tempTarget.y = _hero.y + (_pressY - stageTouch.globalY);
						} else
						{
							var pm:Point = stageTouch.getLocation(_heroView);
							_globalPoint.x = stageTouch.globalX;
							_globalPoint.y = stageTouch.globalY;
							onTouchHerorHandler(stageTouch.phase, _globalPoint);
						}
						break;
				}
			}
		}
		
		private function trySwim(x:Number):void
		{
			var dir:Number = (x > Config.GAME_VIEW_H_CENTER) ? 1 : -1;
//			if(_gauge.gauge.volume > 0) 
//			{
				_hero.swim(true,dir, (_gauge.gauge.volume > 0) ? _gauge.swim.speedSwim : 0.8);
				onSwimStartSignal.dispatch(dir);
//			}
		}
		
		private function resetToIdle():void
		{
			_hero.resetToIdle();
		}
		
		private function cleanupFakeRelease():void
		{
			if(_fakeReleasedCleaned) return;
			for (var i:int = 0; i < _fakeReleaseLength; i++) 
			{
				_fakeReleaseItems[i].visible = false;
			}
			_fakeReleasedCleaned = true;
		}
		
		private var _space:Space;
		private var _fakeHero:Body;
		private var _fakeSpace:Space;
		private function setupFaceSpace():void
		{
			_fakeSpace = new Space(_world.gravity);
			_fakeSpace.worldLinearDrag = GameConstants.GAME_DRAG;
			_fakeHero = _hero.body.copy();
			_fakeHero.gravMass = GameConstants.HERO_MASS_GRAV;
			_fakeHero.space = _fakeSpace;
		}
		
		private var _fakeSteps:Vector.<Vec2>;
		private var _maxSuperJumpPercentage:Number;
		private var _maxPulbackDistance:int;
		private var _camera:StarlingCamera;
		
		private function fakeRelease(power:Number, angle:Number):void 
		{
			_fakeReleasedCleaned = false;
			_velx = Math.cos(angle)*(power * _gauge.jump.powerJump);
			_vely = Math.sin(angle)*(power * _gauge.jump.powerJump);
			_smoothFakeRelease = 1;
			_fakeHero.velocity.setxy(0,0);
			_fakeHero.position = _hero.body.position;
			_fakeSteps = new Vector.<Vec2>();
			_fakeHero.velocity.setxy(_velx,_vely);
			var i:int=0;
			for (i = 0; i < _fakeReleaseLength; i++)
			{
				if(_fakeSpace)
				{
					_fakeSpace.step(1/20,1,1);
					_fakeSpace.step(1/20,1,1);
					_fakeSteps.push(Vec2.weak(_fakeHero.position.x - _hero.body.position.x,_fakeHero.position.y - _hero.body.position.y));
				}
			}
			
			for (i = 0; i < _fakeSteps.length; i++) 
			{
				_gtl.x = _heroView.viewCenter.x + _fakeSteps[i].x;
				_gtl.y = _heroView.viewCenter.y + _fakeSteps[i].y;
				_smoothFakeRelease += 0.02;
				_fakeReleaseItems[i].visible = true;
				_fakeReleaseItems[i].x = _gtl.x;
				_fakeReleaseItems[i].y = _gtl.y+45;
				_fakeReleaseItems[i].scaleX = _fakeReleaseItems[i].scaleY = 0.7/_smoothFakeRelease;
				_fakeReleaseItems[i].alpha =  1/_smoothFakeRelease;
				_fakeReleaseItems[i].color = (_superJump) ? 0xff3300 : 0xffffff;
			}
		}
	}
}