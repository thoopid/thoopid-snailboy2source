package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import citrus.objects.CitrusSprite;
	
	import nape.phys.Body;
	
	import starling.animation.IAnimatable;
	import starling.core.Starling;

	public class AbstractVentGenerator extends CitrusSprite implements IEnableable
	{
		protected var _isRendered:Boolean;
		protected var _speed:Number = 100;
		protected var _fireRate:Number = 5;
		protected var _explodeDelay:Number = 0;
		protected var _rot:Number = 0;
		protected var _dc:IAnimatable;
		protected var _isOn:Boolean = false;
		
		public function AbstractVentGenerator(name:String, params:Object)
		{
			super(name,params);
			_rot = _params.rotation;
			
			for (var o:Object in _params) 
			{
				try
				{
					this[o] = _params[o];
				} catch(err:Error)
				{
					
				}
			}
			
			isRendered = false;
			
			this.view = null;
			
			trigger(isOn);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isOn():Boolean
		{
			return _isOn;
		}

		public function set isOn(value:Boolean):void
		{
			_isOn = value;
		}

		public function get explodeDelay():Number
		{
			return _explodeDelay;
		}

		public function set explodeDelay(value:Number):void
		{
			_explodeDelay = value;
		}

		public function get isRendered():Boolean
		{
			return _isRendered;
		}
		
		public function set isRendered(value:Boolean):void
		{
			if(value == _isRendered) return;
			_isRendered = value;
		}
		
		public function get fireRate():Number
		{
			return _fireRate;
		}
		
		public function set fireRate(value:Number):void
		{
			_fireRate = value;
		}
		
		public function get speed():Number
		{
			return _speed;
		}
		
		public function set speed(value:Number):void
		{
			_speed = value;
		}
		
		public function trigger(on:Boolean=true):void
		{
			_isOn = on;
			Starling.juggler.remove(_dc);
			if(_isOn)
			{
				_dc = Starling.juggler.delayCall(checkCanFire, 2);
			}
		}
		
//		public function get position():Vec2
//		{
//			return Vec2.weak(_x,_y);
//		}
		
		public function reset():void
		{
			isRendered = false;
		}
		
		public function get body():Body
		{
			return null;
		}
		
		
		override public function destroy():void
		{
			super.destroy();
			reset();
			if(_dc) Starling.juggler.remove(_dc);
//			_params = null;
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function fire():void
		{
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function checkCanFire():void
		{
			if(_dc) Starling.juggler.remove(_dc);
			_dc = Starling.juggler.delayCall(checkCanFire, _fireRate);
			if(_isRendered)
			{
				fire();
			}
		}
	}
}