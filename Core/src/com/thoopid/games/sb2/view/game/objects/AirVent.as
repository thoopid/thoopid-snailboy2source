package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import citrus.objects.platformer.nape.Sensor;
	
	public class AirVent extends Sensor implements IEnableable
	{
		private var _isOn:Boolean = true;
		private var _enabled:Boolean = true;
		private var _ventBubbles:VentBubbles;
		public function AirVent(name:String, params:Object=null)
		{
			super(name, params);
			
			this.view = _ventBubbles = new VentBubbles(params.height);
			
			beginContactCallEnabled = false;
			endContactCallEnabled = false;
			
			_isOn = params.isOn;
			
			trigger(_isOn);
		}
		
		public function get isOn():Boolean
		{
			return _isOn;
		}

		public function trigger(on:Boolean=true):void
		{
			_isOn = on;
			toggle(_isOn);
		}
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		public function set isRendered(v:Boolean):void
		{
			if(v == _enabled || !_isOn) return;
			_enabled = v;
			toggle(_enabled);
		}
		
		public function reset():void
		{
			trigger(_isOn);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function toggle(toggle:Boolean):void
		{
			_ventBubbles.enable(toggle);
		}
	}
}