package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import citrus.objects.CitrusSprite;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class AirVentTrigger extends CitrusSprite
	{
		private var _holder:Sprite;
		private var _on:Boolean = false;
		private var _glow:PoolImage;
		
		public function AirVentTrigger(name:String, params:Object=null)
		{
			super(name, params);
			
			_holder = new Sprite();
			_holder.addChild(Factory.imagePool.getImage(Assets.assets.getTexture("pp.triggerAirvent.base"),true));
			_holder.addChild(Factory.imagePool.getImage(Assets.assets.getTexture("pp.triggerAirvent"),true));
			_holder.addChild(_glow = Factory.imagePool.getImage(Assets.assets.getTexture("pp.triggerAirvent.fx"),true));
			_glow.alpha = 0;
			this.view = _holder;
			
			_holder.addEventListener(TouchEvent.TOUCH, onTouch);
			this.touchable = true;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function destroy():void
		{
			_holder.removeEventListener(TouchEvent.TOUCH, onTouch);
			_holder = null;
			_glow = null;
			
			super.destroy();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouch(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(_holder,TouchPhase.ENDED);
			if(touch)
			{
				var item:AirVent = _ce.state.getObjectByName(_params.target) as AirVent;
				if(item)
				{
					_on = !_on;
					toggle();
					item.trigger(_on);
				}
			}
		}
		
		private function toggle():void
		{
			Starling.juggler.tween(_glow, 0.3, {alpha:(_on) ? 1 : 0, transition:Transitions.EASE_IN});
		}
	}
}