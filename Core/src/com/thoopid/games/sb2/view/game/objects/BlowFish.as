package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.view.game.objects.view.BlowFishView;

	public class BlowFish extends MoveableKillzone
	{
		private var _blowfishView:BlowFishView;
		private var _orginStartY:Number;
		private var _orginEndY:Number;
		private var _randIndex:Number = 0;
		public function BlowFish(name:String, params:Object=null)
		{
			super(name, params);
			
			beginContactCallEnabled = false;
			endContactCallEnabled = false;
			
			_orginStartY = startY;
			_orginEndY = endY;
			
			this.view = _blowfishView = new BlowFishView();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isAlive():Boolean
		{
			return _blowfishView.isAlive;
		}
		
		override public function set isRendered(value:Boolean):void
		{
			if(value != isRendered) 
			{
				super.isRendered = value;
				if(_blowfishView) _blowfishView.visible = value;
			}
		}
		
		override public function update(timeDelta:Number):void
		{
			_randIndex += 0.02;
			_end.y = _orginEndY + (Math.sin(_randIndex) * 50);
			_start.y = _orginStartY + (Math.sin(_randIndex) * 50);
			
			if(_randIndex > 360)
			{
				_randIndex = 0;
			}
			
			super.update(timeDelta);
			if(_blowfishView) _blowfishView.scaleX = (_forward) ? 1 : -1;
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
		}
		
		override public function initialize(poolObjectParams:Object=null):void
		{
			_params.width = _params.width - 40;
			_params.height = _params.height - 40;
			super.initialize(_params);
		}
		
		override public function destroy():void
		{
			_blowfishView = null;
			super.destroy();
		}
	}
}