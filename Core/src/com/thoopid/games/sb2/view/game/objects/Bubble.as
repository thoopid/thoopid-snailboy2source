package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.game.factory.ObstacleFactories;
	import com.thoopid.games.sb2.view.game.objects.view.BubbleView;
	
	import citrus.objects.NapePhysicsObject;
	
	import nape.callbacks.InteractionListener;
	import nape.geom.Vec2;
	import nape.phys.BodyType;
	
	import starling.animation.IAnimatable;
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class Bubble extends NapePhysicsObject implements IEnableable
	{
		private var _image:BubbleView;
		private var _speed:Vec2 = Vec2.get();
		private var _popDelay:Number = 3;
		private var _started:Boolean = false;
		private var _bubbleContactListener:InteractionListener;
		private var _stickyHero:SnailboyHero;
		private var _popDelayCall:IAnimatable;
		private var _enabled:Boolean = true;
		
		public function Bubble(name:String, params:Object=null)
		{
			super(name, params);
			this.touchable = true;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get popDelay():Number
		{
			return _popDelay;
		}

		public function set popDelay(value:Number):void
		{
			_popDelay = value;
		}

		public function get speed():Vec2
		{
			return _speed;
		}

		public function set speed(value:Vec2):void
		{
			_speed = value;
		}
		
		public function reset():void
		{
			
		}

		public function start():void
		{
			// View
			if(_image == null)
			{
				this.view = _image = new BubbleView();
				_image.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			} else
			{
//				trace("Error")
			}
			
			_image.scaleX = _image.scaleY = 0.2;
			Starling.juggler.tween(_image, 0.5, {scaleX:1, scaleY:1, transition:Transitions.EASE_IN});
			
			// Physics
			_inverted = _speed.x < 0 || _speed.y < 0;
			this.body.setShapeFilters(CollisionConstants.COLLISION_BUBBLE_FILTER);
			updateCallEnabled = true;
			_started = true;
			
//			if(_popDelayCall) Starling.juggler.remove(_popDelayCall);
			_popDelayCall = Starling.juggler.delayCall(pop,_popDelay);
		}
		
		public function stop():void
		{
			if(_popDelayCall) Starling.juggler.remove(_popDelayCall);
			
			_started = false;
			updateCallEnabled = false;
			_body.velocity = Vec2.get();
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
		}
		
		public function attacheHero(hero:SnailboyHero):void
		{
			if(_stickyHero) return;
			_stickyHero = hero;
		}
		
		public function pop():void
		{
			stop();
			
			if(_stickyHero) 
			{
				_stickyHero.releaseFromBubble();
				_stickyHero = null;
			}
			
			if(_image)
			{
				_image.visible = false;
				_image.alpha = 0;
				_image = null;
			}
			
			this.view = null;
			
			ObstacleFactories.getInstance().bubblePool.add(this);
		}
		
		override public function update(timeDelta:Number):void {
			
			super.update(timeDelta);
			if(_started)
			{
				_body.velocity = _speed;
				_image.update();
			}
		}
		
		override public function destroy():void
		{
			pop();
			_stickyHero = null;
			super.destroy();
		}
		
		override protected function defineBody():void
		{
			_bodyType = BodyType.DYNAMIC;
		}
		
		override protected function createFilter():void
		{
			super.createFilter();
			_shape.sensorEnabled = true;
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			_body.velocity = _speed;
		}
		
		override protected function createBody():void
		{
			super.createBody();
			_body.allowRotation = false;
			_body.gravMass = 0;
		}
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(v:Boolean):void
		{
			if(v == _enabled) return;
			if(!_started) return;
			_enabled = v;
			
			_image.visible = v;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouchHandler(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(_image, TouchPhase.BEGAN);
			if(touch)
			{
				pop();
			}
		}
	}
}