package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.view.game.factory.ObstacleFactories;
	
	import nape.geom.Vec2;

	public class BubbleGenerator extends AbstractVentGenerator
	{
		public function BubbleGenerator(name:String, params:Object)
		{
			super(name, params);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		
		//*********************
		//   PRIVATE
		//*********************
		
		override protected function fire():void
		{
			var bubble:Bubble;
			
			bubble = ObstacleFactories.getInstance().bubblePool.getItem() as Bubble;
			if(bubble)
			{
				bubble.x = this.x;
				bubble.y = this.y;
				if(_speed > 0) _speed = _speed * -1;
				bubble.speed = Vec2.get(0,_speed);
				trace("explodeDelay: "+explodeDelay);
				bubble.popDelay = explodeDelay;
				if(!_ce.state.getObjectByName(bubble.name))
				{
					_ce.state.add(bubble);
				}
				bubble.start();
			} else
			{
//				trace("Error NO more missiles");
			}
		}
	}
}