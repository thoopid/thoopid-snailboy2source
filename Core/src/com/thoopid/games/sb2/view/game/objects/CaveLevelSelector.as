package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.game.elements.ui.CaveSelectorView;
	
	import citrus.objects.CitrusSprite;
	
	import nape.phys.Body;
	
	import org.osflash.signals.Signal;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class CaveLevelSelector extends CitrusSprite implements IEnableable
	{
		public var cavelevelSelected:Signal = new Signal(CaveLevelSelector);
		
		private var _levelId:String;
		private var _enabled:Boolean = true;
		private var _selector:CaveSelectorView;
		
		public function CaveLevelSelector(name:String, params:Object=null)
		{
			super(name, params);
			_levelId = params.levelId;
			
			this.view = _selector = new CaveSelectorView(_levelId);
			
			this.touchable = true;
			
			_selector.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function reflectModel():void
		{
			
		}
		
		public function set levelId(value:String):void
		{
			_levelId = value;
		}

		public function get levelId():String
		{
			return _levelId;
		}

		override public function destroy():void
		{
			_selector.removeEventListener(TouchEvent.TOUCH, onTouch);
			
			_selector = null;
			
			super.destroy();
		}
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(value:Boolean):void
		{
			if(value == _enabled) return;
			
			_enabled = value;
			_selector.visible = _enabled;
		}
		
		public function get body():Body
		{
			return null;
		}
		
		public function reset():void
		{
			
		}
		
		public function toggle(t:Boolean):void
		{
			_selector.toggle(t);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouch(e:TouchEvent):void
		{
			var t:Touch = e.getTouch(_selector, TouchPhase.ENDED);
			if(t)
			{
				cavelevelSelected.dispatch(this);
			}
		}
	}
}