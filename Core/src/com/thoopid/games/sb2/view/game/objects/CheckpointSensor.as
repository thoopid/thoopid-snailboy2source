package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import citrus.objects.platformer.nape.Sensor;
	
	public class CheckpointSensor extends Sensor implements IEnableable
	{
		public var id:String = "";
		private var _isRendered:Boolean = false;
		
		public function CheckpointSensor(name:String, params:Object=null)
		{
			super(name, params);
			
			id = params.id;
			
			endContactCallEnabled = false;
		}
		
		public function get isRendered():Boolean
		{
			return _isRendered;
		}
		public function set isRendered(v:Boolean):void
		{
			if(_isRendered == v) return;
				
			this.beginContactCallEnabled = v;
		}
		public function reset():void
		{
			
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function createConstraint():void
		{
			super.createConstraint();
			_body.cbTypes.add(CollisionConstants.checkpointCollitionType);
		}
	}
}