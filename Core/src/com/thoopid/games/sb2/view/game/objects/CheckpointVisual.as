package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import citrus.objects.CitrusSprite;
	
	import nape.phys.Body;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	
	public class CheckpointVisual extends CitrusSprite implements IEnableable
	{
		private var _image:PoolImage;
		private var _isOn:Boolean;
		private var _enabled:Boolean = false;
		
		public function CheckpointVisual(name:String, params:Object=null)
		{
			super(name, params);
			
			this.view = _image = Factory.imagePool.getImage(Assets.assets.getTexture("g.checkpoint.on"),true);
			_image.visible = false;
			this.touchable = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(value:Boolean):void
		{
			if(value == _enabled) return;
			
			_enabled = value;
			_image.visible = _enabled && _isOn;
		}
		
		public function get body():Body 
		{
			return null;
		}
		public function reset():void
		{
			toggle(false);
		}
		
		public function toggle(v:Boolean):void
		{
			if(v == _isOn) return;
			
			_isOn = v;
			_image.visible = v;
			_image.alpha = 0;
			Starling.juggler.tween(_image, 2, {alpha:(v) ? 1 : 0, transition:Transitions.EASE_OUT_BACK});
		}
	}
}