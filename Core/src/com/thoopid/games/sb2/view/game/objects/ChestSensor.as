package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IChest;
	
	import citrus.objects.platformer.nape.Sensor;
	
	import nape.geom.Vec2;
	
	import org.osflash.signals.Signal;
	
	public class ChestSensor extends Sensor implements IChest
	{
		private var _onOpenSignal:Signal = new Signal(Vec2, String);
		
		public function ChestSensor(name:String, params:Object=null)
		{
			super(name, params);
			
			beginContactCallEnabled = false;
			endContactCallEnabled = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		public function get onOpenSignal():Signal
		{
			return _onOpenSignal;
		}
		
		override public function destroy():void
		{
			_onOpenSignal.removeAll();
			_onOpenSignal = null;
			
			super.destroy();
		}
		
		public function open():void
		{
			
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function createConstraint():void
		{
			super.createConstraint();
			_body.cbTypes.add(CollisionConstants.crateCollitionType);
		}
	}
}