package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.util.ui.Warrble;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.extensions.particles.PDParticleSystem;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	
	public class CollectConsumable extends CollectableItem
	{
		private var _holder:Sprite;
		private var _image:Image;
		private var _effect:PDParticleSystem;
		
		public function CollectConsumable(name:String, params:Object=null)
		{
			super(name, params);
			
			// MUST SET THIS
			collectableType = GameCollectableTypes.TYPE_CONSUME;
			init();
		}
		
		override public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			super.isRendered = v;
			
			if(_image) _image.visible = v;
			if(_effect && !_collected) 
			{
				if(v)
				{
					Starling.juggler.add(_effect);
					_effect.start();
//					Warrble.getInstance().add(_image);
				} else
				{
					_effect.stop();
					Starling.juggler.remove(_effect);
//					Warrble.getInstance().remove(_image);
				}
			}
		}
		
		override public function collect():Boolean
		{
			if(!super.collect()) return false;
			
			if(_image)
			{
				_image.visible = false;
			}
			if(_effect)
			{
				_effect.stop();
			}
			
			return true;
		}
		
		override public function destroy():void
		{
			if(_image)
			{
				_image.removeFromParent(true);
				_image = null;
			}
			if(_effect)
			{
				_effect.stop();
				Starling.juggler.remove(_effect);
				_effect = null;
			}
			
			super.destroy();
		}
		
		override public function reset():void
		{
			super.reset();
			if(_image) _image.visible = _isRendered;
		}
		
		override public function init(params:Object=null):void
		{
			
			var tex:Texture;
			var assets:AssetManager = Assets.assets;
			tex = assets.getTexture(typeId);
//			switch(typeId)
//			{
//				case GameCollectableTypes.TYPE_VIAL_SMALL:
//				case GameCollectableTypes.TYPE_VIAL_MED:
//				case GameCollectableTypes.TYPE_VIAL_LARGE:
//					tex = assets.getTexture(typeId);
//					break;
//			}
			
			this.view = _holder = new Sprite();
			
			if(tex)
			{
				_image = new Image(tex)
				_image.scaleX = _image.scaleY = 0.6;
				
				// Effect
				_effect = new PDParticleSystem(Assets.assets.getXml("CollectableEffect"), Assets.assets.getTexture("SlimeEffectTexture"));
				_holder.addChild(_effect);
				_effect.x = _image.width >> 1;
				_effect.y = _image.height >> 1;
				
				_holder.addChild(_image);
			}
			
			super.init(params);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
	}
}