package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.util.ui.Warrble;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	
	import nape.geom.Vec2;
	import nape.phys.BodyType;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;

	public class CollectGemItem extends CollectableItem
	{
		private var _linkId:String;
		
		private var _holder:Sprite;
		private var _image:Image;
//		private var _effect:PDParticleSystem;
		
		private var _originalPoint:Vec2;
		private var _revealed:Boolean;
		
		private var _mule:Vec2 = Vec2.get(0,0);
		
		public function CollectGemItem(name:String, params:Object=null)
		{
			super(name, params);
			collectableType = GameCollectableTypes.TYPE_TRESURE;
			
			// TODO make more types of gems
			init(params);
			
			hide();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get linkId():String
		{
			return _linkId;
		}

		override public function init(params:Object=null):void
		{
			_linkId = params.linkId || "";
			
			var assets:AssetManager = Assets.assets;
			var tex:Texture = assets.getTexture(typeId);
			this.view = _holder = new Sprite();
			
			if(tex)
			{
				_image = Factory.imagePool.getImage(tex);
				_holder.addChild(_image);
			}
			
			super.init(params);
		}
		
		public function reveal(fromPoint:Vec2):void
		{
			_image.alpha = 0;
			_holder.visible = true;
			_mule.x = fromPoint.x;
			_mule.y = fromPoint.y;
			Starling.juggler.tween(_mule, 0.6 + (Math.random() * 0.4), {x:_originalPoint.x, y:_originalPoint.y, onComplete:revealed, onUpdate:onUpdate, transition:Transitions.EASE_OUT_BACK});
			Starling.juggler.tween(_image, 0.3, {delay:0.1, alpha:1, transition:Transitions.EASE_OUT_BACK});
		}
		
		override public function collect():Boolean
		{
			if(!_revealed) return false;
			
			_image.visible = false;
//			Warrble.getInstance().remove(_image);
			
			return super.collect();
		}
		
		override public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			super.isRendered = v;
			
			if(_image) _image.visible = v;
		}
		
		override public function addPhysics():void 
		{
			super.addPhysics();
			
			_originalPoint = this.body.position;
			this.body.gravMass = 0;
		}
		
		override public function reset():void
		{
			super.reset();
			
			hide();
		}
		
		override protected function defineBody():void {
			
			_bodyType = BodyType.KINEMATIC;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onUpdate():void
		{
			this.x = _mule.x;
			this.y = _mule.y;
		}
		
		private function revealed():void
		{
			_revealed = true;
//			Warrble.getInstance().add(_image);
		}
		
		private function hide():void
		{
			_revealed = false;
			_holder.visible = false;
//			Warrble.getInstance().remove(_image);
		}
	}
}