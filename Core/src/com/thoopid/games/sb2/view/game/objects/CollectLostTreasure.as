package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.view.game.objects.view.TresureView;
	
	public class CollectLostTreasure extends CollectableItem
	{
		private var _tresureView:TresureView;
		
		public function CollectLostTreasure(name:String, params:Object=null)
		{
			super(name, params);
			
			// MUST SET THIS
			collectableType = GameCollectableTypes.TYPE_TRESURE;
			init();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			super.isRendered = v;
			
			_tresureView.visible = (_collected) ? false : v;
		}
		
		override public function collect():Boolean
		{
			if(!super.collect()) return false;
			_tresureView.visible = false;
			return true;
		}
		
		override public function destroy():void
		{
			_tresureView = null;
			super.destroy();
		}
		
		override public function reset():void
		{
			super.reset();
			_tresureView.visible = !_collected;
		}
		
		override public function init(params:Object=null):void
		{
			super.init(params);
			this.view = _tresureView = new TresureView(typeId);
		}
	}
}