package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.ObjectNames;
	import com.thoopid.games.sb2.view.game.objects.view.StoryItemView;
	
	public class CollectStoryItem extends CollectableItem
	{
		private var _storyItemView:StoryItemView;
		
		public function CollectStoryItem(name:String, params:Object=null)
		{
			super(ObjectNames.LEVEL_STORY_ITEM, params);
			
			collectableType = GameCollectableTypes.TYPE_STORY;
			init();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function init(params:Object=null):void
		{
			this.view = _storyItemView = new StoryItemView(typeId);
			super.init(params);
		}
		
		override public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			super.isRendered = v;
			
//			_storyItemView.enable = !_collected;
			_storyItemView.enable = (_collected) ? !_collected : v;
		}
		
		override public function collect():Boolean
		{
			if(!super.collect()) return false;
			_storyItemView.enable = false;
			return true;
		}
		
		override public function destroy():void
		{
			_storyItemView = null;
			super.destroy();
		}
		
		override public function reset():void
		{
			super.reset();
			_storyItemView.enable = !_collected;
		}
	}
}