package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import flash.geom.Point;
	
	import citrus.objects.platformer.nape.Sensor;
	
	import starling.display.DisplayObject;
	
	public class CollectableItem extends Sensor implements IEnableable
	{
		
		protected var _isRendered:Boolean = false;
		protected var _collected:Boolean = false;
		
		private var _collectableType:String = "";		// The type of collectale
		private var _typeId:String = "";						// The individual collectable element typ

		private var _viewPosition:Point;
		
		public function CollectableItem(name:String, params:Object=null)
		{
			super(name, params);
			
			_typeId = params.typeId || "";
			
			beginContactCallEnabled = true;
			updateCallEnabled = false;
			endContactCallEnabled = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get viewClip():DisplayObject
		{
			return this.view;
		}
		
		public function init(params:Object=null):void
		{
			if(collectableType == "" || typeId == "")
			{
				throw new Error("Must Setup Collectable properties");
			}
		}
		
		public function set typeId(value:String):void
		{
			_typeId = value;
		}

		public function get collectableType():String
		{
			return _collectableType;
		}

		public function set collectableType(value:String):void
		{
			_collectableType = value;
		}

		public function get typeId():String
		{
			return _typeId;
		}

		public function get isRendered():Boolean
		{
			return _isRendered;
		}
		
		public function set isRendered(v:Boolean):void
		{
			if(_collected) return;
			if(v == _isRendered) return;
			
			_isRendered = v;
			
			if(v)
			{
//				Warrble.getInstance().add(this.view);
			} else
			{
//				Warrble.getInstance().remove(this.view);
			}
			
			if(_collected)
			{
				this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
			} else
			{
				this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
			}
		}
		
		public function collect():Boolean
		{
			if(_collected) return false;
			_collected = true;
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
			
//			Warrble.getInstance().remove(this.view);
			
			return true;
		}
		
		public function reset():void
		{
			_collected = false;
			_isRendered = false;
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
		}
		
		override protected function createFilter():void
		{
			super.createFilter();
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
		}
		
		override protected function createConstraint():void
		{
			super.createConstraint();
			_body.cbTypes.add(CollisionConstants.collectableCollitionType);
		}
	}
}