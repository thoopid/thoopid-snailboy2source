package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IChest;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.game.objects.view.CrackView;
	import com.thoopid.games.sb2.view.game.objects.view.TresureChestView;
	
	import nape.geom.Vec2;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class Crack extends ChestSensor implements IEnableable, IChest
	{
		private var _holder:CrackView;
		private var _enabled:Boolean = false;
		private var _opened:Boolean;
		
		public function Crack(name:String, params:Object=null)
		{
			super(name, params);
			
			this.view = _holder = new CrackView();
			
			this.touchable = true;
			_holder.addEventListener(TouchEvent.TOUCH, onTrigger);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(value:Boolean):void
		{
			if(value == _enabled) return;
			
			_enabled = value;
			_holder.visible = _enabled;
		}
		
		public function reset():void
		{
			_holder.togggle(false);
			_opened = false;
		}
		
		override public function destroy():void
		{
			_holder = null;
			super.destroy();
		}
		
		override public function open():void
		{
			if(!_opened)
			{
				_holder.togggle(true);
				_opened = true;
				onOpenSignal.dispatch(Vec2.get(this.x,this.y), name);
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTrigger(e:TouchEvent):void
		{
			var ended:Touch = e.getTouch(_holder, TouchPhase.ENDED);
			if(ended)
			{
				open();
			}
		}
	}
}