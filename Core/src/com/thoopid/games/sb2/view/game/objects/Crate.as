package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IChest;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.game.objects.view.CrateView;
	
	import nape.geom.Vec2;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class Crate extends ChestSensor implements IEnableable, IChest
	{
		private var _holder:CrateView;
		
		private var _enabled:Boolean = true;
		private var _opened:Boolean;
		
		public function Crate(name:String, params:Object=null)
		{
			super(name, params);
			
			this.view = _holder = new CrateView();
			this.touchable = true;
			_holder.addEventListener(TouchEvent.TOUCH, onTrigger);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(value:Boolean):void
		{
			if(value == _enabled) return;
			
			_enabled = value;
			_holder.visible = _enabled;
		}
		
		public function reset():void
		{
			_holder.togggle(false);
			_opened = false;
		}
		
		override public function destroy():void
		{
			_holder = null;
			super.destroy();
		}
		
		override public function open():void
		{
			if(!_opened)
			{
				_holder.togggle(true);
				_opened = true;
				onOpenSignal.dispatch(Vec2.get(this.x,this.y), name);
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTrigger(e:TouchEvent):void
		{
			var ended:Touch = e.getTouch(_holder, TouchPhase.ENDED);
			if(ended)
			{
				open();
			}
		}
	}
}