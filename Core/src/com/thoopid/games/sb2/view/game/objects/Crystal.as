package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import citrus.objects.CitrusSprite;
	
	import nape.phys.Body;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.extensions.particles.PDParticleSystem;
	import starling.utils.deg2rad;
	
	public class Crystal extends CitrusSprite implements IEnableable
	{
		private var _holder:Sprite;
		private var _enabled:Boolean;
		private var _isOn:Boolean = false;

		private var _effect:PDParticleSystem;
		public function Crystal(name:String, params:Object=null)
		{
			super(name, params);
			
			this.view = _holder = new Sprite();
			
			_effect = new PDParticleSystem(Assets.assets.getXml("CrystalEffect"), Assets.assets.getTexture("pp.crystalA.fx.01"));
			_effect.rotation = deg2rad(90);
			_effect.emitterYVariance = width >> 1;
			_effect.emitterXVariance = height >> 1;
			_effect.maxNumParticles *= (width*0.01);
			_holder.addChild(_effect);
			
			trigger(isOn);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function destroy():void
		{
			super.destroy();
			
			if(_effect)
			{
				_effect.stop();
				_effect.removeFromParent(true);
				_effect = null;
			}
		}

		public function get isOn():Boolean
		{
			return _isOn;
		}

		public function set isOn(value:Boolean):void
		{
			_isOn = value;
		}

		public function trigger(on:Boolean=true):void
		{
			_isOn = on;
			toggle(_isOn);
		}
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		public function set isRendered(v:Boolean):void
		{
			if(v == _enabled || !_isOn) return;
			
			_enabled = v;
			
			toggle(_enabled);
		}
		
		private function toggle(toggle:Boolean):void
		{
			if(toggle)
			{
				Starling.juggler.add(_effect);
				_effect.start();
			} else
			{
				Starling.juggler.remove(_effect);
				_effect.stop();
			}
		}
		public function get body():Body
		{
			return null;
		}
		public function reset():void
		{
			toggle(_isOn);
		}
	}
}