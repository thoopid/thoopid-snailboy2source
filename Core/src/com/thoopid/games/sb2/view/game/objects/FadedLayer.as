package com.thoopid.games.sb2.view.game.objects
{
	import starling.display.Quad;
	
	public class FadedLayer extends ParalaxLayer
	{
		private var _fadedImage:Quad;
		public function FadedLayer()
		{
			super("FadeLayer", {});
			_holder.addChild(_fadedImage = new Quad(1000,1000,Config.GAME_PARALAX_COLOR));
			_fadedImage.pivotX = _fadedImage.width >> 1;
			_fadedImage.pivotY = _fadedImage.height >> 1;
			_fadedImage.alpha = 0.7;
			
			init(false)
		}
	}
}