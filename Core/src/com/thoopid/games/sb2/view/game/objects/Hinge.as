package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	import citrus.objects.NapePhysicsObject;
	
	import nape.geom.Vec2;
	import nape.phys.BodyType;
	import nape.phys.Material;
	
	import starling.display.Image;
	import starling.utils.deg2rad;
	
	public class Hinge extends NapePhysicsObject
	{
		private var _pivotPoint:Vec2 = Vec2.get();
		private var _destinationRot:Number;
		private var _origRot:Number;
		private var _image:Image;
		private var _on:Boolean = false;
		
		public function Hinge(name:String, params:Object=null)
		{
			params.height = 10;
			super(name, params);
			_pivotPoint.x = (params.width >> 1) - 25;
			_pivotPoint.y = 0;
			_destinationRot = deg2rad(_params.rotationEnd || 0);
			_origRot = deg2rad(_params.rotation);
			
			this.view = _image = new Image(Assets.assets.getTexture("pp.hingePlatform"));
//			_image.pivotY = -10;
//			_image.scaleX = _image.scaleY = 0.5
		}
		
		override protected function createFilter():void {
			
			_body.setShapeFilters(CollisionConstants.COLLISION_HINGE_FILTER);
		}
		
		override protected function defineBody():void {
			
			_bodyType = BodyType.DYNAMIC;
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			
			_body.gravMass = 0;
			_body.mass = 100;
			_body.rotation = _origRot;
		}
		
		override protected function createMaterial():void
		{
			_material = new Material(0.65, 0.57, 1.2, 1, 0);
		}
		
		public function trigger(on:Boolean):void
		{
			_on = on;
			updateCallEnabled = true;
		}
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			
			var dest:Number = (_on) ? _destinationRot - this.body.rotation : _origRot - this.body.rotation;
			var dif:Number = Math.abs(dest);
			var velocity:Number = 0;
			
			
			if (dif > 0.1)
			{
				// Still has futher to go. Normalize the velocity to the speed
				velocity = 0.04;
			} else
			{
				velocity = dif;
				updateCallEnabled = false;
			}
			velocity *= (dest/Math.abs(dest));
			
			this.body.rotate(this.body.localPointToWorld(_pivotPoint),velocity);
			
		}
	}
}