package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import citrus.objects.CitrusSprite;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.deg2rad;
	
	public class HingeTrigger extends CitrusSprite
	{
		private var _btn:Button;
		private var _holder:Sprite;
		private var _box:PoolImage;
		private var _lever:PoolImage;
		
		private var _on:Boolean = false;
		public function HingeTrigger(name:String, params:Object=null)
		{
			super(name, params);
//			this.x = params.x;
//			this.y = params.y;
			
			_holder = new Sprite();
			_holder.addChild(_lever = Factory.imagePool.getImage(Assets.assets.getTexture("pp.triggerHinge.lever")));
			_lever.pivotY = _lever.height >> 1;
			_lever.rotation = deg2rad(-15);
			_lever.x = 10;
			_holder.addChild(_box = Factory.imagePool.getImage(Assets.assets.getTexture("pp.triggerHinge.base"),true));
			this.view = _holder;
			
			_holder.addEventListener(TouchEvent.TOUCH, onTouch);
			this.touchable = true
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function destroy():void
		{
			super.destroy();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouch(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(_holder,TouchPhase.ENDED);
			if(touch)
			{
				var item:Hinge = _ce.state.getObjectByName(_params.target) as Hinge;
				if(item)
				{
					_on = !_on;
					toggle();
					item.trigger(_on);
				}
			}
		}
		
		private function toggle():void
		{
			Starling.juggler.tween(_lever, 0.3, {rotation:(_on) ? deg2rad(15) : deg2rad(-15), transition:Transitions.EASE_IN});
		}
	}
}