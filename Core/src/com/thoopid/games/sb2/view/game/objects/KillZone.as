package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	import flash.display.BitmapData;
	
	import citrus.objects.NapePhysicsObject;
	import citrus.objects.platformer.nape.Sensor;
	
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class KillZone extends Sensor
	{
		private var _target:NapePhysicsObject;
		
		private var _targetName:String;
		private var _linked:Boolean = false;
		
		public function KillZone(name:String, params:Object=null)
		{
			super(name, params);
			
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function addPhysics():void
		{
			super.addPhysics();
			
			// Collision type
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
			// Collision type
//			this.body.cbTypes.add(CollisionConstants.killZoneCollitionType);
			
			if(Config.DEBUG)
			{
				this.view = new Image(Texture.fromBitmapData(new BitmapData(this.width, this.height, false, 0x990000)));
			}
		}
		
		override public function destroy():void
		{
			this.onBeginContact.removeAll();
			this.onEndContact.removeAll();
			super.destroy();
		}
	}
}