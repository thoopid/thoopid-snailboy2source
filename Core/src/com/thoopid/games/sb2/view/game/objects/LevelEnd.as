package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.constants.game.ObjectNames;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.effect.LevelEndCaveEffect;
	
	import citrus.objects.platformer.nape.Sensor;
	
	public class LevelEnd extends Sensor implements IEnableable
	{
		private var _isRendered:Boolean = false;
		private var _effect:LevelEndCaveEffect;
		
		public function LevelEnd(name:String, params:Object=null)
		{
			super(ObjectNames.LEVEL_END, params);
			
			beginContactCallEnabled = true;
			updateCallEnabled = false;
			endContactCallEnabled = false;
			
			this.view = _effect = new LevelEndCaveEffect();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override protected function createFilter():void
		{
			super.createFilter();
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
		}
		
		public function get isRendered():Boolean
		{
			return true;
		}
		
		public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			
			_isRendered = v;
			
			_effect.enabled = _isRendered;
			
			beginContactCallEnabled = _isRendered;
			
			if(_isRendered)
			{
				this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
			} else
			{
				this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
			}
		}
		
		public function collect():Boolean
		{
			return true;
		}
		
		public function reset():void
		{
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
		}
	}
}