package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.game.factory.ObstacleFactories;
	import com.thoopid.games.sb2.view.game.objects.view.MineHeadView;
	
	import citrus.objects.platformer.nape.Sensor;
	
	import coza.runtime.utils.number.RandomRange;
	
	public class Mine extends Sensor implements IEnableable
	{
		private var _w:Number = 0;
		private var _h:Number = 0;
		private var _holder:MineHeadView;
		private var _enabled:Boolean = false;
		
		public function Mine(name:String, params:Object=null)
		{
			super(name, params);
			
			beginContactCallEnabled = false;
			endContactCallEnabled = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function reset():void
		{
			
		}
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(v:Boolean):void
		{
			if(_enabled == v) return;
			
			_enabled = v;
			_holder.enabled = v;
		}
		
		public function init(params:Object):void
		{
			_params = params;
			this.x = params.x;
			this.y = params.y;
			
			_w = params.width;
			_h = params.height;
			
			if(_holder == null)
			{
				this.view = _holder = new MineHeadView(_params.chainLength || RandomRange.randRange(110,160));
				_holder.width = _w;
				_holder.height = _h;
			}
			
			initialize(_params);
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
		}
		
		override public function initialize(poolObjectParams:Object=null):void
		{
			_params.width = _w-40;
			_params.height = _h-40;
			super.initialize(_params);
		}
		
		override public function destroy():void
		{
			_holder = null;
			ObstacleFactories.getInstance().minesPool.add(this);
			super.destroy();
		}
	}
}