package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	
	public class MoveableKillzone extends MoveableObstacle
	{
		private var _isKillzone:Boolean = false;
		
		public function MoveableKillzone(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function addPhysics():void
		{
			super.addPhysics();
			
			this.body.setShapeFilters(CollisionConstants.COLLISION_OBSTACLE_FILTER);
//			this.body.cbTypes.add(CollisionTriggers.killZoneCollitionType);
			// Collision filter
		}
		
		override protected function createFilter():void
		{
			super.createFilter();
			_shape.sensorEnabled = true;
		}
	}
}