package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	
	import flash.display.BitmapData;
	
	import citrus.objects.platformer.nape.MovingPlatform;
	
	import nape.geom.Vec2;
	
	import starling.animation.DelayedCall;
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class MoveableObstacle extends MovingPlatform implements IEnableable
	{
		private var _triggered:Boolean = false;
		private var _startPoint:Vec2;
		private var _startEnabled:Boolean = false;
		private var _isOneWay:Boolean = false;
		private var _delay:Number = 0;
		
		private var _currentForward:Boolean = true;
		private var _currentJuglerDelay:DelayedCall;
		
		private var _isRendered:Boolean = false;
		
		public function MoveableObstacle(name:String, params:Object=null)
		{
			// Strip Out any parameters built into the name
			super(name, params);
			
			_startPoint = Vec2.get(params.x, params.y);
			this.enabled = _startEnabled = params.started;
			this.speed = params.speed;
			this.startX = Math.round(params.x);
			this.startY = Math.round(params.y);
			this.endX = Math.round(params.destX);
			this.endY = Math.round(params.destY);
			
			if(Config.DEBUG)
			{
				this.view = new Image(Texture.fromBitmapData(new BitmapData(this.width, this.height, false, 0x00CCFF)));
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isRendered():Boolean
		{
			return _isRendered;
		}

		public function set isRendered(value:Boolean):void
		{
			if(_isRendered == value) return;
			
			_isRendered = value;
		}

		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			
			// Collision filter
			this.body.setShapeFilters(CollisionConstants.COLLISION_OBSTACLE_FILTER);
		}
		
		override public function destroy():void
		{
			super.destroy();
		}
		
		public function trigger():void
		{
			if(_triggered) return;
			_triggered = true;
		}
		
		public function reset():void
		{
			this.x = _startPoint.x;
			this.y = _startPoint.y;
			_triggered = false;
		}
		
		public function get triggered():Boolean
		{
			return _triggered;
		}
	}
}