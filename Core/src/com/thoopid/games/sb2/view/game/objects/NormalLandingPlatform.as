package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	import citrus.objects.platformer.nape.Platform;
	
	public class NormalLandingPlatform extends Platform
	{
		public function NormalLandingPlatform(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			this.body.setShapeFilters(CollisionConstants.COLLISION_PLATFORM_FILTER);
			this.body.cbTypes.add(CollisionConstants.normalPlatformCollitionType);
		}
	}
}