package com.thoopid.games.sb2.view.game.objects
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class ParalaxLayer extends CitrusSprite
	{
		protected var _holder:Sprite;
		
		public function ParalaxLayer(name:String, params:Object=null)
		{
			super(name, params);
			_holder = new Sprite();
//			trace("Paralax layer Alpha: "+_holder.alpha)
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get holder():Sprite
		{
			return _holder;
		}

		public function add(image:Image):void
		{
			_holder.addChild(image);
		}
		
		public function init(touch:Boolean=false, flatten:Boolean=true):void
		{
			if(flatten) _holder.flatten();
			_holder.touchable = touch;
			this.touchable = touch
			this.view = _holder;
		}
		
		override public function destroy():void
		{
//			_holder.unflatten();
			_holder.removeFromParent(false);
			_holder = null;
			this.view = null;
			
			super.destroy();
		}
	}
}