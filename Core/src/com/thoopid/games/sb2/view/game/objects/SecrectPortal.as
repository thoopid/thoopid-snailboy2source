package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.game.objects.view.PortalView;
	
	import citrus.objects.NapePhysicsObject;
	import citrus.objects.platformer.nape.Sensor;
	
	import nape.geom.Vec2;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	
	public class SecrectPortal extends Sensor implements IEnableable
	{
		private var _holder:PortalView;
		private var _enabled:Boolean = true;
		private var _contact:NapePhysicsObject;
		private var _linkedPortalId:String = "";
		private var _collectorClass:Class = SnailboyHero;
		private var _isPrimary:Boolean;
		
		public function SecrectPortal(name:String, params:Object=null)
		{
			super(name, params);
			_linkedPortalId = params.portalId || "";
			_isPrimary = (params.isPrimary != null) ? params.isPrimary : false;
			
			this.view = _holder =  new PortalView();
			
			beginContactCallEnabled = false;
			endContactCallEnabled = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function addPhysics():void
		{
			super.addPhysics();
			
			if(_isPrimary)
			{
				enable();
			} else
			{
				disable();
			}
		}
		
		override protected function createFilter():void
		{
			super.createFilter();
		}
		
		public function enable():void
		{
			this.body.setShapeFilters(CollisionConstants.COLLISION_PORTAL_FILTER);
		}
		public function disable():void
		{
			_body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
		}
		
		public function acceptTransprt(hero:SnailboyHero):void
		{
			hero.body.velocity = Vec2.weak(0,0);
			hero.x = this.x;
			hero.y = this.y;
			hero.visible = false;
			var prevMass:Number = hero.body.gravMass;
			hero.body.gravMass = 0;
			Starling.juggler.delayCall(completeTransportAccept,2,hero,prevMass);
		}
		
		public function transport(hero:SnailboyHero):void
		{
			var targetPortal:SecrectPortal = _ce.state.getObjectByName(_linkedPortalId) as SecrectPortal;
			if(targetPortal && hero)
			{
				disable();
				Starling.juggler.tween(hero, 0.8, {x:this.x, y:this.y, transition:Transitions.EASE_IN, onComplete:function():void{targetPortal.acceptTransprt(hero);}});
			}
		}
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(value:Boolean):void
		{
			if(value == _enabled) return;
			
			_enabled = value;
			_holder.enabled = _enabled
		}
		
		public function reset():void
		{
			_enabled = true;
		}
		
		override public function destroy():void
		{
			_holder = null;
			super.destroy();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function createConstraint():void
		{
			super.createConstraint();
			_body.cbTypes.add(CollisionConstants.secretAreaCollitionType);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function completeTransportAccept(hero:SnailboyHero,mass:Number):void
		{
			hero.visible = true;
			hero.body.gravMass = mass;
			hero.body.velocity = Vec2.get(20,20);
			Starling.juggler.delayCall(enable,2);
			hero.sink();
		}
		
	}
}