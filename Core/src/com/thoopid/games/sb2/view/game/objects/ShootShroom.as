package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	
	import citrus.objects.platformer.nape.Sensor;
	
	import nape.geom.Vec2;

	public class ShootShroom extends Sensor
	{
		private var _powerY:int;
		private var _enabled:Boolean = false;
		private var _shootVec:Vec2;
		private var _targetVec:Vec2;
		
		public function ShootShroom(name:String, params:Object=null)
		{
			params.width-= 40;
			params.height-= 10;
			super(name, params);
			
			_targetVec = Vec2.get(params.targetX || 0, params.targetY || 0);
			this.view = Factory.imagePool.getImage(Assets.assets.getTexture("pp.trampoline.top_01"));
		}
		
		public function get targetVec():Vec2
		{
			return _targetVec;
		}
		public function get shootVec():Vec2
		{
			return _shootVec;
		}

		public function get enabled():Boolean
		{
			return _enabled;
		}
		
		public function set enabled(v:Boolean):void
		{
			if(v == _enabled) return;
			_enabled = v;
			this.view.visible = v;
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
			this.body.cbTypes.add(CollisionConstants.shootPlatformCollitionType);
		}
	}
}