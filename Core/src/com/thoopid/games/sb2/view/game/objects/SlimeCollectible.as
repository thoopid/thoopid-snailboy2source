package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.util.ui.Warrble;
	import com.thoopid.games.sb2.view.game.factory.ObstacleFactories;
	import com.thoopid.games.sb2.view.game.factory.SlimeyCollectEffectFactory;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	
	import citrus.sounds.CitrusSound;
	
	import coza.runtime.utils.number.RandomRange;
	
	import nape.geom.Vec2;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.extensions.particles.PDParticleSystem;
	
	public class SlimeCollectible extends CollectableItem
	{
		private var _coinCollectEffect:PDParticleSystem;
		private var _slimey:MovieClip;
		private var _startPoint:Vec2;
		private var _sound:CitrusSound;
		private var _isSuper:Boolean;
		
		public function SlimeCollectible(name:String, params:Object=null)
		{
			params.typeId = GameCollectableTypes.TYPE_SLIME;
			super(name, params);
			
			// MUST SET THIS
			collectableType = GameCollectableTypes.TYPE_CONSUME;
			
			_startPoint = Vec2.get(params.x, params.y);
			this.view = new Sprite();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function init(params:Object=null):void
		{
			super.init(params);
			
			_isSuper = params.isSuper;
			
			this.x = params.x;
			this.y = params.y;
//			this.view.addChild(_slimey = Factory.imagePool.getImage(Assets.assets.getTexture((!_isSuper) ? "is.slimey.01" : "is.superSlimey.01")));
			this.view.addChild(_slimey = Factory.movieclipPool.getMovie(Assets.assets.getTextures((!_isSuper) ? "is.slimey." : "is.superSlimey.")));
			_slimey.currentFrame = RandomRange.randRange(1,_slimey.numFrames-1);
			Starling.juggler.add(_slimey);
			_slimey.play();
			_slimey.visible = false;
		}
		
		override public function set isRendered(v:Boolean):void
		{
			if(v == _isRendered) return;
			super.isRendered = v;
			_slimey.visible = v;
		}
		
		override public function collect():Boolean
		{
			if(!super.collect()) return false;
			
			_slimey.visible = false;
			_coinCollectEffect = SlimeyCollectEffectFactory.getInstance().getItem();
			this.view.addChild(_coinCollectEffect);
			Starling.juggler.add(_coinCollectEffect);
			_coinCollectEffect.x = _slimey.width * 0.5;
			_coinCollectEffect.y = _slimey.height * 0.5;
			_coinCollectEffect.start(0.4);
			
//			Warrble.getInstance().remove(_slimey);
			_slimey.stop();
			Starling.juggler.remove(_slimey);
			
			return true;
		}
		
		override public function destroy():void
		{
			ObstacleFactories.getInstance().slimePool.add(this);
			_slimey.stop();
			Starling.juggler.remove(_slimey);
			super.destroy();
		}
		
		override public function reset():void
		{
			super.reset();
			_slimey.visible = _isRendered;
			
			Starling.juggler.add(_slimey);
			_slimey.play();
		}
		
//		override protected function createConstraint():void
//		{
//			super.createConstraint();
//			this.body.cbTypes.add(CollisionTriggers.coinCollitionType);
//		}
		
		//*********************
		//   PRIVATE
		//*********************
		
	}
}