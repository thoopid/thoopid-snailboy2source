package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.util.ProjectileUtils;
	import com.thoopid.games.sb2.view.game.objects.view.Character;
	
	import flash.geom.Point;
	
	import citrus.objects.NapePhysicsObject;
	import citrus.objects.platformer.nape.MovingPlatform;
	import citrus.physics.nape.NapeUtils;
	
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.callbacks.PreCallback;
	import nape.callbacks.PreFlag;
	import nape.callbacks.PreListener;
	import nape.geom.Vec2;
	
	import org.osflash.signals.Signal;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	
	public class SnailboyHero extends NapePhysicsObject
	{
		public var onStickablePlatformCallback:Signal = new Signal();
		public var onLandCallback:Signal = new Signal();
		public var onHeroDie:Signal = new Signal();
		
		private var _heroView:Character;
		private var _startPoint:Point;
		private var _onGround:Boolean = false;
		private var _movingGround:NapePhysicsObject;
		private var _slapVel:Number = 0;
		private var _pullToPortal:Boolean = false;
		private var _goalPos:Vec2;
		private var _swimming:Boolean;
		private var _paddleing:Boolean;
		private var _swimDir:Number;
		private var _power:Number;
		private var _angle:Number;
		
		// Listeners
		private var _stickyWallLeftPreContactListener:PreListener;
		private var _stickyWallRightPreContactListener:PreListener;
		private var _platformPreContactListener:PreListener;
		private var _shootPlatformPreContactListener:PreListener;
		
		private var _swimImpulse:Number;
		private var _volcanoPreContactListener:InteractionListener;
		private var _bubbleSticky:Bubble;
		private var _ventTouch:Boolean;
		private var _defaultForward:int = 1;
		
		public static var instance:SnailboyHero;
		private var _walking:Boolean;
		private var _walkingDir:int;
		
		public function SnailboyHero(params:Object=null)
		{
			super(GameConstants.HERO_NAME, {width:GameConstants.HERO_BODY_WIDTH, height:GameConstants.HERO_BODY_HEIGHT, x:params.x, y:params.y});
			_startPoint = new Point(params.x,params.y);
			_defaultForward = params.direction;
			touchable = false;
			
			instance = this;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function walk(dir:int, on:Boolean=false):void
		{
			_walking = on;
			_walkingDir = dir;
		}
		
		public function get paddleing():Boolean
		{
			return _paddleing;
		}

		public function set paddleing(value:Boolean):void
		{
			_paddleing = value;
		}

		public function get onGround():Boolean
		{
			return _onGround;
		}

		public function get swimming():Boolean
		{
			return _swimming;
		}
		
		public function get heroView():Character
		{
			return _heroView;
		}
		
		public function get canJump():Boolean
		{
			return _heroView.canJump;
		}
		
		public function get canSwim():Boolean
		{
			return _heroView.canSwim && !_bubbleSticky;
		}
		
		public function get canJumpDown():Boolean
		{
			return _heroView.canJumpDowm();
		}
		
		public function get centerPoint():Point
		{
			return _heroView.viewCenter;
		}

		public function init(goalVector:Vec2):void
		{
			_goalPos = goalVector;
		}
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			
			if(_walking)
			{
				this.body.gravMass = GameConstants.HERO_MASS_GRAV_WALK;
				this.body.velocity.setxy(_walkingDir * 50,0);
//				velocity.x += _walkingDir * 50;
			}
			
			if(_paddleing)
			{
				_body.applyImpulse(Vec2.get(_swimImpulse*_swimDir,0));
			}
			
			if(_bubbleSticky)
			{
				var closeA:Vec2 = Vec2.get();
				var destination:Vec2 = Vec2.weak(_bubbleSticky.x, _bubbleSticky.y);
				destination.subeq(body.position);
				var vel:Vec2 = destination;
				if (vel.length >= 1)
				{
					vel.muleq(5);
				}
				this.body.velocity.set(vel);
			}
			
			if(_ventTouch)
			{
				airBoost();
			}
			
			_heroView.update(this.body.velocity);
		}
		
		override protected function createFilter():void
		{
			super.createFilter();
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER);
		}
		override protected function createConstraint():void
		{
			super.createConstraint();
			this.body.cbTypes.add(CollisionConstants.heroCollitionType);
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			
			this.body.gravMass = GameConstants.HERO_MASS_GRAV;
			this.body.allowRotation = false;

			this.view = _heroView = new Character(Assets.assets);
			_heroView.defaultForward = _defaultForward;
			_heroView.jumpSignal.add(onJumpFromSpriteHandler);
			_heroView.swimFromSwimStartSignal.add(onSwimFromSpriteHandler);
			
			_stickyWallLeftPreContactListener = new PreListener(InteractionType.COLLISION, CollisionConstants.stickyWallCollitionTypeLeft, CollisionConstants.heroCollitionType, handleStickyLeftPreContact);
			_stickyWallRightPreContactListener = new PreListener(InteractionType.COLLISION, CollisionConstants.stickyWallCollitionTypeRight, CollisionConstants.heroCollitionType, handleStickyRightPreContact);
			_platformPreContactListener = new PreListener(InteractionType.COLLISION, CollisionConstants.normalPlatformCollitionType, CollisionConstants.heroCollitionType, handlePlatformPreContact);
			_shootPlatformPreContactListener = new PreListener(InteractionType.SENSOR, CollisionConstants.shootPlatformCollitionType, CollisionConstants.heroCollitionType, handleShootShroomPreContact);
			this.body.space.listeners.add(_stickyWallLeftPreContactListener);
			this.body.space.listeners.add(_stickyWallRightPreContactListener);
			this.body.space.listeners.add(_platformPreContactListener);
			this.body.space.listeners.add(_shootPlatformPreContactListener);
			
			this.updateCallEnabled = true;
			this.beginContactCallEnabled = true;
			this.endContactCallEnabled = true;
		}
		
		override public function destroy():void
		{
			instance = null;
			
			onStickablePlatformCallback.removeAll();
			onStickablePlatformCallback = null;
			
			onLandCallback.removeAll();
			onLandCallback = null;
			
			onHeroDie.removeAll();
			onHeroDie = null;
			
//			_heroView.jumpSignal.removeAll();
//			_heroView.removeFromParent(true);
			_heroView = null;
			
			super.destroy();
		}
		
		public function reset():void
		{
			_heroView.reset();
			_onGround = false;
			_pullToPortal = false;
			_swimming = false;
			_paddleing = false;
			
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER);
			this.body.gravMass = GameConstants.HERO_MASS_GRAV;
			this.body.velocity.setxy(0,0);
			_heroView.enabled = true;
			
			beginContactCallEnabled = true;
			endContactCallEnabled = true;
		}
		
		public function removeForces():void
		{
			this.body.velocity = Vec2.get();
		}
		
		public function stickToBubble(bubble:Bubble):void
		{
			if(_bubbleSticky) return;
			
			clearSwim();
			_bubbleSticky = bubble;
			_bubbleSticky.attacheHero(this);
			this.body.gravMass = GameConstants.HERO_MASS_GRAV_NONE;
			
			_heroView.toggleBubble(true);
		}
		
		public function releaseFromBubble():void
		{
			_bubbleSticky = null;
			this.body.gravMass = GameConstants.HERO_MASS_GRAV;
			
			_heroView.toggleBubble(false);
		}
		
		public function levelEnd(exit:NapePhysicsObject):void
		{
			this.body.gravMass = GameConstants.HERO_MASS_GRAV_NONE;
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
			this.body.velocity = Vec2.weak(0,0);
			
			Starling.juggler.tween(this, 0.4, {x:exit.x, y:exit.y, transition:Transitions.EASE_IN});
			
			_heroView.warp();
		}
		
		public function die():void
		{
			beginContactCallEnabled = false;
			endContactCallEnabled = false;
			
			clearSwim();
			_ventTouch = false;
			
			this.body.velocity.setxy(0,-40);
			this.body.gravMass = GameConstants.HERO_MASS_GRAV_NONE;
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
			
			_heroView.enabled = false;
			_heroView.die();
			
			onHeroDie.dispatch();
		}
		
		override public function handleBeginContact(interactionCallback:InteractionCallback):void
		{
			super.handleBeginContact(interactionCallback);
			var collider:NapePhysicsObject = NapeUtils.CollisionGetOther(this, interactionCallback);
			if (collider is VolcanoFireball || collider is Mine)
			{
				die();
			} else if(collider is SecrectPortal)
			{
				if(_bubbleSticky) 
				{
					_bubbleSticky.pop();
				}
				_heroView.warp();
//				(collider as SecrectPortal).transport(this);
			} else if(collider is Bubble)
			{
				stickToBubble(collider as Bubble);
			} else if(collider is AirVent)
			{
				if((collider as AirVent).isOn)
				{
					clearSwim();
					
					var xM:Number = _body.velocity.x;
					xM *= 0.65;
					_body.applyImpulse(Vec2.get(-xM,0));
					_ventTouch = true;
					_heroView.boost();
				}
			} else if (collider is BlowFish)
			{
				if((collider as BlowFish).isAlive)
				{
					die();
				}
			} else if (collider is KillZone)
			{
				die();
			}
		}
		
		override public function handleEndContact(callback:InteractionCallback):void
		{
			var collider:NapePhysicsObject = NapeUtils.CollisionGetOther(this, callback);
			if((collider as MovingPlatform))
			{
				_movingGround = null;
			} else if(collider is AirVent)
			{
				_ventTouch = false;
				_heroView.boostEnd();
			}
			
			if(_onGround)
			{
				_onGround = false;
			}
		}
		
		public function airBoost():void
		{
			if(_body.velocity.y >= -100)
			{
				this.body.gravMass = GameConstants.HERO_MASS_GRAV;
				_body.applyImpulse(Vec2.get(0,-8));
			}
		}
		
		public function achiveGoal():void
		{
			this.body.gravMass = GameConstants.HERO_MASS_GRAV_NONE;
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
			_heroView.enabled = false;
			this.body.velocity.setxy(0,0);
			
			_pullToPortal = true;
			_heroView.warp();
		}
		
		public function sink():void
		{
			_heroView.sink();
		}
		
		public function updateSwimImpulse(v:Number):void
		{
			_swimImpulse = v;
		}
		
		public function swim(swim:Boolean, dir:Number=1, speed:int=1):void
		{
			_swimming = swim;
			_swimDir = dir;
//			_swimDir = -_heroView.forward;
			if(swim)
			{
				this.body.gravMass = GameConstants.HERO_MASS_GRAV_SWIM
//				_swimImpulse = GameConstants.HERO_SWIM_SPEED;
				_swimImpulse = speed;
				_heroView.swim(true, -_swimDir);
			} else
			{
				_paddleing = false;
				_heroView.swim(false);
				this.body.gravMass = GameConstants.HERO_MASS_GRAV;
			}
		}
		
		public function pull(power:Number, angle:Number):void
		{
			_heroView.pull(power, angle);
		}
		
		public function resetToIdle():void
		{
			_heroView.resetToIdle();
		}
		
		private function onJumpFromSpriteHandler():void
		{
			jump(_power, _angle);
		}
		
		private function onSwimFromSpriteHandler(swim:Boolean):void
		{
			if(swim)
			{
				_paddleing = true
				var vel:Vec2 = this.body.velocity;
				vel.x += 40*_swimDir;
				vel.y -= 30;
				this.body.velocity.set(vel);
			}
		}
		
		public function launch(power:Number, angle:Number, isSuper:Boolean):void
		{
			_power = power;
			_angle = angle;
			_heroView.launch(_power,_angle, isSuper);
		}
		
		public function bounce(powerX:int, powerY:int):void
		{
//			var speed:Vec2 = this.body.velocity;
//			speed.y = -speed.y;
//			speed = speed.mul(Math.min(power,20));
//			this.body.applyImpulse(speed);
//			_heroView.bounce();
			
			_heroView.bounce();
			this.body.velocity.x = powerX * -_heroView.forward;
			this.body.velocity.y = powerY;
		}
		
		public function shoot(powerX:int,powerY:int):void
		{
			_heroView.bounce();
			this.body.velocity.x = powerX;
			this.body.velocity.y = powerY;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function handleStickyRightPreContact(callback:PreCallback):PreFlag
		{
			if((callback.arbiter.collisionArbiter.normal.x < 0) != callback.swapped || callback.arbiter.collisionArbiter.normal.x == 0)
			{
				return PreFlag.IGNORE;
			}
			heroToStickyWall(1);
			return PreFlag.ACCEPT;
		}
		
		private function handleStickyLeftPreContact(callback:PreCallback):PreFlag
		{
			if((callback.arbiter.collisionArbiter.normal.x > 0) != callback.swapped || callback.arbiter.collisionArbiter.normal.x == 0)
			{
				return PreFlag.IGNORE;
			}
			heroToStickyWall(-1);
			return PreFlag.ACCEPT;
		}
		
		private function handleShootShroomPreContact(callback:PreCallback):PreFlag
		{
			var shootPlatform:ShootShroom = callback.int1.userData.myData as ShootShroom;
			
			this.body.velocity.x = 0;
			this.body.velocity.y = 0;
			
			var x:Number = shootPlatform.targetVec.x - this.body.position.x;
			var y:Number = -(shootPlatform.targetVec.y - this.body.position.y);
			var x2:Number = x;
			var y2:Number = y;
			
			// TODO - optimize this shit!!!
			var vel:Number = Math.sqrt(x2*x2 + y2*y2);
			for (var i:int = 0; i < 5000; i++) 
			{
				if(ProjectileUtils.canHitCoordinate(x,y,i,GameConstants.GAME_GRAVITY))
				{
					vel = i;
					break;
				}
			}
			
			// Viable solution
			var dis:Number = Math.sqrt(x2*x2 + y2*y2);
			// Compensate for world drag
			vel += (Math.sqrt(dis) + ((Math.sqrt(dis) * GameConstants.GAME_DRAG) * 2 * 2));
			
			var angle1:Number = ProjectileUtils.calculateAngle1ToHitCoordinate(x, y, vel, GameConstants.GAME_GRAVITY);
			var angle2:Number = ProjectileUtils.calculateAngle2ToHitCoordinate(x, y, vel, GameConstants.GAME_GRAVITY);
			var sign:Number = x > 0 ? 1 : -1;
			var v1:Vec2 = new Vec2(sign * Math.cos(angle2) * vel, -sign * Math.sin(angle2) * vel);
			var v2:Vec2 = new Vec2(sign * Math.cos(angle1) * vel, -sign * Math.sin(angle1) * vel);
			shoot(v2.x,v2.y);
			
			return PreFlag.IGNORE;
		}
		
		private function handlePlatformPreContact(callback:PreCallback):PreFlag
		{
			if ((callback.arbiter.collisionArbiter.normal.y > 0) != callback.swapped || callback.arbiter.collisionArbiter.normal.y == 0)
			{
				return PreFlag.IGNORE;
			}
			else
			{ 
				var spring:SpringShroom = callback.int1.userData.myData as SpringShroom;
				if(spring)
				{
					bounce(spring.powerX, spring.powerY);
				} else
				{
					land();
				}
				return PreFlag.ACCEPT;
			}
		}
		
		private function heroToStickyWall(dir:int=1):void
		{
			clearSwim();
			this.body.gravMass = GameConstants.HERO_MASS_GRAV_NONE;
			this.body.velocity.setxy(0,0);
			
			_heroView.stick(dir);
			
			onStickablePlatformCallback.dispatch();
		}
		
		private function jump(power:Number, angle:Number):void
		{
			_onGround = false; 
			_movingGround = null;
			this.body.gravMass = GameConstants.HERO_MASS_GRAV;
			
			var x:Number = Math.cos(angle)*power;
			var y:Number = Math.sin(angle)*power;
			this.body.velocity.setxy(x,y);
		}
		
		private function land():void
		{
			if(_heroView.land())
			{
				clearSwim();
				_onGround = true;
				this.body.velocity.setxy(0,0);
				onLandCallback.dispatch();
			}
		}
		
		private function clearSwim():void
		{
			_swimming = false;
			_paddleing = false;
		}
	}
}