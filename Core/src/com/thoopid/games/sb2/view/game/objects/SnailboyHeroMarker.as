package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	import citrus.objects.NapePhysicsObject;
	
	public class SnailboyHeroMarker extends NapePhysicsObject
	{
		private var _defaultForward:String;
		public function SnailboyHeroMarker(name:String, params:Object=null)
		{
			super("heroPos", params);
			_defaultForward = params.direction || "right";
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get defaultForward():int
		{
			return (_defaultForward == "left") ? 1 : -1;
		}

		override protected function createFilter():void
		{
			this.body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
		}
	}
}