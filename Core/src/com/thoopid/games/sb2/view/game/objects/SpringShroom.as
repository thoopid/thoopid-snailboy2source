package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.ui.comps.Factory;

	public class SpringShroom extends NormalLandingPlatform implements IEnableable
	{
		private var _powerX:int = 0;
		private var _powerY:int = 0;
		private var _enabled:Boolean = false;
		public function SpringShroom(name:String, params:Object=null)
		{
			super(name, params);
			_powerX = params.powerX || 100;
			_powerY = params.powerY || 100;
			_powerY *= -1;
			
			this.view = Factory.imagePool.getImage(Assets.assets.getTexture("pp.trampoline.top_02"));
		}
		
		public function get powerY():int
		{
			return _powerY;
		}

		public function get powerX():int
		{
			return _powerX;
		}

		public function get isRendered():Boolean
		{
			return _enabled;
		}
		
		public function set isRendered(v:Boolean):void
		{
			if(v == _enabled) return;
			_enabled = v;
			this.view.visible = v;
		}
		
		public function reset():void
		{
			
		}
	}
}