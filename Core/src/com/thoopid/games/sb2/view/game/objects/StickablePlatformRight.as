package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	import citrus.objects.platformer.nape.Platform;
	
	import nape.callbacks.CbType;
	import nape.callbacks.PreListener;
	
	public class StickablePlatformRight extends Platform
	{
		private const STICK_ONEWAY_PLATFORM:CbType = new CbType();
		
		private var _oneWay:Boolean = false;
		private var _preListener:PreListener;
		
		public function StickablePlatformRight(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			this.body.setShapeFilters(CollisionConstants.COLLISION_PLATFORM_FILTER);
			this.body.cbTypes.add(CollisionConstants.stickyWallCollitionTypeRight);
		}
	}
}