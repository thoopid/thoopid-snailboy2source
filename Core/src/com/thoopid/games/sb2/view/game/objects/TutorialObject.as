package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.view.game.tut.DragTutorial;
	import com.thoopid.games.sb2.view.game.tut.ExitTutorial;
	import com.thoopid.games.sb2.view.game.tut.PullTutorial;
	import com.thoopid.games.sb2.view.game.tut.SecretAreaTutorial;
	import com.thoopid.games.sb2.view.game.tut.StickTutorial;
	import com.thoopid.games.sb2.view.game.tut.StoryItemTutorial;
	import com.thoopid.games.sb2.view.game.tut.SuperJumpTutorial;
	import com.thoopid.games.sb2.view.game.tut.SwimTutorial;
	
	import citrus.objects.CitrusSprite;
	
	public class TutorialObject extends CitrusSprite
	{
		public var tutId:String = "";
		public function TutorialObject(name:String, params:Object=null)
		{
			super(name, params);
			
			this.touchable = true;
			
			this.tutId = params.tutId;
			
			switch(tutId)
			{
				case TutorialConstants.TUT_STEP_JUMP:
					this.view = new PullTutorial();
					break;
				case TutorialConstants.TUT_STEP_SUPER_JUMP:
					this.view = new SuperJumpTutorial();
					break;
				case TutorialConstants.TUT_STEP_STICK:
					this.view = new StickTutorial();
					break;
				case TutorialConstants.TUT_STEP_STORY_ELEMENT:
					this.view = new StoryItemTutorial();
					break;
				case TutorialConstants.TUT_STEP_SECRET_AREA:
					this.view = new SecretAreaTutorial();
					break;
				case TutorialConstants.TUT_STEP_SWIM:
					this.view = new SwimTutorial();
					break;
				case TutorialConstants.TUT_STEP_EXIT:
					this.view = new ExitTutorial();
					break;
				case TutorialConstants.TUT_STEP_DRAG:
					this.view = new DragTutorial();
					break;
			}
		}
	}
}