package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	import citrus.objects.platformer.nape.Sensor;
	
	public class TutorialSensor extends Sensor
	{
		private var _id:String;
		public function TutorialSensor(name:String, params:Object=null)
		{
			super(name, params);
			
			_id = params.id || "";
		}
		
		public function get id():String
		{
			return _id;
		}

		override protected function createFilter():void
		{
			super.createFilter();
			this.body.setShapeFilters(CollisionConstants.COLLISION_SENSOR_FILTER);
			this.body.cbTypes.add(CollisionConstants.tutorialCollitionType);
		}
	}
}