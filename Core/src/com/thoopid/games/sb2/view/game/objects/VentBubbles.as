package com.thoopid.games.sb2.view.game.objects
{
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.extensions.particles.PDParticleSystem;
	import starling.utils.AssetManager;
	
	public class VentBubbles extends Sprite
	{
		private var _ps:PDParticleSystem;
		public function VentBubbles(height:Number)
		{
			super();
			
			var a:AssetManager = Assets.assets;
			
			var obj:Object = a.getXml("VentBubbles");
			var xml:XML = XML(obj);
			this.addChild(_ps = new PDParticleSystem(xml,a.getTexture("pp.bubble.01")));
			this.scaleY = -1;
			this.pivotY = height >> 1;
		}
		
		public function enable(value:Boolean):void
		{
			this.visible = value;
			if(value)
			{
				Starling.juggler.add(_ps);
				_ps.start();
			} else
			{
				Starling.juggler.remove(_ps);
				_ps.stop();
			}
		}
		
		override public function dispose():void
		{
			_ps.stop();
			_ps.removeFromParent(true);
			_ps = null;
			
			super.dispose();
		}
	}
}