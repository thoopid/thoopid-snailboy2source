package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	import com.thoopid.games.sb2.interfaces.game.IEnableable;
	import com.thoopid.games.sb2.view.game.factory.ObstacleFactories;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	
	import citrus.objects.NapePhysicsObject;
	
	import nape.callbacks.InteractionCallback;
	import nape.geom.Vec2;
	
	import org.osflash.signals.Signal;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.MovieClip;
	
	public class VolcanoFireball extends NapePhysicsObject implements IEnableable
	{
		public var onExplode:Signal = new Signal(VolcanoFireball);
		
		private var _velocity:Vec2 = Vec2.get();
		private var _velocityDead:Vec2 = Vec2.get(0.0);
		private var _speed:Number = 100;
		private var _angle:Number = 100;

		private var _fireballClip:MovieClip;
		private var _started:Boolean = false;
		private var _enabled:Boolean = false;
		
		public function VolcanoFireball(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isRendered():Boolean
		{
			return _enabled;
		}

		public function set isRendered(value:Boolean):void
		{
			if(!_started) return;
			if(value == _enabled) return;
			
			_enabled = value;
		}

		public function get angle():Number
		{
			return _angle;
		}

		public function set angle(value:Number):void
		{
			_angle = value;
		}

		public function get speed():Number
		{
			return _speed;
		}

		public function set speed(value:Number):void
		{
			_speed = value;
		}
		
		public function reset():void
		{
			stop();
			killMissile();
		}
		
		public function init():void
		{
			if(_fireballClip == null)
			{
				this.view = _fireballClip = Factory.movieclipPool.getMovie(Assets.assets.getTextures("pp.volcanoFX.0"));
			} else
			{
//				trace("Error")
			}
			_fireballClip.loop = true;
			_fireballClip.pivotX = 15;
		}
		
		public function start():void
		{
			// Physics
			_velocity = new Vec2(speed, 0);
			_velocity.rotate(angle * Math.PI / 180);
			_body.rotation = angle * Math.PI / 180;
			_inverted = speed < 0;
			
			_fireballClip.visible = true;
			_fireballClip.play();
			Starling.juggler.add(_fireballClip);
			
			// Fade fireball in
			Starling.juggler.removeTweens(_fireballClip);
			_fireballClip.alpha = 0;
			Starling.juggler.tween(_fireballClip,0.4,{alpha:1, transition:Transitions.EASE_IN});
			
			this.body.setShapeFilters(CollisionConstants.COLLISION_VALCANO_FILTER);
			
			updateCallEnabled = true;
			_beginContactCallEnabled = true;
			_started = true;
		}
		
		public function stop():void
		{
			_enabled = false;
			_started = false;
			updateCallEnabled = false;
			_beginContactCallEnabled = false;
			
			_body.setShapeFilters(CollisionConstants.COLLISION_HERO_FILTER_NONE);
		}

		override public function update(timeDelta:Number):void 
		{
			super.update(timeDelta);
			_body.velocity = _velocity;
		}
		
		override public function destroy():void
		{
			killMissile();
			super.destroy();
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
			_body.velocity = _velocity;
		}
		
		override protected function createFilter():void
		{
			super.createFilter();
			_shape.sensorEnabled = true;
		}
		
//		override protected function createConstraint():void
//		{
//			super.createConstraint();
//			this.body.cbTypes.add(CollisionTriggers.volcanoCollitionType);
//		}
		
		override protected function createBody():void
		{
			super.createBody();
			_body.allowRotation = false;
			_body.gravMass = 0;
		}
		
		override public function handleBeginContact(callback:InteractionCallback):void
		{
			explode();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		private function explode():void
		{
			onExplode.dispatch(this);
			stop();
			killMissile();
		}
		
		private function killMissile():void
		{
			if(_body) _body.velocity = _velocityDead;
			
			if(_fireballClip)
			{
				_fireballClip.visible = false;
				_fireballClip.alpha = 0;
				_fireballClip.stop();
				_fireballClip = null;
			}
			
			this.view = null;
			
			ObstacleFactories.getInstance().volcanoMisslePool.add(this);
			
			onExplode.removeAll();
		}
	}
}