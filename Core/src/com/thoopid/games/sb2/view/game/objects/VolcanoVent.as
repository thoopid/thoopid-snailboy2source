package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.view.game.factory.FireballEffectsFactory;
	import com.thoopid.games.sb2.view.game.factory.ObstacleFactories;
	
	import starling.core.Starling;
	import starling.extensions.particles.PDParticleSystem;

	public class VolcanoVent extends AbstractVentGenerator
	{
		public function VolcanoVent(name:String, params:Object)
		{
			super(name, params);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		
		//*********************
		//   PRIVATE
		//*********************
		
		override protected function fire():void
		{
			var missile:VolcanoFireball;
			
			var angle:Number = _rot - 90;
			missile = ObstacleFactories.getInstance().volcanoMisslePool.getItem() as VolcanoFireball;
			if(missile)
			{
				missile.init();
				missile.onExplode.addOnce(onExplode);
				missile.x = _x;
				missile.y = _y;
				missile.speed = _speed;
				missile.angle = angle;
				if(!_ce.state.getObjectByName(missile.name))
				{
					_ce.state.add(missile);
				}
				
				// Particle Effect
				makeEffect(missile);
				
				missile.start();
			} else
			{
//				trace("No Missile to Fire this time")
			}
		}
		
		private function onExplode(missile:VolcanoFireball):void
		{
			makeEffect(missile);
		}
		
		private function makeEffect(m:VolcanoFireball):void
		{
			var fireballShootEffect:PDParticleSystem = FireballEffectsFactory.getInstance().getItem();
			Starling.juggler.add(fireballShootEffect);
			fireballShootEffect.start(0.1);
			fireballShootEffect.x = m.x;
			fireballShootEffect.y = m.y;
		}
		
	}
}