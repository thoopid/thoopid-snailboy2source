package com.thoopid.games.sb2.view.game.objects
{
	import com.thoopid.games.sb2.constants.game.CollisionConstants;
	
	import citrus.objects.platformer.nape.Platform;
	
	public class Wall extends Platform
	{
		public function Wall(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override protected function createFilter():void
		{
			super.createFilter();
			this.body.setShapeFilters(CollisionConstants.COLLISION_WALL_FILTER);
		}
		
		override public function addPhysics():void
		{
			super.addPhysics();
		}
	}
}