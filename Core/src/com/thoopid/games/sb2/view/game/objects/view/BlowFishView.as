package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.IAnimatable;
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	public class BlowFishView extends Sprite
	{
		private var _base:PoolImage;
		private var _active:PoolImage;
		private var _isAlive:Boolean;
		private var _dc:IAnimatable;
		public function BlowFishView()
		{
			super();
			
			this.addChild(_base = Factory.imagePool.getImage(Assets.assets.getTexture("pp.blowfishA.off.01"),true));
			this.addChild(_active = Factory.imagePool.getImage(Assets.assets.getTexture("pp.blowfishA.on.02"),true));
			
			toggle(!_isAlive);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
			//---------------------------------------------
			// Hack to center views for nape physics
			//---------------------------------------------
			override public function get x():Number
			{
				return 0;
			}
			override public function set x(value:Number):void
			{
				_active.x = 0;
			}
			
			override public function get y():Number
			{
				return 0;
			}
			override public function set y(value:Number):void
			{
				_active.y = 0;
			}
			//---------------------------------------------
		
		override public function dispose():void
		{
			if(_dc) Starling.juggler.remove(_dc);
			_base = null;
			_active  = null;
			super.dispose();
		}
		
		
		//*********************
		//   PRIVATE
		//*********************
		
		public function get isAlive():Boolean
		{
			return _isAlive;
		}

		private function toggle(t:Boolean):void
		{
			_isAlive = t;
			Starling.juggler.tween(_base, 0.4, {scaleX:(t) ? 1.1 : 1, rotation:(t) ? deg2rad(6.4) : 0, scaleY:(t) ? 1.57 : 1, alpha:(t) ? 0 : 1, transition:Transitions.EASE_IN});
			Starling.juggler.tween(_active, 0.4, {scaleX:(t) ? 1 : 0.8, scaleY:(t) ? 1 : 0.4, alpha:(t) ? 1 : 0, transition:Transitions.EASE_IN});
			
			
			if(_dc) Starling.juggler.remove(_dc);
			_dc = Starling.juggler.delayCall(toggle, 2, !_isAlive);
		}
	}
}