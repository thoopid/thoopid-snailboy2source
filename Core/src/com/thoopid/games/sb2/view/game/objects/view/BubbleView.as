package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	public class BubbleView extends Sprite
	{
		private var _image:PoolImage;
		private var _index:Number = 0;
		public function BubbleView()
		{
			super();
			
			this.addChild(_image = Factory.imagePool.getImage(Assets.assets.getTexture("pp.bubble.01"),true));
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function update():void
		{
			_index += 4;
			_index %= 360;
			_image.skewX = Math.sin(deg2rad(_index)-0.06);
			_image.skewY = Math.sin(deg2rad(_index)+0.06);
		}
		
		//---------------------------------------------
		// Hack to center views for nape physics
		//---------------------------------------------
		override public function get x():Number
		{
			return 0;
		}
		override public function set x(value:Number):void
		{
			_image.x = 0;
		}
		
		override public function get y():Number
		{
			return 0;
		}
		override public function set y(value:Number):void
		{
			_image.y = 0;
		}
		//---------------------------------------------
		
		override public function dispose():void
		{
			super.dispose();
			_image = null;
		}
	}
}