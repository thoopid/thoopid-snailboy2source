package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.constants.game.GameConstants;
	import com.thoopid.games.sb2.util.MathTrigUtil;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	import citrus.sounds.SoundManager;
	
	import nape.geom.Vec2;
	
	import org.osflash.signals.Signal;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	public class Character extends Sprite
	{
		public var jumpSignal:Signal = new Signal();
		public var slapSignal:Signal = new Signal();
		public var jumpFromBounceSignal:Signal = new Signal();
		public var swimFromSwimStartSignal:Signal = new Signal(Boolean);
		public var landSignal:Signal = new Signal();
		
		/**
		 * The regeon inside the hero sprite that allows the jump/drag trigger
		 */
		public var heroDragRegion:Rectangle;
		
		/**
		 * Property set when hero body is falling
		 */

		public static const STATE_IDLE:String = "idleGrnd/idleGrnd_";
		public static const STATE_IDLE_WALL:String = "idleStk/idleStk_";
		public static const STATE_JUMP_STANDARD:String = "jumpFwd/jumpFwd_";
		public static const STATE_JUMP_SUPER:String = "jumpFwdS/jumpFwdS_";
		public static const STATE_JUMP_UP:String = "jumpUp/jumpUp_";
		public static const STATE_JUMP_UP_SUPER:String = "jumpUpS/jumpUpS_";
		public static const STATE_LAND:String = "land/land_";
		public static const STATE_LAUNCH_FROM_STAND:String = "launchFwd/launchFwd_";
		public static const STATE_LAUNCH_FROM_STAND_SUPER:String = "launchFwdS/launchFwdS_";
		public static const STATE_LAUNCH_FROM_WALL:String = "launchStk/launchStk_";
		public static const STATE_LAUNCH_FROM_WALL_SUPER:String = "launchStkS/launchStkS_";
		public static const STATE_LAUNCH_UP:String = "launchUp/launchUp_";
		public static const STATE_LAUNCH_UP_SUPER:String = "launchUpS/launchUpS_";
		public static const STATE_SWIM:String = "swim/swim_";
		public static const STATE_SWIM_START:String = "swimStart/swimStart_";
		public static const STATE_SWIM_END:String = "swimEnd/swimEnd_";
		public static const STATE_PADDLE:String = "paddle/paddle_";
		public static const STATE_BOOST_START:String = "boostStart/boostStart_";
		public static const STATE_BOOST:String = "boost/boost_";
		public static const STATE_BOOST_END:String = "boostEnd/boostEnd_";
		
		public static const STATE_PULLBACK_GROUND:String = "pullBck/pullBck_";
		public static const STATE_PULLBACK_UP:String = "pullDwn/pullDwn_";
		public static const STATE_PULLBACK_ON_WALL:String = "pullStk/pullStk_";
		public static const STATE_SINK:String = "sink/sink_";
		public static const STATE_STUCK_TO_WALL:String = "stick/stick_";							// once the second tap hapens
		public static const STATE_BUBBLE:String = "bubble/bubble_";							// once the second tap hapens
		public static const STATE_BUBBLE_START:String = "bubbleStart/bubbleStart_";							// once the second tap hapens
		public static const STATE_BUBBLE_END:String = "bubbleEnd/bubbleEnd_";							// once the second tap hapens
		public static const STATE_BOUNCE:String = "bounce/bounce_";							// once the second tap hapens
		public static const STATE_BOUNCE_END:String = "bounceEnd/bounceEnd_";							// once the second tap hapens
		
//		public static const STATE_PULLBACK_ON_WALL_REVERSE:String = "pullbackWallReverse_";
//		public static const STATE_LAND_FWD:String = "landFwd_";
		public static const STATE_WARP:String = "warp/warp_";
//		public static const STATE_FALLING_FORWARD:String = "fallingFwd_";
		public static const STATE_DIE:String = "death/death_";							// 
		
		private var _current_state:String = "";
		
		private var _mustAnimate:Boolean = false;		// For animating clips
		private var _mustLoop:Boolean = false;		// For animating clips
		private var _forward:int = -1;
		private var _pullState:String = "";
		private var _anims:Dictionary;
		private var _currentAnim:MovieClip;
		
		public var vDirection:Number = 0;
		public var hDirection:Number = 0;
		public var acrobaticsEnabled:Boolean = false;
		
		private var _fps:Number = 40;
		
		private var _enabled:Boolean = false;

		private var _jumpAngle:Number = 0;
		
		private var _stateTween:Tween;
		private var _pullTween:Tween;
		private var _holder:Sprite;
		private var _sound:SoundManager;
		private var _assets:AssetManager;

		public function Character(assets:AssetManager)
		{
//			this.scaleX = this.scaleY = 0.6;
			_assets = assets;
			// Animations ***************************************************
			_anims = new Dictionary();
			this.addChild(_holder = new Sprite());
			prepareClip(STATE_IDLE, _fps);
			prepareClip(STATE_IDLE_WALL, _fps);
			prepareClip(STATE_PULLBACK_GROUND, _fps);
			prepareClip(STATE_PULLBACK_ON_WALL, _fps);
			prepareClip(STATE_PULLBACK_UP, _fps);
			prepareClip(STATE_LAUNCH_FROM_STAND, _fps);
			prepareClip(STATE_LAUNCH_FROM_STAND_SUPER, _fps);
			prepareClip(STATE_LAUNCH_FROM_WALL, _fps);
			prepareClip(STATE_LAUNCH_FROM_WALL_SUPER, _fps);
			prepareClip(STATE_LAUNCH_UP, _fps);
			prepareClip(STATE_LAUNCH_UP_SUPER, _fps);
			prepareClip(STATE_JUMP_STANDARD, _fps);
			prepareClip(STATE_JUMP_SUPER, _fps);
			prepareClip(STATE_JUMP_UP, _fps);
			prepareClip(STATE_JUMP_UP_SUPER, _fps);
			prepareClip(STATE_SWIM, _fps);
			prepareClip(STATE_SWIM_START, _fps);
			prepareClip(STATE_SWIM_END, _fps);
			prepareClip(STATE_PADDLE, _fps);
			prepareClip(STATE_LAND, _fps);
			prepareClip(STATE_SINK, _fps);
			prepareClip(STATE_STUCK_TO_WALL, _fps);
			prepareClip(STATE_BOOST, _fps);
			prepareClip(STATE_BOOST_START, _fps);
			prepareClip(STATE_BOOST_END, _fps);
			prepareClip(STATE_BUBBLE, _fps);
			prepareClip(STATE_BUBBLE_START, _fps);
			prepareClip(STATE_BUBBLE_END, _fps);
			prepareClip(STATE_BOUNCE, _fps);
			prepareClip(STATE_BOUNCE_END, _fps);
			prepareClip(STATE_WARP, _fps);
			
			prepareClip(STATE_DIE, _fps);
			
			//********************************************************************
			
			_sound = SoundManager.getInstance();
			
			reset();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get forward():int
		{
			return _forward;
		}

		public function get canJump():Boolean
		{
			return _canJump;
		}
		
		public function get canSwim():Boolean
		{
			return _canSwim;
		}

		public function set defaultForward(value:int):void
		{
			_defaultForward = value;
			updateSpriteDirection();
		}

		public function get enabled():Boolean
		{
			return _enabled;
		}

		public function set enabled(value:Boolean):void
		{
			_enabled = value;
		}
		
		override public function dispose():void
		{
			jumpFromBounceSignal.removeAll();
			jumpFromBounceSignal = null;
			jumpSignal.removeAll();
			jumpSignal = null;
			slapSignal.removeAll();
			slapSignal = null;
			swimFromSwimStartSignal.removeAll();
			swimFromSwimStartSignal = null;
			landSignal.removeAll();
			landSignal = null;
			
			_sound = null
			
			for each (var i:Object in _anims)
			{
				i.texture.dispose();
				delete _anims[i];
//				_anims[i] = null;
			}
			_anims = null;
			
			super.dispose();
		}
		
		public function reset():void
		{
			enabled = true;
			_forward = _defaultForward;
			setState(STATE_SINK);
		}
		
		public function resetToIdle():void
		{
			if(_current_state == STATE_PULLBACK_GROUND || _current_state == STATE_PULLBACK_UP)
			{
				setState(STATE_IDLE);
//			} else if(_current_state == STATE_PULLBACK_ON_WALL || _current_state == STATE_PULLBACK_ON_WALL_REVERSE)
			} else if(_current_state == STATE_PULLBACK_ON_WALL)
			{
				setState(STATE_IDLE_WALL);
			}
		}

		public function launch(power:Number, angle:Number, isSuper:Boolean):void
		{
			// Sound
			_sound.playSound(GameConstants.SOUND_JUMP);
			
			_power = power;
			_angle = angle;
			if(_current_state == STATE_PULLBACK_GROUND)
			{
				if(isSuper)
				{
					setState(STATE_LAUNCH_FROM_STAND_SUPER);
				} else
				{
					setState(STATE_LAUNCH_FROM_STAND);
					
				}
			} else if(_current_state == STATE_PULLBACK_ON_WALL)
			{
				if(isSuper)
				{
					setState(STATE_LAUNCH_FROM_WALL_SUPER);
				} else
				{
					setState(STATE_LAUNCH_FROM_WALL);
				}
			} else if(_current_state == STATE_PULLBACK_UP)
			{
				if(isSuper)
				{
					setState(STATE_LAUNCH_UP_SUPER);
				} else
				{
					setState(STATE_LAUNCH_UP);
					
				}
			} else
			{
				if(isSuper)
				{
					setState(STATE_LAUNCH_FROM_STAND_SUPER);
				} else
				{
					setState(STATE_LAUNCH_FROM_STAND);
					
				}
			}
		}
		
		public function float():void
		{
			setState(STATE_LAUNCH_FROM_STAND);
		}
		
		public function canJumpDowm():Boolean
		{
			switch(_current_state)
			{
				case STATE_IDLE_WALL:	
				case STATE_PULLBACK_ON_WALL:
					return true;
					break;
			}
			return false;
		}
		
		public function canSlap():Boolean
		{
			switch(_current_state)
			{
				case STATE_IDLE:
				case STATE_IDLE:
				case STATE_LAND:
					return true;
					break;
			}
			return false;
		}
		
		public function boost():void
		{
			setState(STATE_BOOST_START);
		}
		public function boostEnd():void
		{
			setState(STATE_BOOST_END);
		}
		
		public function warp():void
		{
			setState(STATE_WARP);
		}
		
		public function sink():void
		{
			setState(STATE_SINK);
		}
		
		public function land():Boolean
		{
			switch(_current_state)
			{
				case STATE_JUMP_UP:
				case STATE_JUMP_UP_SUPER:
				case STATE_SINK:
				case STATE_SWIM:
				case STATE_SWIM_START:
				case STATE_SWIM_END:
				case STATE_PADDLE:
				case STATE_LAUNCH_FROM_WALL:
				case STATE_LAUNCH_FROM_WALL_SUPER:
				case STATE_JUMP_STANDARD:
				case STATE_JUMP_SUPER:
				case STATE_BOUNCE:
				case STATE_BOUNCE_END:
					setState(STATE_LAND);
					
					// SOUND
					_sound.playSound(GameConstants.SOUND_LAND);
					
					return true;
					break;
			}
			return false;
		}
		
		public function stick(dir:int):void
		{
			_forward = dir;
			setState(STATE_STUCK_TO_WALL);
			landSignal.dispatch();
			
			// SOUND
			_sound.playSound(GameConstants.SOUND_STICK);
		}
		
		public function pull(dist:Number, angle:Number):void
		{
			_jumpAngle = angle;
			if(_current_state == STATE_PULLBACK_GROUND || _current_state == STATE_PULLBACK_UP || _current_state == STATE_IDLE || current_state == STATE_LAND /*|| current_state == STATE_LAND_FWD*/)
			{
				updateDirectionFromPull(angle);
				
				if(_pullState != _current_state)
				{
					setState(_pullState);
					
					// SOUND
					_sound.playSound(GameConstants.SOUND_PULL);
				}
			} else if(current_state == Character.STATE_STUCK_TO_WALL || current_state == STATE_IDLE_WALL)
			{
				if(_current_state != STATE_PULLBACK_ON_WALL /*|| _current_state != STATE_PULLBACK_ON_WALL_REVERSE*/)
				{
					setState(STATE_PULLBACK_ON_WALL);
					
					// SOUND
					_sound.playSound(GameConstants.SOUND_PULL);
					
//					if(forward == 1)
//					{
//						setState(STATE_PULLBACK_ON_WALL);
//					} else if (forward == -1)
//					{
//						setState(STATE_PULLBACK_ON_WALL_REVERSE);
//					}
				}
			}
			
			var per:Number = (dist -GameConstants.HERO_PULLBACK_MIN) / (GameConstants.HERO_PULLBACK_MAX - GameConstants.HERO_PULLBACK_MIN);
			var frame:Number = Math.floor(_currentAnim.numFrames * per);

			if(frame > 0 && frame < _currentAnim.numFrames) _currentAnim.currentFrame = frame;
		}
		
		public function die():void
		{
			setState(STATE_DIE);
		}
		
		public function swim(mustSwim:Boolean, dir:int=1):void
		{
			if(mustSwim)
			{
				_forward = dir;
				swimFromSwimStartSignal.dispatch(true);
				setState(STATE_SWIM);
			} else
			{
				setState(STATE_SWIM_END);
			}
		}
		
		public function toggleBubble(toggle:Boolean):void
		{
			if(toggle)
			{
				setState(STATE_BUBBLE_START);
			} else
			{
				setState(STATE_BUBBLE_END);
			}
		}
		
		public function bounce():void
		{
			setState(STATE_BOUNCE);
		}
		
		public override function render(support:RenderSupport, parentAlpha:Number):void
		{
			super.render(support, parentAlpha);
		}
		
		public function get canReleaseFromStick():Boolean
		{
			return _current_state == STATE_IDLE_WALL;
		}
		
		public function get dragArea():DisplayObject
		{
			return _currentAnim;
		}
		
		public function get current_state():String
		{
			return _current_state;
		}
		
		public function update(velocity:Vec2):void
		{
//			if(velocity.x != 0)
//			{
//				hDirection = Math.abs(velocity.x) / velocity.x;
//				_forward = -hDirection;
//				updateSpriteDirection();
//			}
			if(velocity.y >= 0)
			{
				if(current_state == STATE_BOUNCE)
				{
					setState(STATE_BOUNCE_END);
				}
			}
		}
		
		private var _viewCenter:Point = new Point();
		private var _spriteYOffset:Number = 19// / Starling.current.contentScaleFactor;
		
		public function get viewCenter():Point
		{
			_viewCenter.x = _currentAnim.pivotX;
			_viewCenter.y = _currentAnim.pivotY - _spriteYOffset;
			return _viewCenter;
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function prepareClip(name:String, fps:Number=60, texturesName:String=null, reversed:Boolean=false):void
		{
			if(texturesName == null) texturesName = name;
			var frames:Vector.<Texture> = _assets.getTextures(texturesName);
			var mc:MovieClip = new MovieClip(frames,fps);
			
			_anims[name] = mc;
			
			mc.stop();
			mc.loop = false;
			mc.pivotX = mc.width / 2;
			mc.pivotY = mc.height / 2;
			mc.x = mc.pivotX;
			mc.y = mc.pivotY - _spriteYOffset;
			if(reversed) mc.scaleX = -1;
		}
		
		public function setState(state:String):void
		{
			trace("SETTING STATE: "+state);
			
			_mustAnimate = true;
			_mustLoop = false;
			
			// Set new anim
			if(_anims && _anims[state])
			{
				if(_current_state != state)
				{
					if(!_stateTween)
					{
						_stateTween = new Tween(null,0);
					}
					
					// Remove
					_stateTween.onComplete = null;
					Starling.current.juggler.removeTweens(_stateTween);
					if(_currentAnim)
					{
						_currentAnim.currentFrame = 0;
						_holder.removeChild(_currentAnim);
					}
					
					// setnew state and anim clip
					_current_state = state;
					_currentAnim = _anims[_current_state];
					
					// Add
//					updateSpriteDirection();
					_holder.addChild(_currentAnim);
				}
				
				_canJump = false;
				_canSwim = false;
				
//				updateSpriteDirection();
				
				// play
				switch(_current_state)
				{
					case STATE_PULLBACK_ON_WALL:
						_mustAnimate = false;
						break;
					case STATE_PULLBACK_UP:
						_mustAnimate = false;
						break;
					case STATE_PULLBACK_GROUND:
						_mustAnimate = false;
						break;
					case STATE_LAUNCH_FROM_WALL:
					case STATE_LAUNCH_FROM_WALL_SUPER:
						break;
					case STATE_LAUNCH_FROM_STAND:
					case STATE_LAUNCH_FROM_STAND_SUPER:
						break;
					case STATE_LAUNCH_UP:
						_canSwim = true;
						break;
					case STATE_LAUNCH_UP_SUPER:
						_canSwim = true;
						break;
					case STATE_JUMP_STANDARD:
					case STATE_JUMP_SUPER:
						_canSwim = true;
						break;
					case STATE_JUMP_UP:
					case STATE_JUMP_UP_SUPER:
						_canSwim = true;
						break;
					case STATE_LAND:
						landSignal.dispatch();
						_canJump = true;
						break;
					case STATE_SWIM:
						_canSwim = false;
						break;
					case STATE_SWIM_START:
						_canSwim = false;
						break;
					case STATE_SWIM_END:
						_canSwim = false;
						break;
					case STATE_PADDLE:
						_canSwim = false;
						_mustLoop = true;
						break;
					case STATE_IDLE:
						_canJump = true;
						_mustLoop = true;
						break;
					case STATE_DIE:
						_canJump = false;
						_canSwim = false;
						break;
					case STATE_IDLE_WALL:
						_canJump = true;
						_mustLoop = true;
						break;
					case STATE_WARP:
						_mustAnimate = true;
						_mustLoop = false;
						break;
					case STATE_SINK:
						_mustAnimate = true;
						_mustLoop = true;
						_canSwim = true;
						break;
					case STATE_STUCK_TO_WALL:
						_canJump = true;
						_mustAnimate = true;
						break;
					case STATE_BOOST_START:
						_canSwim = false;
						_canJump = false;
						_mustAnimate = true;
						break;
					case STATE_BOOST:
						_canSwim = false;
						_canJump = false;
						_mustAnimate = true;
						_mustLoop = true;
						break;
					case STATE_BOOST_END:
						_canSwim = false;
						_canJump = false;
						_mustAnimate = true;
						break;
					case STATE_BUBBLE:
						_canSwim = false;
						_canJump = false;
						_mustAnimate = true;
						_mustLoop = true;
						break;
					case STATE_BUBBLE_START:
						_canSwim = false;
						_canJump = false;
						_mustAnimate = true;
						break;
					case STATE_BUBBLE_END:
						_canSwim = false;
						_canJump = false;
						_mustAnimate = true;
						break;
					case STATE_BOUNCE:
						_canSwim = false;
						_canJump = false;
						_mustAnimate = true;
						_mustLoop = true;
						break;
					case STATE_BOUNCE_END:
						_canSwim = false;
						_canJump = true;
						_mustAnimate = true;
						break;
				}
				
				if(_mustAnimate)
				{
					_currentAnim.currentFrame = 0;
					
					_stateTween.reset(_currentAnim, _currentAnim.numFrames / _currentAnim.fps, Transitions.LINEAR);
					_stateTween.onComplete = onAnimComplete;
					_stateTween.repeatCount = (_mustLoop) ? 999 : 1;
					updateSpriteDirection();
					Starling.current.juggler.add(_stateTween);
					
					_stateTween.animate("currentFrame", _currentAnim.numFrames - 1); //0 based frames
				}
			}
		}
		
		private function updateSpriteDirection():void
		{
//			trace("Updating: "+_forward);
//			if(_forward != _currentAnim.scaleX) _currentAnim.scaleX = _forward;
			for each (var mc:MovieClip in _anims) 
			{
				mc.scaleX = _forward;
			}
			
		}
		
		private function onAnimComplete():void
		{
			switch(_current_state)
			{
				case STATE_LAUNCH_FROM_STAND_SUPER:
				case STATE_LAUNCH_FROM_STAND:
				case STATE_LAUNCH_FROM_WALL:
				case STATE_LAUNCH_FROM_WALL_SUPER:
				case STATE_LAUNCH_UP:
				case STATE_LAUNCH_UP_SUPER:
					jump();
					jumpSignal.dispatch();
					break;
//				case STATE_LAND_FWD:
				case STATE_LAND:
					setState(STATE_IDLE);
					break;
//				case STATE_FALLING_FORWARD:
//					break;
//				case STATE_JUMP_STANDARD:
//					setState(STATE_FALLING_FORWARD);
//					break;
				case STATE_STUCK_TO_WALL:
					setState(STATE_IDLE_WALL);
					break;
				case STATE_JUMP_UP:
				case STATE_JUMP_UP_SUPER:
				case STATE_JUMP_STANDARD:
				case STATE_JUMP_SUPER:
				case STATE_BOOST_END:
				case STATE_BUBBLE_END:
				case STATE_BOUNCE_END:
					setState(STATE_SINK);
					break;
//				case STATE_SWIM_START:
//					swimFromSwimStartSignal.dispatch(true);
//					setState(STATE_SWIM);
//					break;
				case STATE_SWIM:
					setState(STATE_PADDLE);
					break;
				case STATE_SWIM_END:
					swimFromSwimStartSignal.dispatch(false);
					setState(STATE_SINK);
					break;
				case STATE_BOOST_START:
					setState(STATE_BOOST);
					break;
				case STATE_BUBBLE_START:
					setState(STATE_BUBBLE);
					break;
			}
		}
		
		public function jump():void
		{
			if(_current_state == STATE_LAUNCH_FROM_WALL)
			{
				_forward *= -1;
				setState(STATE_JUMP_STANDARD);
			} else if(_current_state == STATE_LAUNCH_FROM_WALL_SUPER)
			{
				_forward *= -1;
				setState(STATE_JUMP_SUPER);
			} else if(_current_state == STATE_LAUNCH_UP)
			{
				setState(STATE_JUMP_UP);	
			} else if(_current_state == STATE_LAUNCH_UP_SUPER)
			{
				setState(STATE_JUMP_UP_SUPER);
			} else if(_current_state == STATE_LAUNCH_FROM_STAND)
			{
				setState(STATE_JUMP_STANDARD);
			} else if(_current_state == STATE_LAUNCH_FROM_STAND_SUPER)
			{
				setState(STATE_JUMP_SUPER);
			}
		}
		
		private var _randJump:Number=0;
		private var _defaultForward:int = -1;
		private var _canJump:Boolean;
		private var _power:Number;
		private var _angle:Number;
		private var _canSwim:Boolean;
		
		private function updateDirectionFromPull(angle:Number):void
		{
			var deg:Number = MathTrigUtil.radiansToDegrees(angle);
			if(between(deg, GameConstants.HERO_JUMP_ANGLE_NORMAL_MIN, GameConstants.HERO_JUMP_ANGLE_NORMAL_MAX))
			{
				_forward = 1;
			} else if(between(deg, GameConstants.HERO_JUMP_ANGLE_NORMAL_START, GameConstants.HERO_JUMP_ANGLE_NORMAL_MIN))
			{
				_forward = -1;
			}
			updateSpriteDirection();
			_pullState = (between(deg, GameConstants.HERO_JUMP_ANGLE_UP_MIN, GameConstants.HERO_JUMP_ANGLE_UP_MAX)) ? STATE_PULLBACK_UP : STATE_PULLBACK_GROUND;
		}
		
		private function between(i:Number, a:Number, b:Number):Boolean
		{
			if(i < a && i > b) return true;
			
			return false;
		}
	}
}