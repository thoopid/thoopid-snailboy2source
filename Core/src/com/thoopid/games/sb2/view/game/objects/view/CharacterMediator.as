package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.constants.game.CharacterActions;
	import com.thoopid.games.sb2.controller.signals.game.CharacterActionSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ToggleSwimControlsSignal;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class CharacterMediator extends StarlingMediator
	{
		[Inject]
		public var view:Character;
		
		// Signals
		[Inject]
		public var characterActions:CharacterActionSignal;
		[Inject]
		public var toggleSwimControlsSignal:ToggleSwimControlsSignal;
		
		public function CharacterMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			view.jumpSignal.add(onJumpHandler);
			view.swimFromSwimStartSignal.add(onSwimHandler);
			view.landSignal.add(onLandHandler);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onJumpHandler():void
		{
			characterActions.dispatch(CharacterActions.JUMP);
			toggleSwimControlsSignal.dispatch(true);
		}
		
		private function onLandHandler():void
		{
			toggleSwimControlsSignal.dispatch(false);
		}
		
		private function onSwimHandler(swim:Boolean):void
		{
			if(swim)
			{
				characterActions.dispatch(CharacterActions.SWIM_START);
				
			} else
			{
				characterActions.dispatch(CharacterActions.SWIM_STOP);
			}
		}
	}
}