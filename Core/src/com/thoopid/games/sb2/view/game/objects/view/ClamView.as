package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import coza.runtime.utils.number.RandomRange;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class ClamView extends Sprite
	{
		private var _closed:PoolImage;
		private var _open:PoolImage;
		private var _effect:PoolImage;
		
		public function ClamView()
		{
			super();
			
			this.touchable = true
			
			this.addChild(_closed = Factory.imagePool.getImage(Assets.assets.getTexture("ib.clam.shut")));
			this.addChild(_open = Factory.imagePool.getImage(Assets.assets.getTexture("ib.clam.open")));
			this.addChild(_effect = Factory.imagePool.getImage(Assets.assets.getTexture("ib.clam.fx")));
			
			togggle(false);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function set enabled(v:Boolean):void
		{
			this.visible = v;
			toggleEffect(v);
		}
		
		public function togggle(open:Boolean = false):void
		{
			_open.visible = open; 
			_closed.visible = !open; 
			toggleEffect(!open);
		}
		
		override public function get width():Number
		{
			return _closed.width;
		}
		override public function get height():Number
		{
			return _closed.height;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function toggleEffect(v:Boolean):void
		{
			_effect.visible = v;
			if(v)
			{
				_effect.alpha = 0.5;
				Starling.juggler.tween(_effect, RandomRange.randRange(0.6,0.9), {alpha:1, repeatCount:999, reverse:true, transition:Transitions.EASE_IN_OUT});
			} else
			{
				Starling.juggler.removeTweens(_effect);
			}
		}
	}
}