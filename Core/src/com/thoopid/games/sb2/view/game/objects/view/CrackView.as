package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class CrackView extends Sprite
	{
		private var _glow:PoolImage;
		private var _shut:PoolImage;
		private var _open:PoolImage;
		public function CrackView()
		{
			super();
			
			this.touchable = true;
			
			this.addChild(_shut = Factory.imagePool.getImage(Assets.assets.getTexture("ib.rockCrack.shut")));
			this.addChild(_glow = Factory.imagePool.getImage(Assets.assets.getTexture("ib.rockCrack.fx")));
			_glow.alpha = 0;
			
			togggle(false);
			
			toggleEffect(true);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function togggle(open:Boolean = false):void
		{
			_glow.visible = !open;
			_shut.visible = !open;
			
			toggleEffect(!open);
		}
		
		private function toggleEffect(toggle:Boolean):void
		{
			Starling.juggler.removeTweens(_glow);
			if(toggle)
			{
				_glow.alpha = 0;
				Starling.juggler.tween(_glow, 1, {alpha:1, repeatCount:999, reverse:true});
			}
		}
		
		override public function get width():Number
		{
			return _glow.width;
		}
		override public function get height():Number
		{
			return _glow.height;
		}
		override public function set width(value:Number):void
		{
			_glow.width = value;
		}
		override public function set height(value:Number):void
		{
			_glow.height = value;
		}
	}
}