package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.view.effect.BoxEffect;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class CrateView extends Sprite
	{
		private var _boxEffect:BoxEffect;
		private var _closed:PoolImage;
		private var _open:PoolImage;
		public function CrateView()
		{
			super();
			
			this.touchable = true;
			
			this.addChild(_boxEffect = new BoxEffect());
			
			this.addChild(_closed = Factory.imagePool.getImage(Assets.assets.getTexture("ib.crate.shut")));
			this.addChild(_open = Factory.imagePool.getImage(Assets.assets.getTexture("ib.crate.open")));
			_boxEffect.x = _closed.width >> 1;
			_boxEffect.y = _closed.height >> 1;
			
			togggle(false);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function togggle(open:Boolean = false):void
		{
			_open.visible = open; 
			_closed.visible = !open; 
		}
		
		private function toggleEffect(toggle:Boolean):void
		{
			Starling.juggler.removeTweens(_boxEffect);
			if(toggle)
			{
				_boxEffect.alpha = 0;
				Starling.juggler.tween(_boxEffect, 1, {alpha:1, repeatCount:999, reverse:true});
			}
		}
		
		override public function get width():Number
		{
			return _closed.width;
		}
		override public function get height():Number
		{
			return _closed.height;
		}
		override public function set width(value:Number):void
		{
			_closed.width = value;
//			_boxEffect.x = _closed.width >> 1;
		}
		override public function set height(value:Number):void
		{
			_closed.height = value;
//			_boxEffect.y = _closed.height >> 1;
		}
	}
}