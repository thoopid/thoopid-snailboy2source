package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import feathers.display.TiledImage;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	public class MineHeadView extends Sprite
	{
		private var _image:Image;
		private var _glow:Image;
		
		private var _enabled:Boolean = false;
		private var _chainLength:Number;
		private var _chainBase:PoolImage;
		private var _chainImage:TiledImage;
		public function MineHeadView(chainLength:Number = 150)
		{
			super();
			
			var assets:AssetManager = Assets.assets;
			_image = Factory.imagePool.getImage(assets.getTexture("pp.mine.head.01"),false);
			_glow = Factory.imagePool.getImage(assets.getTexture("pp.mine.fx.01"),true);
			
			_chainLength = chainLength;
			
			_chainBase = Factory.imagePool.getImage(assets.getTexture("pp.mine.base.01"), true);
			_chainImage = new TiledImage(assets.getTexture("pp.mine.chain.01"));
			_chainImage.pivotX = _chainImage.width >> 1;
			
			this.addChild(_chainBase);
			this.addChild(_chainImage);
			this.addChild(_image);
			this.addChild(_glow);
			
			_chainBase.y = _chainLength;
			_chainImage.height = _chainLength;
			_chainBase.x = width >> 1;
			_chainImage.x = width >> 1;
			
			this.visible = _enabled;
		}
		
		public function get enabled():Boolean
		{
			return _enabled;
		}

		public function set enabled(value:Boolean):void
		{
			_enabled = value;
			this.visible = value;
			if(value)
			{
				Starling.juggler.removeTweens(_glow);
				_glow.alpha = 1;
				Starling.juggler.tween(_glow,0.6,{alpha:0, reverse:true, repeatCount:0});
			} else
			{
				Starling.juggler.removeTweens(_glow);
			}
		}

		override public function get width():Number
		{
			return _image.width;
		}
		override public function get height():Number
		{
			return _image.height;
		}
		override public function set width(value:Number):void
		{
			_image.width = value;
			_glow.scaleX = _image.scaleX;
			_glow.x = _image.width >> 1;
		}
		override public function set height(value:Number):void
		{
			_image.height = value;
			_glow.scaleY = _image.scaleY;
			_glow.y = _image.height >> 1;
		}
		
		override public function dispose():void
		{
			Starling.juggler.removeTweens(_glow);
			_glow = null;
			_image = null;
			super.dispose();
		}
	}
}