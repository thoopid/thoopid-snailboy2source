package com.thoopid.games.sb2.view.game.objects.view
{
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.extensions.particles.PDParticleSystem;
	import starling.utils.AssetManager;
	
	public class PortalView extends Sprite
	{
		private var _ps:PDParticleSystem;
		public function PortalView()
		{
			super();
			
			var a:AssetManager = Assets.assets;
			var holder:Sprite = new Sprite();
			this.addChild(holder);
			var obj:Object = a.getXml("PortalBubbles");
			var xml:XML = XML(obj);
			holder.addChild(_ps = new PDParticleSystem(xml,a.getTexture("pp.bubble.01")));
			Starling.juggler.add(_ps);
			holder.scaleY = -1;
			holder.pivotY = 130;
			holder.scaleX = holder.scaleY = -0.7;
			_ps.start();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function set enabled(v:Boolean):void
		{
			this.visible = v;
			if(v)
			{
				_ps.start();
			} else
			{
				_ps.stop();
			}
		}
		
		override public function dispose():void
		{
			_ps.stop();
			_ps.removeFromParent(true);
			_ps = null;
			
			super.dispose();
		}
	}
}