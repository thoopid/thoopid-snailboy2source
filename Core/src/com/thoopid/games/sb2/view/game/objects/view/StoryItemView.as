package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class StoryItemView extends Sprite
	{
		private var _image:Image;
		private var _glow:PoolImage;
		
		private var _enable:Boolean = false;
		private var _bubble:PoolImage;
		
		public function StoryItemView(typeId:String)
		{
			super();
			
			var tex:Texture = Assets.assets.getTexture(typeId);
			if(tex)
			{
				_image = Factory.imagePool.getImage(tex, true);
				_image.scaleX = _image.scaleY = 0.9;
				_glow = Factory.imagePool.getImage(Assets.assets.getTexture("iq.glow"), true);
				_glow.scaleX = _glow.scaleY = 1.3;
				_bubble = Factory.imagePool.getImage(Assets.assets.getTexture("iq.bubble"), true);
				
				this.addChild(_glow);
				this.addChild(_image);
				this.addChild(_bubble);
			}
		}
		
		public function get enable():Boolean
		{
			return _enable;
		}

		public function set enable(value:Boolean):void
		{
			_enable = value;
			this.visible = _enable;
			
			if(_enable)
			{
				_glow.alpha = 0.4;
				Starling.juggler.tween(_glow, 0.6, {alpha:0.8, repeatCount:999, reverse:true, transition:Transitions.EASE_OUT});
			} else
			{
				Starling.juggler.removeTweens(_glow);
			}
		}

		override public function get width():Number
		{
			return _image.width;
		}
		override public function get height():Number
		{
			return _image.height;
		}
		override public function set width(value:Number):void
		{
			_image.width = value;
		}
		override public function set height(value:Number):void
		{
			_image.height = value;
		}
	}
}