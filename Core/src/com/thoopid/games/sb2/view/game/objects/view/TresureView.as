package com.thoopid.games.sb2.view.game.objects.view
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.view.effect.SimpleGlowEffect;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	
	public class TresureView extends Sprite
	{
		private var _image:PoolImage;
		private var _effect:SimpleGlowEffect;
		public function TresureView(typeId:String)
		{
			super();
			
			var tex:Texture;
			var assets:AssetManager = Assets.assets;
			switch(typeId)
			{
				case GameCollectableTypes.TREASURE_VASE:
					tex = assets.getTexture("iq.vase");
					break;
				case GameCollectableTypes.TREASURE_IDOL:
					tex = assets.getTexture("iq.idol");
					break;
				case GameCollectableTypes.TREASURE_BOTTLE:
					tex = assets.getTexture("iq.bottle");
					break;
			}
			
			if(tex)
			{
				_image = Factory.imagePool.getImage(tex);
				
//				// Effect
				_effect = new SimpleGlowEffect();
				_effect.x = _image.width >> 1;
				_effect.y = _image.height >> 1;
				this.addChild(_effect);
				this.addChild(_image);
			}
		}
		
		override public function get width():Number
		{
			if(_image ) return _image.width;
			else return 0;
		}
		override public function get height():Number
		{
			if(_image) return _image.height;
			else return 0;
		}
		override public function set width(value:Number):void
		{
			_image.width = value;
			_effect.x = _image.width >> 1;
		}
		override public function set height(value:Number):void
		{
			_image.height = value;
			_effect.y = _image.height >> 1;
		}
	}
}