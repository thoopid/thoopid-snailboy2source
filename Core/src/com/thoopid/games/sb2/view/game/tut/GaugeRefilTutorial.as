package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.events.TouchEvent;

	public class GaugeRefilTutorial extends TutElementBaseView
	{
		private var _arrow:Image;
		private var _mcModal:Image;
		public function GaugeRefilTutorial()
		{
			super(TutorialConstants.TUT_STEP_GAME_SLOTS);
			
			this.visible = false;
			
		}
		
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function show():void
		{
			super.show();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			
			_arrow = LayoutBuilder.findChild(this,"mcArrow") as Image;
			_mcModal = LayoutBuilder.findChild(this,"mcModal") as Image;
			_mcModal.touchable = true;
			_arrow.x = Config.GAME_WIDTH - 110;
			
			this.addEventListener(TouchEvent.TOUCH, onTouch);
			
			this.visible = true;
			showPull();
		}
		
		override public function hide():void
		{
			super.hide();
			
			Starling.juggler.removeTweens(_arrow);
			cleanup();
			
			this.removeEventListener(TouchEvent.TOUCH, onTouch);
			
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouch(e:TouchEvent):void
		{
			switch(e.target)
			{
				case _mcModal:
					e.stopImmediatePropagation();
					break;
			}
		}
		
		private function showPull():void
		{
			Starling.juggler.removeTweens(_arrow);
			Starling.juggler.tween(_arrow, 0.5, {y:260, transition:Transitions.EASE_OUT, repeatCount:999, reverse:true});
		}
	}
}