package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;

	public class InventoryBuyTutorial extends TutElementBaseView
	{
		public function InventoryBuyTutorial()
		{
			super(TutorialConstants.TUT_STEP_INVENTORY_BUY);
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function show():void
		{
			super.show();
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			this.visible = true;
		}
		
		override public function hide():void
		{
			super.hide();
			cleanup();
			this.visible = false;
		}
	}
}