package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;

	public class InventoryCloseTutorial extends TutElementBaseView
	{
		public function InventoryCloseTutorial()
		{
			super(TutorialConstants.TUT_STEP_INVENTORY_CLOSE);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function show():void
		{
			super.show();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			
			LayoutBuilder.findChild(this, "mcArrow1").x = Config.GAME_WIDTH - 65;
			
			this.visible = true;
		}
		
		override public function hide():void
		{
			super.hide();
			
			cleanup();
			
			this.visible = false;
		}
	}
}