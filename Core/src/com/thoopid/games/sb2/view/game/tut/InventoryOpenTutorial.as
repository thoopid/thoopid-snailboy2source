package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.TouchEvent;

	public class InventoryOpenTutorial extends TutElementBaseView
	{
		private var _arrow:DisplayObject;
		private var _mcModal:Image;
		private var _arrowStartY:Number;
		
		public function InventoryOpenTutorial()
		{
			super(TutorialConstants.TUT_STEP_INVENTORY_OPEN);
			
			this.visible = false;
		}
		
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function show():void
		{
			super.show();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			
			_arrow = LayoutBuilder.findChild(this,"mcArrow");
			_arrowStartY = _arrow.y;
			
			_mcModal = LayoutBuilder.findChild(this,"mcModal") as Image;
			_mcModal.touchable = true;
			
			_arrow.x = Config.GAME_WIDTH - 15;
			
			this.addEventListener(TouchEvent.TOUCH, onTouch);
			
			this.visible = true;
			showPull();
		}
		
		override public function hide():void
		{
			super.hide();
			
			this.removeEventListener(TouchEvent.TOUCH, onTouch);
			
			Starling.juggler.removeTweens(_arrow);
			
			cleanup();
			
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouch(e:TouchEvent):void
		{
			
			switch(e.target)
			{
				case _mcModal:
					e.stopImmediatePropagation();
					break;
			}
		}
		
		private function showPull():void
		{
			Starling.juggler.removeTweens(_arrow);
			Starling.juggler.tween(_arrow, 0.5, {y:_arrowStartY-30, transition:Transitions.EASE_OUT, repeatCount:999, reverse:true});
		}
	}
}