package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;

	public class PullTutorial extends TutElementBaseView
	{
		private var _hand:DisplayObject;
		private var _arrow:DisplayObject;
		public function PullTutorial()
		{
			super(TutorialConstants.TUT_STEP_JUMP);
			
			this.visible = false;
			
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		
		override public function show():void
		{
			super.show();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			_hand = LayoutBuilder.findChild(this,"mcFinger");
			_arrow = LayoutBuilder.findChild(this,"mcArrow");
			
			this.visible = true;
			showPull();
		}
		
		override public function hide():void
		{
			super.hide();
			Starling.juggler.removeTweens(_hand);
			cleanup();
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function showPull():void
		{
			Starling.juggler.removeTweens(_hand);
			Starling.juggler.removeTweens(_arrow);
			
			_hand.x = -10;
			_hand.y = 30;
			_arrow.x = -10;
			_arrow.y = 20;
			_arrow.scaleX = 0;
			_arrow.scaleY = 0;
			
			Starling.juggler.tween(_arrow, 1.4, {x:-24, y:32, scaleX:1, scaleY:1, transition:Transitions.EASE_OUT});
			Starling.juggler.tween(_hand, 1.4, {x:-50, y:70, transition:Transitions.EASE_OUT, onComplete:showPull});
		}
	}
}