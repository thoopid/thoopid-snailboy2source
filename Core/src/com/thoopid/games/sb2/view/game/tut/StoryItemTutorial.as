package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;

	public class StoryItemTutorial extends TutElementBaseView
	{
		private var _arrow:DisplayObject;
		private var _arrowStartY:Number;
		public function StoryItemTutorial()
		{
			super(TutorialConstants.TUT_STEP_STORY_ELEMENT);
			
			
			this.visible = false;
			
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function show():void
		{
			super.show();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			_arrow = LayoutBuilder.findChild(this,"mcArrow");
			_arrowStartY = _arrow.y;
			
			animateArrow();
			
			this.visible = true;
		}
		
		override public function hide():void
		{
			super.hide();
			
			Starling.juggler.removeTweens(_arrow);
			cleanup();
			
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function animateArrow():void
		{
			Starling.juggler.removeTweens(_arrow);
			Starling.juggler.tween(_arrow, 0.5, {y:_arrowStartY+20, transition:Transitions.EASE_OUT, repeatCount:999, reverse:true});
		}
	}
}