package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;

	public class SuperJumpTutorial extends TutElementBaseView
	{
		private var _arrow:DisplayObject;
		public function SuperJumpTutorial()
		{
			super(TutorialConstants.TUT_STEP_SUPER_JUMP);
			
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function show():void
		{
			super.show();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			_arrow = LayoutBuilder.findChild(this,"mcArrow");
			
			showPull();
			
			this.visible = true;
		}
		
		override public function hide():void
		{
			super.hide();
			
			Starling.juggler.removeTweens(_arrow);
			cleanup();
			
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function showPull():void
		{
			Starling.juggler.removeTweens(_arrow);
			_arrow.x = 0;
			_arrow.y = -10;
			Starling.juggler.tween(_arrow, 0.5, {x:0, y:-30, transition:Transitions.EASE_OUT, repeatCount:999, reverse:true});
		}
	}
}