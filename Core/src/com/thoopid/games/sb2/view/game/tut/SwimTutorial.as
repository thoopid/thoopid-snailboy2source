package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.display.DisplayObject;

	public class SwimTutorial extends TutElementBaseView
	{
		private var _swimIcon:DisplayObject;
		public function SwimTutorial()
		{
			super(TutorialConstants.TUT_STEP_SWIM);
			
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function show():void
		{
			super.show();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA(tutorialId), Assets.assets);
			_swimIcon = LayoutBuilder.findChild(this,"mcSwimIcon") as DisplayObject;
			_swimIcon.scaleX = -1;
			
			this.visible = true;
		}
		
		override public function hide():void
		{
			super.hide();
			cleanup();
			this.visible = false;
		}
	}
}