package com.thoopid.games.sb2.view.game.tut
{
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorial_ShowSignal;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class TutElementBaseMediator extends StarlingMediator
	{
		[Inject]
		public var view:TutElementBaseView;
		
		// models
		[Inject]
		public var tutModel:TutorialModel;
		
		// Signals
		[Inject]
		public var tutSignal:TriggerTutorial_ShowSignal;
		
		public function TutElementBaseMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			tutSignal.add(onTutSignal);
		}
		
		override public function onRemove():void
		{
			tutSignal.remove(onTutSignal);
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTutSignal(state:String):void
		{
			if(view.tutorialId == state)
			{
				view.show();
			} else
			{
				view.hide();
			}
		}
	}
}