package com.thoopid.games.sb2.view.game.tut
{
	import starling.display.Sprite;
	
	public class TutElementBaseView extends Sprite
	{
		public var tutorialId:String = "";
		
		protected var _isOpen:Boolean = false;
		
		public function TutElementBaseView(tutId:String)
		{
			tutorialId = tutId;
			super();
		}
		
		public function show():void
		{
			_isOpen = true;
		}
		
		public function hide():void
		{
			_isOpen = false;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			cleanup();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function cleanup():void
		{
			var i:int = this.numChildren;
			while(i--)
			{
				this.getChildAt(i).removeFromParent();
			}
		}
	}
}