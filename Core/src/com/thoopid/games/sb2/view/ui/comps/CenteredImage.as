package com.thoopid.games.sb2.view.ui.comps
{
	import starling.display.Image;
	
	public class CenteredImage extends Image
	{
		public function CenteredImage(params:Object)
		{
			super(Assets.assets.getTexture(params.texture));
			this.pivotX = this.width / 2;
			this.pivotY = this.height / 2;
		}
		
		override public function dispose():void
		{
			this.texture.dispose();
			super.dispose();
		}
	}
}