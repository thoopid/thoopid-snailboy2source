package com.thoopid.games.sb2.view.ui.comps
{
	import com.thoopid.games.sb2.view.ui.theme.CoinValueLabel;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	
	public class CoinValue extends Sprite
	{
		private var _coin:DisplayObject;
		
		private var _label:CoinValueLabel;
		private var _bubble:PoolImage;
		private var _animated:Boolean;
		
		public function CoinValue(params:Object)
		{
			super();
			
			_animated = (params.animated == null) ? true : params.animated;
			
			if(_animated)
			{
				_coin = Factory.movieclipPool.getMovie(Assets.assets.getTextures("ui_generic_coin_"), true);
				Starling.juggler.add(_coin as MovieClip);
				(_coin as MovieClip).fps = 10;
			} else
			{
				_coin = Factory.imagePool.getImage(Assets.assets.getTexture("ui_generic_coin_01"), true);
			}
			this.addChild(_coin);
			
			this.addChild(_bubble = Factory.imagePool.getImage(Assets.assets.getTexture("ui_HUD_coin_bubble")));
			
			this.addChild(_label = new CoinValueLabel({width:100, height:20, text:""}));
			
			_coin.x = _bubble.width >> 1;
			_coin.y = _bubble.height >> 1;
			
			_label.x = _coin.width + 5;
			_label.y = 5;
			_label.touchable = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function update(value:Number):void
		{
//			if(_label) _label.text = "x " + value.toString();
			if(_label) _label.text = value.toString();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
//			_coin = null;
//			_bubble = null;
//			_label = null;
		}
	}
}