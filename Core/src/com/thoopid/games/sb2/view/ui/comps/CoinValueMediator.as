package com.thoopid.games.sb2.view.ui.comps
{
	import com.thoopid.games.sb2.controller.signals.game.GameModelUpdateSignal;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class CoinValueMediator extends StarlingMediator
	{
		[Inject]
		public var view:CoinValue;
		
		// Signals
		[Inject]
		public var gameModelUpdateSignal:GameModelUpdateSignal;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		public function CoinValueMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			gameModelUpdateSignal.add(onGameUpdate);
			playerService.onCoinsUpdated.add(onGameUpdate);
			onGameUpdate();
		}
		
		override public function onRemove():void
		{
			playerService.onCoinsUpdated.remove(onGameUpdate);
			gameModelUpdateSignal.remove(onGameUpdate);
			
			view = null;
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onGameUpdate():void
		{
			view.update(playerService.player.coins);
		}
	}
}