package com.thoopid.games.sb2.view.ui.comps
{
	public class Factory
	{
		private static var _imagePool:ImagePool;
		private static var _movieclipPool:MovieclipPool;
		
		public function Factory()
		{
		}
		
		public static function get imagePool():ImagePool {return _imagePool;}
		public static function set imagePool(value:ImagePool):void
		{
			_imagePool = value;
		}
		
		public static function get movieclipPool():MovieclipPool {return _movieclipPool;}
		public static function set movieclipPool(value:MovieclipPool):void
		{
			_movieclipPool = value;
		}
	}
}