package com.thoopid.games.sb2.view.ui.comps
{
	
	import flash.geom.Matrix;
	
	import starling.core.Starling;
	import starling.textures.Texture;
	import starling.utils.Color;

	public class ImagePool
	{
		private var _items:Array;
		private var _c:int;
		private var _tex:Texture;
		
		private var _count:int=0;
		public function ImagePool(type:Class, len:int, tex:Texture)
		{
			_items = [];
			_c = len;
			_tex = tex;
			
			var i:int = len;
			var cc:*;
			while(--i > -1)
			{
				cc = new type(tex);
				_items.push(cc);
				_count++;
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function getImage(tex:Texture, centerAlign:Boolean = false):PoolImage
		{
			if(_items.length <= 0)
			{
				throw new Error("NO MORE Pool Images");
				return null;
			}
			
//			var im:PoolImage = _items.shift();
////			var im:PoolImage = _items[--_count];
			
			Starling.juggler.removeTweens(im);
			
//			var m:Matrix = im.transformationMatrix;
//			m.identity();
//			im.texture = tex;
//			im.transformationMatrix = m;
//			im.readjustSize();
//			im.visible = true;
//			im.alpha = 1;
//			im.color = Color.WHITE;
//			im.pivotX = (centerAlign) ? tex.frame.width >> 1 : 0;
//			im.pivotY = (centerAlign) ? tex.frame.height >> 1 : 0;
//			trace("POOLING!: "+_items.length);
//			return im;
			if(tex == null) tex = Assets.assets.getTexture("empty");
			var im:PoolImage = new PoolImage(tex);
			
			im.pivotX = (centerAlign) ? ((tex.frame) ? tex.frame.width : tex.width) >> 1 : 0;
			im.pivotY = (centerAlign) ? ((tex.frame) ? tex.frame.height : tex.height) >> 1 : 0;
			return im;
		}
		
		public function returnImage(s:PoolImage):void
		{
			if(_items.indexOf(s) != -1)
			{
				throw new Error("Image already in pool!!");
			}
////			_items[_count++] = s;
			
			
//			_items.push(s);
		}
	}
}