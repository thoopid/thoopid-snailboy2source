package com.thoopid.games.sb2.view.ui.comps
{
	import flash.geom.Matrix;
	
	import starling.core.Starling;
	import starling.textures.Texture;

	public class MovieclipPool
	{
		
		private var _items:Array;
		private var _c:int;
		private var _tex:Vector.<Texture>;
		
		public function MovieclipPool(type:Class, len:int, tex:Vector.<Texture>,fps:Number=15)
		{
			_items = [];
			_c = len;
			_tex = tex;
			
			var i:int = len;
			var cc:*;
			while(--i > -1)
			{
				cc = new type(tex,fps);
				_items.push(cc);
			}
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function getMovie(textures:Vector.<Texture>, centerAlign:Boolean = false):PoolMovieclip
		{
			
			if(_items.length <= 0)
			{
				trace("NO MORE MOVIES")
				return null;
			}
			var im:PoolMovieclip = _items.shift();
			
			while(im.numFrames > 1){
				im.removeFrameAt(0);
			}
			
			// add new frames
			for each (var texture:Texture in textures){
				im.addFrame(texture);
			}
			
			// remove that last previous frame
			var m:Matrix = im.transformationMatrix;
			m.identity();
			im.transformationMatrix = m;
			
			im.removeFrameAt(0);
			im.currentFrame = 1;
			im.readjustSize();
			im.alignPivot();
			
			var w:Number = (im.getFrameTexture(0).frame) ? im.getFrameTexture(0).frame.width : im.getFrameTexture(0).width;
			var h:Number = (im.getFrameTexture(0).frame) ? im.getFrameTexture(0).frame.height : im.getFrameTexture(0).height;
			
			im.pivotX = (centerAlign) ? w >> 1 : 0;
			im.pivotY = (centerAlign) ? h >> 1 : 0;
			
			return im;
		}
		
		public function returnMovieClip(s:PoolMovieclip):void
		{
//			s.pivotX = 0;
//			s.pivotY = 0;
//			s.scaleX = 1;
//			s.scaleY = 1;
//			s.x = 0;
//			s.y = 0;
//			s.visible = true;
//			s.alpha = 1;
//			s.rotation = 0;
			_items.push(s);
			
//			if(_items.indexOf(s) != -1)
//			{
//				throw new Error("Messup somewhere with movieclips");
//			} else
//			{
//				_items.push(s);
//			}
		}
	}
}