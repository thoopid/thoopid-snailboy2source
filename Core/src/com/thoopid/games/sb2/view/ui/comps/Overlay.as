package com.thoopid.games.sb2.view.ui.comps
{
	import flash.geom.Rectangle;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import starling.display.Sprite;
	
	
	public class Overlay extends Sprite
	{
		private var _glow:PoolImage;
		private var _bg:Scale9Image;
		public function Overlay(params:Object)
		{
//			super(params.texture);
			super();
			
			this.addChild(_bg = new Scale9Image(new Scale9Textures(Assets.assets.getTexture(params.texture), new Rectangle(5,5,2,2))));
			
			_bg.width = Config.GAME_WIDTH;
			_bg.height = Config.GAME_HEIGHT;
			this.pivotX = _bg.width >> 1;
			this.pivotY = _bg.height >> 1;
			
			this.addChild(_glow = Factory.imagePool.getImage(Assets.assets.getTexture("ui_popUp_glow"), true));
			_glow.x = this.pivotX;
			_glow.y = this.pivotY + 25;
			
			this.touchable = true;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_glow = null;
		}
	}
}