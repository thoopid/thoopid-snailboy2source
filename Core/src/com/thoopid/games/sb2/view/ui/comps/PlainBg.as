package com.thoopid.games.sb2.view.ui.comps
{
	public class PlainBg extends UIImage
	{
		public function PlainBg(params:Object)
		{
			super(params);
			
			this.color = Config.GAME_BG_COLOR;
			this.alpha = 0.85;
			this.touchable = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function set width(value:Number):void
		{
			super.width = Config.GAME_WIDTH;
		}
		
		override public function set height(value:Number):void
		{
			super.height = Config.GAME_HEIGHT;
		}
	}
}