package com.thoopid.games.sb2.view.ui.comps
{
	
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class PoolImage extends Image
	{
		public function PoolImage(texture:Texture)
		{
			super(texture);
			
//			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
		}
		
		private function onRemove():void
		{
			// TODO Auto Generated method stub
//			Factory.imagePool.returnImage(this);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function dispose():void
		{
//			if(this.parent) 
//			{
//				this.removeFromParent(false);
//			}
			Factory.imagePool.returnImage(this);
			super.dispose();
		}
	}
}