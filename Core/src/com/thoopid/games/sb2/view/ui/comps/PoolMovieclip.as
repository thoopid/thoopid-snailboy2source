package com.thoopid.games.sb2.view.ui.comps
{
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.textures.Texture;
	
	public class PoolMovieclip extends MovieClip
	{
		public function PoolMovieclip(textures:__AS3__.vec.Vector.<starling.textures.Texture>, fps:Number=12)
		{
			super(textures, fps);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function dispose():void
		{
			if(Starling.juggler.contains(this)) Starling.juggler.remove(this);
			
			if(this.parent) this.removeFromParent(false);
			Factory.movieclipPool.returnMovieClip(this);
			super.dispose();
		}
	}
}