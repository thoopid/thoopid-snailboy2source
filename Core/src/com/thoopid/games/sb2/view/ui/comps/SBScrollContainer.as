package com.thoopid.games.sb2.view.ui.comps
{
	import feathers.controls.ScrollContainer;
	
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class SBScrollContainer extends ScrollContainer
	{
		private var _lastPosition:Number;
		private var _scrolling:Boolean;
		private var _isDragging:Boolean;
		private var _enable:Boolean = false;
		
		public function SBScrollContainer(params:Object)
		{
			super();
			
			this.width = params.width;
			this.height = params.height;
			
			enable = true;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get enable():Boolean
		{
			return _enable;
		}

		public function set enable(value:Boolean):void
		{
			_enable = value;
			
			if(_enable)
			{
				this.addEventListener(Event.SCROLL, onScroll);
				this.addEventListener(TouchEvent.TOUCH, onTouchHandler);
				
			} else
			{
				this.removeEventListener(Event.SCROLL, onScroll);
				this.removeEventListener(TouchEvent.TOUCH, onTouchHandler);
			}
		}

		public function get scrolling():Boolean
		{
			return _scrolling;
		}
		
		override public function dispose():void
		{
			enable = false;
			super.dispose();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouchHandler(e:TouchEvent):void
		{
			if(e.getTouch(this,TouchPhase.ENDED))
			{
				_scrolling = false;
				_isDragging = false;
				_lastPosition = this.horizontalScrollPosition;
			} else if(e.getTouch(this,TouchPhase.BEGAN))
			{
				_isDragging = true;
				_lastPosition = this.horizontalScrollPosition;	
			}
		}
		
		private function onScroll():void
		{
			if(Math.abs(this.horizontalScrollPosition - _lastPosition) > 10 && _isDragging)
			{
				_scrolling = true;
			} else
			{
				_scrolling = false;
			}
		}
	}
}