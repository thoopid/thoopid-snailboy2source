package com.thoopid.games.sb2.view.ui.comps
{
	import starling.display.Image;
	
	public class UIImage extends Image
	{
		public function UIImage(params:Object)
		{
			super(Assets.assets.getTexture(params.texture));
		}
	}
}