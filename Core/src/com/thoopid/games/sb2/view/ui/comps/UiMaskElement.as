package com.thoopid.games.sb2.view.ui.comps
{
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.pixelmask.PixelMaskDisplayObject;
	
	public class UiMaskElement extends Sprite
	{
		private var _params:Object;
		public function UiMaskElement(params:Object)
		{
			super();
			
			_params = params;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onAdded(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			// myCustomDisplayObject and myCustomMaskDisplayObject can be any Starling display object:
			if(this.parent)
			{
				var target:DisplayObject = parent.getChildByName(_params.target);
				if(target)
				{
					// for masks with no animation (note, MUCH better performance!)
					var maskedDisplayObject:PixelMaskDisplayObject = new PixelMaskDisplayObject(-1, false);
					maskedDisplayObject.addChild(target);
					
					var extendedMask:Sprite = new Sprite();
					var q:Quad;
					extendedMask.addChild(q = new Quad(target.width, target.height,0xff3300,true));
					q.alpha = 0;
					var mask:Image = Factory.imagePool.getImage(Assets.assets.getTexture(_params.texture), true);
					mask.width = _params.width;
					mask.height = _params.height;
					mask.x = this.x;
					mask.y = this.y;
					extendedMask.addChild(mask);
					extendedMask.flatten();
					maskedDisplayObject.mask = extendedMask;
					maskedDisplayObject.inverted = true;
					
					parent.addChild(maskedDisplayObject);
				}
			}
		}		
	}
}