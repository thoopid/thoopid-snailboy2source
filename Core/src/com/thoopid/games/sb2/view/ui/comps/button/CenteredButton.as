package com.thoopid.games.sb2.view.ui.comps.button
{
	import starling.textures.Texture;
	import starling.utils.AssetManager;

	public class CenteredButton extends UiButton
	{
		public function CenteredButton(params:Object=null)
		{
			var assets:AssetManager = Assets.assets;
			var tex:Texture = assets.getTexture(params.upState);
			super({upState:tex, text:"", downState:assets.getTexture(params.downState)});
			this.width = params.width || tex.width;
			this.height = params.height || tex.height;
			this.pivotX = upState.width / 2;
			this.pivotY = upState.height / 2;
			
//			this.fontName = ApplicationTheme.rod1Font.name;
//			this.fontSize = 16;
//			this.fontColor = 0xffffff;
//			this.textBounds = new Rectangle(0,params.textTopPadd || 0,upState.width,30);
		}
	}
}