package com.thoopid.games.sb2.view.ui.comps.button
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;

	public class IconCenteredButton extends CenteredButton
	{
		private var _icon:PoolImage;
		public function IconCenteredButton(params:Object=null)
		{
			// Override hack
//			params.upState = "ui_btn_bg";
			super(params);
			this.width = params.width;
			this.height = params.height;
			this.addChild(_icon = Factory.imagePool.getImage(Assets.assets.getTexture(params.iconName),true));
			_icon.x = this.pivotX;
			_icon.y = this.pivotY;
			
			_icon.scaleX = _icon.scaleY = (upState.width / this.width);
		}
	}
}