package com.thoopid.games.sb2.view.ui.comps.button
{
	import flash.display.BitmapData;
	
	import feathers.controls.Label;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class TextButton extends UiButton
	{
		private var _label:Label;
		public function TextButton(params:Object)
		{
			var t:Texture = Texture.fromBitmapData(new BitmapData(params.width,params.height,true,0xFFFFFF));
			super({upState:t});
			this.scaleWhenDown = 1;
			
			this.addChild(_label = new MenuLabel());
			_label.width = params.width;
			_label.height = 16;
			_label.text = params.text || "No Label Supplied";
			_label.y = params.height/2 - _label.height/2;
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function onTrigger(e:Event):void
		{
			_label.pivotX = 10;
			Starling.juggler.removeTweens(_label);
			Starling.juggler.tween(_label, 0.2,{pivotX:0,delay:0.2});
		}
		
		override public function get text():String
		{
			return _label.text;
		}
		override public function set text(value:String):void
		{
			if(_label) _label.text = value;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
//		private function onTrigger(e:Event):void
//		{
//			_label.alpha = 0;
//			Starling.juggler.tween(_label, 0.2, {alpha:1});
//		}
	}
}