package com.thoopid.games.sb2.view.ui.comps.button
{
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class ToggleButton extends Sprite
	{
		private var _on:UiButton;
		private var _off:UiButton;
		public function ToggleButton(params:Object)
		{
			this.addChild(_off = new UiButton({upState:params.offState}));
			this.addChild(_on = new UiButton({upState:params.onState}));
			
			_on.addEventListener(Event.TRIGGERED, onTrigger);
			_off.addEventListener(Event.TRIGGERED, onTrigger);
			
			toggleState(false);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function set scaleWhenDown(v:Number):void
		{
			_on.scaleWhenDown = v;
			_off.scaleWhenDown = v;
		}
		
		public function toggleState(toggle:Boolean):void
		{
//			_on.visible = toggle;
			Starling.juggler.tween(_off, 0.7, {alpha:(toggle) ? 1 : 0, transition:Transitions.EASE_OUT});
//			_off.visible = toggle;
		}
		
		public function enable():void
		{
			_on.enabled = true;
			_off.enabled = true;
		}
		
		public function disable():void
		{
			_on.enabled = false;
			_off.enabled = false;
		}
		
		override public function dispose():void
		{
			_on.removeEventListener(Event.TRIGGERED, onTrigger);
			_off.removeEventListener(Event.TRIGGERED, onTrigger);
			super.dispose();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTrigger(e:Event):void
		{
			dispatchEvent(new Event(Event.TRIGGERED,true,this));
		}
	}
}