package com.thoopid.games.sb2.view.ui.comps.button
{
	import com.thoopid.games.sb2.constants.game.GameConstants;
	
	import citrus.core.CitrusEngine;
	
	import starling.display.Button;
	import starling.events.Event;
	
	public class UiButton extends Button
	{
		public function UiButton(params:Object)
		{
			super(params.upState, params.text || "", params.downState);
			this.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function dispose():void
		{
			this.removeEventListener(Event.TRIGGERED, onTrigger);
			this.upState.dispose();
			this.downState.dispose();
			
			super.dispose();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		protected function onTrigger(e:Event):void
		{
			// TODO
			CitrusEngine.getInstance().sound.playSound(GameConstants.SOUND_BUTTON_UI);
		}
	}
}