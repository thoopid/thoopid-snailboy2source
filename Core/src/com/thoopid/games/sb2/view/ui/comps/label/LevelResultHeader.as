package com.thoopid.games.sb2.view.ui.comps.label
{
	import feathers.controls.Label;
	
	public class LevelResultHeader extends Label
	{
		public function LevelResultHeader(params:Object)
		{
			super();
			
			this.text = params.text;
			this.width = params.width;
			this.height = params.height;
			this.pivotX = this.width >> 1;
			this.pivotY = this.height >> 1;
			
			if(params.namesList) this.nameList.add(params.namesList);
		}
	}
}