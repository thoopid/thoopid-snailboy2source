package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmCallbackSignal;
	import com.thoopid.games.sb2.model.ui.ConfirmPopupVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import feathers.controls.Label;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class ConfirmPopup extends Sprite
	{
		private var _callbackSignal:ConfirmCallbackSignal;
		
		public function ConfirmPopup()
		{
			super();
			this.visible = false;
			this.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function show(vo:ConfirmPopupVO):void
		{
			_callbackSignal = vo.callbackSignal;
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.CONFIRM_SCREEN), Assets.assets);
			
			(LayoutBuilder.findChild(this,"tfTitle") as Label).text = vo.title;
			(LayoutBuilder.findChild(this,"tfSub") as Label).text = vo.sub;
			(LayoutBuilder.findChild(this,"tfMessage") as Label).text = vo.message;
			(LayoutBuilder.findChild(this,"mcCoinvalue") as PopupCoinValue).update(vo.cost);
			
			this.visible = true;
		}
		
		public function hide():void
		{
			cleanup();
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		private function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnYes":
					_callbackSignal.dispatch(true);
					hide();
					break;
				case "btnNo":
					_callbackSignal.dispatch(false);
					hide();
					break;
			}
		}
		
		private function cleanup():void
		{
			var i:int = this.numChildren;
			while(i--)
			{
				this.getChildAt(i).removeFromParent(true);
			}
			
			_callbackSignal.removeAll();
			_callbackSignal = null;
		}
	}
}