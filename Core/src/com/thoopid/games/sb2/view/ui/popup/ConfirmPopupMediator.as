package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.controller.signals.ui.ConfirmPopupRequestSignal;
	import com.thoopid.games.sb2.model.ui.ConfirmPopupVO;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class ConfirmPopupMediator extends StarlingMediator
	{
		
		[Inject]
		public var view:ConfirmPopup;
		
		// SIgnals
		[Inject]
		public var confirmPopupSignal:ConfirmPopupRequestSignal;
		
		public function ConfirmPopupMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			confirmPopupSignal.add(onScreenrequest);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onScreenrequest(vo:ConfirmPopupVO):void
		{
			view.show(vo); 
		}
	}
}