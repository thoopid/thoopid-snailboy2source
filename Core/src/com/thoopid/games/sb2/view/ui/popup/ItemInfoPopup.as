package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.game.ItemInfoVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.UiSprite;
	
	import feathers.controls.Label;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class ItemInfoPopup extends Sprite
	{
		private var _holder:UiSprite;
		public function ItemInfoPopup()
		{
			super();
			this.visible = false;
			this.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function show(vo:ItemInfoVO):void
		{
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.ITEM_INFO_SCREEN), Assets.assets);
			
			(LayoutBuilder.findChild(this,"tfTitle") as Label).text = vo.title;
			(LayoutBuilder.findChild(this,"tfSub") as Label).text = vo.name;
			(LayoutBuilder.findChild(this,"tfMessage") as Label).text = vo.info;
			
			_holder = LayoutBuilder.findChild(this,"mcImage") as UiSprite;
			_holder.addChild(Factory.imagePool.getImage(Assets.assets.getTexture(vo.id),true));
			
			this.visible = true;
		}
		
		public function hide():void
		{
			cleanup();
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		private function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnClose":
					hide();
					break;
			}
		}
		
		private function cleanup():void
		{
			var i:int = this.numChildren;
			while(i--)
			{
				this.getChildAt(i).removeFromParent(true);
			}
		}
	}
}