package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.controller.signals.ui.ItemInfoPopupRequestSignal;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class ItemInfoPopupMediator extends StarlingMediator
	{
		[Inject]
		public var view:ItemInfoPopup;
		
		[Inject]
		public var requestSignal:ItemInfoPopupRequestSignal;
		
		public function ItemInfoPopupMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			requestSignal.add(view.show);
		}
		
		override public function onRemove():void
		{
			requestSignal.remove(view.show);
			super.onRemove();
		}
	}
}