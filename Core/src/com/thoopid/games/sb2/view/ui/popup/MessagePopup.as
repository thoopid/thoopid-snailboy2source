package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.ui.MessagePopupVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import feathers.controls.Label;
	
	import starling.display.Sprite;
	
	public class MessagePopup extends Sprite
	{
		private var _messageVo:MessagePopupVO;
		
		public function MessagePopup()
		{
			super();
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get messageVo():MessagePopupVO
		{
			return _messageVo;
		}

		public function show(vo:MessagePopupVO):void
		{
			_messageVo = vo;
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.MESSAGE_SCREEN), Assets.assets);
			
			(LayoutBuilder.findChild(this,"tfTitle") as Label).text = vo.title;
			(LayoutBuilder.findChild(this,"tfSub") as Label).text = vo.subtitle;
			(LayoutBuilder.findChild(this,"tfMessage") as Label).text = vo.messege;
			
//			(LayoutBuilder.findChild(this,"mcCoinvalue") as PopupCoinValue).update(vo.cost);
			
			this.visible = true;
		}
		
		public function hide():void
		{
			cleanup();
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function cleanup():void
		{
			var i:int = this.numChildren;
			while(i--)
			{
				this.getChildAt(i).removeFromParent(true);
			}
			
			_messageVo = null;
		}
	}
}