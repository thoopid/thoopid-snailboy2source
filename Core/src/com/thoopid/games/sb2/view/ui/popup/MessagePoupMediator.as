package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.controller.signals.ui.MessagePopupRequestSignal;
	import com.thoopid.games.sb2.model.ui.MessagePopupVO;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	import starling.events.Event;
	
	public class MessagePoupMediator extends StarlingMediator
	{
		[Inject]
		public var view:MessagePopup;
		
		[Inject]
		public var messegePopupRequestSignal:MessagePopupRequestSignal;
		
		public function MessagePoupMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			messegePopupRequestSignal.add(onScreenrequest);
			
			this.view.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnYes":
					if(view.messageVo.callback)
					{
						view.messageVo.callback.call(view.messageVo.callbackData);
					}
					view.hide();
					break;
				case "btnNo":
					view.hide();
					break;
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onScreenrequest(vo:MessagePopupVO):void
		{
			view.show(vo); 
		}
	}
}