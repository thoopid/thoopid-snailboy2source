package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.view.ui.theme.ShortMsgLabel;
	
	import feathers.controls.Label;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class ShortMessagePopup extends Sprite
	{
		private var _label:Label;
		
		public function ShortMessagePopup()
		{
			super();
			this.addChild(_label = new ShortMsgLabel({width:300, height:20}));
			
			this.x = Config.GAME_VIEW_H_CENTER;
			this.y = Config.GAME_VIEW_V_CENTER + 40;
			
			this.visible = false;
			this.touchable = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function show(message:String):void
		{
			this.visible = true;
			
			_label.scaleX = 0.2;
			_label.scaleY = 0.2;
			_label.alpha = 1;
			
			_label.text = message;
			
			Starling.juggler.removeTweens(_label);
			Starling.juggler.tween(_label, 0.4, {alpha:1, scaleX:1, scaleY:1, transition:Transitions.EASE_OUT_BACK, onComplete:onComple});
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onComple():void
		{
			Starling.juggler.tween(_label, 0.4, {delay:2.5, alpha:0, scaleX:1.2, scaleY:1.2, transition:Transitions.EASE_OUT, onComplete:onOutDone});
		}
		
		private function onOutDone():void
		{
			this.visible = false;
		}
	}
}