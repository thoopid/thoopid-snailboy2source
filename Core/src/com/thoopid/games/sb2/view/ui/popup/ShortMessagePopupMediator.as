package com.thoopid.games.sb2.view.ui.popup
{
	import com.thoopid.games.sb2.controller.signals.ui.ShortMessageDisplaySignal;
	
	import citrus.core.CitrusEngine;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	public class ShortMessagePopupMediator extends StarlingMediator
	{
		[Inject]
		public var view:ShortMessagePopup;
		
		[Inject]
		public var shortMessageDisplaySignal:ShortMessageDisplaySignal;
		
		public function ShortMessagePopupMediator()
		{
			super();
			
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			if(Config.DEBUG_CONSOLE)
			{
				CitrusEngine.getInstance().console.addCommand("showShort", view.show);
			}
			
			super.onRegister();
			
			shortMessageDisplaySignal.add(view.show);
		}
		
		override public function onRemove():void
		{
			shortMessageDisplaySignal.remove(view.show);
			
			super.onRemove()
		}
	}
}