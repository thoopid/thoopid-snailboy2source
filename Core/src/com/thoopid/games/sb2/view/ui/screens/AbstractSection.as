package com.thoopid.games.sb2.view.ui.screens
{
	import org.osflash.signals.Signal;
	
	import starling.display.Sprite;
	
	public class AbstractSection extends Sprite
	{
		public var showCompleteSignal:Signal = new Signal();
		public var hideCompleteSignal:Signal = new Signal();
		public var showStartSignal:Signal = new Signal();
		public var hideStartSignal:Signal = new Signal();
		
		protected var _active:Boolean = false;
		
		public function AbstractSection()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get active():Boolean
		{
			return _active;
		}

		public function showComplete():void
		{
			_active = true;
			showCompleteSignal.dispatch();
		}
		
		public function hideComplete():void
		{
			hideCompleteSignal.dispatch();
		}
		
		public function showStart(mustcomplete:Boolean=false):void
		{
			if(mustcomplete) showComplete();
			showStartSignal.dispatch();
		}
		
		public function hideStart():void
		{
			_active = false;
			hideStartSignal.dispatch();
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function cleanup():void
		{
			var i:int = this.numChildren;
			while(i--)
			{
				this.getChildAt(i).removeFromParent(true);
			}
		}
	}
}