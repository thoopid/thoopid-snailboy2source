package com.thoopid.games.sb2.view.ui.screens
{
	import org.robotlegs.mvcs.StarlingMediator;
	
	import starling.events.Event;
	
	public class AbstractSectionMediator extends StarlingMediator
	{
		// View
		[Inject]
		public var view:AbstractSection;
		
		// Models
		
		// Signals
		
		public function AbstractSectionMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			view.showCompleteSignal.add(showComplete);
			view.hideCompleteSignal.add(hideComplete);
			view.showStartSignal.add(showStart);
			view.hideStartSignal.add(hideStart);
			
			view.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		override public function onRemove():void
		{
			view.removeEventListener(Event.TRIGGERED, onTrigger);
			super.onRemove();
		}
		
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function showComplete():void
		{
			// release section model
			
		}
		
		protected function hideComplete():void
		{
			
		}
		
		protected function hideStart():void
		{
			
		}
		
		protected function showStart():void
		{
			
		}
		
		protected function onTrigger(e:Event):void
		{
			
		}
	}
}