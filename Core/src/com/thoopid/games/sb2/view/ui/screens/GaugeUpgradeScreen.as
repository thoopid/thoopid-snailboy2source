package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.game.GaugeVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.screens.hud.GaugeUpgradeView;
	
	import feathers.controls.Label;

	public class GaugeUpgradeScreen extends AbstractSection
	{
		private var _gaugeItem:GaugeUpgradeView;
		private var _jumpItem:GaugeUpgradeView;
		private var _swimItem:GaugeUpgradeView;
		
		public function GaugeUpgradeScreen()
		{
			super();
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function reflect(vo:GaugeVO):void
		{
			var g:GaugeVO = new GaugeVO();
			g.gauge.level = vo.gauge.level;
			g.jump.level = 0;
			g.swim.level = 0;
			_gaugeItem.reset(g);
			
			g.gauge.level = 0;
			g.jump.level = vo.jump.level;
			g.swim.level = 0;
			_jumpItem.reset(g);
			
			g.gauge.level = 0;
			g.jump.level = 0;
			g.swim.level = vo.swim.level;
			_swimItem.reset(g);
			
			var gaugeText:Label = LayoutBuilder.findChild(this,"tfSubSize") as Label;
			var jumpText:Label = LayoutBuilder.findChild(this,"tfSubJump") as Label;
			var swimText:Label = LayoutBuilder.findChild(this,"tfSubSwim") as Label;
			
			gaugeText.text = "SIZE   LEVEL "+vo.gauge.level
			jumpText.text = "JUMP LEVEL "+vo.jump.level
			swimText.text = "SWIM LEVEL "+vo.swim.level
			
		}
		
		override public function showStart(mustcomplete:Boolean=false):void
		{
			super.showStart(true);
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.GAUGE_UPGRADE), Assets.assets);
			this.visible = true;
			
			_gaugeItem = LayoutBuilder.findChild(this,"mcGauge") as GaugeUpgradeView;
			_jumpItem = LayoutBuilder.findChild(this,"mcJump") as GaugeUpgradeView;
			_swimItem = LayoutBuilder.findChild(this,"mcSwim") as GaugeUpgradeView;
			
			_gaugeItem.scaleX = _gaugeItem.scaleY = 0.85;
			_jumpItem.scaleX = _jumpItem.scaleY = 1.2;
			_swimItem.scaleX = _swimItem.scaleY = 1.8;
		}
		
		override public function hideStart():void
		{
			super.hideStart();
			this.visible = true;
			cleanup();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
	}
}