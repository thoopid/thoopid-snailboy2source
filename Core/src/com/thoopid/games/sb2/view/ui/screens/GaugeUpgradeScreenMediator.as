package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.game.PlayerElements;
	import com.thoopid.games.sb2.controller.signals.ui.GaugeUpgradeScreenSignal;
	import com.thoopid.games.sb2.controller.signals.ui.UpgradeGaugeRequestSignal;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.game.GaugeVO;
	import com.thoopid.games.sb2.view.game.objects.SnailboyHero;
	
	import flash.display.MovieClip;
	
	import starling.core.Starling;
	import starling.events.Event;

	public class GaugeUpgradeScreenMediator extends AbstractSectionMediator
	{
		// signals
		[Inject]
		public var gaugeUpgradeScreenSignal:GaugeUpgradeScreenSignal;
		[Inject]
		public var upgradeGaugeRequestSignal:UpgradeGaugeRequestSignal;
		
		// model
		[Inject]
		public var gaugeModel:GaugeModel;
		
		public function GaugeUpgradeScreenMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			gaugeUpgradeScreenSignal.add(onShowUpgradeScreen);
			
			view.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		override public function onRemove():void
		{
			gaugeUpgradeScreenSignal.remove(onShowUpgradeScreen);
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onShowUpgradeScreen(show:Boolean):void
		{
			if(show && !view.active)
			{
				view.showStart();
				gaugeModel.updateModelSignal.add(onUpdateGaugeModelHandler);
				(view as GaugeUpgradeScreen).reflect(gaugeModel.gauge);
			} else
			{
				gaugeModel.updateModelSignal.remove(onUpdateGaugeModelHandler);
				view.hideStart();
			}
		}
		
		private function onUpdateGaugeModelHandler(vo:GaugeVO, isReplenish:Boolean):void
		{
			(view as GaugeUpgradeScreen).reflect(gaugeModel.gauge);
		}
		
		private function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnClose":
					gaugeUpgradeScreenSignal.dispatch(false);
					break;
				case "btnPlusSize":
					upgradeGaugeRequestSignal.dispatch(PlayerElements.GAUGE_GAUGE);
					break;
				case "btnPlusJump":
					upgradeGaugeRequestSignal.dispatch(PlayerElements.GAUGE_JUMP);
					break;
				case "btnPlusSwim":
					upgradeGaugeRequestSignal.dispatch(PlayerElements.GAUGE_SWIM);
					break;
			}
		}
	}
}