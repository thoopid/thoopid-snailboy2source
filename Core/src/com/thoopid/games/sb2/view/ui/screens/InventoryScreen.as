package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.game.GameCollectableTypes;
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.util.ui.GridPlacement;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.tut.InventoryBuyTutorial;
	import com.thoopid.games.sb2.view.game.tut.InventoryCloseTutorial;
	import com.thoopid.games.sb2.view.ui.comps.SBScrollContainer;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	import com.thoopid.games.sb2.view.ui.screens.inventory.InventoryItemBtn;
	import com.thoopid.games.sb2.view.ui.screens.inventory.InventorySlotBtn;
	
	import feathers.controls.Button;
	import feathers.controls.Scroller;
	
	import starling.core.Starling;
	import starling.display.Sprite;

	public class InventoryScreen extends AbstractSection
	{
		private var _scroller:SBScrollContainer;
		
		private var _slots:Vector.<InventorySlotBtn>;
		private var _bg:Sprite;
		
		private var _maxInventoryItems:Number = 15;
		
		public function InventoryScreen()
		{
			super();
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(items:Vector.<InventoryItemVO>):void
		{
			Starling.juggler.tween(_bg, 0.2, {alpha:1});
			
			var iButton:InventoryItemBtn;
			var iItem:InventoryItemVO;
			for (var i:int = 0; i < items.length; i++) 
			{
				iItem = items[i];
				iButton = _scroller.getChildAt(i) as InventoryItemBtn;
				if(iButton)
				{
					iButton.init(iItem);
					
					Starling.juggler.tween(iButton, 0.4, {alpha:1, delay:0.1 * i});
				}
			}
			
			if(items.length < _maxInventoryItems)
			{
				var start:Number = items.length;
				for (var j:int = start; j < _maxInventoryItems; j++) 
				{
					iButton = _scroller.getChildAt(j) as InventoryItemBtn;
					if(iButton)
					{
						Starling.juggler.tween(iButton, 0.4, {alpha:1, delay:0.1 * j});
					}
				}
			}
			
			var iSlot:InventorySlotBtn;
			for (var s:int = 0; s < _slots.length; s++) 
			{
				iSlot = _slots[s];
				Starling.juggler.tween(iSlot, 0.4, {alpha:1, delay:0.5 + (0.1 * s)});
			}
		}
		
		public function reflectModel(items:Vector.<InventoryItemVO>):void
		{
			var iButton:InventoryItemBtn;
			var iItem:InventoryItemVO;
			for (var i:int = 0; i < items.length; i++) 
			{
				iItem = items[i];
				
				var len:int = _scroller.numChildren;
				while(len--)
				{
					iButton = _scroller.getChildAt(i) as InventoryItemBtn;
					if(iButton && iButton.id == iItem.id)
					{
						iButton.update(iItem);
					}
				}
			}
		}
		
		public function reflectSlots(sItems:Vector.<InventoryItemVO>):void
		{
			var i:int = _slots.length;
			while(i--)
			{
				_slots[i].cleanUp();
			}
			
			var slot:InventorySlotBtn;
			for (var j:int = 0; j < sItems.length; j++) 
			{
				slot = _slots[j];
				slot.init(sItems[j]);
			}
		}
		
		override public function showStart(mustcomplete:Boolean=false):void
		{
			super.showStart(true);
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.INVENTORY_SCREEN), Assets.assets);
			this.visible = true;
			
			_bg = LayoutBuilder.findChild(this, "mcBg") as Sprite;
			_bg.alpha = 0;
			
			_scroller = LayoutBuilder.findChild(this,"mcHolder") as SBScrollContainer;
			_scroller.width = Config.GAME_WIDTH - (_scroller.x * 2);
			_scroller.horizontalScrollPosition = 0;
			_scroller.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_scroller.clipContent = false;
			
			var arrangement:Array = [];
			var item:InventoryItemBtn;
			for (var i:int = 0; i < 15; i++) 
			{
				_scroller.addChild(item = new InventoryItemBtn());
				arrangement.push(item);
				item.alpha = 0;
			}
			
			// Layout grid
			GridPlacement.PlaceOnGrid(arrangement, (Config.GAME_WIDTH < 550) ? 4 : 5, 90, 90);
			
			// Slot items
			_slots = new Vector.<InventorySlotBtn>();
			var slotBtn:InventorySlotBtn;
			for (var j:int = 0; j < 3; j++) 
			{
				_slots.push(slotBtn = new InventorySlotBtn());
				this.addChild(slotBtn);
				
				slotBtn.alpha = 0;
				slotBtn.x = Config.GAME_WIDTH - slotBtn.width - 10;
				slotBtn.y = _scroller.y + (j * 90);
			}
			
			// Add Tutorials
			this.addChild(new InventoryBuyTutorial());
			this.addChild(new InventoryCloseTutorial());
		}
		
		override public function hideStart():void
		{
			cleanup();
			this.visible = false;
		}
		
		override protected function cleanup():void
		{
			super.cleanup();
			
			_slots = null;
			_scroller = null;
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		public function toggleTutorialAccess(state:String, toggle:Boolean):void
		{
			_scroller.enable = toggle;
			
			var s:int = _slots.length;
			var sItem:InventorySlotBtn;
			while(s--)
			{
				sItem = _slots[s];
				if(sItem)
				{
					sItem.enable = toggle;
				}
			}
			
			
			// Switch on items needed and not needed
			switch(state)
			{
				case TutorialConstants.TUT_STEP_INVENTORY_BUY:
					toggleForBuyTut(toggle);
					break;
				case TutorialConstants.TUT_STEP_INVENTORY_CLOSE:
					toggleForCloseTut(toggle);
					break;
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function toggleForBuyTut(toggle:Boolean):void
		{
			var i:int = _scroller.numChildren;
			var item:InventoryItemBtn;
			while(i--)
			{
				item = _scroller.getChildAt(i) as InventoryItemBtn;
				if(item)
				{
					if(item.id != GameCollectableTypes.TYPE_ORA_GAUGE_REPLENISH) item.enable = toggle;
				}
			}
			
			LayoutBuilder.findChild(this, "btnClose") as CenteredButton
		}
		
		private function toggleForCloseTut(toggle:Boolean):void
		{
			var i:int = _scroller.numChildren;
			var item:InventoryItemBtn;
			while(i--)
			{
				item = _scroller.getChildAt(i) as InventoryItemBtn;
				if(item)
				{
					item.enable = toggle;
				}
			}
		}
		
	}
}