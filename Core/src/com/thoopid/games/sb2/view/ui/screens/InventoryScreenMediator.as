package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryUpdatedSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ItemInfoPopupRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.PurchaseInventoryItemRequestSignal;
	import com.thoopid.games.sb2.controller.signals.ui.RemoveSlotItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.SlotsUpdateSignal;
	import com.thoopid.games.sb2.model.game.StoryDataModel;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.ui.screens.inventory.InventoryInfoButton;
	import com.thoopid.games.sb2.view.ui.screens.inventory.InventoryItemBtn;
	import com.thoopid.games.sb2.view.ui.screens.inventory.InventoryMinusButton;
	
	import starling.events.Event;

	public class InventoryScreenMediator extends AbstractSectionMediator
	{
		// Signals
		[Inject]
		public var inventoryShowSignal:InventoryShowSignal;
		[Inject]
		public var inventoryUpdateSignal:InventoryUpdatedSignal;
		[Inject]
		public var slotsUpdateSignal:SlotsUpdateSignal;
		[Inject]
		public var purchaseSignal:PurchaseInventoryItemRequestSignal;
		[Inject]
		public var removeSlotItemSignal:RemoveSlotItemSignal;
		[Inject]
		public var itemInfoRequestSignal:ItemInfoPopupRequestSignal;
		[Inject]
		public var triggerTutorialSignal:TriggerTutorialSignal;
		
		// Models
		[Inject]
		public var storyDataModel:StoryDataModel;
		[Inject]
		public var tutorialModel:TutorialModel;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		public function InventoryScreenMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get inventoryView():InventoryScreen
		{
			return view as InventoryScreen;
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			inventoryShowSignal.add(onShowInventory);
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnClose":
					inventoryShowSignal.dispatch(false);
					break;
			}
			
			var minusBtn:InventoryMinusButton = (e.target as InventoryMinusButton);
			if(minusBtn)
			{
				removeSlotItemSignal.dispatch(minusBtn.type, true);
				e.stopImmediatePropagation();
				return;
			}
			
			var infoButton:InventoryInfoButton = e.target as InventoryInfoButton;
			if(infoButton)
			{
				itemInfoRequestSignal.dispatch(storyDataModel.getItemById(infoButton.id));
				e.stopImmediatePropagation();
				return;
			}
			
			var iButton:InventoryItemBtn = e.target as InventoryItemBtn;
			if(iButton)
			{
				if(iButton.isSlot)
				{
					
				} else
				{
					if(iButton.id) purchaseSignal.dispatch(iButton.id);
				}
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onShowInventory(show:Boolean):void
		{
			if(show)
			{
				inventoryUpdateSignal.add(onUpdate);
				slotsUpdateSignal.add(onUpdateSlots);
				
				inventoryView.showStart();
				inventoryView.init(playerService.inventoryGetAll());
				onUpdateSlots();
				
				// Step tutorial if needed
				if(tutorialModel.enabled)
				{
					triggerTutorialSignal.dispatch(TutorialConstants.TUT_STEP_INVENTORY_BUY);
				}
			} else
			{
				slotsUpdateSignal.remove(onUpdateSlots);
				inventoryUpdateSignal.remove(onUpdate);
				inventoryView.hideStart();
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		private function onUpdateSlots():void
		{
			inventoryView.reflectSlots(playerService.slotsGetAll());
		}
		
		private function onUpdate():void
		{
			inventoryView.reflectModel(playerService.inventoryGetAll());
		}
	}
}