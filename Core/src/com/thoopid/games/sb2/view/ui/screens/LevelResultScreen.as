package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.game.LevelVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	import com.thoopid.games.sb2.view.ui.screens.levelresult.BadgeIcon;
	import com.thoopid.games.sb2.view.ui.screens.levelresult.ItemIcon;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	
	
	public class LevelResultScreen extends AbstractSection
	{
		private var _itemIcon:DisplayObject;
		private var _isComplete:Boolean;
		private var _buttonsList:Array;
		private var _bg:Sprite;
		
		public function LevelResultScreen()
		{
			super();
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get isComplete():Boolean
		{
			return _isComplete;
		}

		override public function showStart(mustcomplete:Boolean=false):void
		{
			super.showStart(true);
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.LEVEL_RESULT), Assets.assets);
			
			_bg = LayoutBuilder.findChild(this,"mcBg") as Sprite;
			_bg.alpha = 0;
			
			_itemIcon = LayoutBuilder.findChild(this,"mcItem") as ItemIcon;
			
			_buttonsList = [];
			
			_buttonsList.push(LayoutBuilder.findChild(this,"lblVictory"));
			_buttonsList.push(LayoutBuilder.findChild(this,"lblPaused"));
			_buttonsList.push(LayoutBuilder.findChild(this,"mcItem"));
			_buttonsList.push(LayoutBuilder.findChild(this,"mcCoinvalue"));
			
			_buttonsList.push(LayoutBuilder.findChild(this,"deathsBadge"));
			_buttonsList.push(LayoutBuilder.findChild(this,"slimeBadge"));
			_buttonsList.push(LayoutBuilder.findChild(this,"secretAreaBadge"));
			_buttonsList.push(LayoutBuilder.findChild(this,"jumpBadge"));
			
			_buttonsList.push(LayoutBuilder.findChild(this,"btnNext") as CenteredButton);
			_buttonsList.push(LayoutBuilder.findChild(this,"btnRetry") as CenteredButton);
			_buttonsList.push(LayoutBuilder.findChild(this,"btnLevels") as CenteredButton);
			_buttonsList.push(LayoutBuilder.findChild(this,"btnOptions") as CenteredButton);
			
			var i:int = _buttonsList.length;
			while(i--)
			{
				_buttonsList[i].alpha = 0;
			}
			
			this.visible = true;
		}
		
		public function close():void
		{
			this.visible = false;
			cleanup();
		}
		
		override public function hideStart():void
		{
			super.hideStart();
		}
		
		override public function hideComplete():void
		{
			super.hideComplete();
			cleanup();
		}
		
		public function setupState(isComplete:Boolean, levelVO:LevelVO):void
		{
			_isComplete = isComplete;
			LayoutBuilder.findChild(this,"lblVictory").visible = isComplete;
			LayoutBuilder.findChild(this,"lblPaused").visible = !isComplete;
			
			(LayoutBuilder.findChild(this,"deathsBadge") as BadgeIcon).enable = levelVO.deathBadgeCollected;
			(LayoutBuilder.findChild(this,"slimeBadge") as BadgeIcon).enable = levelVO.slimeBadgeCollected;
			(LayoutBuilder.findChild(this,"secretAreaBadge") as BadgeIcon).enable = levelVO.secretAreaBadgeCollected;
			
			(LayoutBuilder.findChild(this,"mcItem") as ItemIcon).enable = levelVO.storyItemCollected;
			
			// Animation
			Starling.juggler.tween(_bg, (isComplete) ? 0.8 : 0.4, {alpha:1, transition:Transitions.EASE_OUT});
			
			var toX:Number;
			var toY:Number;
			var d:Number;
			for (var i:int = 0; i < _buttonsList.length; i++) 
			{
				
				toX = _buttonsList[i].x;
				toY = _buttonsList[i].y;
				d = (isComplete) ? 0.7 : 0.3;
				_buttonsList[i].y = toY + 20;
				
				Starling.juggler.tween(_buttonsList[i], (isComplete) ? 0.6 : 0.4, {delay:d + (i*((isComplete) ? 0.2 : 0.1)), 
																					alpha:1, 
																					x:toX, 
																					y:toY, 
																					transition:Transitions.EASE_OUT_BACK});
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function cleanup():void
		{
			var i:int = this.numChildren;
			while(i--)
			{
				this.getChildAt(i).removeFromParent(true);
			}
		}
	}
}