package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.controller.signals.game.ResumeLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.RetryLevelSignal;
	import com.thoopid.games.sb2.controller.signals.system.LevelResultToggleSignal;
	import com.thoopid.games.sb2.controller.signals.system.LoadLevelSignal;
	import com.thoopid.games.sb2.model.game.GameModel;
	
	import starling.core.Starling;
	import starling.events.Event;

	public class LevelResultScreenMediator extends AbstractSectionMediator
	{
		[Inject]
		public var levelResultScreenSignal:LevelResultToggleSignal;
		[Inject]
		public var retryLevelSignal:RetryLevelSignal;
		[Inject]
		public var resumeLevelSignal:ResumeLevelSignal;
		[Inject]
		public var loadLevelSignal:LoadLevelSignal;
		
		[Inject]
		public var gameModel:GameModel;
		
		public function LevelResultScreenMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get levelResultView():LevelResultScreen
		{
			return view as LevelResultScreen;
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			// System signals
			levelResultScreenSignal.add(onLevelResultHandler);
		}
		
		override public function onRemove():void
		{
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onLevelResultHandler(toggle:Boolean, isComplete:Boolean):void
		{
			if(toggle)
			{
				Starling.juggler.delayCall(
					function():void{
						levelResultView.showStart();
						levelResultView.setupState(isComplete, gameModel.currentLevel);
					}, (isComplete) ? 1 : 0
				);
			} else
			{
				levelResultView.hideStart();
			}
		}
		
		override protected function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnRetry":
					retryLevelSignal.dispatch();
					levelResultView.close();
					break;
				case "btnNext":
					if(levelResultView.isComplete)
					{
						fadeAndExit();
					} else
					{
						levelResultView.close();
						resumeLevelSignal.dispatch();
					}
					break;
				case "btnLevels":
					fadeAndExit();
					break;
			}
		}
		
		private function fadeAndExit():void
		{
			levelResultView.close();
			loadLevelSignal.dispatch(LevelNames.KINGDOM_1);
		}
	}
}