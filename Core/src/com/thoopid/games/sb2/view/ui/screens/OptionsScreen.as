package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	public class OptionsScreen extends AbstractSection
	{
		public function OptionsScreen()
		{
			super();
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function showStart(mustcomplete:Boolean=false):void
		{
			super.showStart(true);
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.OPTIONS_SCREEN), Assets.assets);
			this.visible = true;
		}
		
		override public function hideStart():void
		{
			cleanup();
			this.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
	}
}