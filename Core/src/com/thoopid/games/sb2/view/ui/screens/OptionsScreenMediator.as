package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.controller.signals.ui.OptionsShowSignal;
	
	import starling.events.Event;
	
	public class OptionsScreenMediator extends AbstractSectionMediator
	{
		[Inject]
		public var optionsShowSignal:OptionsShowSignal;
		
		public function OptionsScreenMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			optionsShowSignal.add(onShowOptions);
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		override protected function onTrigger(e:Event):void
		{
			switch(e.target["name"])
			{
				case "btnClose":
					view.hideStart();
					break;
			}
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onShowOptions(show:Boolean):void
		{
			if(show)
			{
				view.showStart();
			} else
			{
				view.hideStart();
			}
		}
		
	}
}