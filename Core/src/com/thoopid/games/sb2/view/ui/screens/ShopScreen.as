package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.game.ShopItemVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.SBScrollContainer;
	import com.thoopid.games.sb2.view.ui.screens.shop.ShopItemHolder;
	
	import feathers.controls.Scroller;

	public class ShopScreen extends AbstractSection
	{
		private var _scrollContainer:SBScrollContainer;
		private var _shopItems:Vector.<ShopItemVO>;
		private var _scrolling:Boolean = false;
		
		public function ShopScreen()
		{
			super();
			this.visible = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get scrollContainer():SBScrollContainer
		{
			return _scrollContainer;
		}

		public function init(items:Vector.<ShopItemVO>):void
		{
			_shopItems = items;
		}
		
		override public function showStart(mustcomplete:Boolean=false):void
		{
			super.showStart(true);
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.SHOP_SCREEN), Assets.assets);
			
			_scrollContainer = LayoutBuilder.findChild(this, "mcHolder") as SBScrollContainer;
			_scrollContainer.width = Config.GAME_WIDTH - (_scrollContainer.x * 2);
			_scrollContainer.height = Config.GAME_HEIGHT - (_scrollContainer.y * 2);
			_scrollContainer.verticalScrollPosition = 0;
			_scrollContainer.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_scrollContainer.snapToPages = true;
			
			var item:ShopItemHolder;
			for (var i:int = 0; i < _shopItems.length; i++) 
			{
				_scrollContainer.addChild(item = new ShopItemHolder({}));
				item.init(_shopItems[i]);
				item.x = (120) + (i * 150);
				item.y = 115;
			}
			
			this.visible = true;
		}
		
		override public function hideStart():void
		{
			cleanup();
			this.visible = false;
		}
		
		override protected function cleanup():void
		{
			super.cleanup();
			
			_scrollContainer = null;
		}
	}
}