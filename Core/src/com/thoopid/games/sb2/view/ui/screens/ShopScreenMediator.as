package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.controller.signals.ui.PurchaseCoinSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ShopShowSignal;
	import com.thoopid.games.sb2.model.game.ShopModel;
	import com.thoopid.games.sb2.view.ui.screens.shop.ShopItemHolder;
	
	import starling.events.Event;

	public class ShopScreenMediator extends AbstractSectionMediator
	{
		[Inject]
		public var shopShowSignal:ShopShowSignal;
		[Inject]
		public var purchaseCoinSignal:PurchaseCoinSignal;
		
		[Inject]
		public var shopModel:ShopModel;
		
		public function ShopScreenMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get shopScreen():ShopScreen
		{
			return view as ShopScreen;
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			shopScreen.init(shopModel.shopItems);
			
			shopShowSignal.add(onShow);
		}
		
		override protected function onTrigger(e:Event):void 
		{
			super.onTrigger(e);
			switch(e.target["name"])
			{
				case "btnClose":
					shopShowSignal.dispatch(false);
					break;
			}
			
			var shopItembutton:ShopItemHolder = e.target as ShopItemHolder;
			if(shopItembutton)
			{
				if(!shopScreen.scrollContainer.scrolling)
				{
					purchaseCoinSignal.dispatch(shopItembutton.id);
				}
			}
			
			e.stopImmediatePropagation();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onShow(show:Boolean):void
		{
			if(show)
			{
				shopScreen.showStart();
			} else
			{
				shopScreen.hideStart();
			}
		}
	}
}