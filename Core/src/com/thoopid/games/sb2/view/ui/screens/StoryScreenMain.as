package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.model.game.StoryLineItemVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.UiSprite;
	import com.thoopid.games.sb2.view.ui.screens.story.SpeachBubbleGeneric;
	
	import starling.animation.Transitions;
	import starling.core.Starling;

	public class StoryScreenMain extends AbstractSection
	{
		private var _baseImage:UiSprite;
		private var _speachBubble:SpeachBubbleGeneric;
		private var _storyItem:StoryLineItemVO;
		private var _ready:Boolean = false;
		
		public function StoryScreenMain()
		{
			super();
			this.visible = false;
			
			this.y = Config.GAME_HEIGHT;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(storyLineItem:StoryLineItemVO):void
		{
			_storyItem = storyLineItem;
		}
		
		override public function showStart(mustcomplete:Boolean=false):void
		{
			super.showStart(mustcomplete);
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.STORY_SCREEN_MAIN), Assets.assets);
			
			// Add image according to story
			_baseImage = LayoutBuilder.findChild(this, "mcMainImage") as UiSprite;
			_baseImage.addChild(Factory.imagePool.getImage(Assets.assets.getTexture(_storyItem.imageId),true));
			
			_speachBubble = LayoutBuilder.findChild(this, "mcSpeachBubble") as SpeachBubbleGeneric;
			_speachBubble.init(_storyItem.message);
			
			this.visible = true;
			this.alpha = 0;
			Starling.juggler.tween(this, 1, {alpha:1, onComplete:showComplete, transition:Transitions.EASE_OUT});
		}
		
		override public function hideStart():void
		{
			if(!_active) return;
			
			super.hideStart();
			
			Starling.juggler.removeTweens(this);
			this.alpha = 1;
			Starling.juggler.tween(this, 1, {alpha:0, onComplete:hideComplete, transition:Transitions.EASE_OUT});
		}
		
		override public function hideComplete():void
		{
			cleanup();
			this.visible = false;
			super.hideComplete();
		}
	}
}