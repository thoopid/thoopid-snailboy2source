package com.thoopid.games.sb2.view.ui.screens
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.ui.StoryMainShowSignal;
	import com.thoopid.games.sb2.model.game.StoryDataModel;
	import com.thoopid.games.sb2.model.game.StoryLineItemVO;
	import com.thoopid.games.sb2.model.tut.TutorialModel;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class StoryScreenMainMediator extends AbstractSectionMediator
	{
		[Inject]
		public var storyMainShowSignal:StoryMainShowSignal;
		[Inject]
		public var triggerTutorialSignal:TriggerTutorialSignal;
		
		[Inject]
		public var tutorialModel:TutorialModel;
		[Inject]
		public var storyModel:StoryDataModel;
		[Inject]
		public var playerService:PlayerService;
		
		public function StoryScreenMainMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get screen():StoryScreenMain
		{
			return view as StoryScreenMain;
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			storyMainShowSignal.add(onShow);
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onShow(show:Boolean, item:StoryLineItemVO):void
		{
			screen.removeEventListener(TouchEvent.TOUCH, onTouch);
			if(show)
			{
				screen.init(item);
				screen.showStart();
				screen.addEventListener(TouchEvent.TOUCH, onTouch);
				
			} else
			{
				screen.hideStart();
			}
		}
		
		private function onTouch(e:TouchEvent):void
		{
			var end:Touch = e.getTouch(screen, TouchPhase.ENDED);
			if(end)
			{
				view.hideStart();
				
				// trigger tut if needed
				if(tutorialModel.enabled)
				{
					if(playerService.player.mainStoryIndex == 1)
					{
						triggerTutorialSignal.dispatch(TutorialConstants.TUT_STEP_LEVEL_SELECT);
					}
				}
			}
		}
	}
}