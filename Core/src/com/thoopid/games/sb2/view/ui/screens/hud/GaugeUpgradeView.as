package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	

	public class GaugeUpgradeView extends GaugeView
	{
		public function GaugeUpgradeView(params:Object)
		{
			super(params);
			
			// Hide bases
			
			LayoutBuilder.findChild(this,"ui_gaugeBase_lvl1").visible = false;
			LayoutBuilder.findChild(this,"ui_gaugeBase_lvl2").visible = false;
			LayoutBuilder.findChild(this,"ui_gaugeBase_lvl3").visible = false;
			LayoutBuilder.findChild(this,"ui_gaugeBase_lvl4").visible = false;
			LayoutBuilder.findChild(this,"ui_gaugeBase_lvl5").visible = false;
			LayoutBuilder.findChild(this,"ui_gauge_slime_01").visible = false;
			LayoutBuilder.findChild(this,"ui_gauge_slime_used_01").visible = false;
		}
	}
}