package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.model.game.GaugeVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	public class GaugeView extends Sprite
	{
		private var _bg:Image;
		private var _fill:Image;
		private var _deplete:Image;
		private var _vo:GaugeVO;
		
		private var _w:Number = 100;
		private var _h:Number = 10;
		
		// Jump Elements
		private var _jumpBases:Vector.<Image>;
		private var _jumpNeedles:Vector.<Image>;
		private var _jumpGears1:Vector.<Image>;
		private var _jumpGears2:Vector.<Image>;
		
		// Gauge Elements
		private var _gaugeCapacities:Vector.<Image>;
		
		// Swim Elements
		private var _gaugeSwimFans:Vector.<Image>;
		private var _gaugeSwims:Vector.<Image>;
		
		// UI Elements
			// Slime Fill
			private var _slimeFill:Image;
			private var _slimeFillStartY:Number;
			
			// Gears to spin
			private var _gearA:Image;
			private var _gearB:Image;
			private var _swimFan:Image;
			private var _slimeFillUsed:HudSlime;
			
			private var _gaugeCapacityHeight:Number = 100;
			private var _glow:Image;
		
		public function GaugeView(params:Object)
		{
			super();
			
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA("GaugeView"), Assets.assets);
			
			// Level s of Elements
				_jumpBases = new Vector.<Image>();
				_jumpNeedles = new Vector.<Image>();
				_jumpGears1 = new Vector.<Image>();
				_jumpGears2 = new Vector.<Image>();
				
				_gaugeCapacities = new Vector.<Image>();
				
				_gaugeSwimFans = new Vector.<Image>();
				_gaugeSwims = new Vector.<Image>();
			
			var item:Image;
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				item = this.getChildAt(i) as Image;
				if(item)
				{
					if(item.name.indexOf("needle") != -1 && item.name.indexOf("gaugeJ") != -1)		// Jump Elements
					{
						_jumpNeedles.push(item);
					} else if(item.name.indexOf("gearA") != -1 && item.name.indexOf("gaugeJ") != -1)
					{
						_jumpGears1.push(item);
					} else if(item.name.indexOf("gearB") != -1 && item.name.indexOf("gaugeJ") != -1)
					{
						_jumpGears2.push(item);
					} else if(item.name.indexOf("gaugeJ") != -1 && item.name.indexOf("needle") == -1 && item.name.indexOf("gearA") == -1 && item.name.indexOf("gearB") == -1)
					{
//						trace("Added Jump Bases: ");
						_jumpBases.push(item);
					} else if(item.name.indexOf("gaugeC") != -1) // Gage Capacities
					{
						_gaugeCapacities.push(item);
					} else if(item.name.indexOf("gaugeS") != -1 && item.name.indexOf("fan") != -1) // Swim Elements
					{
						_gaugeSwimFans.push(item);
					} else if(item.name.indexOf("gaugeS") != -1 && item.name.indexOf("fan") == -1 && item.name.indexOf("Slime") == -1) // Swim Elements
					{
						_gaugeSwims.push(item);
					}
				}
			}
			
			// Slime Fill
			_slimeFill = LayoutBuilder.findChild(this,"ui_gauge_slime_01") as Image;
			_slimeFillUsed = LayoutBuilder.findChild(this,"ui_gauge_slime_used_01") as HudSlime;
			_glow = LayoutBuilder.findChild(this,"ui_gauge_glow") as Image;
			_glow.alpha = 0;
			
			_slimeFillStartY = _slimeFill.y;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function jump():void
		{
			Starling.juggler.removeTweens(_gearA);
			Starling.juggler.removeTweens(_gearB);
			Starling.juggler.tween(_gearA, 1.5, {rotation:deg2rad(360), transition:Transitions.EASE_OUT});
			Starling.juggler.tween(_gearB, 1.5, {rotation:deg2rad(-360), transition:Transitions.EASE_OUT});
		}
		
		public function swim(toggle:Boolean):void
		{
			Starling.juggler.removeTweens(_swimFan);
			if(toggle)
			{
				Starling.juggler.tween(_swimFan, 15, {rotation:deg2rad(3000), transition:Transitions.EASE_OUT});
			}
		}
		
		public function reset(vo:GaugeVO):void
		{
			_vo = vo;
			resetSlimeFillProps();
			redraw();
		}

		public function update(vo:GaugeVO):void
		{
			reset(vo);
			_slimeFill.y = (_slimeFillStartY + _gaugeCapacityHeight) - ((_gaugeCapacityHeight) * (_vo.gauge.volume / _vo.gauge.capacity));
//			var toY:Number = (_slimeFillStartY + _gaugeCapacityHeight) - ((_gaugeCapacityHeight) * (_vo.gauge.volume / _vo.gauge.capacity));
//			Starling.juggler.removeTweens(_slimeFill);
//			Starling.juggler.tween(_slimeFill, 0.3, {y:toY, transition:Transitions.EASE_OUT});
			
//			simulateSuper(0);
		}
		
		public function simulateSuper(percentage:Number):void
		{
//			trace("percentage : "+(_vo.jump.costSuperJump / _vo.gauge.capacity) * percentage);
//			_slimeFillUsed.y = ((_slimeFillStartY + (_slimeFill.y - _slimeFillStartY)) + (_gaugeCapacityHeight * (_vo.jump.costSuperJump / _vo.gauge.capacity) * percentage))
			var toY:Number = ((_slimeFillStartY + (_slimeFill.y - _slimeFillStartY)) + (_gaugeCapacityHeight * (_vo.jump.costSuperJump / _vo.gauge.capacity) * percentage))
			Starling.juggler.tween(_slimeFillUsed, 0.4, {y:toY, transition:Transitions.EASE_OUT});
		}
		
		public function replenish():void
		{
			Starling.juggler.removeTweens(_glow);
			_glow.alpha = 1;
			Starling.juggler.tween(_glow, 0.8, {alpha:0, transition:Transitions.EASE_OUT});
		}
		
		public function clear():void
		{
//			_deplete.visible = false;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		public function redraw():void
		{
			var i:int = 0;
			
			resetSlimeFillProps();
			
			// Jump Elements
			for (i = 0; i < _jumpNeedles.length; i++) 
			{
				_jumpNeedles[i].visible = (_vo.jump.level == (i+1));
			}
			
			for (i = 0; i < _jumpBases.length; i++)
			{
				_jumpBases[i].visible = (_vo.jump.level == (i+1));
			}
			
			for (i = 0; i < _jumpGears1.length; i++)
			{
				_jumpGears1[i].visible = (_vo.jump.level == (i+1));
				if(_jumpGears1[i].visible) _gearA = _jumpGears1[i];
			}
			
			for (i = 0; i < _jumpGears2.length; i++)
			{
				_jumpGears2[i].visible = (_vo.jump.level == (i+1));
				if(_jumpGears2[i].visible) _gearB = _jumpGears2[i];
			}
			
			// Gauge Elements
			for (i = 0; i < _gaugeCapacities.length; i++)
			{
				_gaugeCapacities[i].visible = (_vo.gauge.level == (i+1));
			}
			
			// Swim Elements
			for (i = 0; i < _gaugeSwims.length; i++)
			{
				_gaugeSwims[i].visible = (_vo.swim.level == (i+1));
			}
			for (i = 0; i < _gaugeSwimFans.length; i++)
			{
				_gaugeSwimFans[i].visible = (_vo.swim.level == (i+1));
				if(_gaugeSwimFans[i].visible) _swimFan = _gaugeSwimFans[i];
			}
		}
		
		private function resetSlimeFillProps():void
		{
			// Slime fill height
			switch(_vo.gauge.level)
			{
				case 1:
					_gaugeCapacityHeight = 54;
					_slimeFillStartY = 6;
					_glow.y = 6;
					break;
				case 2:
					_gaugeCapacityHeight = 71;
					_slimeFillStartY = -2;
					_glow.y = -3;
					break;
				case 3:
					_gaugeCapacityHeight = 98;
					_slimeFillStartY = -15;
					_glow.y = -16;
					break;
				case 4:
					_gaugeCapacityHeight = 125;
					_slimeFillStartY = -28;
					_glow.y = -30;
					break;
				case 5:
					_gaugeCapacityHeight = 152;
					_slimeFillStartY = -43;
					_glow.y = -40;
					break;
			}
		}
	}
}