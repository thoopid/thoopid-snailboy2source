package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.constants.game.CharacterActions;
	import com.thoopid.games.sb2.controller.signals.game.CharacterActionSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GaugeUpgradeScreenSignal;
	import com.thoopid.games.sb2.model.game.GaugeModel;
	import com.thoopid.games.sb2.model.game.GaugeVO;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class GaugeViewMediator extends StarlingMediator
	{
		[Inject]
		public var view:GaugeView;
		
		// model
		[Inject]
		public var gaugeModel:GaugeModel;
		
		// Services
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var gaugeUpgradeScreenSignal:GaugeUpgradeScreenSignal;
		
		[Inject]
		public var characterActions:CharacterActionSignal;
		
		public function GaugeViewMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			view.addEventListener(TouchEvent.TOUCH, onTrigger);
			gaugeModel.updateModelSignal.add(onUpdateGaugeModelHandler);
			
			characterActions.add(onCharacterActions);
			
			// Setup
			view.update(gaugeModel.gauge);
			view.simulateSuper(0);
		}
		
		override public function onRemove():void
		{
			view.removeEventListener(Event.TRIGGERED, onTrigger);
			gaugeModel.updateModelSignal.remove(onUpdateGaugeModelHandler);
			
			characterActions.remove(onCharacterActions);
			
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onUpdateGaugeModelHandler(gauge:GaugeVO, isReplenish:Boolean):void
		{
			view.update(gauge);
			view.simulateSuper(gauge.gauge.superSimPercentage);
			
			if(isReplenish)
			{
				view.replenish();
			}
		}
		
		private function onTrigger(e:TouchEvent):void
		{
			var end:Touch = e.getTouch(view, TouchPhase.ENDED);
			if(end && playerService.player.gaugeUpgradeEnabled)
			{
				gaugeUpgradeScreenSignal.dispatch(true);
			}
		}
		
		private function onCharacterActions(action:String):void
		{
			if(action == CharacterActions.JUMP)
			{
				view.jump();
			} else if(action == CharacterActions.SWIM_START)
			{
				view.swim(true);
			} else if(action == CharacterActions.SWIM_STOP)
			{
				view.swim(false);
				
			}
		}
	}
}