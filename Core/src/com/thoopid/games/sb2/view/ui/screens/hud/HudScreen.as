package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.constants.screen.ScreensList;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.game.tut.GaugeRefilTutorial;
	import com.thoopid.games.sb2.view.game.tut.InventoryOpenTutorial;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	import com.thoopid.games.sb2.view.ui.screens.AbstractSection;
	
	import flash.geom.Point;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	
	public class HudScreen extends AbstractSection
	{
		private var _btnNext:Button;
		private var _btnPrev:Button;
		
		private var _gauge:GaugeView;
		
		private var _coinValue:HudCoinValue;
		private var _slots:HudSlots;
		private var _walkRight:CenteredButton;
		private var _walkLeft:CenteredButton;
		private var _swimArrowLeft:DisplayObject;
		private var _swimArrowRight:DisplayObject;
		private var _swimIconeLeft:DisplayObject;
		private var _swimIconRight:DisplayObject;
		
		public function HudScreen()
		{
			super();
			
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get walkLeft():CenteredButton
		{
			return _walkLeft;
		}

		public function get walkRight():CenteredButton
		{
			return _walkRight;
		}

		public function get slots():HudSlots
		{
			return _slots;
		}
		
		public function toggleSwimControlls(toggle:Boolean):void
		{
			Starling.juggler.tween(_swimArrowLeft, 0.2, {alpha:(toggle) ? 0.3 : 0, transition:Transitions.EASE_OUT});
			Starling.juggler.tween(_swimArrowRight, 0.2, {alpha:(toggle) ? 0.3 : 0, transition:Transitions.EASE_OUT});
			Starling.juggler.tween(_swimIconeLeft, 0.2, {alpha:(toggle) ? 0.3 : 0, transition:Transitions.EASE_OUT});
			Starling.juggler.tween(_swimIconRight, 0.2, {alpha:(toggle) ? 0.3 : 0, transition:Transitions.EASE_OUT});
		}

		override public function showStart(mustcomplete:Boolean=false):void
		{
			super.showStart(true);
			
			// Add Tutorials
			this.addChild(new GaugeRefilTutorial());
			this.addChild(new InventoryOpenTutorial());
			
			LayoutBuilder.buildUiFromData(this, Assets.getSCREEN_DATA(ScreensList.HUD),Assets.assets);
			
			
			_coinValue = LayoutBuilder.findChild(this, "mcCoinvalue") as HudCoinValue;
			_slots = LayoutBuilder.findChild(this, "mcSlots") as HudSlots;
			_walkRight = LayoutBuilder.findChild(this, "btnWalkRight") as CenteredButton;
			_walkLeft = LayoutBuilder.findChild(this, "btnWalkLeft") as CenteredButton;
			_gauge = LayoutBuilder.findChild(this, "mcGauge") as GaugeView;
			
			_swimArrowLeft = LayoutBuilder.findChild(this, "mcSwimArrowLeft") as Image;
			_swimArrowRight = LayoutBuilder.findChild(this, "mcSwimArrowRight") as Image;
			_swimIconeLeft = LayoutBuilder.findChild(this, "mcSwimIconLeft") as Image;
			_swimIconRight = LayoutBuilder.findChild(this, "mcSwimIconRight") as Image;
			
			_swimArrowLeft.alpha = 0;
			_swimArrowRight.alpha = 0;
			_swimIconeLeft.alpha = 0;
			_swimIconRight.alpha = 0;
			
			_swimArrowRight.scaleX = -1;
			_swimIconRight.scaleX = -1;
			
			_swimArrowLeft.touchable = false;
			_swimArrowRight.touchable = false;
			_swimIconeLeft.touchable = false;
			_swimIconRight.touchable = false;
			
			_slots.x = Config.GAME_WIDTH - _slots.width;
			_slots.y = Config.GAME_HEIGHT - _slots.height;
		}
		
		override public function hideStart():void
		{
			super.hideStart();
			this.visible = true;
			cleanup();
		}
		
		public function collectCoin(item:DisplayObject):void
		{
			_collectPoint.x = item.x;
			_collectPoint.y = item.y;
			
			item.localToGlobal(_collectPoint,_collectPoint);
			
			var coin:Image = Factory.imagePool.getImage(Assets.assets.getTexture("ig.coin"), true);
			this.addChild(coin);
			coin.x = _collectPoint.x;
			coin.y = _collectPoint.y;
			
			Starling.juggler.tween(coin, 0.5, 
				{x:_coinValue.x + 30,
				y:_coinValue.y + 10,
				scaleX:0.3,
				scaleY:0.3,
				alpha:0.2,
				transition:Transitions.EASE_OUT, 
				onComplete:function():void
				{
					coin.parent.removeChild(coin);
					coin = null;
				}});
		}
		
		private var _collectPoint:Point = new Point();
		
		public function collectSlime(item:DisplayObject):void
		{
			_collectPoint.x = item.x;
			_collectPoint.y = item.y;
			
			item.localToGlobal(_collectPoint,_collectPoint);
			
			var slimey:Image = Factory.imagePool.getImage(Assets.assets.getTexture("is.slimey.01"), true);
			this.addChild(slimey);
			
			slimey.x = _collectPoint.x;
			slimey.y = _collectPoint.y;
			
			Starling.juggler.tween(slimey, 0.5, 
				{x:_gauge.x,
				y:_gauge.y - 20,
				alpha:0.2,
				scaleX:0.3,
				scaleY:0.3,
				transition:Transitions.EASE_OUT, 
				onComplete:function():void
				{
					slimey.parent.removeChild(slimey);
					slimey = null;
				}});
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		public function get coinValue():HudCoinValue
		{
			return _coinValue;
		}

		private function onTrigger(e:Event):void
		{
			
		}
	}
}