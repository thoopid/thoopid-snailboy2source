package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.controller.signals.game.GameModelUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.game.PauseLevelSignal;
	import com.thoopid.games.sb2.controller.signals.game.ShowCoinCollectedSignal;
	import com.thoopid.games.sb2.controller.signals.game.ShowSlimeCollectedSignal;
	import com.thoopid.games.sb2.controller.signals.game.ToggleOraSignal;
	import com.thoopid.games.sb2.controller.signals.game.UseInventoryItemSignal;
	import com.thoopid.games.sb2.controller.signals.ui.GameHudShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.SlotsUpdateSignal;
	import com.thoopid.games.sb2.controller.signals.ui.ToggleSwimControlsSignal;
	import com.thoopid.games.sb2.model.game.GameModel;
	import com.thoopid.games.sb2.service.PlayerService;
	import com.thoopid.games.sb2.view.game.objects.SnailboyHero;
	import com.thoopid.games.sb2.view.ui.screens.AbstractSectionMediator;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class HudScreenMediator extends AbstractSectionMediator
	{
//		[Inject]
//		public var view:GameHud;
		
		// Models
		[Inject]
		public var gameModel:GameModel;
		
		// Serives
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var pauseLevelSignal:PauseLevelSignal;
		[Inject]
		public var gameHudShowSignal:GameHudShowSignal;
		[Inject]
		public var gameModelUpdateSignal:GameModelUpdateSignal;
		[Inject]
		public var useInventorySignal:UseInventoryItemSignal;
		
		[Inject]
		public var showCoinCollectedSignal:ShowCoinCollectedSignal;
		[Inject]
		public var showSlimeCollectedSignal:ShowSlimeCollectedSignal;
		[Inject]
		public var toggleSwimControlsSignal:ToggleSwimControlsSignal;
		
		public function HudScreenMediator()
		{
			super();
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			gameHudShowSignal.add(onHudShow);
			
			showCoinCollectedSignal.add(onShowCoinCollected);
			showSlimeCollectedSignal.add(onShowSlimeCollected);
			toggleSwimControlsSignal.add(toggleSwimControlls);
			
			view.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		override public function onRemove():void
		{
			showCoinCollectedSignal.remove(onShowCoinCollected);
			showSlimeCollectedSignal.remove(onShowSlimeCollected);
			toggleSwimControlsSignal.remove(toggleSwimControlls);
			
			view.removeEventListener(TouchEvent.TOUCH, onTouch);
			super.onRemove();
			
		}
		
		public function get hudView():HudScreen
		{
			return view as HudScreen;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onHudShow(show:Boolean):void
		{
			if(show && !view.active)
			{
				view.showStart();
				gameModelUpdateSignal.add(onGameModelUpdate);
				
				onGameModelUpdate();
				
			} else
			{
				gameModelUpdateSignal.remove(onGameModelUpdate);
				view.hideStart();
			}
		}
		
		
		
		private function onGameModelUpdate():void
		{
			hudView.coinValue.update(playerService.player.coins + gameModel.currentLevel.coinsCount);
		}
		
		
		override protected function onTrigger(e:Event):void
		{
			super.onTrigger(e);
			switch(e.target["name"])
			{
				case "btnPause":
					pauseLevelSignal.dispatch();
					e.stopImmediatePropagation();
					break;
			}
			
			var slotbtn:HudSlotItem = e.target as HudSlotItem;
			if(slotbtn)
			{
				if(slotbtn.id != "") 
				{
					useInventorySignal.dispatch(slotbtn.id);
					return;
				}
			}
		}
		
		private function onShowCoinCollected(item:DisplayObject):void
		{
			hudView.collectCoin(item);
		}
		
		private function onShowSlimeCollected(item:DisplayObject):void
		{
			hudView.collectSlime(item);
		}
		
		private function toggleSwimControlls(t:Boolean):void
		{
			if(playerService.player.swimEnabled) hudView.toggleSwimControlls(t);
		}
		
		private function onTouch(e:TouchEvent):void
		{
			var touchRightWalk:Touch = e.getTouch(hudView.walkRight);
			if(touchRightWalk)
			{
				e.stopImmediatePropagation();
				if(touchRightWalk.phase == TouchPhase.BEGAN)
				{
					SnailboyHero.instance.walk(1,true);
				} else if(touchRightWalk.phase == TouchPhase.ENDED)
				{
					SnailboyHero.instance.walk(0,false);
				}
			}
			
			var touchLeftWalk:Touch = e.getTouch(hudView.walkLeft);
			if(touchLeftWalk)
			{
				e.stopImmediatePropagation();
				if(touchLeftWalk.phase == TouchPhase.BEGAN)
				{
					SnailboyHero.instance.walk(-1,true);
				} else if(touchLeftWalk.phase == TouchPhase.ENDED)
				{
					SnailboyHero.instance.walk(0,false);
				}
			}
		}
	}
}