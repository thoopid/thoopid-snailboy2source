package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.comps.PoolMovieclip;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class HudSlime extends Sprite
	{
		private var _base:PoolImage;
		private var _clip:PoolMovieclip;
		public function HudSlime(params:Object)
		{
			super();
			
//			this.addChild(_base = Factory.imagePool.getImage(Assets.assets.getTexture("ui_gaugeSlime_base"), true));
			this.addChild(_clip = Factory.movieclipPool.getMovie(Assets.assets.getTextures("ui_gaugeSlime_"),true));
			_clip.fps = 15;
			Starling.juggler.add(_clip);
//			_base.width = Math.round(_base.width);
//			_base.height = Math.round(_base.height);
//			_clip.width = Math.round(_clip.width);
//			_clip.height = Math.round(_clip.height);
//			_clip.x = -_base.width >> 1;
//			_clip.y = -(_base.height >> 1) + 2;
		}
		
		override public function set width(value:Number):void
		{
			_clip.width = value;
		}
		
		override public function set height(value:Number):void
		{
			_clip.height = value;
			
		}
	}
}