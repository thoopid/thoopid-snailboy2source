package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.comps.button.UiButton;
	import com.thoopid.games.sb2.view.ui.theme.CenteredLabel;
	
	import flash.display.BitmapData;
	
	import coza.runtime.utils.number.RandomRange;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.textures.Texture;
	import starling.utils.deg2rad;
	
	public class HudSlotItem extends UiButton
	{
		private var _item:InventoryItemVO;
		private var _currentImage:PoolImage;
		private var _quantityLabel:CenteredLabel;
		private var _shine:PoolImage;
		
		public function HudSlotItem()
		{
//			super({upState:Assets.assets.getTexture("ui_HUD_slot")});
			super({upState:Texture.fromBitmapData(new BitmapData(40,40,true,0xff3300))});
			
			this.addChild(_shine = Factory.imagePool.getImage(Assets.assets.getTexture("shine"),true));
			_shine.alpha = 0;
			_shine.visible = false;
			
			this.rotation = deg2rad(RandomRange.randRange(-3,3));
		}
		
		public function get id():String
		{
			if(_item) 
				return _item.id;
			else
				return "";
		}

		public function init(item:InventoryItemVO):void
		{
			if(!_item && item != null)
			{
				var tex:String = item.id;
				_currentImage = Factory.imagePool.getImage(Assets.assets.getTexture(tex), true);
				_currentImage.x = upState.width >> 1;
				_currentImage.y = upState.height >> 1;
				_currentImage.scaleX = _currentImage.scaleY = 0.6;
				this.addChild(_currentImage);
				
				_quantityLabel = new CenteredLabel({width:50,height:20});
				_quantityLabel.y = 5;
				_quantityLabel.x = upState.width - 6;
				_quantityLabel.nameList.add("inventoryItem");
				this.addChild(_quantityLabel);
				
				_shine.x = _currentImage.x;
				_shine.y = _currentImage.y;
			}
			
			if(item == null || item.quantity < 1)
			{
				cleanup();
				return;
			}
			
			_item = item;
			_quantityLabel.text = "x"+_item.quantity;
		}
		
		public function toggleHighlight(toggle:Boolean):void
		{
			Starling.juggler.removeTweens(_shine);
			_shine.rotation = 0;
			if(toggle)
			{
				_shine.alpha = 1;
				_shine.visible = true;
				Starling.juggler.tween(_shine, 2, {rotation:deg2rad(360), reverse:true, repeatCount:999, transition:Transitions.EASE_OUT_IN});
			}
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_item = null;
			_quantityLabel = null;
			_shine = null;
		}
		
		public function cleanup():void
		{
			if(_currentImage) 
				_currentImage.removeFromParent(true);
			
			if(_quantityLabel) 
				_quantityLabel.removeFromParent(true);
			
			_quantityLabel = null
			_currentImage = null;
			
			_item = null;
		}
	}
}