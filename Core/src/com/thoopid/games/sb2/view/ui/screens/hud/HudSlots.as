package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.comps.button.UiButton;
	
	import flash.display.BitmapData;
	
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class HudSlots extends Sprite
	{
		private var _slots:Vector.<HudSlotItem>;
		private var _baseImg:PoolImage;
		private var _params:Object;
		private var _btnOpen:UiButton;
		private var _baseExtraImg:PoolImage;
		private var _invBtnImage:PoolImage;
		
		public function HudSlots(params:Object)
		{
			_params = params;
			super();
			
			this.addChildAt(_baseExtraImg = Factory.imagePool.getImage(Assets.assets.getTexture("ui_slot_extra")),0);
			_baseExtraImg.x = params.width - _baseExtraImg.width;
			_baseExtraImg.y = params.height - _baseExtraImg.height;
			
			this.addChild(_invBtnImage = Factory.imagePool.getImage(Assets.assets.getTexture("ui_inventory_btn")));
			_invBtnImage.x = params.width - _invBtnImage.width;
			_invBtnImage.y = params.height - _invBtnImage.height;
			
			this.addChild(_baseImg = Factory.imagePool.getImage(Assets.assets.getTexture("ui_slots")));
			_baseImg.x = params.width - _baseImg.width;
			_baseImg.y = params.height - _baseImg.height;
			
			this.addChild(_btnOpen = new UiButton({upState:Texture.fromBitmapData(new BitmapData(30,30,true,0xff3300))}));
			_btnOpen.x = width-_btnOpen.width - 10;
			_btnOpen.y = -_btnOpen.width - 10;
			
			_slots = new Vector.<HudSlotItem>();
			var s:HudSlotItem;
			for (var i:int = 0; i < 3; i++) 
			{
				_slots.push(s = new HudSlotItem());
				s.x = i * 50;
				this.addChild(s);
			}
			
//			ui_inventory_btn.png
//			ui_slot_extra.png
			
			
			
			canOpen = false;
			
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function set canOpen(v:Boolean):void
		{
			_btnOpen.visible = v;
		}
		
		public function get btnOpen():UiButton
		{
			return _btnOpen;
		}

		public function toggleSlotHighlight(id:String, toggle:Boolean):void
		{
			var i:int = _slots.length;
			var slot:HudSlotItem
			while(i--)
			{
				slot = _slots[i];
				if(slot.id == id)
				{
					slot.toggleHighlight(toggle);
				}
			}
		}
		
		public function update(slotsList:Vector.<InventoryItemVO>):void
		{
			var si:int = _slots.length;
			while(si--)
			{
				_slots[si].cleanup();
			}
			
			var s:HudSlotItem;
			var sData:InventoryItemVO;
			for (var i:int = 0; i < _slots.length; i++) 
			{
				sData = null;
				if(i < slotsList.length)
				{
					sData = slotsList[i];
				}
				
				_slots[i].init(sData);
			}
			
		}
		
		override public function get width():Number
		{
			return _params.width;
		}
		override public function get height():Number
		{
			return _params.height;
		}
		
	}
}