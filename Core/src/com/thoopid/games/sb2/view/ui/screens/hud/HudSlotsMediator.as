package com.thoopid.games.sb2.view.ui.screens.hud
{
	import com.thoopid.games.sb2.constants.game.TutorialConstants;
	import com.thoopid.games.sb2.controller.signals.game.ToggleOraSignal;
	import com.thoopid.games.sb2.controller.signals.game.TriggerTutorialSignal;
	import com.thoopid.games.sb2.controller.signals.ui.InventoryShowSignal;
	import com.thoopid.games.sb2.controller.signals.ui.SlotsUpdateSignal;
	import com.thoopid.games.sb2.service.PlayerService;
	
	import org.robotlegs.mvcs.StarlingMediator;
	
	import starling.events.Event;
	
	public class HudSlotsMediator extends StarlingMediator
	{
		[Inject]
		public var view:HudSlots;
		
		// Serives
		[Inject]
		public var playerService:PlayerService;
		
		// Signals
		[Inject]
		public var slotsUpdateSignal:SlotsUpdateSignal;
		[Inject]
		public var toggleOraSignal:ToggleOraSignal;
		[Inject]
		public var inventoryShowSignal:InventoryShowSignal;
		[Inject]
		public var triggerTutorialSignal:TriggerTutorialSignal;
		
		public function HudSlotsMediator()
		{
			super();
		}
		//*********************
		//   PUBLIC
		//*********************
		
		override public function onRegister():void
		{
			super.onRegister();
			
			slotsUpdateSignal.add(onSlotsUpdate);
			toggleOraSignal.add(onOrbsToggle);
			triggerTutorialSignal.add(onTutTrigger);
			
			onSlotsUpdate();
			
			view.canOpen = playerService.player.inventoryEnabled;
			
			view.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		override public function onRemove():void
		{
			slotsUpdateSignal.remove(onSlotsUpdate);
			toggleOraSignal.remove(onOrbsToggle);
			triggerTutorialSignal.remove(onTutTrigger);
			
			view.removeEventListener(Event.TRIGGERED, onTrigger);
			
			super.onRemove();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTrigger(e:Event):void
		{
			switch(e.target)
			{
				case view.btnOpen:
					inventoryShowSignal.dispatch(true);
					break;
			}
		}
		
		private function onOrbsToggle(id:String, toggle:Boolean):void
		{
			view.toggleSlotHighlight(id,toggle);
		}
		
		private function onSlotsUpdate():void
		{
			view.update(playerService.slotsGetAll());
		}
		
		private function onTutTrigger(id:String):void
		{
			if(id == TutorialConstants.TUT_STEP_INVENTORY_OPEN)
			{
				playerService.player.inventoryEnabled = true;
				view.canOpen = true;
			}
		}
	}
}