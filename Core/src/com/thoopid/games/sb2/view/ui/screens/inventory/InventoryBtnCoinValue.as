package com.thoopid.games.sb2.view.ui.screens.inventory
{
	import com.thoopid.games.sb2.view.ui.comps.CoinValue;
	
	public class InventoryBtnCoinValue extends CoinValue
	{
		public function InventoryBtnCoinValue(params:Object)
		{
			params.animated = false;
			super(params);
		}
	}
}