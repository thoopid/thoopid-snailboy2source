package com.thoopid.games.sb2.view.ui.screens.inventory
{
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	
	public class InventoryInfoButton extends CenteredButton
	{
		public var id:String = "";
		
		public function InventoryInfoButton(id:String)
		{
			this.id = id;
			super({upState:"ui_generic_info_btn"});
		}
	}
}