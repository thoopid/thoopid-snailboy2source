package com.thoopid.games.sb2.view.ui.screens.inventory
{
	import com.thoopid.games.sb2.model.game.InventoryItemVO;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	import com.thoopid.games.sb2.view.ui.comps.button.UiButton;
	import com.thoopid.games.sb2.view.ui.theme.CenteredLabel;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class InventoryItemBtn extends Sprite
	{
		public var isSlot:Boolean = false;
		
		protected var _item:InventoryItemVO;
		
		private var _coinValue:InventoryBtnCoinValue;
		private var _quantityLabel:CenteredLabel;
		
		private var _currentImage:PoolImage;
		private var _btnInfo:CenteredButton;
		private var _btnBase:UiButton;
		
		private var _enable:Boolean = false;
		
		public function InventoryItemBtn(params:Object=null)
		{
			this.addChild(_btnBase = new UiButton({upState:Assets.assets.getTexture("ui_inventory_box")}));
			enable = true;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function get enable():Boolean
		{
			return _enable;
		}

		public function set enable(value:Boolean):void
		{
			_enable = value;
			
			if(_enable)
			{
				_btnBase.addEventListener(Event.TRIGGERED, onClick);
			} else
			{
				_btnBase.removeEventListener(Event.TRIGGERED, onClick);
			}
		}

		public function get id():String
		{
			if(_item)
				return _item.id;
			else
				return "";
		}

		public function init(item:InventoryItemVO):void
		{
			if(!_item)
			{
				_item = item;
				
				setupCoinValue();
				
				_quantityLabel = new CenteredLabel({width:50,height:20});
				_quantityLabel.touchable = false;
				_quantityLabel.y = 5;
				_quantityLabel.x = _btnBase.upState.width - 10;
				_quantityLabel.nameList.add("inventoryItem");
				this.addChild(_quantityLabel);
				
				var tex:String = item.id;
				_currentImage = Factory.imagePool.getImage(Assets.assets.getTexture(tex), true);
				_currentImage.x = _btnBase.upState.width >> 1;
				_currentImage.y = _btnBase.upState.height >> 1;
				_currentImage.touchable = false;
				_currentImage.scaleX = _currentImage.scaleY = 0.7;
				this.addChild(_currentImage);
				
			}
			
			update(item, true);
		}
		
		public function update(item:InventoryItemVO, force:Boolean=false):void
		{
			if(_item.quantity != item.quantity || force)
			{
				_item = item;
				
				if(_coinValue) _coinValue.update(item.price);
					
				Starling.juggler.removeTweens(_quantityLabel);
				_quantityLabel.scaleX = 1.8;
				_quantityLabel.scaleY = 1.8;
				Starling.juggler.tween(_quantityLabel, 0.3, {scaleX:1, scaleY:1, transition:Transitions.EASE_OUT});
				
				_quantityLabel.text = "x"+item.quantity;
			}
		}
		
		public function cleanUp():void
		{
			if(_currentImage)
			{
				_currentImage.removeFromParent(true);
				_currentImage = null;
			}
			
			if(_quantityLabel)
			{
				_quantityLabel.removeFromParent(true);
				_quantityLabel = null;
			}
			
			if(_coinValue)
			{
				_coinValue.removeFromParent(true);
				_coinValue = null;
			}
			
			_item = null;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			enable = false;
			
			_btnBase = null;
			_coinValue = null;
			_item = null;
			_currentImage = null;
		}
		
		//*********************
		//   PROTECTED
		//*********************
		
		protected function setupCoinValue():void
		{
			this.addChild(_coinValue = new InventoryBtnCoinValue({animated:false}));
			_coinValue.scaleX = _coinValue.scaleY = 0.75;
			_coinValue.y = _btnBase.upState.height - _coinValue.height;
			_coinValue.x = 4;
			
			this.addChild(_btnInfo = new InventoryInfoButton(id));
			_btnInfo.x = 5;
			_btnInfo.y = 5;
		}
		
		private function onClick(e:Event):void
		{
			e.stopImmediatePropagation();
			this.dispatchEvent(new Event(Event.TRIGGERED, true));
		}
	}
}