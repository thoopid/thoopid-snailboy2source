package com.thoopid.games.sb2.view.ui.screens.inventory
{
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	
	public class InventoryMinusButton extends CenteredButton
	{
		public var type:String;
		
		public function InventoryMinusButton(params:Object=null)
		{
			super({upState:"ui_generic_minus_btn"});
		}
	}
}