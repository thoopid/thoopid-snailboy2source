package com.thoopid.games.sb2.view.ui.screens.inventory
{
	
	

	public class InventorySlotBtn extends InventoryItemBtn
	{
		private var _minusBtn:InventoryMinusButton;
		
		public function InventorySlotBtn(params:Object=null)
		{
			super(params);
			
			isSlot = true;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		//*********************
		//   PROTECTED
		//*********************
		
//		override public function update(item:InventoryItemVO, force:Boolean=false):void
//		{
//			if(!_item)
//			{
//				init(item);
//				return;
//			}
//			if(item.id != _item.id)
//			{
//				cleanUp();
//				if(item.quantity > 0) init(item);
//			} else
//			{
//			}
//			super.update(item);
//		}
		
		override protected function setupCoinValue():void
		{
			// NOTHING, no coin Value for this item
			
			this.addChild(_minusBtn = new InventoryMinusButton());
			_minusBtn.type = this.id;
			_minusBtn.x = 5;
			_minusBtn.y = 5;
		}
		
		override public function cleanUp():void
		{
			super.cleanUp();
			
			if(_minusBtn)
			{
				_minusBtn.removeFromParent(true);
				_minusBtn = null;
			}
		}
	}
}