package com.thoopid.games.sb2.view.ui.screens.kingdomselect
{
	import com.thoopid.games.sb2.constants.game.LevelNames;
	import com.thoopid.games.sb2.model.game.KingdomVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.button.IconCenteredButton;
	import com.thoopid.games.sb2.view.ui.theme.KingdomSelectItemLabel;
	
	import feathers.controls.Label;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class KingdomSelectItem extends Sprite
	{
		private var _selectedSignal:Signal;
		
		private var _label:Label;
		private var _lock:IconCenteredButton;
		private var _play:IconCenteredButton;
		private var _foreground:Sprite;
		
		private var _kingdomVo:KingdomVO;
		
		public function KingdomSelectItem(params:Object)
		{
			super();
					
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA("KingdomSelectItem"), Assets.assets);
			
			_foreground = LayoutBuilder.findChild(this, "mcForeground") as Sprite;
			_lock = LayoutBuilder.findChild(this, "mcLock") as IconCenteredButton;
			_play = LayoutBuilder.findChild(this, "mcPlay") as IconCenteredButton;
			_label = LayoutBuilder.findChild(this, "tfTitle") as KingdomSelectItemLabel;
			
			this.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(kingdomVo:KingdomVO, signal:Signal):void
		{
			_kingdomVo = kingdomVo;
			_selectedSignal = signal;
			_label.text = kingdomVo.name;
			_lock.visible = kingdomVo.locked;
			_play.visible = !kingdomVo.locked;
	
		switch (kingdomVo.kingdomId) {
				case LevelNames.KINGDOM_1 :
					_foreground.addChild(Factory.imagePool.getImage(Assets.assets.getTexture("ui_kingdomSelect_grouping_1"),true));
					break;
				case LevelNames.KINGDOM_2 :
					_foreground.addChild(Factory.imagePool.getImage(Assets.assets.getTexture("ui_kingdomSelect_grouping_2"),true));
					break;
				case LevelNames.KINGDOM_3 :
					_foreground.addChild(Factory.imagePool.getImage(Assets.assets.getTexture("ui_kingdomSelect_grouping_3"),true));
					break;
			}
		}
		
		override public function dispose():void
		{
			_selectedSignal = null;
			_kingdomVo = null;
			super.dispose();
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouch(e:TouchEvent):void
		{
			var end:Touch = e.getTouch(this, TouchPhase.ENDED);
			if(end)
			{
				if(!_kingdomVo.locked) _selectedSignal.dispatch(_kingdomVo.kingdomId);
			}
		}
	}
}