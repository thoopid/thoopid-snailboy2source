package com.thoopid.games.sb2.view.ui.screens.levelresult
{
	import com.thoopid.games.sb2.constants.game.PlayerElements;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.theme.BadgeIconLabel;
	
	import starling.display.Sprite;
	
	public class BadgeIcon extends Sprite
	{
		protected var _label:BadgeIconLabel;
		
		private var _baseOff:PoolImage;
		private var _baseOn:PoolImage;
		private var _id:String;
		private var _iconOff:PoolImage;
		private var _iconOn:PoolImage;
		private var _params:Object;
		
		public function BadgeIcon(params:Object)
		{
			_params = params;
			super();
			
			_id = params.name;
			this.addChild(_baseOff = Factory.imagePool.getImage(Assets.assets.getTexture("ui_generic_badge_off"), true));
			this.addChild(_baseOn = Factory.imagePool.getImage(Assets.assets.getTexture("ui_generic_badge_on"), true));
			
			var texOff:String = "ui_generic_badgeSlime_off";
			var texOn:String = "ui_generic_badgeSlime_on";
			switch(_id)
			{
				case PlayerElements.BADGE_DEATHS:
					texOff = "ui_generic_badgeDeath_off";
					texOn = "ui_generic_badgeDeath_on";
					break;
				case PlayerElements.BADGE_JUMP:
					texOff = "ui_generic_badgeJump_off";
					texOn = "ui_generic_badgeJump_on";
					break;
				case PlayerElements.BADGE_SECRET_AREA:
					texOff = "ui_generic_badgeSecret_off";
					texOn = "ui_generic_badgeSecret_on";
					break;
				case PlayerElements.BADGE_SLIME:
					texOff = "ui_generic_badgeSlime_off";
					texOn = "ui_generic_badgeSlime_on";
					break;
			}
			
			this.addChild(_iconOff = Factory.imagePool.getImage(Assets.assets.getTexture(texOff), true));
			this.addChild(_iconOn = Factory.imagePool.getImage(Assets.assets.getTexture(texOn), true));
			
			_label = new BadgeIconLabel({width:50, height:60, text:PlayerElements.getBadgeMsg(_id)})
			this.addChild(_label);
			
			
			_label.y = _baseOff.y + _baseOff.height + 5;
			
			enable = false;
			
			this.touchable = false;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		override public function set width(value:Number):void
		{
			_baseOff.width = _params.width;
			_baseOn.width = _params.width;
			
			_iconOff.scaleX = _baseOn.scaleX;
			_iconOn.scaleX = _baseOn.scaleX;
			
			_label.scaleX = _baseOn.scaleX;
		}
		
		override public function set height(value:Number):void
		{
			_baseOff.height = _params.height;
			_baseOn.height = _params.height;
			
			_iconOff.scaleY = _baseOn.scaleY;
			_iconOn.scaleY = _baseOn.scaleY;
			
			_label.scaleY = _baseOn.scaleY;
		}
		
		public function set enable(v:Boolean):void
		{
			_baseOff.visible = !v;
			_baseOn.visible = v;
			_iconOff.visible = !v;
			_iconOn.visible = v;
		}
	}
}