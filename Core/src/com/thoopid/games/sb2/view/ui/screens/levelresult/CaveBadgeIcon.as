package com.thoopid.games.sb2.view.ui.screens.levelresult
{
	public class CaveBadgeIcon extends BadgeIcon
	{
		public function CaveBadgeIcon(params:Object)
		{
			super(params);
			
			_label.visible = false;
		}
	}
}