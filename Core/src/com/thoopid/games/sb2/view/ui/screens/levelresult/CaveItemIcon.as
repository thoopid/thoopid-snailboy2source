package com.thoopid.games.sb2.view.ui.screens.levelresult
{
	public class CaveItemIcon extends ItemIcon
	{
		public function CaveItemIcon(params:Object)
		{
			super(params);
			
			_label.visible = false;
			
			_bubble.pivotX = _bubble.width >> 1;
			_bubble.pivotY = _bubble.height >> 1;
			_icon.x = 0;
			_icon.y = 0;
		}
		
		//*********************
		//   PUBLIC
		//*********************
	}
}