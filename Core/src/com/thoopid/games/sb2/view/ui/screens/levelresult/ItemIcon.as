package com.thoopid.games.sb2.view.ui.screens.levelresult
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.theme.CoinValueLabel;
	
	import starling.display.Sprite;
	
	public class ItemIcon extends Sprite
	{
		protected var _label:CoinValueLabel;
		
		protected var _bubble:PoolImage;
		protected var _icon:PoolImage;
		
		public function ItemIcon(params:Object)
		{
			super();
			
			this.addChild(_bubble = Factory.imagePool.getImage(Assets.assets.getTexture("ui_HUD_coin_bubble")));
			this.addChild(_label = new CoinValueLabel({width:100, height:20, text:"ITEM FOUND"}));
			this.addChild(_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_btn_gauge_accept"), true));
			_icon.scaleX = _icon.scaleY = 0.5;
			_icon.x = _bubble.width >> 1;
			_icon.y = _bubble.height >> 1;
			
			_label.x = _bubble.width + 4;
			_label.y = 5;
			
			enable = false;
		}
		
		public function set enable(v:Boolean):void
		{
			_bubble.alpha = (v) ? 1 : 0.2;
			_label.alpha = (v) ? 1 : 0.2;
			_icon.visible = v;
		}
		
		override public function set width(value:Number):void
		{
			_bubble.width = value;
			
//			_baseOff.width = _params.width;
//			_baseOn.width = _params.width;
//			
//			_iconOff.scaleX = _baseOn.scaleX;
//			_iconOn.scaleX = _baseOn.scaleX;
//			
//			_label.scaleX = _baseOn.scaleX;
		}
		
		override public function set height(value:Number):void
		{
			_bubble.height = value;
//			_baseOff.height = _params.height;
//			_baseOn.height = _params.height;
//			
//			_iconOff.scaleY = _baseOn.scaleY;
//			_iconOn.scaleY = _baseOn.scaleY;
//			
//			_label.scaleY = _baseOn.scaleY;
		}
	}
}