package com.thoopid.games.sb2.view.ui.screens.menu
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	
	import starling.display.Image;

	public class MenuScreenButton extends CenteredButton
	{
		private var _icon:Image;
		private var _name:String;
		//private var _selectedSignal:Signal;
		
		public function MenuScreenButton(params:Object)
		{
			super(params);
			
			_name = params.name
			//_selectedSignal = signal;
			
			switch(_name)
			{
				case "btnPlayer":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_menu_achievements"),true);
					break;
				case "btnOptions":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_menu_options"),true);
					break;
				case "btnTreasure":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_menu_lostTreasure"),true);
					break;
				case "btnGauge":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_menu_gauge"),true);
					break;
				case "btnInventory":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_menu_inventory"),true);
					break;
				case "btnShop":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_menu_shop"),true);
					break;
				};
			
			if(_icon){
				this.addChild(_icon);
				_icon.x = upState.width / 2;
				_icon.y = upState.height / 2;
				
			};
		};
		
		//*********************
		//   PRIVATE
		//*********************
		
		
	};
};