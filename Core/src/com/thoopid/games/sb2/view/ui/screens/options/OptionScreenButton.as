package com.thoopid.games.sb2.view.ui.screens.options
{
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.button.CenteredButton;
	
	import starling.display.Image;

	public class OptionScreenButton extends CenteredButton
	{
		private var _icon:Image;
		
		public function OptionScreenButton(params:Object)
		{
			super(params);
			switch(this.name)
			{
				case "btnFacebook":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_options_facebook"),true);
					break;
				case "btnContact":
					_icon = Factory.imagePool.getImage(Assets.assets.getTexture("ui_options_contact"),true);
					break;
			};
			
			if(_icon){
				this.addChild(_icon);
				_icon.x = upState.width / 2;
				_icon.y = upState.height / 2;
				
			};
		}
		
	}
}