package com.thoopid.games.sb2.view.ui.screens.shop
{
	import com.thoopid.games.sb2.model.game.ShopItemVO;
	import com.thoopid.games.sb2.util.ui.LayoutBuilder;
	import com.thoopid.games.sb2.view.ui.comps.CoinValue;
	import com.thoopid.games.sb2.view.ui.comps.Factory;
	import com.thoopid.games.sb2.view.ui.comps.UiSprite;
	
	import feathers.controls.Label;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	public class ShopItemHolder extends Sprite
	{
		public var id:String;
		public function ShopItemHolder(params:Object)
		{
			super();
			LayoutBuilder.buildUiElementFromDATA(this, Assets.getUI_DATA("ShopItemBase"), Assets.assets);
			
			this.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(item:ShopItemVO):void
		{
			id = item.id;
			var foreground:UiSprite = LayoutBuilder.findChild(this,"mcForeground") as UiSprite;
			var tex:Texture = Assets.assets.getTexture(item.image);
			var image:Image = Factory.imagePool.getImage(tex,true);
			foreground.addChild(image);
			
			var coinValue:CoinValue = LayoutBuilder.findChild(this,"mcCoinvalue") as CoinValue;
			coinValue.update(item.quantity);
			var title:Label = LayoutBuilder.findChild(this,"tfTitle") as Label;
			title.text = item.name;
			var cost:Label = LayoutBuilder.findChild(this,"tfCost") as Label;
			cost.text = item.cost;
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function onTouch(e:TouchEvent):void
		{
			if(e.getTouch(this,TouchPhase.ENDED))
			{
				this.dispatchEvent(new Event(Event.TRIGGERED,true));
			}
		}
	}
}