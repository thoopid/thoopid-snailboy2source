package com.thoopid.games.sb2.view.ui.screens.story
{
	import com.thoopid.games.sb2.view.ui.comps.PoolImage;
	import com.thoopid.games.sb2.view.ui.theme.SpeachBubbleLabel;
	
	import flash.geom.Rectangle;
	
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import starling.display.Sprite;
	
	public class SpeachBubbleGeneric extends Sprite
	{
		private var _label:SpeachBubbleLabel;
		private var _params:Object;
		private var _image:Scale9Image;
		
		public function SpeachBubbleGeneric(params:Object)
		{
			super();
			
			_params = params;
			
			this.addChild(_image = new Scale9Image(new Scale9Textures(Assets.assets.getTexture("ui_char_speechbubble"), new Rectangle(30,30,4,4))));
			_image.width = params.width;
			_image.height = params.height;
			_image.pivotX = _image.width >> 1;
			_image.pivotY = _image.height >> 1;
			this.addChild(_label = new SpeachBubbleLabel({text:params.text, width:params.width - 10/*, height:params.height * 0.5*/}));
//			_label.x = -_label.width >> 1;
//			_label.y = -_label.height >> 1;
		}
		
		//*********************
		//   PUBLIC
		//*********************
		
		public function init(text:String):void
		{
			_label.text = text;
			_label.validate();
			
			_label.x = -_label.width >> 1;
			_label.y = -_label.height >> 1;
			
			_image.height = _label.height + 30;
			
			_image.pivotX = _image.width >> 1;
			_image.pivotY = _image.height >> 1;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_image = null;
			_label = null;
		}
	}
}