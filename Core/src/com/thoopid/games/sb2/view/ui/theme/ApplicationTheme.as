package com.thoopid.games.sb2.view.ui.theme
{
	import com.thoopid.games.sb2.view.ui.comps.label.LevelResultHeader;
	
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import feathers.controls.IScrollBar;
	import feathers.controls.Label;
	import feathers.controls.Panel;
	import feathers.controls.ScrollBar;
	import feathers.controls.TextArea;
	import feathers.controls.TextInput;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.controls.text.ITextEditorViewPort;
	import feathers.controls.text.TextFieldTextEditorViewPort;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.DisplayListWatcher;
	import feathers.core.FeathersControl;
	import feathers.core.ITextRenderer;
	import feathers.text.BitmapFontTextFormat;
	
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class ApplicationTheme extends DisplayListWatcher
	{
//		[Embed(source = "../../../../../../../../assets/font/Mikado_Large.fnt", mimeType="application/octet-stream")]
		[Embed(source = "../../../../../../../../assets/font/Heading_80.fnt", mimeType="application/octet-stream")]
		public static const TitleFontXML:Class;
		
//		[Embed(source = "../../../../../../../../assets/font/Mikado_Large.png")]
		[Embed(source = "../../../../../../../../assets/font/Heading_80.png")]
		public static const TitleGameFontTexture:Class;
		
		[Embed(source = "../../../../../../../../assets/font/CopyFont.fnt", mimeType="application/octet-stream")]
		public static const CopyFontXML:Class;
		
		[Embed(source = "../../../../../../../../assets/font/CopyFont.png")]
		public static const CopyFontTexture:Class;
		
		// Fonts
		private static var _headerFont:BitmapFont;
		private static var _copyFont:BitmapFont;
		
		private const BACKGROUND_COLOR:uint = 0xFFFFFF;
		
		public function ApplicationTheme(root:DisplayObjectContainer)
		{
			super(root);
//			Starling.current.nativeStage.color = BACKGROUND_COLOR;
//			if(root.stage)
//			{
//				root.stage.color = BACKGROUND_COLOR;
//			}
//			else
//			{
//				root.addEventListener(Event.ADDED_TO_STAGE, root_addedToStageHandler);
//			}
			this.initialize();
		}
		
		
		//*********************
		//   PROTECTED
		//*********************
		
		public static function get rod1Font():BitmapFont
		{
			return _headerFont;
		}

		protected function initialize():void
		{
			TextField.registerBitmapFont(_headerFont = new BitmapFont(Texture.fromBitmap(new TitleGameFontTexture()), XML(new TitleFontXML())));
			_headerFont.lineHeight = 75;
			TextField.registerBitmapFont(_copyFont = new BitmapFont(Texture.fromBitmap(new CopyFontTexture()), XML(new CopyFontXML())));
			_copyFont.lineHeight = 30;
			
//			FeathersControl.defaultTextRendererFactory = textRendererFactory;
			FeathersControl.defaultTextRendererFactory = defaultFontTextRenderer;
			
			this.setInitializerForClass(Label, labelInitializer);
			this.setInitializerForClass(ShortMsgLabel, shortMsgLabelInitializer);
			this.setInitializerForClass(LevelResultHeader, levelResultHeaderLabelInitializer, "paused");
			this.setInitializerForClass(LevelResultHeader, levelResultHeaderCompleteLabelInitializer, "complete");
			this.setInitializerForClass(ConfirmScreenHeaderLabel, confirmScreenHeaderLabelInitializer);
			this.setInitializerForClass(ConfirmScreenHeaderLabel, confirmScreenHeaderSubLabelInitializer, "subHeader");
			this.setInitializerForClass(ConfirmScreenHeaderLabel, confirmScreenHeaderGagueLabelInitializer, "gaugeUpgrade");
			this.setInitializerForClass(ConfirmScreenHeaderLabel, infoPopupTitleLabelInitializer, "infoPopupTitle");
			this.setInitializerForClass(ConfirmScreenHeaderLabel, infoPopupHeaderLabelInitializer, "infoPopupHeader");
			this.setInitializerForClass(ConfirmScreenCopyLabel, confirmScreenCopyLabelInitializer);
			this.setInitializerForClass(ConfirmScreenCopyLabel, infoScreenCopyLabelInitializer, "infoCopy");
			this.setInitializerForClass(TutorialTextLabel, tutorialCopyLabelInitializer);
			this.setInitializerForClass(CoinValueLabel, coinValueLabelInitializer);
			this.setInitializerForClass(CenteredLabel, inventoryCountLabelInventoryItemInitializer, "inventoryItem");
			this.setInitializerForClass(CaveSelectorLabel, caveSelectorLabelInit);
			this.setInitializerForClass(SpeachBubbleLabel, speachBubbleLabelInit);
			
			this.setInitializerForClass(ScreenHeaderLabel, screenHeaderLabelInitializer);
			this.setInitializerForClass(KingdomSelectItemLabel, kingdomSelectItemLabelInitializer);
			this.setInitializerForClass(KingdomSelectItemLabel, shopItemCostLabelInitializer, "shopCostLabel");
			this.setInitializerForClass(BadgeIconLabel, badgeIconLabelInitializer);
			
			this.setInitializerForClass(TextInput, inputInit);
			this.setInitializerForClass(Panel, panelInitializer);
		}
		
		private function panelInitializer(panel:Panel):void
		{
			panel.verticalScrollBarFactory = function():IScrollBar
			{
				return new ScrollBar();
			}
		}
		
		private function textAreaInit(textArea:TextArea):void
		{
			textArea.textEditorFactory = function():ITextEditorViewPort
			{
				var editor:TextFieldTextEditorViewPort = new TextFieldTextEditorViewPort();
				editor.textFormat = new TextFormat( "_sans", 20, 0x333333 );
				return editor;
			}
		}
		
		private function inputInit(input:TextInput):void
		{
			input.textEditorProperties.fontFamily = "_sans";
			input.textEditorProperties.fontSize = 17;
			input.textEditorProperties.color = 0X333333;
			input.promptProperties.textFormat = new BitmapFontTextFormat(_headerFont,32);
			input.promptProperties.textFormat.color = 0x000000;
		}
		
		private function defaultFontTextRenderer():ITextRenderer
		{
			var bitmapFontTextRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
			bitmapFontTextRenderer.textFormat = new BitmapFontTextFormat(_headerFont,32);
			return bitmapFontTextRenderer;
		}
		
		protected function textRendererFactory():TextFieldTextRenderer
		{
			return new TextFieldTextRenderer();
		}
		
		protected function labelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(20);
		}
		
		protected function shortMsgLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(20,0xffffff,TextFormatAlign.CENTER);
		}
		
		protected function levelResultHeaderLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(30,0xffffff,TextFormatAlign.CENTER);
		}
		protected function levelResultHeaderCompleteLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(60,0xffffff,TextFormatAlign.CENTER);
		}
		
		protected function screenHeaderLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(18,0xffffff,TextFormatAlign.CENTER);
		}
		protected function kingdomSelectItemLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(19,0xffffff,TextFormatAlign.CENTER);
		}
		protected function shopItemCostLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(6,0xffffff,TextFormatAlign.CENTER);
		}
		protected function badgeIconLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(12,0xffffff,TextFormatAlign.CENTER);
		}
		protected function coinValueLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(16,0xffffff,TextFormatAlign.LEFT);
		}
		protected function inventoryCountLabelInventoryItemInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(16,0xffffff,TextFormatAlign.CENTER);
		}
		protected function caveSelectorLabelInit(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(16,0xffffff,TextFormatAlign.CENTER);
		}
		protected function speachBubbleLabelInit(label:Label):void
		{
			label.textRendererFactory = copyRendererFactory(10,0x000000,TextFormatAlign.CENTER);
		}
		protected function confirmScreenHeaderLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(18,0xffffff,TextFormatAlign.CENTER);
		}
		protected function confirmScreenHeaderSubLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(57,0xffffff,TextFormatAlign.CENTER);
		}
		protected function confirmScreenHeaderGagueLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(24,0xffffff,TextFormatAlign.CENTER);
		}
		protected function infoPopupTitleLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(25,0xffffff,TextFormatAlign.LEFT);
		}
		protected function infoPopupHeaderLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(35,0xffffff,TextFormatAlign.LEFT);
		}
		protected function confirmScreenCopyLabelInitializer(label:Label):void
		{
			label.textRendererFactory = copyRendererFactory(10,0xffffff,TextFormatAlign.CENTER);
		}
		protected function infoScreenCopyLabelInitializer(label:Label):void
		{
			label.textRendererFactory = copyRendererFactory(10,0xffffff,TextFormatAlign.LEFT);
		}
		protected function tutorialCopyLabelInitializer(label:Label):void
		{
			label.textRendererFactory = headerRendererFactory(9,0xffffff,TextFormatAlign.CENTER);
		}
		
		private function verticalScrollBarInitializer():void
		{
			
		}
		
		//*********************
		//   PRIVATE
		//*********************
		
		private function headerRendererFactory(size:Number,color:uint=0xFFFFFF,align:String=TextFormatAlign.LEFT,lineHeight:Number=30,wordWrap:Boolean=true):Function
		{
			return function titleLabelFactory():ITextRenderer
			{
				var tr:BitmapFontTextRenderer = new BitmapFontTextRenderer();
				tr.textFormat = new BitmapFontTextFormat(_headerFont,size);
				tr.textFormat.isKerningEnabled = false;
				tr.textFormat.letterSpacing = 0;
				tr.textFormat.color = color;
				tr.wordWrap = wordWrap;
				tr.textFormat.align = align;
				tr.snapToPixels = true;
				
				
				return tr;
			};
		}
		
		private function copyRendererFactory(size:Number,color:uint=0xFFFFFF,align:String=TextFormatAlign.LEFT,lineHeight:Number=30,wordWrap:Boolean=true):Function
		{
			return function titleLabelFactory():ITextRenderer
			{
				var tr:BitmapFontTextRenderer = new BitmapFontTextRenderer();
				tr.textFormat = new BitmapFontTextFormat(_copyFont,size);
				tr.textFormat.isKerningEnabled = false;
				tr.textFormat.letterSpacing = 0;
				tr.textFormat.color = color;
				tr.wordWrap = wordWrap;
				tr.textFormat.align = align;
				tr.snapToPixels = true;
				
				return tr;
			};
		}
		
		private function root_addedToStageHandler(e:Event):void
		{
			DisplayObject(e.currentTarget).stage.color = BACKGROUND_COLOR;
		}
	}
}