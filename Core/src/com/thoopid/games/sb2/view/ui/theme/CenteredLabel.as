package com.thoopid.games.sb2.view.ui.theme
{
	import feathers.controls.Label;
	
	public class CenteredLabel extends Label
	{
		public function CenteredLabel(params:Object)
		{
			super();
			
			this.text = params.text;
			this.width = params.width;
			this.height = params.height;
			this.explicitWidth = params.width;
			this.explicitHeight = params.height;
			this.pivotX = this.width >> 1;
			this.pivotY = this.height >> 1;
			
//			this.addChild(new Image(Texture.fromBitmapData(new BitmapData(this.width, this.height, false, 0xff3300))));
			
			if(params.namesList) this.nameList.add(params.namesList);
		}
	}
}