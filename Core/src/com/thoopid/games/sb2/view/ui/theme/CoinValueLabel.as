package com.thoopid.games.sb2.view.ui.theme
{
	import feathers.controls.Label;
	
	public class CoinValueLabel extends Label
	{
		public function CoinValueLabel(params:Object)
		{
			super();
			
			this.text = params.text;
			this.width = params.width;
			this.height = params.height;
			
			this.touchable = false;
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
	}
}