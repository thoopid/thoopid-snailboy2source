package com.thoopid.games.sb2.view.ui.theme
{
	import feathers.controls.Label;
	
	public class SpeachBubbleLabel extends Label
	{
		public function SpeachBubbleLabel(params:Object)
		{
			super();
			
			this.text = params.text;
			this.width = params.width;
			this.height = params.height;
			this.explicitWidth = params.width;
			this.explicitHeight = params.height;
			
			if(params.namesList) this.nameList.add(params.namesList);
		}
	}
}